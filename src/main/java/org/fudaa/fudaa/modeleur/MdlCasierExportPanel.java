/*
 * @creation 8 sept. 06
 * @modification $Date: 2009-03-10 17:37:38 +0100 (mar., 10 mars 2009) $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.modeleur;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueVisitor;
import org.fudaa.ebli.calque.CalqueGISTreeModel;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesAbstract;
import org.fudaa.ebli.calque.ZCalqueGeometry;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.edition.ZModeleGeometryDefault;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.ebli.trace.TraceSurface;
import org.fudaa.ebli.trace.TraceSurfaceModel;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Polygon;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuVerticalLayout;

/**
 * Un panneau de saisie des géométries altimétriques pour l'export de casiers.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MdlCasierExportPanel extends CtuluDialogPanel {
  
  private MdlVisuPanel pn_;
  private GrBoite initZoom_;
  private JTree trLayers_;
  private ZCalqueGeometry cqTmp_;
  private GISZoneCollection support_;
  private CtuluListSelection sel_=new CtuluListSelection();
  private CtuluFileChooserPanel fileChooser_;
  private CtuluUI ui_;
  private MdlCasierExporter[] exporters_;
  BuRadioButton rbAllLayers_;
  BuRadioButton rbSelectedLayers_;
  BuRadioButton rbVisibleLayers_;
  protected BuRadioButton rbSelectedGeometries_; 
  BuComboBox coFormat_; 
  
  public MdlCasierExportPanel(MdlCasierExporter[] _exporters, CtuluUI _ui, MdlVisuPanel _pn) {
    pn_=_pn;
    exporters_=_exporters;
    ui_=_ui;
    setLayout(new BuVerticalLayout(5,true,true));
    
    JPanel pnFormat=new JPanel();
    pnFormat.setLayout(new BorderLayout(5,5));
    coFormat_=new BuComboBox();
    for (MdlCasierExporter exporter: _exporters)
      coFormat_.addItem(exporter.getFileFormat().getFileFormat().createFileFilter().getShortDescription());
    coFormat_.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange()==ItemEvent.DESELECTED) return;
        coFormatItemStateChanged();
      }
    });
    
    pnFormat.add(new BuLabel(MdlResource.getS("Format d'export")),BorderLayout.WEST);
    pnFormat.add(coFormat_);
    add(pnFormat);
    
    BuLabel lbTitle=new BuLabel(MdlResource.getS("Sélectionnez les géométries à intégrer aux casiers"));
    add(lbTitle);

    CalqueGISTreeModel md=new CalqueGISTreeModel(null,_pn.getGroupAlti());
    md.setMask(GISLib.MASK_MULTIPOINT);
    trLayers_=md.createView(true,true);
    
    trLayers_.addTreeSelectionListener(new TreeSelectionListener() {
      public void valueChanged(TreeSelectionEvent e) {
        treeSelectionChanged(getSelectedGeomInTree(),true);
      }
    });
    JScrollPane sp=new JScrollPane(trLayers_);
    sp.setPreferredSize(new Dimension(300,200));
    add(sp);
    
    final String title=MdlResource.getS("Fichier d'exportation");
    fileChooser_=new CtuluFileChooserPanel(title);
    fileChooser_.setAllFileFilter(false);
    fileChooser_.setWriteMode(true);

    BuLabel lbFile=new BuLabel(title);
    JPanel pnFile=new JPanel();
    pnFile.setLayout(new BorderLayout(3, 3));
    pnFile.add(lbFile, BorderLayout.WEST);
    pnFile.add(fileChooser_, BorderLayout.CENTER);
    add(pnFile);
    
    rbAllLayers_=new BuRadioButton(MdlResource.getS("Exporter les casiers de tous les calques"));
    rbSelectedLayers_=new BuRadioButton(MdlResource.getS("Exporter les casiers des calques sélectionnés (et leurs sous-calques)"));
    rbVisibleLayers_=new BuRadioButton(MdlResource.getS("Exporter les casiers des seuls calques visibles"));
    rbSelectedGeometries_=new BuRadioButton(MdlResource.getS("Exporter les casiers sélectionnés"));
    add(rbAllLayers_);
    add(rbSelectedLayers_);
    add(rbVisibleLayers_);
    add(rbSelectedGeometries_);
    ButtonGroup bgLayers=new ButtonGroup();
    bgLayers.add(rbAllLayers_);
    bgLayers.add(rbSelectedLayers_);
    bgLayers.add(rbVisibleLayers_);
    bgLayers.add(rbSelectedGeometries_);

    BuLabel lbAide=new BuLabel(MdlResource.getS("Remarque : Les géométries non visibles ne seront pas exportées"));
    lbAide.setFont(MdlResource.HELP_FONT);
    lbAide.setForeground(MdlResource.HELP_FORGROUND_COLOR);
    lbAide.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
    add(lbAide);
    
    coFormatItemStateChanged();
    rbAllLayers_.setSelected(true);

    update(false);
  }
  
  /**
   * Mise a jour de la fenetre.
   * @param _bpreselect true : Des géométries préselectionnées existent. false sinon.
   */
  public void update(boolean _bpreselect) {
    rbSelectedGeometries_.setEnabled(_bpreselect);
    if (_bpreselect)
      rbSelectedGeometries_.setSelected(true);
    else {
      if (rbSelectedGeometries_.isSelected())
        rbAllLayers_.setSelected(true);
    }

    // Pour eviter des problème de Z incorrect.
    // TODO : Devrait être fait a chaque modification de l'attribut Z de la zone.
    pn_.getGroupAlti().apply(new BCalqueVisitor() {
      public boolean visit(BCalque _cq) {
        if (_cq instanceof ZCalqueGeometry) {
          ((ZCalqueGeometry)_cq).modeleDonnees().getGeomData().prepareExport();
        }
        return true;
      }
    });
    
    // Mise a jour de l'arbre en fonction des calques.
    CalqueGISTreeModel md=(CalqueGISTreeModel)trLayers_.getModel();
    md.setRootLayer(pn_.getGroupAlti());
  }

  /**
   * Retourne les indexs sélectionnées de la scene.
   * @return Les indexs sous la forme d'une liste.
   */
  private CtuluListSelection getSelectedGeomInTree() {
    sel_.clear();
    
    if (trLayers_.isSelectionEmpty()) {
      return sel_;
    }
    
    ZScene scn=pn_.getScene();

    TreePath[] selpaths=trLayers_.getSelectionPaths();
    for (int i=0; i<selpaths.length; i++) {
      final CalqueGISTreeModel.LayerNode node=(CalqueGISTreeModel.LayerNode)selpaths[i].getLastPathComponent();
      final ZCalqueAffichageDonneesAbstract cq = (ZCalqueAffichageDonneesAbstract) node.getUserObject();
      final int idx=node.getIdxGeom();

      sel_.add(scn.layerId2SceneId(cq,idx));
    }
    
    return sel_;
  }

  /**
   * Changement de format d'export.
   */
  private void coFormatItemStateChanged() {
    CalqueGISTreeModel md=(CalqueGISTreeModel)trLayers_.getModel();
    md.setMask(getSelectedExporter().getMaskForSubSelection());
    
    BuFileFilter flt=getSelectedExporter().getFileFormat().getFileFormat().createFileFilter();
    fileChooser_.setFilter(new BuFileFilter[]{flt});

    // Changement de l'extension du fichier.
    File f=fileChooser_.getFile();
    if (f!=null) {
      f=CtuluLibFile.getSansExtension(f);
      f=CtuluLibFile.appendExtensionIfNeeded(f, flt.getFirstExt());
      fileChooser_.setFile(f);
    }
  }
  
  /**
   * Retourne l'exporter selectionné
   */
  public MdlCasierExporter getSelectedExporter() {
     return exporters_[coFormat_.getSelectedIndex()];
  }
  
  /**
   * Réaffichage de la fenetre 2D en cas de selection d'un objet.
   * @param _sel Les indices des géométries sélectionnées.
   * @param _zoom True si le zoom doit se faire sur les géometries sélectionnées.
   */
  private void treeSelectionChanged(final CtuluListSelection _sel, final boolean _zoom) {
    if (pn_==null) {
      return;
    }
    if (_sel == null || _sel.isEmpty()) {
      if (cqTmp_ != null) {
        cqTmp_.setVisible(false);
      }
      return;
    }

    ZModeleGeometryDefault mdl=new ZModeleGeometryDefault();
    support_=mdl.getGeomData();
    ZScene scn=pn_.getScene();

    for (int i=_sel.getMinIndex(); i<=_sel.getMaxIndex(); i++) {
      if (!_sel.isSelected(i)) continue;
      support_.addGeometry((Geometry)scn.getObject(i), null, null);
    }
    
    // Ajout de l'enveloppe externe pour visualisation.
    Geometry g=support_.convexHull();
    if (g instanceof Polygon)
      support_.addGeometry(((Polygon)g).getExteriorRing(), null, null);
    else
      support_.addGeometry(g, null, null);
    
    if (cqTmp_ == null) {
      initZoom_ = pn_.getVueCalque().getViewBoite();
      cqTmp_ = new ZCalqueGeometry(mdl);
      cqTmp_.setDestructible(true);
      final TraceIconModel model = new TraceIconModel(TraceIcon.PLUS, 4, Color.RED);
      cqTmp_.setIconModel(0, model);
      cqTmp_.setIconModel(1, model);
      final TraceLigneModel ligne = new TraceLigneModel(TraceLigne.INVISIBLE, 2, Color.RED);
      cqTmp_.setLineModel(0, ligne);
      cqTmp_.setLineModel(1, ligne);
      final TraceSurfaceModel surfMdl=new TraceSurfaceModel(TraceSurface.UNIFORME,new Color(255,50,0,40),null);
      cqTmp_.setSurfaceModel(0,surfMdl);
      pn_.getVueCalque().getCalque().enPremier(cqTmp_);
      pn_.getCqInfos().enPremier();
    }
    cqTmp_.modele(mdl);
    cqTmp_.setVisible(true);
    
    if (_zoom) {
      BArbreCalqueModel.actionCenter(cqTmp_, pn_);
    }
  }

  @Override
  public boolean isDataValid() {
    CtuluAnalyze ana=new CtuluAnalyze();
    if (!getSelectedExporter().isDataOk(getSelection(),ana)) {
      // TODO BM Controler que c'est la bonne facon de faire.
//      setErrorText(ana.getErrors()[0].getMessage());
      setErrorText(ana.getResume());
      return false;
    }
    if (isFileOK())
      return true;
    return false;
  }
  
  private boolean isFileOK() {
    final File f=fileChooser_.getFile();
    if (f==null) {
      setErrorText(MdlResource.getS("Donnez un nom au fichier d'exportation"));
      return false;
    }

    // Verification que l'extension est autorisée
    if (!getSelectedExporter().getFileFormat().getFileFormat().createFileFilter().accept(getFile())) {
      setErrorText(MdlResource.getS("Le fichier choisi a une extension invalide"));
      return false;
    }

  // Verification que le fichier n'existe pas.
    if(f.exists()) {
      if (ui_.question(MdlResource.getS("Attention"), MdlResource.getS("Le fichier '{0}' existe déjà. Voulez-vous l'écraser?",
          f.getName())))
        f.delete();
      else
        return false;
    }
    
    if (CtuluLibFile.getExtension(CtuluLibFile.getSansExtension(f.getName()))!=null) {
      setErrorText(MdlResource.getS("Ne mettez pas plusieurs '.' dans le nom."));
      return false;
    }
    setErrorText(null);
    return true;
  }
  
  /**
   * Retourne la selection. Elle peut être vide.
   */
  public CtuluListSelection getSelection() {
    return sel_;
  }

  /**
   * L'export se fait-il sur tous les calques casiers.
   */
  public boolean isExportOnAllLayers() {
    return rbAllLayers_.isSelected();
  }
  
  /**
   * L'export se fait-il sur les calques casiers selectionnés uniquement.
   */
  public boolean isExportOnSelectedLayers() {
    return rbSelectedLayers_.isSelected();
  }
  
  /**
   * L'export se fait-il sur les calques casiers visibles uniquement.
   */
  public boolean isExportOnVisibleLayers() {
    return rbVisibleLayers_.isSelected();
  }
  
  /**
   * L'export se fait-il sur les casiers selectionnés uniquement.
   */
  public boolean isExportOnSelectedGeometries() {
    return rbSelectedGeometries_.isSelected();
  }
  
  /**
   * Fermeture du dialogue, et suppression du calque qui visualise les géométries sélectionnées.
   */
  private void close() {
    if (cqTmp_!=null) {
      cqTmp_.detruire();
      cqTmp_=null;
      // Suppression de l'enveloppe externe.
      support_.removeGeometries(new int[]{support_.getNbGeometries()-1}, null);
      pn_.getVueCalque().getCalque().repaint();
    }
    pn_.getVueCalque().changeRepere(this, initZoom_);
  }
  
  @Override
  public boolean cancel() {
    close();
    return super.cancel();
  }

  @Override
  public boolean ok() {
    close();
    return super.ok();
  }

  /**
   * Reoutne le fichier selectionné.
   * @return Le fichier.
   */
  public File getFile() {
    return fileChooser_.getFile();
  }
}