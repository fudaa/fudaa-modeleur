/*
 * @creation     16 mars 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur;

import gnu.trove.TIntArrayList;

import java.util.ArrayList;
import java.util.List;

import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISDataModelFilterAdapter;
import org.fudaa.ctulu.gis.GISDataModelMultiAdapter;
import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueVisitor;
import org.fudaa.ebli.calque.ZModeleGeometry;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dCasier;

/**
 * Un filtre qui retourne un modele de casiers pour des calques/géométries selectionnées
 * de l'arbre des calques.
 * 
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MdlCasierExporterFilter implements BCalqueVisitor {

  boolean isOnlyOnSelectedLayers_;
  boolean isOnlyOnVisibleLayers_;
  boolean isOnlyOnSelectedGeometries_;
  boolean isOnlyOnVisibleGeometries_=true;
  BArbreCalqueModel mdTree_;
  List<GISDataModel> zones=new ArrayList<GISDataModel>();
  
  
  public MdlCasierExporterFilter(BArbreCalqueModel _mdTree) {
    mdTree_=_mdTree;
  }
  
  public boolean visit(BCalque _cq) {
    if (_cq instanceof MdlLayer2dCasier) {
      MdlLayer2dCasier cq=(MdlLayer2dCasier)_cq;
      GISDataModel md;
      
      // Si pas visible on passe.
      if (isOnlyOnVisibleLayers_ && !_cq.isVisible()) 
        return true;
      // Si pas de géométries on passe.
      if (cq.modeleDonnees().getNombre()==0) 
        return true;

      md=cq.modeleDonnees().getGeomData();
      
      // On ne retient que les géométries sélectionnées du calque.
      if (isOnlyOnSelectedGeometries_) {
        // Si pas de géométries selectionnées on passe.
        if (cq.isSelectionEmpty())
          return true;
        
        // Si des géométries sélectionnées, on trie.
        if (cq.getNbSelected()!=cq.modeleDonnees().getNombre()) {
          int[] sel=cq.getSelectedIndex();
          md=new GISDataModelFilterAdapter(md,null,sel);
        }
      }
      
      // On ne retient que les géométries visibles du calque.
      else if (isOnlyOnVisibleGeometries_) {
        ZModeleGeometry mod=cq.modeleDonnees();
        TIntArrayList idx=new TIntArrayList(mod.getNombre());
        for (int id=0; id<mod.getNombre(); id++) {
          if (mod.isGeometryVisible(id))
            idx.add(id);
        }
        if (idx.size()==0)
          return true;
        
        md=new GISDataModelFilterAdapter(md, null, idx.toNativeArray());
      }
      zones.add(md);
    }
    return true;
  }
  
  /**
   * Lance le traitement et retourne le modele de data en conséquence. Peut être null si 
   * aucune donnée récupérée après filtre.
   */
  public GISDataModel filter() {
    // On transfert dans les zones casier les Z de l'attribut vers la coordonnée.
    mdTree_.getRootCalque().apply(new BCalqueVisitor() {
      public boolean visit(BCalque _cq) {
        if (_cq instanceof MdlLayer2dCasier) {
          ((MdlLayer2dCasier)_cq).modeleDonnees().getGeomData().prepareExport();
        }
        return true;
      }
    });
    
    zones.clear();
    if (isOnlyOnSelectedLayers_) {
      final BCalque[] parent=mdTree_.getSelection();
      if (parent!=null)
        for (int i=0; i<parent.length; i++)
          parent[i].apply(this);
    }
    else {
      mdTree_.getRootCalque().apply(this);
    }
    
    if (zones.size()==0)
      return null;
    else
      return new GISDataModelMultiAdapter(zones.toArray(new GISDataModel[0]));
  }

  /**
   * Le traitement ne s'applique qu'aux seuls calques séléectionnés.
   * @param _b True : Uniquement les calques visibles sont considérés.
   */
  public void setTreatmentOnlyOnSelectedLayers(boolean _b) {
    isOnlyOnSelectedLayers_=_b;
  }

  /**
   * Le traitement ne s'applique qu'aux seuls calques visibles.
   * @param _b True : Uniquement les calques visibles sont considérés.
   */
  public void setTreatmentOnlyOnVisibleLayers(boolean _b) {
    isOnlyOnVisibleLayers_=_b;
  }

  /**
   * Le traitement s'applique uniquement aux géométries selectionnées.
   */
  public void setTreatmentOnlySelectedGeometries(boolean _b) {
    isOnlyOnSelectedGeometries_=_b;
  }

  /**
   * Le traitement s'applique uniquement aux géométries visibles.
   */
  public void setTreatmentOnlyOnVisibleGeometries(boolean _b) {
    isOnlyOnVisibleGeometries_=_b;
  }
}
