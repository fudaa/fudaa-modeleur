package org.fudaa.fudaa.modeleur;

import com.memoire.bu.BuAbstractPreferencesComponent;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Arrays;
import java.util.List;
import javax.swing.JScrollPane;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.fudaa.modeleur.layer.MdlLayerInterface;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.AbstractMutableTreeTableNode;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.TreeTableNode;

/**
 * Un composant pour les pr�f�rences de construction initiales de l'arbre de calques.
 * @author marchand@deltacad.fr
 */
public class MdlLayerTreePreferencesComponent extends BuAbstractPreferencesComponent {
  
  /** La propri�t� pour le calque de type donn� */
  public static final String TREE_INITIAL_NUMBER_LAYER_OF_TYPE="tree.initnumber.layeroftypenumber.";
  /** Le mod�le de TreeTable */
  private LayerTreeTableModel mdl_;
  /** True : Le composant est en cours de mise � jour */
  private boolean isUpdating_=false;
  
  /**
   * Une classe d'un noeud d'arbre.
   * @author marchand@deltacad.Fr
   */
  class LayerTreeTableNode extends AbstractMutableTreeTableNode {
    /** Le calque associ� */
    BCalque cq_;
    /** Le nombre de calques par defaut */
    int nbCalques=1;
    
    public LayerTreeTableNode(BCalque _cq) {
      cq_=_cq;
    }

    public Object getValueAt(int column) {
      switch(column) {
      case 0:
        return cq_.getTitle();
      case 1:
        return isMdlLayer() ? (int)nbCalques:"";
      default:
        return null;
      }
    }

    /**
     * Editable seulement si colonne 1 et calque de type MdlLayer.
     * @param column Le num�ro de colonne.
     * @return True : La cellule est �ditable.
     */
    @Override
    public boolean isEditable(int column) {
      switch (column) {
      case 0:
      default:
        return false;
      case 1:
        return isMdlLayer();
      }
    }

    @Override
    public void setValueAt(Object aValue, int column) {
      switch (column) {
      case 1:
        if (nbCalques!=(Integer)aValue) {
          nbCalques=(Integer)aValue;
          if (isUpdating_) return;
          
          setModified(true);
          setSavabled(true);
        }
      }
    }
    

    @Override
    public boolean isLeaf() {
      return !(cq_ instanceof BGroupeCalque);
    }
    
    /**
     * @return Le calque associ�.
     */
    public BCalque getCalque() {
      return cq_;
    }
    
    public boolean isMdlLayer() {
      return cq_ instanceof MdlLayerInterface;
    }

    public int getColumnCount() {
      return 2;
    }
  }
  
  /**
   * Un mod�le pour la TreeTable
   * @author marchand@deltacad.fr
   */
  class LayerTreeTableModel extends DefaultTreeTableModel {
    public LayerTreeTableModel(TreeTableNode root, List<?> columnNames) {
      super(root, columnNames);
    }

    @Override
    public Class<?> getColumnClass(int column) {
      switch (column) {
      case 0:
      default:
        return String.class;
      case 1:
        return Integer.class;
      }
      
    }
    
  }
  
  public MdlLayerTreePreferencesComponent() {
    super(MdlPreferences.MDL);
    
    BGroupeCalque cqRoot=new BGroupeCalque();
    MdlVisuPanel.buildDefaultTree(cqRoot, null, true, false);
    
    JXTreeTable ttLayers=new JXTreeTable();
    setLayout(new BorderLayout());
    JScrollPane spLayers=new JScrollPane(ttLayers);
    spLayers.setPreferredSize(new Dimension(200,200));
    add(spLayers,BorderLayout.CENTER);
    LayerTreeTableNode ndRoot=new LayerTreeTableNode(cqRoot);
    buildTree(ndRoot);
    mdl_=new LayerTreeTableModel(ndRoot,Arrays.asList(MdlResource.getS("Type de calque"),MdlResource.getS("Nombre")));
    ttLayers.setTreeTableModel(mdl_);
    ttLayers.getColumn(0).setPreferredWidth(250);
    ttLayers.expandAll();
    
    updateComponent();
  }
  
  /**
   * Cr�e l'arbre par r�cursivit�.
   * @param _parent Le noeud parent.
   */
  private void buildTree(LayerTreeTableNode _parent) {
    for (BCalque cq : _parent.getCalque().getCalques()) {
      LayerTreeTableNode nd=new LayerTreeTableNode(cq);
      _parent.add(nd);
      buildTree(nd);
    }
  }
  
  @Override
  public boolean isPreferencesValidable() {
    return true;
  }
  
  @Override
  public boolean isPreferencesCancelable() {
    return true;
  }

  @Override
  public String getTitle() {
    return MdlResource.getS("Initialisation de l'arbre des calques");
  }

  @Override
  protected void updateComponent() {
    isUpdating_=true;
    updateNodeFromProp((LayerTreeTableNode)mdl_.getRoot());
    isUpdating_=false;
  }
  
  /**
   * Met a jour l'arbre depuis les propri�t�s de mani�re r�cursive.
   * @param _parent 
   */
  private void updateNodeFromProp(LayerTreeTableNode _parent) {
    for (int i=0; i<_parent.getChildCount(); i++) {
      LayerTreeTableNode nd=(LayerTreeTableNode)_parent.getChildAt(i);
      if (nd.isLeaf()) {
        int nb=MdlPreferences.MDL.getIntegerProperty(TREE_INITIAL_NUMBER_LAYER_OF_TYPE+((MdlLayerInterface)nd.getCalque()).getExtName(),1);
        mdl_.setValueAt(nb, nd, 1);
      }
      updateNodeFromProp(nd);
    }
  }

  @Override
  protected void updateProperties() {
    updatePropFromNode((LayerTreeTableNode)mdl_.getRoot());
  }
  
  /**
   * Met a jour les propri�t�s � partir des noeuds de l'arbre de mani�re r�cursive.
   * @param _parent 
   */
  private void updatePropFromNode(LayerTreeTableNode _parent) {
    for (int i=0; i<_parent.getChildCount(); i++) {
      LayerTreeTableNode nd=(LayerTreeTableNode)_parent.getChildAt(i);
      if (nd.isMdlLayer()) {
        int nb=(Integer)mdl_.getValueAt(nd, 1);
        MdlPreferences.MDL.putIntegerProperty(TREE_INITIAL_NUMBER_LAYER_OF_TYPE+((MdlLayerInterface)nd.getCalque()).getExtName(),nb);
      }
      updatePropFromNode(nd);
    }
  }
}
