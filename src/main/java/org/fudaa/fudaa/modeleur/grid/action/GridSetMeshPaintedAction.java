/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid.action;

import java.awt.event.ActionEvent;
import org.apache.commons.lang.ArrayUtils;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.modeleur.grid.LayerGridController;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Permet de demander l'affichage des isosurfaces sur les éléments sélectionnés
 *
 * @author Frederic Deniger
 */
public class GridSetMeshPaintedAction extends EbliActionSimple {

  private final LayerGridController delegate;

  public GridSetMeshPaintedAction(LayerGridController delegate) {
    super(MdlResource.getS("Ne plus ignorer les éléments sélectionnés"), null, "DONT_IGNORE_GRID");
    setDefaultToolTip(MdlResource.getS("Permet de dessiner les isolines/surfaces sur les éléments sélectionnés"));
    this.delegate = delegate;
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    if (delegate.isGridCreated()) {
      final int[] selectedElementIdx = delegate.getGridLayer().getSelectedElementIdx();
      if (ArrayUtils.isNotEmpty(selectedElementIdx)) {
        delegate.setEltPainted(selectedElementIdx, true);
      }
    }
  }
}
