/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid;

import java.awt.Color;
import java.awt.Graphics2D;

import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.ZModelGeometryListener;
import org.fudaa.ebli.calque.edition.ZCalqueMultiPointEditable;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.meshviewer.layer.MvIsoLayerQuickPainter;
import org.fudaa.fudaa.modeleur.LibUtils;
import org.fudaa.fudaa.modeleur.grid.action.ExportGridAction;
import org.fudaa.fudaa.modeleur.grid.action.GridSetMeshPaintedAction;
import org.fudaa.fudaa.modeleur.grid.action.GridUnsetMeshPaintedAction;
import org.fudaa.fudaa.modeleur.grid.action.Show3DAction;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.fudaa.fudaa.sig.layer.FSigEditor;
import org.locationtech.jts.geom.Geometry;

/**
 * Controller permettant de g�rer le maillage associ� a un calque de semis de points. Ecoute les modifications sur le calque de semis et efface le
 * maillage si la g�om�trie est modifi�e.
 *
 * @author Frederic Deniger
 */
public class LayerGridController implements ZModelGeometryListener {

  private final MdlCalqueMultiPointEditable parentLayer;
  private MdlElementModel elementModel;
  private MdlIsoModelDefault isoModel;
  private MdlGridData gridData;
  private MvIsoLayerQuickPainter painter;

  public LayerGridController(MdlCalqueMultiPointEditable parentLayer) {
    this.parentLayer = parentLayer;
    parentLayer.modeleDonnees().addModelListener(this);
  }

  public CtuluUI getUI() {
    return ((FSigEditor) parentLayer.getEditor()).getUi();
  }

  @Override
  public void geometryAction(Object _source, int _indexGeom, Geometry _geom, int _action) {
    if (elementModel != null) {
      elementModel = null;
      gridData = null;
      parentLayer.remove(getGridLayer());
    }
  }

  /**
   * Dessine les isosurfaces/lignes. Si rapide dessine des isolignes.
   *
   * @param width
   * @param height
   * @param _g
   * @param _versEcran
   * @param _versReel
   * @param _clipReel
   */
  public void paintDonnees(int width, int height, final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel, final GrBoite _clipReel) {
    if (gridData == null || parentLayer.getPaletteCouleur() == null) {
      return;
    }
    if (!parentLayer.isTraceIsoLine() && !parentLayer.isTraceIsoSurface()) {
      return;
    }
    if (isoModel == null) {
      isoModel = new MdlIsoModelDefault(gridData, elementModel);
      isoModel.setVar(H2dVariableType.BATHYMETRIE);
    }
    if (painter == null) {
      painter = new MvIsoLayerQuickPainter(isoModel);
    }
    painter.setModel(isoModel);
    //si bcp de donn�es on trace des iso lignes.
    painter.setTraceIsoLine(parentLayer.isRapideForBigData() ? true : parentLayer.isTraceIsoLine());
    painter.setPaletteCouleur(parentLayer.getPaletteCouleur());
    painter.setIsAntaliasing(parentLayer.isAntialiasing());
    painter.setAlpha(parentLayer.getAlpha());
    painter.setLigneModel(parentLayer.getLineModel(0));
    painter.setRapide(parentLayer.isRapide());
    painter.paintDonnees(width, height, _g, _versEcran, _versReel, _clipReel);

  }

  @Override
  public void attributeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _action) {
  }

  @Override
  public void attributeValueChangeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _indexGeom, Object _newValue) {
  }

  public MdlElementLayer getGridLayer() {
    return eltLayer;
  }

  /**
   *
   * @return true si un maillage est pr�sent.
   */
  public boolean isGridCreated() {
    return elementModel != null;
  }

  public ZCalqueMultiPointEditable getParentLayer() {
    return parentLayer;
  }

  public MdlGridData getExportGridData() {
    return gridData;
  }

  public void clearGrid() {
    elementModel = null;
    gridData = null;
    isoModel = null;
    if (eltLayer != null) {
      parentLayer.remove(eltLayer);
      parentLayer.clearCacheAndRepaint();
      eltLayer = null;
    }
  }

  public void setGridLayer(MdlElementModel grid) {
    this.elementModel = grid;
    gridData = null;
    isoModel = null;

    if (eltLayer != null) {
      parentLayer.remove(eltLayer);
      eltLayer = null;
    }
    if (grid != null) {
      installGridLayer(grid);
    }
    parentLayer.clearCacheAndRepaint();
    ((FSigEditor) parentLayer.getEditor()).getPanel().getVueCalque().repaint(0);
    ((FSigEditor) parentLayer.getEditor()).getPanel().getVueCalque().revalidate();
  }
  MdlElementLayer eltLayer;

  public void installGridLayer(MdlElementModel grid) {
    if (eltLayer == null) {
      eltLayer = new MdlElementLayer();
      eltLayer.setTitle(MdlResource.getS("Maillage"));
      eltLayer.setName("cqGrid");
      eltLayer.setLineModel(0, new TraceLigneModel(TraceLigne.LISSE, 1f, Color.LIGHT_GRAY));
//      eltLayer.setAlpha(128);
      eltLayer.setVisible(false);
      parentLayer.add(eltLayer);
    }
    final int attributeZ = LibUtils.getAttributeZPosition(parentLayer.modeleDonnees().getGeomData());
    gridData = new MdlGridData(grid, attributeZ);
    eltLayer.setActions(new EbliActionInterface[]{
              new GridUnsetMeshPaintedAction(this),
              new GridSetMeshPaintedAction(this),
              null,
              new ExportGridAction(this),
              new Show3DAction(this)
            });
    eltLayer.setModele(grid);
    eltLayer.repaint();
  }

  public void setEltPainted(int[] selectedElementIdx, boolean b) {
    getGridLayer().modele().setPainted(selectedElementIdx, b);
    getGridLayer().clearCacheAndRepaint();
    parentLayer.clearCacheAndRepaint();
  }
}
