/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.comparator.CoordinateComparator;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.dodico.ef.triangulation.TriangulationConvexHullBuilder;
import org.fudaa.dodico.ef.triangulation.TriangulationPolyDataInterface;
import org.fudaa.dodico.ef.triangulation.TriangulationUniqueDataContent;
import org.fudaa.dodico.ef.triangulation.TriangulationUniqueDataContentBuilder;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.calque.ZModeleMultiPoint;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.locationtech.jts.algorithm.CGAlgorithms;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.Point;
import org.poly2tri.Poly2Tri;
import org.poly2tri.geometry.polygon.Polygon;
import org.poly2tri.geometry.polygon.PolygonPoint;
import org.poly2tri.triangulation.Triangulatable;
import org.poly2tri.triangulation.TriangulationPoint;
import org.poly2tri.triangulation.delaunay.DelaunayTriangle;
import org.poly2tri.triangulation.point.TPoint;
import org.poly2tri.triangulation.sets.PointSet;

/**
 * Utilise http://code.google.com/p/poly2tri/
 *
 * @author Frederic Deniger
 */
public class CalqueCreateGridPoly2TriProcessor extends AbstractCalqueCreateGridProcessor {

  public CalqueCreateGridPoly2TriProcessor(ZModeleMultiPoint points, CtuluUI ui, ZCalqueLigneBrisee contour, LinearRing convexHull) {
    super(points, ui, contour, convexHull);
  }

  @Override
  public EfGridInterface process(ProgressionInterface prog) {
    stop = false;
    TriangulationPolyDataInterface data = createPolyData(prog);
    ProgressionUpdater updater = new ProgressionUpdater(prog);
    updater.majProgessionStateOnly(MdlResource.getS("Construction des donn�es d'entr�e"));

    if (data.getNbLines() > 0) {
      TriangulationConvexHullBuilder builder = new TriangulationConvexHullBuilder();
      int nbExtern = builder.getNbExternPolygon(data);
      if (nbExtern > 1) {
        ui.error(MdlResource.getS("Une seule fronti�re ext�rieure est support�e par le triangulateur interne"));
        return null;
      }
      updater.majProgessionStateOnly(MdlResource.getS("Construction de l'enveloppe externe"));
      data = builder.addConvexHullIfNeeded(data);
      int idxExternPoly = builder.getExternPolygonIdx(data);
      updater.majProgessionStateOnly(MdlResource.getS("Construction des points"));
      TreeSet<Coordinate> addedCoordinate = new TreeSet<Coordinate>(new CoordinateComparator(eps));
      LinearRing externRing = (LinearRing) data.getLine(idxExternPoly);
      LinearRing isInMainRing = externRing;
      List<LinearRing> isInHoles = new ArrayList<LinearRing>();
      List<PolygonPoint> polygonPoints = createPolygonPoints(externRing, addedCoordinate);
      Polygon polygon = new Polygon(polygonPoints);
      if (data.getNbLines() > 1) {
        for (int idxPoly = 0; idxPoly < data.getNbLines(); idxPoly++) {
          if (idxPoly != idxExternPoly && data.isClosed(idxPoly)) {
            isInHoles.add((LinearRing) data.getLine(idxPoly));
            polygon.addHole(new Polygon(createPolygonPoints((LinearRing) data.getLine(idxPoly), addedCoordinate)));
          }
        }
      }
      int nbPoints = data.getPtsNb();
      updater.setValue(10, nbPoints);
      for (int idxPt = 0; idxPt < nbPoints; idxPt++) {
        double x = data.getPtX(idxPt);
        double y = data.getPtY(idxPt);
        Coordinate c = new Coordinate(x, y);
        Point p = GISGeometryFactory.INSTANCE.createPoint(c);

        if (!addedCoordinate.contains(c)) {
          addedCoordinate.add(c);
          if (isInMainRing.contains(p) && isNotIn(isInHoles, p)) {
            polygon.addSteinerPoint(new TPoint(x, y));
          }
          updater.majAvancement();
        }
      }
      updater.majProgessionStateOnly(MdlResource.getS("Triangulation"));
      if (prog != null) {
        prog.setProgression(0);
      }
      try {
        Poly2Tri.triangulate(polygon);
      } catch (Exception e) {
        Logger.getLogger(CalqueCreateGridPoly2TriProcessor.class.getName()).log(Level.SEVERE, "message {0}", e);
      } catch (StackOverflowError e) {
        Logger.getLogger(CalqueCreateGridPoly2TriProcessor.class.getName()).log(Level.SEVERE, "message {0}", e);
      }

      return createGrid(polygon, prog);
    } else {
      TriangulationUniqueDataContent content = new TriangulationUniqueDataContentBuilder(data, eps).build(prog);
      List<TriangulationPoint> pointSet = new ArrayList<TriangulationPoint>();
      for (int i = 0; i < content.getNbPoints(); i++) {
        pointSet.add(new TPoint(content.getX(i), content.getY(i)));
      }
      PointSet pt = new PointSet(pointSet);
      updater.majProgessionStateOnly(MdlResource.getS("Triangulation"));
      try {
        Poly2Tri.triangulate(pt);
      } catch (Exception e) {
        Logger.getLogger(CalqueCreateGridPoly2TriProcessor.class.getName()).log(Level.SEVERE, "message {0}", e);
      } catch (StackOverflowError e) {
        Logger.getLogger(CalqueCreateGridPoly2TriProcessor.class.getName()).log(Level.SEVERE, "message {0}", e);
      }
      return createGrid(pt, prog);
    }
  }

  public EfGridInterface createGrid(Triangulatable pt, ProgressionInterface prog) {
    return createGrid(pt.getTriangles(), prog);
  }

  public EfGridInterface createGrid(List<DelaunayTriangle> triangles, ProgressionInterface prog) {
    if (triangles.isEmpty()) {
      return null;
    }
    EfElement[] elts = new EfElement[triangles.size()];
    TreeMap<Coordinate, Integer> map = new TreeMap<Coordinate, Integer>(new CoordinateComparator());
    int idx = 0;
    ProgressionUpdater updater = new ProgressionUpdater(prog);
    updater.majProgessionStateOnly(MdlResource.getS("Construction des �l�ments"));
    updater.setValue(10, triangles.size());
    for (int idxElt = 0; idxElt < triangles.size(); idxElt++) {
      final TriangulationPoint[] points = triangles.get(idxElt).points;
      int[] elt = new int[points.length];
      for (int idxPt = 0; idxPt < points.length; idxPt++) {
        TriangulationPoint triangulationPoint = points[idxPt];
        Coordinate c = new Coordinate(triangulationPoint.getX(), triangulationPoint.getY());
        Integer ptIdx = map.get(c);
        if (ptIdx == null) {
          ptIdx = idx;
          map.put(c, idx++);
        }
        elt[idxPt] = ptIdx;
      }
      elts[idxElt] = new EfElement(elt);
      updater.majAvancement();
    }
    updater.majProgessionStateOnly(MdlResource.getS("Construction des noeuds"));
    updater.setValue(10, map.size());
    EfNode[] nodes = new EfNode[map.size()];
    for (Map.Entry<Coordinate, Integer> entry : map.entrySet()) {
      Integer idxNode = entry.getValue();
      nodes[idxNode] = new EfNode(entry.getKey());
      updater.majAvancement();
    }
    final EfGrid efGrid = new EfGrid(nodes, elts);
    efGrid.computeBord(prog, new CtuluAnalyze());
    return efGrid;
  }

  public List<PolygonPoint> createPolygonPoints(LinearRing ring, TreeSet<Coordinate> addedCoordinate) {
    List<PolygonPoint> polygonPoints = new ArrayList<PolygonPoint>();
    boolean ccW = CGAlgorithms.isCCW(ring.getCoordinates());
    for (int idxPt = 0; idxPt < ring.getNumPoints() - 1; idxPt++) { //dernier point=premier point
      Coordinate coordinateN = ring.getCoordinateSequence().getCoordinateCopy(idxPt);
      addedCoordinate.add(coordinateN);
      polygonPoints.add(new PolygonPoint(coordinateN.x, coordinateN.y));
    }
    if (ccW) {
      Collections.reverse(polygonPoints);
    }
    return polygonPoints;
  }
}
