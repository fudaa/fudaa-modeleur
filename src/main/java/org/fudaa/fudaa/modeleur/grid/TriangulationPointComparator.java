/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid;

import java.util.Comparator;
import org.fudaa.ctulu.gis.comparator.AbstractComparator;
import org.poly2tri.triangulation.TriangulationPoint;

/**
 *
 * @author Frederic Deniger
 */
public class TriangulationPointComparator extends AbstractComparator implements Comparator<TriangulationPoint> {

  @Override
  public int compare(TriangulationPoint o1, TriangulationPoint o2) {
    if (o1 == o2) {
      return 0;
    }
    if (o1 == null) {
      return -1;
    }
    if (o1 == null) {
      return 1;
    }
    return super.compare(o1.getX(), o1.getY(), o2.getX(), o2.getY());
  }
}
