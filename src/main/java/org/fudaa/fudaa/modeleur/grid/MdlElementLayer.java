/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid;

import org.fudaa.ebli.calque.BCalqueCacheManager;
import org.fudaa.ebli.calque.BCalqueCacheManagerSelection;
import org.fudaa.fudaa.meshviewer.layer.MvElementLayer;
import org.fudaa.fudaa.meshviewer.model.MvElementModel;

/**
 * Le calque affichage le maillage.
 * @author Frederic Deniger
 */
public class MdlElementLayer extends MvElementLayer {

  public MdlElementLayer() {
    setSelectable(false);
    BCalqueCacheManager.installDefaultCacheManager(this);
    BCalqueCacheManagerSelection.installDefaultCacheManagerSelection(this);
  }

  public MdlElementLayer(MvElementModel _modele) {
    super(_modele);
    setSelectable(false);
  }

  @Override
  public String editSelected() {
    return null;
  }

  @Override
  public boolean isDestructible() {
    return true;
  }

  public void clearCacheAndRepaint() {
    super.clearCache();
    repaint(0);
  }

  @Override
  public MdlElementModel modele() {
    return (MdlElementModel) super.modele();
  }

  @Override
  public boolean canSetSelectable() {
    return true;
  }
}
