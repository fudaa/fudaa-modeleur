/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid.action;

import java.awt.event.ActionEvent;
import org.apache.commons.lang.ArrayUtils;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.modeleur.grid.LayerGridController;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Permet de demander le non-affichage des isosurfaces sur les éléments sélectionnés
 *
 * @author Frederic Deniger
 */
public class GridUnsetMeshPaintedAction extends EbliActionSimple {

  private final LayerGridController delegate;

  public GridUnsetMeshPaintedAction(LayerGridController delegate) {
    super(MdlResource.getS("Ignorer les éléments sélectionnés"), null, "IGNORE_GRID");
    setDefaultToolTip(MdlResource.getS("Permet de ne pas dessiner les isolines/surfaces sur les éléments sélectionnés"));
    this.delegate = delegate;
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    if (delegate.isGridCreated()) {
      final int[] selectedElementIdx = delegate.getGridLayer().getSelectedElementIdx();
      if (ArrayUtils.isNotEmpty(selectedElementIdx)) {
        delegate.setEltPainted(selectedElementIdx, false);
      }
    }
  }
}
