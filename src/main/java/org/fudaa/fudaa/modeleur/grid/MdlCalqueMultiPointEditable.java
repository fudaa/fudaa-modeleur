/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import org.fudaa.ctulu.gis.GISAttributeModelDoubleInterface;
import org.fudaa.ctulu.gis.GISAttributeModelObjectInterface;
import org.fudaa.ebli.calque.BCalqueCacheManager;
import org.fudaa.ebli.calque.BCalqueCacheManagerSelection;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesConfigure;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesTraceConfigure;
import org.fudaa.ebli.calque.ZCalqueGeometryLabelConfigure;
import org.fudaa.ebli.calque.ZModeleGeometry;
import org.fudaa.ebli.calque.ZModeleMultiPoint;
import org.fudaa.ebli.calque.edition.ZCalqueMultiPointEditable;
import org.fudaa.ebli.calque.edition.ZEditorDefault;
import org.fudaa.ebli.calque.edition.ZModeleMultiPointEditable;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.controle.BConfigurableComposite;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.modeleur.action.CalqueCreateGridAction;
import org.fudaa.fudaa.modeleur.action.CalqueGridTriangleConfigurationAction;
import org.fudaa.fudaa.modeleur.grid.action.CreateIsoLinesAction;
import org.fudaa.fudaa.modeleur.grid.action.DeleteGridAction;
import org.fudaa.fudaa.modeleur.grid.action.ExportGridAction;
import org.fudaa.fudaa.modeleur.grid.action.Show3DAction;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 * Un calque de mutlipoint acceptant un maillage comme calque fils.
 *
 * @author Frederic Deniger
 */
public class MdlCalqueMultiPointEditable extends ZCalqueMultiPointEditable {

  private boolean traceIsoLine;
  private boolean traceIsoSurface = true;
  /**
   * classe charg�e de la relation entre le calque multi point et le maillage associ�.
   */
  private final LayerGridController layerGridDelegate;
  /**
   * l'indice des jeux de donn�es contenant la bathym�trie.
   */
  int bathyDonneesIndice = -1;

  public MdlCalqueMultiPointEditable(ZModeleMultiPointEditable _modele, ZEditorDefault _editor) {
    super(_modele, _editor);
    updatePositionDataBathy();
    layerGridDelegate = new LayerGridController(this);
    setLegende(_editor == null ? null : _editor.getPanel().getCqLegend());
    installActions();
    BCalqueCacheManager.installDefaultCacheManager(this);
    BCalqueCacheManagerSelection.installDefaultCacheManagerSelection(this);
  }

  /**
   * Installation des actions pour le layer. Les actions sont communes au layer et objets selectionn�s.
   */
  private void installActions() {
    FSigEditor editor = (FSigEditor) getEditor();
    if (editor == null) {
      return;
    }
    ArrayList<EbliActionInterface> vacts = new ArrayList<EbliActionInterface>();
    final CalqueCreateGridAction createGridAction = new CalqueCreateGridAction(this);
    vacts.add(createGridAction);
    vacts.add(createGridAction.getStopAction());
    vacts.add(new DeleteGridAction(layerGridDelegate));
    vacts.add(null);
    vacts.add(new Show3DAction(layerGridDelegate));
    vacts.add(new CreateIsoLinesAction(layerGridDelegate));
    vacts.add(new ExportGridAction(layerGridDelegate));
    vacts.add(null);
    vacts.add(new CalqueGridTriangleConfigurationAction());
    vacts.add(null);
    vacts.add(editor.getImportAction());
    vacts.add(editor.getExportAction());
    EbliActionInterface[] acts = vacts.toArray(new EbliActionInterface[vacts.size()]);

    super.setActions(acts);
  }

  public LayerGridController getLayerGridDelegate() {
    return layerGridDelegate;
  }

  @Override
  protected BConfigurableInterface getAffichageConf() {
    final BConfigurableInterface[] sect = new BConfigurableInterface[3 + getNbSet()];
    sect[0] = new ZCalqueAffichageDonneesConfigure(this);
    sect[1] = new ZCalqueGeometryLabelConfigure(this);
    sect[2] = new MdlCalqueMultiPointEditableConfigure(this);
    for (int i = 3; i < sect.length; i++) {
      sect[i] = new ZCalqueAffichageDonneesTraceConfigure(this, i - 2);
    }
    return new BConfigurableComposite(sect, EbliLib.getS("Affichage"));
  }

  public boolean isTraceIsoLine() {
    return traceIsoLine;
  }

  private void updatePositionDataBathy() {
    bathyDonneesIndice = modeleDonnees().getGeomData().getIndiceOf(modeleDonnees().getGeomData().getAttributeIsZ());
  }

  @Override
  public boolean isPaletteModifiable() {
    return bathyDonneesIndice >= 0;
  }

  @Override
  public boolean isDonneesBoiteAvailable() {
    return bathyDonneesIndice >= 0;
  }

  @Override
  protected void initTraceForAtomics(TraceIconModel _icon, int idxPoly, int idxVertexInPoly) {
    if (isPaletteCouleurUsed_ && bathyDonneesIndice >= 0) {
      GISAttributeModelObjectInterface modelListPoints = (GISAttributeModelObjectInterface) modeleDonnees().getGeomData().getModel(bathyDonneesIndice);
      GISAttributeModelDoubleInterface model = (GISAttributeModelDoubleInterface) modelListPoints.getValue(idxPoly);
      Color c = ((BPalettePlage) super.paletteCouleur_).getColorFor(model.getValue(idxVertexInPoly));
      if (c == null) {
        c = iconModel_ == null ? Color.BLACK : iconModel_.getCouleur();
      }
      _icon.setCouleur(c);
    }
  }

  @Override
  public void modele(ZModeleGeometry _modele) {
    super.modele(_modele);
    updatePositionDataBathy();
  }

  public boolean isRapideForBigData() {
    return isRapide() && layerGridDelegate.isGridCreated() && layerGridDelegate.getGridLayer().modele().getNbPoint() > 100000;
  }

  @Override
  public void paintDonnees(Graphics2D _g, GrMorphisme _versEcran, GrMorphisme _versReel, GrBoite _clipReel) {
    if ((modele_ == null) || (modele_.getNombre() <= 0)) {
      return;
    }
    //si rapide, le delegate trace des isoligne.
    //dans ce cas on s'assure de tracer les icones sur les points
    if (isRapideForBigData() && isTraceIsoSurface() && (iconModel_.getType() == TraceIcon.RIEN || iconModel_.getTaille() <= 0)) {
      iconModel_.setTaille(1);
      iconModel_.setType(TraceIcon.CARRE);
    }
    paintIconsOnAtomics(_g, _versEcran, _versReel, _clipReel);

    layerGridDelegate.paintDonnees(getWidth(), getHeight(), _g, _versEcran, _versReel, _clipReel);
    TraceIconModel saved = iconModel_.cloneData();

    paintLabelsOnAtomics(_g, _versEcran, _versReel, _clipReel);
    iconModel_.updateData(saved);
  }

  @Override
  protected boolean isIconOnAtomicsPainted(int idxGeometry, int idxPoint) {
    if (getLayerGridDelegate().isGridCreated() && isTraceIsoSurface()) {
      if (isRapide()) {
        return true;//optimisation
      }
      MdlElementModel modele = getLayerGridDelegate().getGridLayer().modele();
      int idx = modele.getPointsAdapter().getIdx(idxGeometry, idxPoint);
      if (modele.isNodePaintedByIso(idx)) {
        return false;
      }
      return true;
    }
    return super.isIconOnAtomicsPainted(idxGeometry, idxPoint);
  }

  /**
   *
   * @return true car ce claque pourra avoir le calque maillage comme calque enfant.
   */
  @Override
  public boolean isGroupeCalque() {
    return true;
  }

  @Override
  public void modele(ZModeleMultiPoint _modele) {
    super.modele(_modele);
    updatePositionDataBathy();
  }

  @Override
  public EbliUIProperties saveUIProperties() {
    EbliUIProperties res = super.saveUIProperties();
    res.put("traceIsoLine", Boolean.valueOf(traceIsoLine));
    res.put("traceIsoSurface", Boolean.valueOf(traceIsoSurface));
    MdlElementLayer gridLayer = getLayerGridDelegate().getGridLayer();
    if (gridLayer != null) {
      res.addAll(gridLayer.saveUIProperties().cloneWithPrefix("subGrid."));
    }
    return res;
  }

  /**
   * @param _idx
   * @return null pour �viter de configurer une ligne qui ne sera pas utilis�.
   */
  @Override
  public TraceLigneModel getLineModel(int _idx) {
    return null;
  }

  @Override
  public void initFrom(EbliUIProperties _p) {
    super.initFrom(_p);
    if (_p.isDefined("traceIsoLine")) {
      traceIsoLine = _p.getBoolean("traceIsoLine");
    }
    if (_p.isDefined("traceIsoSurface")) {
      traceIsoSurface = _p.getBoolean("traceIsoSurface");
    }
    MdlElementLayer gridLayer = getLayerGridDelegate().getGridLayer();
    if (gridLayer != null) {
      gridLayer.initFrom(_p.extractSubset("subGrid."));
    }
  }

  public void setTraceIsoLine(boolean traceIsoLine) {
    if (traceIsoLine != this.traceIsoLine) {
      this.traceIsoLine = traceIsoLine;
      firePropertyChange(MdlCalqueMultiPointEditableConfigure.ISO_LINE_PAINTED, !traceIsoLine, traceIsoLine);
      repaint();
    }
  }

  public boolean isTraceIsoSurface() {
    return traceIsoSurface;
  }

  public void setTraceIsoSurface(boolean traceIsoSurface) {
    if (traceIsoSurface != this.traceIsoSurface) {
      this.traceIsoSurface = traceIsoSurface;
      firePropertyChange(MdlCalqueMultiPointEditableConfigure.ISO_SURFACE_PAINTED, !traceIsoSurface, traceIsoSurface);
      repaint();
    }
  }
}
