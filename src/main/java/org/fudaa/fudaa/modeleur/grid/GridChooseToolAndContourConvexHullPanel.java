/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid;

import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JList;

import org.apache.commons.lang.StringUtils;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.fudaa.modeleur.MdlPreferences;
import org.fudaa.fudaa.modeleur.MdlVisuPanel;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.fudaa.fudaa.sig.wizard.FSigWizardImportHelper;
import org.locationtech.jts.geom.LinearRing;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuTextField;

/**
 *
 * @author Frederic Deniger
 */
public class GridChooseToolAndContourConvexHullPanel extends CtuluDialogPanel {

  JComboBox cbTriangulateur;
  JComboBox cbLayerContour;
  JComboBox cbLayerInteriorLine;
  JComboBox cbConvexHull;
  JCheckBox cbUseHole;
  JCheckBox cbAddPointOnFrontier;
  BuTextField tfEps;
  CtuluFileChooserPanel chooseExe;

  public GridChooseToolAndContourConvexHullPanel(MdlVisuPanel visuPanel) {
    super(false);
    setHelpText(MdlResource.getS("Choisir le calque qui d�terminera le contour et les trous du maillage"));
    setLayout(new BuGridLayout(2, 5, 5));
    BuLabel chooseEps = addLabel(MdlResource.getS("Epsilon"));
    chooseEps.setToolTipText(MdlResource.getS("Distance en-dessous de laquelle 2 points sont consid�r�s comme �gaux"));
    tfEps = addDoubleText();
    tfEps.setText("0.001");
    tfEps.setToolTipText(chooseEps.getToolTipText());
    BuLabel chooseMailleur = addLabel(MdlResource.getS("Triangulateur"));
    chooseMailleur.setToolTipText(MdlResource.getS("<html><body>Choisi le triangulateur<br>Triangle est plus robuste et mieux adapt� au volume important.<br>Si vous utiliser Triangle, vous devez t�l�charger un ex�cutable.<br>https://www.cs.cmu.edu/~quake/triangle.html"));
    cbTriangulateur = new JComboBox(TriangulateurEnum.values());
    add(cbTriangulateur);
    chooseExe = addFileChooserPanel(this, MdlResource.getS("Chemin pour Triangle") + ": ", false, false);
    cbAddPointOnFrontier = new JCheckBox();
    cbTriangulateur.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        updateChooseExecState();
        updateCbAddPointOnFrontierAndContraint();
      }
    });
    String triangleExe = CalqueCreateGridTriangleProcessor.getTriangleExe();
    if (StringUtils.isNotBlank(triangleExe)) {
      chooseExe.setFile(new File(triangleExe));
    }
    if (isTriangleLastUsed()) {
      cbTriangulateur.setSelectedItem(TriangulateurEnum.TRIANGLE);
    }
    BuLabel lbAddPoint = addLabel(MdlResource.getS("Ajouter des points sur la fronti�re externe"));
    lbAddPoint.setToolTipText(MdlResource.getS("Si activ�, le triangulateur peut ajouter des points sur la fronti�re"));

    add(cbAddPointOnFrontier);
    updateCbAddPointOnFrontierAndContraint();
    List<BCalque> calquesContours = visuPanel.getCalquesContours();
    final FSigWizardImportHelper.CalqueCellRender calqueCellRender = new FSigWizardImportHelper.CalqueCellRender();
    calqueCellRender.setNullValue(MdlResource.getS("Aucun"));
    if (!calquesContours.isEmpty()) {
      addLabel(" ");
      addLabel(" ");
      BuLabel addLabel = addLabel(MdlResource.getS("Calque contour"));
      addLabel.setToolTipText(MdlResource.getS("Les polygones du calque formeront l'enveloppe et les trous du maillage"));
      calquesContours.add(0, null);
      BCalque[] bCalques = (BCalque[]) calquesContours.toArray(new BCalque[calquesContours.size()]);
      cbLayerContour = new JComboBox(bCalques);

      cbLayerContour.setRenderer(calqueCellRender);
      add(cbLayerContour);
      BuLabel labelEnveloppe = addLabel(MdlResource.getS("L'enveloppe externe"));
      labelEnveloppe.setToolTipText("<html>" + MdlResource.getS("Choisir le polygone formant l'enveloppe du maillage") + "<br>" + MdlResource.getS("Si aucun, l'enveloppe convexe du semis de point sera utilis�"));
      cbConvexHull = new JComboBox();
      cbConvexHull.setRenderer(new LinearRingCellRenderer());
      add(cbConvexHull);
      BuLabel labelUseHole = addLabel(MdlResource.getS("Utiliser Les autres lignes ferm�es comme des �les"));
      labelUseHole.setToolTipText("<html>" + MdlResource.getS("Si activ�, les lignes ferm�es, hormis l'enveloppe externe, seront consid�r�es comme �tant des �les"));
      cbUseHole = new JCheckBox();
      add(cbUseHole);
      cbUseHole.setSelected(true);
      cbLayerContour.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          updateConvexHullCb();
        }
      });
    }
    List<BCalque> calqueLine2d = visuPanel.getCalquesContraintes();
    addLabel(" ");
    addLabel(" ");
    BuLabel addLabel = addLabel(MdlResource.getS("Lignes de contraintes"));
    addLabel.setToolTipText(MdlResource.getS("Lignes sur lesquelles le maillage devra s'appuyer. Non support� par le mailleur interne."));
    calqueLine2d.add(0, null);
    BCalque[] bCalques = (BCalque[]) calqueLine2d.toArray(new BCalque[calqueLine2d.size()]);
    cbLayerInteriorLine = new JComboBox(bCalques);
    cbLayerInteriorLine.setRenderer(calqueCellRender);
    add(cbLayerInteriorLine);
    updateCbAddPointOnFrontierAndContraint();
    updateChooseExecState();
  }

  protected final void updateChooseExecState() {
    chooseExe.setEnabled(isTriangleSelected());
  }

  protected final void updateCbAddPointOnFrontierAndContraint() {
    cbAddPointOnFrontier.setEnabled(isTriangleSelected());
    if (cbLayerInteriorLine != null) {
      cbLayerInteriorLine.setEnabled(isTriangleSelected());
    }
  }

  @Override
  public boolean isDataValid() {
    if (!isTriangleSelected()) {
      return true;
    }
    File choosenFile = chooseExe.getFile();
    if (choosenFile == null) {
      setErrorText(MdlResource.getS("Le chemin vers l'ex�cutable Triangle n'est pas d�fini"));
      return false;
    } else if (!choosenFile.exists()) {
      setErrorText(MdlResource.getS("Le chemin vers l'ex�cutable Triangle n'existe pas: {0}", choosenFile.getAbsolutePath()));
      return false;
    }
    Double value = getEps();
    if (value == null || value < 0) {
      setErrorText(MdlResource.getS("Une valeur strictement positive pour epsilon est requise"));
      return false;
    }
    return super.isDataValid();
  }

  public Double getEps() {
    return (Double) tfEps.getValue();
  }

  @Override
  public boolean apply() {
    super.apply();
    if (isTriangleSelected()) {
      File choosenFile = chooseExe.getFile();
      if (choosenFile != null) {
        MdlPreferences.MDL.putStringProperty("grid.triangle.path", choosenFile.getAbsolutePath());

      }
    }
    MdlPreferences.MDL.putStringProperty("grid.triangulateur.used", isTriangleSelected() ? "triangle" : "interne");
    MdlPreferences.MDL.writeIniFile();
    
    return true;
  }

  private boolean isTriangleLastUsed() {
    return "triangle".equals(MdlPreferences.MDL.getStringProperty("grid.triangulateur.used"));
  }

  public boolean isTriangleSelected() {
    return TriangulateurEnum.TRIANGLE.equals(cbTriangulateur.getSelectedItem());
  }

  public boolean isUseHoleIsSelected() {
    return cbUseHole.isSelected();
  }

  public boolean isAddPointOnFrontierSelected() {
    return cbAddPointOnFrontier.isSelected();
  }

  private class LinearRingCellRenderer extends CtuluCellTextRenderer {

    @Override
    public Component getListCellRendererComponent(JList _list, Object _value, int _index, boolean _isSelected, boolean _cellHasFocus) {
      return super.getListCellRendererComponent(_list, getValue(_value, _index), _index, _isSelected, _cellHasFocus);
    }

    protected String getValue(Object _value, int selectedIdx) {
      if (_value == null) {
        return MdlResource.getS("Aucun");
      } else {
        int idx = selectedIdx - 1;
        if (selectedIdx < 0) {
          idx = cbConvexHull.getSelectedIndex() - 1;
        }
        if (idx >= 0) {
          int att = getSelectedContourLayer().modeleDonnees().getGeomData().getIndiceOf(GISAttributeConstants.TITRE);
          GISAttributeModel dataModel = getSelectedContourLayer().modeleDonnees().getGeomData().getDataModel(att);
          String title = (String) dataModel.getObjectValueAt(idx);
          if (StringUtils.isEmpty(title)) {
            title = MdlResource.getS("Ligne ferm�e {0}", Integer.toString(idx + 1));
          }
          return title;
        }
      }
      return _value.toString();
    }
  }

  protected void updateConvexHullCb() {
    ZCalqueLigneBrisee selectedLayer = getSelectedContourLayer();
    cbUseHole.setEnabled(selectedLayer != null);
    if (selectedLayer == null) {
      cbConvexHull.setModel(new DefaultComboBoxModel());
    } else {
      cbConvexHull.setModel(new LinearRingComboBoxModel(selectedLayer));
      cbConvexHull.setSelectedIndex(0);
    }

  }

  public ZCalqueLigneBrisee getSelectedContourLayer() {
    return cbLayerContour == null ? null : (ZCalqueLigneBrisee) cbLayerContour.getSelectedItem();
  }

  public ZCalqueLigneBrisee getSelectedContrainteLayer() {
    return cbLayerInteriorLine == null ? null : (ZCalqueLigneBrisee) cbLayerInteriorLine.getSelectedItem();
  }

  public LinearRing getSelectedConvexHull() {
    return cbConvexHull == null ? null : (LinearRing) cbConvexHull.getSelectedItem();
  }
}
