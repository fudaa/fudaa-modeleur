/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid;

import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.fudaa.meshviewer.layer.MvIsoModelDefault;

/**
 *
 * @author Frederic Deniger
 */
public class MdlIsoModelDefault extends MvIsoModelDefault {

  private final MdlElementModel model;

  public MdlIsoModelDefault(EfGridData data, MdlElementModel model) {
    super(data);
    this.model = model;
  }

  @Override
  public boolean isPainted(int idxElt) {
    return model.isPainted(idxElt);
  }
}
