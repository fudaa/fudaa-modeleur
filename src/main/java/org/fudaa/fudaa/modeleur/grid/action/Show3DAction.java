/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid.action;

import com.memoire.bu.BuIcon;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import org.fudaa.ebli.commun.BJava3DVersionTest;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.volume.ZVue3DPanel;
import org.fudaa.fudaa.meshviewer.threedim.Mv3DFactory;
import org.fudaa.fudaa.modeleur.grid.LayerGridController;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Affichage du semis en 3D si un maillage est actif.
 *
 * @author Frederic Deniger
 */
public class Show3DAction extends EbliActionSimple {

  private final LayerGridController delegate;
  JFrame f_;

  public Show3DAction(final LayerGridController delegate) {
    super(EbliLib.getS("Afficher la vue 3D"), EbliResource.EBLI.getToolIcon("3d"), "SHOW_3D");
    this.delegate = delegate;
    setEnabled(true);
    super.setDefaultToolTip(MdlResource.getS("Afficher la vue 3D du maillage associ� au semis de points"));
    super.setUnableToolTip(MdlResource.getS("Le semis doit �tre maill� et Java3D install�"));
  }

  public ZVue3DPanel getVue3D() {
    return (ZVue3DPanel) f_.getContentPane();
  }

  @Override
  public void updateStateBeforeShow() {
    setEnabled(delegate.isGridCreated());
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (!BJava3DVersionTest.isJava3DFound()) {
      delegate.getUI().error(getTitle(), BJava3DVersionTest.getHtmlInstallMessage(true), false);
      return;
    }
    if (!delegate.isGridCreated()) {
      delegate.getUI().error(getTitle(), MdlResource.getS("Le semis doit �tre maill�"), false);
      return;
    }
    if (f_ == null) {
      f_ = new JFrame(super.getTitle() + " : " + delegate.getParentLayer().getTitle());
      f_.setIconImage(((BuIcon) getIcon()).getImage());
      f_.addWindowListener(new WindowAdapter() {
        @Override
        public void windowClosed(final WindowEvent _evt) {
          f_ = null;
        }
      });
      new Mv3DFactory().afficheFrame(f_, delegate.getExportGridData(), null, delegate.getUI());
    } else {
      f_.setTitle(super.getTitle() + " : " + delegate.getParentLayer().getTitle());
      if (f_.getExtendedState() == Frame.ICONIFIED) {
        f_.setExtendedState(Frame.NORMAL);
      }
      f_.toFront();
    }
  }
}
