/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid;

import java.awt.Color;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ebli.calque.BCalqueConfigureSectionAbstract;
import org.fudaa.ebli.controle.BSelecteurCheckBox;
import org.fudaa.ebli.controle.BSelecteurInterface;
import org.fudaa.ebli.controle.BSelecteurLineModel;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.palette.BPalettePlageInterface;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Permet de configurer un calque MdlCalqueMultiPointEditable.
 *
 * @author Frederic Deniger
 */
public class MdlCalqueMultiPointEditableConfigure extends BCalqueConfigureSectionAbstract {

  public static final String ISO_LINE_COLOR_PROP = "isoLine.draw.color";
  public static final String ISO_LINE_PROP = BSelecteurLineModel.getProperty(0);
  public static final String ISO_LINE_PAINTED = "isoLine.painted";
  public static final String ISO_SURFACE_PAINTED = "isoSurface.painted";

  public MdlCalqueMultiPointEditableConfigure(final MdlCalqueMultiPointEditable _target) {
    super(_target, MdlResource.getS("Isolignes / Isosurfaces"));
  }

  @Override
  public BSelecteurInterface[] createSelecteurs() {
    final List dest = new ArrayList();
    final BSelecteurCheckBox cbIsoSurface = new BSelecteurCheckBox(ISO_SURFACE_PAINTED);
    cbIsoSurface.setTitle(MdlResource.getS("Tracer des isosurfaces"));
    final BSelecteurCheckBox cbIsoLine = new BSelecteurCheckBox(ISO_LINE_PAINTED);
    cbIsoLine.setTitle(MdlResource.getS("Tracer des isolignes"));
    final BSelecteurLineModel line = new BSelecteurLineModel(ISO_LINE_PROP);
    line.setAddColor(false);
    dest.add(cbIsoSurface);
    dest.add(cbIsoLine);
    dest.add(line);
    line.setEnabled(((MdlCalqueMultiPointEditable) target_).isTraceIsoLine());
    cbIsoLine.getCb().addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(final ItemEvent _e) {
        line.setEnabled(cbIsoLine.getCb().isSelected());
        if (cbIsoLine.getCb().isSelected()) {
          cbIsoSurface.getCb().setSelected(false);
        }
      }
    });
    cbIsoSurface.getCb().addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(final ItemEvent _e) {
        if (cbIsoSurface.getCb().isSelected()) {
          cbIsoLine.getCb().setSelected(false);
        }
      }
    });

    return (BSelecteurInterface[]) dest.toArray(new BSelecteurInterface[dest.size()]);
  }

  @Override
  public Object getProperty(final String _key) {
    if (ISO_LINE_PAINTED.equals(_key)) {
      return Boolean.valueOf(((MdlCalqueMultiPointEditable) target_).isTraceIsoLine());
    }
    if (ISO_SURFACE_PAINTED.equals(_key)) {
      return Boolean.valueOf(((MdlCalqueMultiPointEditable) target_).isTraceIsoSurface());
    }
    if (ISO_LINE_PROP.equals(_key)) {
      return ((MdlCalqueMultiPointEditable) target_).getLineModel(0);
    }
    if (ISO_LINE_COLOR_PROP.equals(_key)) {
      final BPalettePlageInterface plages = ((MdlCalqueMultiPointEditable) target_).getPaletteCouleur();
      Color c = null;
      if (plages.getNbPlages() > 0) {
        c = plages.getPlageInterface(0).getCouleur();
      }
      if (c == null) {
        return null;
      }
      for (int i = plages.getNbPlages() - 1; i > 0; i--) {
        if (!c.equals(plages.getPlageInterface(i).getCouleur())) {
          return null;
        }

      }
      return c;
    }
    return null;
  }

  @Override
  public boolean setProperty(final String _key, final Object _newProp) {
    if (ISO_LINE_PAINTED.equals(_key)) {
      ((MdlCalqueMultiPointEditable) target_).setTraceIsoLine(((Boolean) _newProp).booleanValue());
      return true;
    }
    if (ISO_SURFACE_PAINTED.equals(_key)) {
      ((MdlCalqueMultiPointEditable) target_).setTraceIsoSurface(((Boolean) _newProp).booleanValue());
      return true;
    }
    if (ISO_LINE_PROP.equals(_key)) {
      ((MdlCalqueMultiPointEditable) target_).setLineModel(0, ((TraceLigneModel) _newProp));
      return true;
    }
    if (ISO_LINE_COLOR_PROP.equals(_key)) {
      final Color c = (Color) _newProp;
      if (c == null) {
        return false;
      }
      final BPalettePlageInterface plages = ((MdlCalqueMultiPointEditable) target_).getPaletteCouleur();
      if (plages != null) {
        final BPalettePlage plage = new BPalettePlage(plages.getPlages());
        for (int i = plage.getNbPlages() - 1; i >= 0; i--) {
          plage.getPlage(i).setCouleur(c);
        }
        ((MdlCalqueMultiPointEditable) target_).setPaletteCouleurPlages(plage);

      }
    }
    return false;
  }
}
