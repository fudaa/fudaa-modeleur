/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid.action;

import com.memoire.bu.BuResource;
import java.awt.event.ActionEvent;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.modeleur.grid.LayerGridController;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Action permettant de supprimer le maillage.
 *
 * @author Frederic Deniger
 */
public class DeleteGridAction extends EbliActionSimple {

  private final LayerGridController delegate;

  public DeleteGridAction(LayerGridController delegate) {
    super(MdlResource.getS("Supprimer le maillage"), BuResource.BU.getIcon("detruire"), "DESTROY_GRID");
    this.delegate = delegate;
  }

  @Override
  public void updateStateBeforeShow() {
    setEnabled(delegate.isGridCreated());
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    if (delegate.isGridCreated()) {
      delegate.clearGrid();
    }
  }
}
