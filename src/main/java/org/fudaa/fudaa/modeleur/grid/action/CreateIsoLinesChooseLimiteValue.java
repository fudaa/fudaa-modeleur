/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid.action;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluListEditorModel;
import org.fudaa.ctulu.iterator.NumberIterator;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.palette.BPalettePlageInterface;
import org.fudaa.ebli.palette.BPalettePlageTarget;
import org.fudaa.fudaa.modeleur.grid.LayerGridController;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Panneau permettant de choisir les valeurs pour la construction des iso-lignes.
 * @author Frederic Deniger
 */
public class CreateIsoLinesChooseLimiteValue extends CtuluListEditorModel {

  final DecimalFormat fmt_ = CtuluLib.getDecimalFormat();
  EfData bathy;
  LayerGridController support_;

  public CreateIsoLinesChooseLimiteValue(final LayerGridController _support) {
    fmt_.setMaximumFractionDigits(1);
    support_ = _support;
    try {
      bathy = support_.getExportGridData().getData(H2dVariableType.BATHYMETRIE, 0);
    } catch (IOException ex) {
      Logger.getLogger(CreateIsoLinesChooseLimiteValue.class.getName()).log(Level.SEVERE, null, ex);
    }
    List defaultValuesFromPalette = getDefaultValuesFromPalette();
    if (defaultValuesFromPalette != null) {
      setData(defaultValuesFromPalette);
    } else {
      updateOrCreateValues();
    }
  }

  @Override
  public String getColumnName(final int _column) {
    return MdlResource.getS("Valeurs");
  }

  private List getDefaultValuesFromPalette() {
    if (support_.getParentLayer() instanceof BPalettePlageTarget) {
      final BPalettePlageTarget cq = (BPalettePlageTarget) support_.getParentLayer();
      final BPalettePlageInterface plage = cq.getPaletteCouleur();
      final int nb = plage.getNbPlages();
      final List res = new ArrayList(nb);
      int inc = 1;
      // pour eviter de proposer trop de valeurs
      if (nb > 20) {
        inc = nb / 20;
      }
      addVal(plage, res, plage.getPlageInterface(0).getMin());
      for (int i = 0; i < nb; i += inc) {
        addVal(plage, res, plage.getPlageInterface(i).getMax());
      }
      return res;
    }
    return null;
  }

  private void addVal(final BPalettePlageInterface _plage, final List _res, final double _val) {
    if (_plage.getFormatter() == null) {
      _res.add(fmt_.format(_val));
    } else {
      _res.add(_plage.getFormatter().format(_val));
    }
  }

  private void initFor(final EfData _data, final List _l) {
    if (_data == null || _l == null) {
      return;
    }
    _l.clear();
    final NumberIterator it = new NumberIterator();
    it.init(_data.getMin(), _data.getMax(), 10);

    while (it.hasNext()) {
      _l.add(CtuluLib.DEFAULT_NUMBER_FORMAT.format(it.currentValue()));
      it.nextMajor();
    }
  }

  private void updateOrCreateValues() {
    final List newValue = new ArrayList();
    initFor(bathy, newValue);
    if (newValue.isEmpty()) {
      newValue.add(CtuluLibString.ZERO);
      newValue.add(CtuluLibString.UN);
      newValue.add(CtuluLibString.DEUX);
    }
    setData(newValue);
  }

  @Override
  public Object createNewObject() {
    final int val = getRowCount();
    if (val >= 2) {
      final double v0 = Double.parseDouble(((String) getValueAt(getRowCount() - 2)));
      final double v1 = Double.parseDouble(((String) getValueAt(getRowCount() - 1)));
      return fmt_.format(v1 + v1 - v0);
    }
    if (bathy != null) {
      if (val == 1) {
        final double v1 = Double.parseDouble(((String) getValueAt(getRowCount() - 1)));
        final double inc = Math.abs((bathy.getMax() - v1) / 10);
        return fmt_.format(v1 + inc);
      }
      return fmt_.format(bathy.getMin());
    }
    return CtuluLibString.ZERO;
  }
}
