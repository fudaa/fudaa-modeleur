/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid;

import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 *
 * @author Frederic Deniger
 */
public enum TriangulateurEnum {

  INTERNE(MdlResource.getS("Interne")),
  TRIANGLE(MdlResource.getS("Triangle"));
  private final String label;

  private TriangulateurEnum(String label) {
    this.label = label;
  }

  @Override
  public String toString() {
    return label;
  }
}
