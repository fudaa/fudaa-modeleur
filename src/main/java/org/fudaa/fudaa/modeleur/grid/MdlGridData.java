/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid;

import java.io.IOException;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 *
 * @author Frederic Deniger
 */
public class MdlGridData implements EfGridData {

  private final MdlElementModel model;
  private final int varIdx;

  public MdlGridData(MdlElementModel model, int bathyIdx) {
    this.model = model;
    this.varIdx = bathyIdx;
  }

  @Override
  public boolean isElementVar(CtuluVariable _idxVar) {
    return false;
  }

  @Override
  public boolean isDefined(CtuluVariable _var) {
    return H2dVariableType.BATHYMETRIE.equals(_var);
  }

  @Override
  public EfData getData(CtuluVariable _o, int _timeIdx) throws IOException {
    double[] data = new double[getGrid().getPtsNb()];
    for (int i = 0; i < data.length; i++) {
      data[i] = model.getValueFromGrid(varIdx, i);
    }
    return new EfDataNode(data);
  }

  @Override
  public double getData(CtuluVariable _o, int _timeIdx, int _idxObjet) throws IOException {
    return model.getValueFromGrid(varIdx, _idxObjet);
  }

  @Override
  public EfGridInterface getGrid() {
    return model.getGrid();
  }
}
