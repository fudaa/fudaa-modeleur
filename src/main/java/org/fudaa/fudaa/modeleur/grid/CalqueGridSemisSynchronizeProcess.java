/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid;

import java.util.TreeMap;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.gis.GisZoneCollectionAsListPointAdapter;
import org.fudaa.ctulu.gis.comparator.CoordinateComparator;
import org.fudaa.ctulu.interpolation.bilinear.InterpolationBilinearSupportSorted;
import org.fudaa.dodico.ef.AllFrontierIteratorInterface;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.locationtech.jts.geom.Coordinate;

import gnu.trove.TIntArrayList;
import gnu.trove.TIntHashSet;

/**
 * Permet d'avoir une relation entre l'indice du noeud dans le maillage et celui dans le semis. S'il n'y a pas de concordance direct, une
 * interpolation est effectu�e et le "quadrant" est renvoy� (InterpolationBilinearSupportSorted).
 *
 * @see org.fudaa.ctulu.interpolation.bilinear.InterpolationBilinearSupportSorted
 *
 * @author Frederic Deniger
 */
public class CalqueGridSemisSynchronizeProcess {

  private final GisZoneCollectionAsListPointAdapter zone;
  private double eps = 1e-3;

  public static class Result {

    //un tableau dont la taille est grid.nbPoint.Pour chaque point du maillage donne l'indice du noeud dans le mod�les multipoint.
    int[][] relations;
    /**
     * contient la liste des points du semis qui sera support� par un point du maillage.
     */
    CtuluListSelection initPointPaintedByMesh;

    public Result(int[][] relations, CtuluListSelection initPointPaintedByMesh) {
      this.relations = relations;
      this.initPointPaintedByMesh = initPointPaintedByMesh;
    }

    /**
     * un tableau dont la taille est grid.nbPoint.Pour chaque point du maillage donne l'indice du noeud dans le mod�les multipoint.
     *
     * @return
     */
    public int[][] getRelations() {
      return relations;
    }

    /**
     *
     * @return les points de la zone support� par un unique point du maillage.
     */
    public CtuluListSelection getInitPointPaintedByMesh() {
      return initPointPaintedByMesh;
    }
  }

  public CalqueGridSemisSynchronizeProcess(GisZoneCollectionAsListPointAdapter triangleNodeAdapter) {
    this.zone = triangleNodeAdapter;
  }

  public void setEps(double eps) {
    this.eps = eps;
  }

  /**
   *
   *
   * @param grid
   * @return un tableau dont la taille est grid.nbPoint.Pour chaque point du maillage donne l'indice du noeud dans le mod�les multipoint.
   */
  public Result getRelations(EfGridInterface grid, ProgressionInterface prog) {
    if (grid == null) {
      return new Result(new int[0][0], new CtuluListSelection());
    }
    int[][] res = new int[grid.getPtsNb()][];

    ProgressionUpdater updater = new ProgressionUpdater(prog);
    TreeMap<Coordinate, TIntArrayList> index = new TreeMap<Coordinate, TIntArrayList>(new CoordinateComparator(eps));
    updater.majProgessionStateOnly(MdlResource.getS("Synchronisation des indices"));
    updater.setValue(10, zone.getPtsNb());
    //indique les points de la zone qui seront repr�sent�s par un unique point du maillage.
    CtuluListSelection initPointPaintedByMesh = new CtuluListSelection(zone.getPtsNb());
    for (int i = 0; i < zone.getPtsNb(); i++) {
      final Coordinate coordinate = new Coordinate(zone.getPtX(i), zone.getPtY(i));
      TIntArrayList ptIdx = index.get(coordinate);
      if (ptIdx == null) {
        ptIdx = new TIntArrayList();
        index.put(coordinate, ptIdx);
      }
      ptIdx.add(i);
      updater.majAvancement();
    }
    updater.setValue(10, grid.getPtsNb());
    boolean rescanElt = false;
    int nbToRescan = 0;
    for (int i = 0; i < grid.getPtsNb(); i++) {
      Coordinate gridCoor = new Coordinate(grid.getPtX(i), grid.getPtY(i));
      TIntArrayList idxs = index.get(gridCoor);
      if (idxs != null) {
        for (int j = idxs.size() - 1; j >= 0; j--) {
          initPointPaintedByMesh.add(idxs.get(j));
        }
        //le premier point de la collection est pris.
        res[i] = new int[]{idxs.get(0)};
      } else {
        nbToRescan++;
        rescanElt = true;
      }
      updater.majAvancement();
    }
    if (rescanElt) {
      //pour les points, on recherche les points adjacents avec des valeurs connus
      final int[][] ptVoisins = EfLib.getConnexionFor(grid, prog, new CtuluAnalyze());
      for (int globalIdx = 0; globalIdx < grid.getPtsNb(); globalIdx++) {
        if (res[globalIdx] == null) {
          int[] voisins = ptVoisins[globalIdx];
          TIntArrayList knownVoisin = null;
          for (int i = 0; i < voisins.length; i++) {
            int idxVoisin = voisins[i];
            if (res[idxVoisin] != null && res[idxVoisin].length == 1) {
              if (knownVoisin == null) {
                knownVoisin = new TIntArrayList();
              }
              knownVoisin.add(res[idxVoisin][0]);
            }
          }
          if (knownVoisin != null) {
            if (knownVoisin.size() == 1) {//pour �viter que le point soit consid�r� comme un point exact.
              knownVoisin.add(-1);
            }
            res[globalIdx] = knownVoisin.toNativeArray();
            nbToRescan--;
          }
        }
      }
      //pour les points fronti�res isol�s, on recommence � chercher les points � utiliser.
      AllFrontierIteratorInterface allFrontierIterator = grid.getFrontiers().getAllFrontierIterator();
      while (allFrontierIterator.hasNext()) {
        int globalIdx = allFrontierIterator.next();
        if (res[globalIdx] == null) {
          int[] voisins = ptVoisins[globalIdx];
          TIntHashSet knownVoisin = null;
          for (int i = 0; i < voisins.length; i++) {
            int idxVoisin = voisins[i];
            if (res[idxVoisin] != null) {
              if (knownVoisin == null) {
                knownVoisin = new TIntHashSet();
              }
              knownVoisin.addAll(res[idxVoisin]);
            }
          }
          if (knownVoisin != null) {
            if (knownVoisin.size() == 1) {//pour �viter que le point soit consid�r� comme un point exact.
              knownVoisin.add(-1);
            }
            res[globalIdx] = knownVoisin.toArray();
            nbToRescan--;
          }
        }
      }
    }
    updater.setValue(10, nbToRescan);
  
    if (rescanElt && nbToRescan > 0) {
        updater.majProgessionStateOnly(MdlResource.getS("Synchronisation des points hors semis"));
      final InterpolationBilinearSupportSorted src = InterpolationBilinearSupportSorted.buildSortedSrc(zone, prog);
      for (int i = 0; i < grid.getPtsNb(); i++) {
        if (res[i] == null) {
          res[i] = new int[4];
          src.getQuadrantIdx(grid.getPtX(i), grid.getPtY(i), res[i]);
          updater.majAvancement();
        }
      }
    }
    //on va affecter a des points, les m�me r�sultats que ceux de l'�l�ments.

    return new Result(res, initPointPaintedByMesh);

  }
}
