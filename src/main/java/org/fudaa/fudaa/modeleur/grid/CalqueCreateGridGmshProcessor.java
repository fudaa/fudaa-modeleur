/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGridSourceDefaut;
import org.fudaa.dodico.ef.io.gmsh.GmshGeoWriter;
import org.fudaa.dodico.ef.io.supertab.SuperTabReader;
import org.fudaa.dodico.ef.io.triangle.TriangulationPolyDataNodeDefault;
import org.fudaa.dodico.objet.CExec;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.calque.ZModeleMultiPoint;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.locationtech.jts.geom.LinearRing;

/**
 * Pas complet: laisser si repris mais ne fonctionne pas comme attendu. Le chemin en dur vers gmsh est laiss� ( voir le TODO).
 *
 * @author Frederic Deniger
 */
public class CalqueCreateGridGmshProcessor extends AbstractCalqueCreateGridProcessor {
  
  public CalqueCreateGridGmshProcessor(ZModeleMultiPoint points, CtuluUI ui, ZCalqueLigneBrisee contour, LinearRing convexHull) {
    super(points, ui, contour, convexHull);
  }
  
  @Override
  public EfGridInterface process(ProgressionInterface prog) {
    try {
      stop = false;
      File tmpDir = CtuluLibFile.createTempDir();
      String fileName = null;
      CExec exe = new CExec();
      exe.setExecDirectory(tmpDir);
      
      fileName = "in.geo";
      TriangulationPolyDataNodeDefault data = createPolyData(prog);
      File polyFile = new File(tmpDir, fileName);
      GmshGeoWriter writer = new GmshGeoWriter();
      writer.setEps(eps);
      CtuluIOOperationSynthese write = writer.write(data, polyFile, prog);
      if (write.containsError()) {
        ui.manageErrorOperationAndIsFatal(write);
        return null;
      }
      final String outFileName = "out.unv";
      //TODO attention laiss� en l'�tat
      exe.setCommand(new String[]{"C:\\devel\\trianglulation\\gmsh-2.6.1-Windows\\gmsh-2.6.1-Windows\\gmsh.exe", fileName, "-2", "-algo", "del2d", "-format", "unv", "-o", outFileName});
//      exe.setCommand(new String[]{"C:\\devel\\trianglulation\\gmsh-2.6.1-Windows\\gmsh-2.6.1-Windows\\gmsh.exe", fileName, "-2",  "-format", "unv", "-o", outFileName});
      if (prog != null) {
        prog.setDesc(MdlResource.getS("Ex�cution du mailleur"));
      }
      exe.setListener(this);
      exe.exec();
      if (stop) {
        return null;
      }
      File targetNode = new File(tmpDir, outFileName);
      if (!targetNode.exists()) {
        ui.error(MdlResource.getS("Le maillage a �chou�"));
        return null;
      }
      SuperTabReader reader = new SuperTabReader();
      CtuluIOOperationSynthese readGrid = reader.read(targetNode, prog);
      if (readGrid.getSource() == null) {
        ui.manageAnalyzeAndIsFatal(readGrid.getAnalyze());
      }
      CtuluLibFile.deleteDir(tmpDir);
      final EfGridSourceDefaut result = (EfGridSourceDefaut) readGrid.getSource();
      return result == null ? null : result.getGrid();
    } catch (IOException ex) {
      ui.error(MdlResource.getS("Impossible de cr�er un dossier temporaire"));
      Logger.getLogger(CalqueCreateGridGmshProcessor.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;
  }
}
