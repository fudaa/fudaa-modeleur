/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.process.PointsMapping;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EFGridArrayZ;
import org.fudaa.dodico.ef.io.triangle.TriangleEleNodeGridCreator;
import org.fudaa.dodico.ef.io.triangle.TriangleNodeSupportAdapter;
import org.fudaa.dodico.ef.io.triangle.TriangleNodeWriter;
import org.fudaa.dodico.ef.io.triangle.TrianglePolyWriter;
import org.fudaa.dodico.ef.io.triangle.TriangulationPolyDataNodeDefault;
import org.fudaa.dodico.objet.CExec;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.calque.ZModeleMultiPoint;
import org.fudaa.fudaa.modeleur.MdlPreferences;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.locationtech.jts.geom.LinearRing;

/**
 *
 * @author Frederic Deniger
 */
public class CalqueCreateGridTriangleProcessor extends AbstractCalqueCreateGridProcessor {

  public static String getTriangleExe() {
    return MdlPreferences.MDL.getStringProperty("grid.triangle.path");
  }

  public static boolean isTriangleExeConfigured() {
    return getTriangleExe() != null && new File(getTriangleExe()).exists();
  }

  public CalqueCreateGridTriangleProcessor(ZModeleMultiPoint points, CtuluUI ui, ZCalqueLigneBrisee contour, LinearRing convexHull) {
    super(points, ui, contour, convexHull);
  }

  @Override
  public EfGridInterface process(ProgressionInterface prog) {
    if (!isTriangleExeConfigured()) {
      return null;
    }
    try {
      stop = false;
      File tmpDir = CtuluLibFile.createTempDir();
      String fileName = null;
      CExec exe = new CExec();
      exe.setExecDirectory(tmpDir);
      if (isNoLinesSelected()) {
        fileName = "in.node";
        File nodeFile = new File(tmpDir, fileName);
        TriangleNodeWriter writer = new TriangleNodeWriter();
        //construction des points filtr�s.
        PointsMapping points = createMapping(prog);
        CtuluIOOperationSynthese write = writer.write(new TriangleNodeSupportAdapter(points), nodeFile, prog);
        if (write.containsError()) {
          ui.manageErrorOperationAndIsFatal(write);
          return null;
        }
        exe.setCommand(new String[]{getTriangleExe(), fileName});
      } else {
        fileName = "in.poly";
        TriangulationPolyDataNodeDefault data = createPolyData(prog);
        File polyFile = new File(tmpDir, fileName);
        TrianglePolyWriter writer = new TrianglePolyWriter();
        writer.setEps(super.eps);
        CtuluIOOperationSynthese write = writer.write(data, polyFile, prog);
        if (write.containsError()) {
          ui.manageErrorOperationAndIsFatal(write);
          return null;
        }
        //https://www.cs.cmu.edu/~quake/triangle.switch.html
        String options = addPointOnFrontier ? "-D" : "-DYY";
        if (convexHull == null) {
          options = addPointOnFrontier ? "-cD" : "-cDYY";
        }
        exe.setCommand(new String[]{getTriangleExe(), options, fileName});
      }
      if (prog != null) {
        prog.setDesc(MdlResource.getS("Ex�cution du mailleur"));
      }



      exe.setListener(this);
      exe.exec();
      if (stop) {
        return null;
      }
      File targetNode = new File(tmpDir, "in.1.node");
      File targetEle = new File(tmpDir, "in.1.ele");
      if (!targetEle.exists() || !targetNode.exists()) {
        ui.error(MdlResource.getS("Le maillage a �chou�"));
        return null;
      }
      TriangleEleNodeGridCreator creator = new TriangleEleNodeGridCreator();
      creator.setProgression(prog);
      creator.setUseFirstAttributesAsZ(false);
      CtuluIOResult<EFGridArrayZ> readGrid = creator.readGrid(targetNode, targetEle);
      if (readGrid.getSource() == null) {
        ui.manageAnalyzeAndIsFatal(readGrid.getAnalyze());
      }
      CtuluLibFile.deleteDir(tmpDir);
      return readGrid.getSource();
    } catch (IOException ex) {
      ui.error(MdlResource.getS("Impossible de cr�er un dossier temporaire"));
      Logger.getLogger(CalqueCreateGridTriangleProcessor.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;
  }
}
