/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid;

import java.util.Arrays;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.gis.GisZoneCollectionAsListPointAdapter;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.fudaa.meshviewer.model.MvElementModelDefault;

/**
 *
 * @author Frederic Deniger
 */
public class MdlElementModel extends MvElementModelDefault {

  public MdlElementModel(EfGridInterface _g) {
    super(_g);
  }
  /**
   * Pour chaque point du maillage donne l'indice du point dans le semis correspondant.
   */
  int[][] altiIdxByGridPtIdx;
  /**
   * est initialis� si les utilisateurs ont demand� de ne pas dessiner certains �l�ments.
   */
  boolean[] userEltPainted;
  /**
   * une repr�sentation des points du semis sous forme de liste.
   */
  private GisZoneCollectionAsListPointAdapter pointsAdapter;
  /**
   * Contient la liste des indices des points (issus de pointsAdapter) qui seront dessin�s par les iso c'est � dire les points du modele initial qui
   * correspondantent pr�cisement � un point du maillage
   */
  CtuluListSelection gisNodePaintedInIso;

  public void setAltiIdxByGridPtIdx(int[][] relations) {
    this.altiIdxByGridPtIdx = relations;
  }

  /**
   *
   * @param initPointPaintedByMesh les points de la zone de points correspondant � un unique point du maillage.
   */
  public void setInitPointPaintedByMesh(CtuluListSelection initPointPaintedByMesh) {
    this.gisNodePaintedInIso = initPointPaintedByMesh;
  }

  /**
   * pour des raisons de perf permet de savoir si un point du PointsAdapter correspond � un point du
   *
   * @param idxInCollection
   * @return
   */
  public boolean isNodePaintedByIso(int idxInCollection) {
    return gisNodePaintedInIso != null && gisNodePaintedInIso.isSelected(idxInCollection);
  }

  public void setPainted(int[] idxElt, boolean painted) {
    if (idxElt == null) {
      return;
    }
    if (userEltPainted == null) {
      userEltPainted = new boolean[getGrid().getEltNb()];
      Arrays.fill(userEltPainted, true);
    }
    for (int i = 0; i < idxElt.length; i++) {
      userEltPainted[idxElt[i]] = painted;
    }
  }

  /**
   *
   * @param idxElt
   * @return false si un des points de l'�l�ment ne peut pas �tre paint.
   */
  @Override
  public boolean isPainted(int idxElt) {
    if (userEltPainted != null && userEltPainted[idxElt] == false) {
      return false;
    }
    EfElement element = getGrid().getElement(idxElt);
    int ptNb = element.getPtNb();
    for (int i = 0; i < ptNb; i++) {
      if (!isPointPainted(element.getPtIndex(i))) {
        return false;
      }
    }
    return true;
  }

  public boolean isPointPainted(int ptIdxInGrid) {
    final int[] idxs = altiIdxByGridPtIdx[ptIdxInGrid];
    if (idxs == null) {
      return false;
    }
    if (idxs.length > 1) {
      for (int i = 0; i < idxs.length; i++) {
        if (idxs[i] >= 0) {
          return true;
        }
      }
    }
    return true;
  }

  public double getValueFromGrid(int idxAttribute, int ptIdxInGrid) {
    final int[] idxs = altiIdxByGridPtIdx[ptIdxInGrid];
    if (idxs == null) {
      return 0;
    }
    if (idxs.length == 1) {
      return pointsAdapter.getValue(idxs[0], idxAttribute);
    }
    //on interpole:
    double num = 0;
    double den = 0;
    final double ptX = getGrid().getPtX(ptIdxInGrid);
    final double ptY = getGrid().getPtY(ptIdxInGrid);
    for (int j = idxs.length - 1; j >= 0; j--) {
      // ptIdx est l'indice du point de reference
      final int ptIdx = idxs[j];
      if (ptIdx >= 0) {
        final double xref = pointsAdapter.getPtX(ptIdx);
        final double yref = pointsAdapter.getPtY(ptIdx);
        final double dist = CtuluLibGeometrie.getD2(xref, yref, ptX, ptY);
        num += pointsAdapter.getValue(ptIdx, idxAttribute) / dist;
        den += 1d / dist;
      }
    }
    return num / den;
  }

  public void setPointsAdapter(GisZoneCollectionAsListPointAdapter triangleNodeAdapter) {
    this.pointsAdapter = triangleNodeAdapter;
  }

  public GisZoneCollectionAsListPointAdapter getPointsAdapter() {
    return pointsAdapter;
  }

  public CtuluListSelection getGisNodePaintedInIso() {
    return gisNodePaintedInIso;
  }
  

  /**
   * Attention on ne fait pas de copie defensive du tableau pour des raisons de performance.
   *
   * @return la correspondance entre les indices du maillage et ceux du semis de points.
   */
  public int[][] getCorrespondance() {
    return altiIdxByGridPtIdx;
  }

  /**
   * Attention on ne fait pas de copie defensive du tableau pour des raisons de performance.
   *
   * @return le tableau des elements � peindre ou non
   */
  public boolean[] getUserEltPainted() {
    return userEltPainted;
  }

  public void setUserVisible(boolean[] userVisible) {
    this.userEltPainted = userVisible;
  }
}
