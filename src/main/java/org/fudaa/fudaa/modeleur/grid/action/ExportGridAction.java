/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid.action;

import com.memoire.bu.BuResource;
import java.awt.event.ActionEvent;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.modeleur.grid.LayerGridController;
import org.fudaa.fudaa.modeleur.grid.MdlGridExportFactory;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Action permettant d'exporter le maillage.
 *
 * @author Frederic Deniger
 */
public class ExportGridAction extends EbliActionSimple {

  private final LayerGridController delegate;

  public ExportGridAction(LayerGridController delegate) {
    super(MdlResource.getS("Exporter le maillage"), BuResource.BU.getIcon("exporter"), "EXPORT_GRID");
    this.delegate = delegate;
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    MdlGridExportFactory.startExport(delegate);
  }
}
