/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid;

import javax.swing.DefaultListModel;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.meshviewer.export.MvExportFactory;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 * Classe utilitaire pour l'export au format maillage des donn�es.
 *
 * @author Frederic Deniger
 */
public class MdlGridExportFactory {

  public static MvExportFactory create(LayerGridController gridDelegate) {
    FSigEditor editor = (FSigEditor) gridDelegate.getParentLayer().getEditor();
    DefaultListModel varModel = new DefaultListModel();
    varModel.addElement(H2dVariableType.BATHYMETRIE);
    MvExportFactory res = new MvExportFactory(editor.getUi(), varModel, null,
            gridDelegate.getExportGridData(), null, null, /** BM : 06/09/2018 : Fix pour compil, valeur a verifier */0);
    return res;
  }

  public static void startExport(LayerGridController gridDelegate) {
    final MvExportFactory exporter = create(gridDelegate);
    MvExportFactory.startExport(exporter, exporter.getUi(), null);
  }
}
