/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISVisitorGeometryCollector;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GisZoneCollectionAsListPointAdapter;
import org.fudaa.ctulu.gis.process.GisZoneCollectionPointDoublonRemover;
import org.fudaa.ctulu.gis.process.PointsMapping;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.io.triangle.TriangulationPolyDataNodeDefault;
import org.fudaa.dodico.objet.CExecListener;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.calque.ZModeleMultiPoint;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.Point;

// FIXME BM : CommentÚ
//import com.vividsolutions.jts.algorithm.SIRtreePointInRing;

/**
 *
 * @author Frederic Deniger
 */
public abstract class AbstractCalqueCreateGridProcessor implements CExecListener {

  protected final CtuluUI ui;
  protected final ZCalqueLigneBrisee contour;
  protected ZCalqueLigneBrisee contraintLayer;
  protected final LinearRing convexHull;
  protected double eps = 1e-3;
  private final GisZoneCollectionAsListPointAdapter pointsAdapter;
  private PointsMapping pointsMapping;
  private boolean useHole;
  protected boolean addPointOnFrontier;

  public AbstractCalqueCreateGridProcessor(ZModeleMultiPoint points, CtuluUI ui, ZCalqueLigneBrisee contour, LinearRing convexHull) {
    this.ui = ui;
    this.convexHull = convexHull;
    this.contour = contour;
    this.pointsAdapter = new GisZoneCollectionAsListPointAdapter(points.getGeomData());
  }

  public void setUseHole(boolean useHole) {
    this.useHole = useHole;
  }

  public void setAddPointOnFrontier(boolean addPointOnFrontier) {
    this.addPointOnFrontier = addPointOnFrontier;
  }

  public PointsMapping getPointsMapping() {
    return pointsMapping;
  }

  public GisZoneCollectionAsListPointAdapter getPointsAdapter() {
    return pointsAdapter;
  }

  public void setEps(double epsForDoublon) {
    this.eps = epsForDoublon;
  }

  @Override
  public final void setProcess(Process _p) {
    this.p = _p;
  }
  volatile boolean stop;

  public final void stop() {
    stop = true;
    if (p != null) {
      p.destroy();
    }
  }
  Process p;

  public abstract EfGridInterface process(ProgressionInterface prog);

  // TODO BM : InutilisÚ
//  final boolean isIn(SIRtreePointInRing in, Coordinate c) {
//    if (in == null) {
//      return true;
//    }
//    return in.isInside(c);
//  }

  // FIXME BM : CommentÚ
  final boolean isNotIn(List<LinearRing> inRings, Point p) {
    for (LinearRing sIRtreePointInRing : inRings) {
      if (sIRtreePointInRing.contains(p)) {
        return false;
      }
    }
    return true;
  }

  protected PointsMapping createMapping(ProgressionInterface prog) {
    pointsMapping = null;
    GisZoneCollectionPointDoublonRemover remover = new GisZoneCollectionPointDoublonRemover(pointsAdapter, eps);
    pointsMapping = remover.createFiltered(prog);

    return pointsMapping;
  }

  private boolean isNoPolygoneSelected() {
    return contour == null || (contour.modeleDonnees().getNbPolygone() == 0) || (convexHull == null && !useHole);
  }

  private boolean isNoConstraintLineSelected() {
    return contraintLayer == null || (contraintLayer.modeleDonnees().getNombre() == 0);
  }

  protected boolean isNoLinesSelected() {
    return isNoPolygoneSelected() && isNoConstraintLineSelected();
  }

  public boolean isPolygoneSelected() {
    return !isNoPolygoneSelected();
  }

  public final TriangulationPolyDataNodeDefault createPolyData(ProgressionInterface prog) {

    TriangulationPolyDataNodeDefault data = new TriangulationPolyDataNodeDefault();
    List<LineString> lines = new ArrayList<LineString>();
    List<Boolean> holes = new ArrayList<Boolean>();
    if (contour != null) {
      if (convexHull != null) {
        lines.add(convexHull);
        holes.add(Boolean.FALSE);
      }
      if (useHole) {
        GISVisitorGeometryCollector collector = new GISVisitorGeometryCollector(GISLib.MASK_POLYGONE);
        contour.modeleDonnees().getGeomData().accept(collector);
        List<Geometry> geometries = collector.getGeometries();
        LinearRing[] linearRings = (LinearRing[]) geometries.toArray(new LinearRing[geometries.size()]);
        for (int i = 0; i < linearRings.length; i++) {
          LinearRing linearRing = linearRings[i];
          if (linearRing != convexHull) {
            lines.add(linearRing);
            holes.add(Boolean.TRUE);
          }
        }
      }
    }
    if (contraintLayer != null) {
      GISZoneCollectionLigneBrisee linesCollection = contraintLayer.modeleDonnees().getGeomData();
      for (int i = 0; i < linesCollection.getNumGeometries(); i++) {
        lines.add((LineString) linesCollection.getGeometry(i));
        holes.add(Boolean.FALSE);
      }
    }
    LineString[] lineStrings = (LineString[]) lines.toArray(new LineString[lines.size()]);
    Boolean[] holesInBoolean = (Boolean[]) holes.toArray(new Boolean[holes.size()]);
    data.setLineString(lineStrings);
    data.setHoles(ArrayUtils.toPrimitive(holesInBoolean));
    PointsMapping filteredPoints = createMapping(prog);
    data.setNodeData(filteredPoints);
    data.setUseZAsAttribute(false);
    return data;
  }

  public void setContraintLines(ZCalqueLigneBrisee selectedContrainteLayer) {
    this.contraintLayer = selectedContrainteLayer;
  }
}
