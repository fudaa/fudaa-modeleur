/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid.action;

import com.memoire.bu.BuTextField;
import gnu.trove.TDoubleHashSet;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISDataModelFilterAdapter;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluListEditorPanel;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridDataInterpolationValuesAdapter;
import org.fudaa.dodico.ef.operation.EfIsoActivity;
import org.fudaa.dodico.ef.operation.EfIsoActivitySearcher;
import org.fudaa.dodico.ef.operation.EfIsoResultDefault;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.modeleur.grid.LayerGridController;
import org.fudaa.fudaa.modeleur.grid.MdlGridData;
import org.fudaa.fudaa.modeleur.layer.MdlLayerFactory;
import org.fudaa.fudaa.modeleur.layer.MdlLayerInterface;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.fudaa.fudaa.sig.layer.FSigEditor;
import org.fudaa.fudaa.sig.layer.FSigLayerLineEditable;

/**
 * Permet de cr�er les isoligne si le maillage assosic� � un semis de point est cr��.
 *
 * @author Frederic Deniger
 */
public class CreateIsoLinesAction extends EbliActionSimple {

  private final LayerGridController delegate;
  double precision = 1e-3;

  public CreateIsoLinesAction(LayerGridController delegate) {
    super(MdlResource.getS("Cr�er les lignes de niveaux"), null, "ISOLINE_CREATE");
    setDefaultToolTip(MdlResource.getS("Permet de cr�er un nouveau calque \"Ligne de niveau\" issues des valeurs de ce calque"));
    setUnableToolTip(MdlResource.getS("Si le semis est maill�, Permet de cr�er un nouveau calque \"Ligne de niveau\" issues des valeurs de ce calque"));
    this.delegate = delegate;
  }

  @Override
  public void updateStateBeforeShow() {
    setEnabled(delegate.isGridCreated());
  }

  /**
   * Ajoute le calque contenant les isolignes demand�es.
   *
   * @param res le r�sultat de la recherche d'isoligne
   */
  private void addIsoLineLayer(EfIsoResultDefault res) {
    FSigEditor editor = (FSigEditor) delegate.getParentLayer().getEditor();
    if (res.getNumGeometries() > 0) {

      FSigLayerLineEditable cq = (FSigLayerLineEditable) MdlLayerFactory.getInstance().createLayer(MdlLayerFactory.LAYER2D_LEVEL, editor);
      res.setAtt(cq.modeleDonnees().getGeomData().getAttributeIsZ());
      final BGroupeCalque gcLayer = (BGroupeCalque) delegate.getParentLayer().getParent();
      cq.setName(BGroupeCalque.findUniqueChildName(gcLayer, ((MdlLayerInterface) cq).getExtName()));
      cq.setTitle(gcLayer.findUniqueChildTitle(cq.getTitle()));
      GISDataModelFilterAdapter adapted = GISDataModelFilterAdapter.buildAdapter(res, cq.modeleDonnees().getGeomData().getAttributes());
      cq.modeleDonnees().getGeomData().addAll(adapted, null, false);
      gcLayer.add(cq);
    }

  }

  private static class ChooseVariablePanel extends CtuluDialogPanel {

    private final BuTextField tfPrecision;
    private final CreateIsoLinesChooseLimiteValue limits;

    public ChooseVariablePanel(CreateIsoLinesChooseLimiteValue limits) {
      super(true);
      setLayout(new BorderLayout(10, 0));
      this.limits = limits;
      JPanel prec = new JPanel(new FlowLayout(FlowLayout.LEADING));
      tfPrecision = addLabelDoubleText(prec, MdlResource.getS("Pr�cision"));
      tfPrecision.setValue(Double.valueOf(0.01));
      add(new CtuluListEditorPanel(limits));
      add(prec, BorderLayout.SOUTH);
    }

    double getPrecision() {
      return ((Double) tfPrecision.getValue()).doubleValue();
    }

    @Override
    public boolean isDataValid() {
      int count = limits.getRowCount();
      if (count == 0) {
        setErrorText(MdlResource.getS("D�finir au moins une valeur"));
        return false;
      }
      setErrorText(null);
      return true;
    }
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    if (!delegate.isGridCreated()) {
      delegate.getUI().warn(getTitle(), MdlResource.getS("Pour cr�er des isolignes, le maillage correspondant doit �tre cr��"), false);
      return;
    }
    final CreateIsoLinesChooseLimiteValue values = new CreateIsoLinesChooseLimiteValue(delegate);
    //le pn permet de choisir les valeurs � utiliser pour les isolignes.
    //par defaut, les valeurs de la palette sont utilis�es.
    ChooseVariablePanel pn = new ChooseVariablePanel(values);
    final String title = MdlResource.getS("Cr�ation des lignes de niveau");
    if (pn.afficheModaleOk(delegate.getUI().getParentComponent(), title)) {
      CtuluTaskDelegate createTask = delegate.getUI().createTask(title);
      final ProgressionInterface mainStateReceiver = createTask.getMainStateReceiver();
      createTask.start(new Runnable() {
        @Override
        public void run() {
          searchIsoLines(mainStateReceiver, values);
        }
      });
    }
  }

  /**
   * D�termination des isolignes
   *
   * @param progression
   * @param values
   */
  private void searchIsoLines(ProgressionInterface progression, CreateIsoLinesChooseLimiteValue values) {
    CtuluAnalyze log = new CtuluAnalyze();
    final TDoubleHashSet set = new TDoubleHashSet(values.getRowCount());
    for (int i = 0; i < values.getRowCount(); i++) {
      set.add(Double.parseDouble((String) values.getValueAt(i)));
    }
    final double[] isoValues = set.toArray();
    Arrays.sort(isoValues);
    final EfIsoResultDefault res = new EfIsoResultDefault();
    MdlGridData gridData = delegate.getExportGridData();
    final EfIsoActivity act = new EfIsoActivity(gridData.getGrid(), null);
    act.setEpsForValues(precision);
    final int nbValues = isoValues.length;
    try {
      //cr�ation des classes permettant de d�terminer les isolignes:
      EfData data = gridData.getData(H2dVariableType.BATHYMETRIE, 0);
      final EfGridDataInterpolationValuesAdapter interpolatedValues = new EfGridDataInterpolationValuesAdapter(gridData, 0);
      final EfIsoActivitySearcher searcher = act.search(H2dVariableType.BATHYMETRIE, data, progression, log, interpolatedValues);
      if (searcher == null) {
        return;
      }
      //recherche pour chaque valeur
      //TODO PERF: multi-threade ce point si n�cesasaire
      for (int v = 0; v < nbValues; v++) {
        final double value = isoValues[v];
        searcher.search(value, res, progression, log);
      }
    } catch (IOException iOException) {
      Logger.getLogger(CreateIsoLinesAction.class.getName()).log(Level.INFO, "message {0}", iOException);
    }
    if (log.containsErrorOrFatalError()) {
      delegate.getUI().manageAnalyzeAndIsFatal(log);
    } else {
      EventQueue.invokeLater(new Runnable() {
        @Override
        public void run() {
          addIsoLineLayer(res);
        }
      });

    }

  }
}
