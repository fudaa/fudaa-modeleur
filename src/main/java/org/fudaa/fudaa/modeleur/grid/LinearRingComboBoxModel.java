/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.grid;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;

/**
 *
 * @author Frederic Deniger
 */
public class LinearRingComboBoxModel extends AbstractListModel implements ComboBoxModel {

  private final ZCalqueLigneBrisee selectedLayer;
  Object selected_;

  public LinearRingComboBoxModel(ZCalqueLigneBrisee selectedLayer) {
    this.selectedLayer = selectedLayer;
  }

  @Override
  public int getSize() {
    return selectedLayer.modeleDonnees().getNombre() + 1;
  }

  @Override
  public Object getElementAt(int index) {
    if (index == 0) {
      return null;
    }
    return selectedLayer.modeleDonnees().getGeomData().getGeometry(index - 1);
  }

  @Override
  public Object getSelectedItem() {
    return selected_;
  }

  @Override
  public void setSelectedItem(final Object _anItem) {
    if (_anItem != selected_) {
      selected_ = _anItem;
      fireContentsChanged(this, -1, -1);
    }
  }
}
