/*
 * @creation     21 janv. 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;

import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuLabel;

/**
 * Une fen�tre modale pour l'obtension d'un Z.
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class MdlZDialog extends JDialog implements ActionListener {
  private JComponent text_;

  public MdlZDialog(Frame _frame, String _title) {
    super(_frame, _title, true);
    // Position & resizable
    setLocation(_frame.getLocation().x+_frame.getSize().width/2, _frame.getLocation().y+_frame.getSize().height/2);
    setResizable(false);
    // Contenu
    JPanel container=new JPanel(new BuBorderLayout(2, 2));
    container.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
    text_=GISAttributeConstants.BATHY.getEditor().createEditorComponent();
    JButton ok=new JButton(MdlResource.getS("Valider"));
    ok.addActionListener(this);
    container.add(new BuLabel(MdlResource.getS("Valeur de Z")+" : "), BuBorderLayout.WEST);
    container.add(text_, BuBorderLayout.EAST);
    container.add(ok, BuBorderLayout.SOUTH);
    add(container);
    pack();
  }

  public void actionPerformed(ActionEvent e) {
    setVisible(false);
    dispose();
  }

  public Double getValue() {
    return (Double)GISAttributeConstants.BATHY.getEditor().getValue(text_);
  }
}
