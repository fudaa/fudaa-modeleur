/*
 * @creation 7 juin 07
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.modeleur;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.filechooser.FileFilter;

import org.fudaa.ctulu.CtuluArkLoader;
import org.fudaa.ctulu.CtuluExportDataInterface;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluRunnable;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooserTestWritable;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.gui.CtuluTablePreferencesComponent;
import org.fudaa.dodico.mascaret.io.MascaretExportPreferencesComponent;
import org.fudaa.dodico.zesri.io.ZesriExportPreferencesComponent;
import org.fudaa.ebli.calque.EbliGISPreferencesComponent;
import org.fudaa.ebli.commun.BJava3DVersionTest;
import org.fudaa.ebli.commun.EbliActionMap;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.ebli.courbe.PaletteCourbes;
import org.fudaa.ebli.impression.EbliMiseEnPagePreferencesPanel;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.commun.FudaaProjectStateListener;
import org.fudaa.fudaa.commun.FudaaProjetStateInterface;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.commun.impl.FudaaGuiLib;
import org.fudaa.fudaa.commun.impl.FudaaLookPreferencesPanel;
import org.fudaa.fudaa.commun.impl.FudaaStartupExitPreferencesPanel;
import org.fudaa.fudaa.commun.save.FudaaSaveLib;
import org.fudaa.fudaa.commun.save.FudaaSaveProject;
import org.fudaa.fudaa.commun.save.FudaaSaveZipLoader;
import org.fudaa.fudaa.commun.trace2d.FudaaPerformancePreferencesPanel;
import org.fudaa.fudaa.modeleur.action.MdlBiefImportAction;
import org.fudaa.fudaa.modeleur.action.MdlShow1DFrameAction;
import org.fudaa.fudaa.modeleur.action.MdlShow2DFrameAction;
import org.fudaa.fudaa.modeleur.modeleur1d.Mdl1dPreferencesComponent;
import org.fudaa.fudaa.modeleur.modeleur1d.MdlFille1d;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.fudaa.fudaa.sig.FSigProjectPersistence;
import org.fudaa.fudaa.sig.exetools.FSigManageExeTools;

import com.memoire.bu.BuAbstractPreferencesComponent;
import com.memoire.bu.BuAbstractPreferencesPanel;
import com.memoire.bu.BuBrowserPreferencesPanel;
import com.memoire.bu.BuColumn;
import com.memoire.bu.BuContainerPreferencesPanel;
import com.memoire.bu.BuDesktopPreferencesPanel;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuLanguagePreferencesPanel;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuMainPanel;
import com.memoire.bu.BuMenu;
import com.memoire.bu.BuMenuBar;
import com.memoire.bu.BuMenuItem;
import com.memoire.bu.BuMenuRecentFiles;
import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuRegistry;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTaskView;
import com.memoire.bu.BuUserPreferencesPanel;
import com.memoire.fu.FuLog;

/**
 * La classe principale de mise en place de l'application, de gestion des actions, des �tats de l'interface, etc.
 *
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MdlImplementation extends FudaaCommonImplementation implements FudaaProjectStateListener {

  protected static BuInformationsSoftware isMdl_ = new BuInformationsSoftware();

  static {
    isMdl_.name = MdlResource.getS("Modeleur");
    isMdl_.version = "1.3";
    isMdl_.date = "2022-01-20";
    isMdl_.rights = MdlResource.getS("Tous droits r�serv�s") + ". CETMEF (c)1999-2022";
    isMdl_.license = "GPL2";
    isMdl_.languages = "fr,en";
    isMdl_.authors = new String[]{"B.Marchand, E.Martin, F.Deniger"};
    isMdl_.contact = "soizic.peron@edf.fr";
    isMdl_.http = "http://www.fudaa.fr/mdl/";

    isMdl_.logo = EbliResource.EBLI.getIcon("draw-palette");
    isMdl_.banner = BuResource.BU.getIcon("aproposde_32");
  }

  public static File getDestFile(File _init) {
    return CtuluLibFile.appendExtensionIfNeeded(_init, getExtension());
  }

  public static String getExtension() {
    return "mod.zip";
  }

  public static void addJava3DTest(final BuMenu _r) {
    _r.addSeparator();
    _r.addMenuItem(MdlResource.getS("Tester Java 3D"), "JAVA3D", EbliResource.EBLI.getIcon("3d"), true);
  }

  /**
   * Un filtre autorisant des extensions avec plusieurs points (.mod.zip)
   *
   * @author Bertrand Marchand
   * @version $Id$
   */
  private static class MdlFileFilter extends FileFilter {

    final String extension_ = '.' + getExtension();

    public boolean accept(File _f) {
      return _f != null && (_f.isDirectory() || _f.getName().endsWith(extension_));
    }

    public String getDescription() {
      return MdlResource.getS("Fudaa Modeleur") + " (*.mod.zip)";
    }
  }

  public static BuInformationsSoftware informationsSoftware() {
    return isMdl_;
  }
  /**
   * Le filtre de fichier pour des extension fichiers projets.
   */
  public final static FileFilter FILTER = new MdlFileFilter();
  /**
   * Fenetre interne pour le modeleur 2D.
   */
  MdlFille2d mdl2dFrame_;
  /**
   * Fenetre interne pour le modeleur 1D
   */
  MdlFille1d mdl1dFrame_ = null;
  /**
   * Projet modeleur.
   */
  MdlProjet project_;
  FSigManageExeTools toolMng_;
  /**
   * Si true, l'utilisateur peut sortir de l'application par Quitter.
   */
  boolean exitCanceled_ = false;

  public MdlImplementation() {
    super();
    useNewHelp_ = false;
  }

  /**
   * Retourne le fichier s�lectionn� par boite de dialogue.
   *
   * @param _saveDialog true : Save dialog, test si fichier existant avant ecriture.
   * @return LE fichier, ou null si op�ration abort�e.
   */
  private File chooseNewFile(boolean _saveDialog) {
    return getDestFile(chooseFile(_saveDialog));
  }

  private File chooseFile(boolean _saveDialog) {
    return FudaaGuiLib.ouvrirFileChooser(MdlResource.getS("Fichier modeleur"), FILTER, this.getFrame(),
            _saveDialog, _saveDialog ? new CtuluFileChooserTestWritable(this) : null);
  }

  protected void setFileIfNeeded() {
    if (project_.getParamsFile() == null) {
      project_.setParamFile(chooseNewFile(true));
    }
  }

  public void projectStateChanged(FudaaProjetStateInterface _proj) {
    updateActionsState();
  }

  /**
   * Mise � jour de l'�tat des boutons.
   */
  void updateActionsState() {
    boolean bprjOpen = project_ != null;
    boolean bprjModPar = bprjOpen && project_.getProjectState().isParamsModified();
    boolean buiMod = bprjOpen && project_.getProjectState().isUIModified();

    setEnabledForAction("ENREGISTRER", bprjModPar | buiMod);
    setEnabledForAction("ENREGISTRERSOUS", bprjOpen);
    setEnabledForAction("FERMER", bprjOpen);
    setEnabledForAction("IMPORT_PROJECT", bprjOpen);
    setEnabledForAction(CtuluExportDataInterface.EXPORT_CMD, bprjOpen);
  }

  /**
   * Creation de la vue 2D, et ajout.
   */
  void createNew2dFrame() {
    mdl2dFrame_ = new MdlFille2d(this);
    addInternalFrame(mdl2dFrame_);
    getExeToolManager().setRootLayer(mdl2dFrame_.getDonneesCalque());
    getExeToolManager().setCommandManager(mdl2dFrame_.getCmdMng());
  }

  /**
   * Creation de la vue 1D. Ajout a la demande.
   */
  void createNew1dFrame() {
    if (mdl1dFrame_ == null) {
      mdl1dFrame_ = new MdlFille1d(this);
    }
  }

  /**
   * @return La vue 1D, ou null si inexistante.
   */
  public MdlFille1d get1dFrame() {
    return mdl1dFrame_;
  }

  /**
   * @return La vue 2D, ou null si inexistante.
   */
  public MdlFille2d get2dFrame() {
    return mdl2dFrame_;
  }

  /**
   * Installe la vue 1d sur le desktop, et la rend active.
   */
  public void install1dFrame() {
    boolean alreadyInstalled = false;
    for (JInternalFrame f : getAllInternalFrames()) {
      if (f.equals(mdl1dFrame_)) {
        alreadyInstalled = true;
        break;
      }
    }
    if (!alreadyInstalled) {
      addInternalFrame(mdl1dFrame_);
    } else {
      activateInternalFrame(mdl1dFrame_);
    }
  }

  protected boolean buildExportDataToolIcon() {
    return true;
  }

  @Override
  protected boolean buildFudaaReportTool() {
    return true;
  }

  @Override
  protected boolean buildImageToolIcon() {
    return true;
  }

  /**
   * Ferme le projet, ferme les fenetres associ�s, sans demande d'enregistrement en cas de modif (ce controle a �t� effectu� en amont).
   */
  protected void closeProject() {
    if (project_ == null) {
      return;
    }
    project_.close();
    try {
      if (mdl2dFrame_ != null) {
        mdl2dFrame_.setClosed(true);
      }
      if (mdl1dFrame_ != null) {
        mdl1dFrame_.setClosed(true);
      }
    } catch (PropertyVetoException _evt) {
      FuLog.error(_evt);

    }
    removeInternalFrames(getAllInternalFrames());
    getMainMenuBar().getMenu("mnPROJECT").setVisible(false);
    project_ = null;
  }

  /**
   * Cr�e un nouveau projet, la fenetre 2D si necessaire.
   *
   * @param _add true : Recr�e la fenetre.
   */
  protected void createProject(final boolean _add) {
    project_ = new MdlProjet();
    project_.setOpening(true);
    if (_add) {
      createNew2dFrame();
    } else {
      ((MdlVisuPanel) mdl2dFrame_.getVisuPanel()).initCalques(false);
    }
    createNew1dFrame();
    project_.install(this);
    BuLib.invokeLater(new Runnable() {
      public void run() {
        project_.setOpening(false);
//        changeSaveActions(false);
        updateActionsState();
        project_.setSaved();
        getMainMenuBar().getMenu("mnPROJECT").setVisible(true);
      }
    });
  }

  @Override
  public void actionPerformed(ActionEvent _evt) {
    final String action = _evt.getActionCommand();
    if (action == null) {
      return;
    }
    if ("OUVRIR".equals(action)) {
      ouvrir(null);
    } else if ("ENREGISTRER".equals(action)) {
      save();
    } else if ("ENREGISTRERSOUS".equals(action)) {
      saveAs();
    } else if ("FERMER".equals(action)) {
      close();
    } else if ("CREER".equals(action)) {
      create();
    } else if ("IMPORT_PROJECT".equals(action)) {
      importProject();
    } else if ("AIDE_INDEX".equals(action)) {
      displayHelp(getAideIndexUrl());
    } else if ("JAVA3D".equals(action)) {
      BJava3DVersionTest.showVersionDialog(this);
    } else if (action.startsWith("TOGGLE")) {
      final BuColumn c = getMainPanel().getRightColumn();
      final JComponent comp = c.getToggleComponent(action);
      if (comp != null) {
        comp.setVisible(!comp.isVisible());
        c.revalidate();
      }
    } else if (action.startsWith("REOUVRIR")) {
      FuLog.trace(action.substring(9, action.length() - 1));
      ouvrir(new File(action.substring(9, action.length() - 1)));
    } else {
      super.actionPerformed(_evt);
    }
  }

  /**
   * Met a jour les fichiers recents chaque fois que necessaire.
   */
  private void updateRecentFiles(File _fichier) {
    getMainMenuBar().addRecentFile(_fichier.getPath(), null);
    MdlPreferences.MDL.writeIniFile();
  }

  /**
   * Surcharge de la m�thode pour pouvoir sauvegarder les pr�f�rences.
   */
  @Override
  public void exit() {
    if (!isExitCanceled()) {
      confirmExit();
    }
  }

  private boolean isExitCanceled() {
    return exitCanceled_;
  }

  /**
   * Definit si l'action Exit est autoris�e (par exemple lors d'une sauvegarde)
   *
   * @param _b True : L'action est autoris�e.
   */
  private void setExitCanceled(boolean _b) {
    exitCanceled_ = _b;
  }

  /**
   * L'action close est g�r�e au niveau applicatif (et non fenetre par fenetre).
   *
   * @return false
   */
  @Override
  public boolean isCloseFrameMode() {
    return false;
  }

  /**
   * Confirmation de la sortie avec ou sans sauvegarde.
   */
  @Override
  public boolean confirmExit() {
    if (!FudaaStartupExitPreferencesPanel.isExitConfirmed()
            || question(MdlResource.getS("Quitter"), MdlResource.getS("Voulez-vous vraiment quitter ce logiciel ?"))) {
      return saveAndCloseProjet(new Runnable() {
        // Lanc� apr�s la sauvegarde, si op�ration de sauvegarde concluante ou si pas de sauvegarde demand�e.
        public void run() {
          savePreferencesAndTerminate();
        }
      });
    }
    return true;
  }

  /**
   * Action "Creer" un nouveau projet.
   */
  public void create() {
    saveAndCloseProjet(new Runnable() {
      public void run() {
        createProject(true);
      }
    });
  }

  /**
   * Action "Fermer" le projet en cours.
   */
  @Override
  public void close() {
    saveAndCloseProjet(null);
    updateActionsState();
  }

  protected String getAideIndexUrl() {
    return getHelpDir() + "modeleur/index.html";
  }

  public BuPreferences getApplicationPreferences() {
    return null;
  }

  public BuInformationsSoftware getInformationsSoftware() {
    return informationsSoftware();
  }

  
  public void beforeLoading() {
      //-- desactivate automatic curve rendering --//
      EGGroup.paletteTraceAuto = null;
      PaletteCourbes.nbDifferentGroups=0;
  }
  
  public void afterLoading() {
      EGGroup.paletteTraceAuto = MdlPaletteCourbe.getInstance();
  }
  
  /**
   * Action "Ouvrir" un nouveau projet. Peut demander la sauvegarde du projet pr�c�demment ouvert si existant.
   *
   * @param _f Le fichier projet � ouvrir. null, si le fichier doit etre choisi par l'utilisateur.
   */
  public void ouvrir(final File _f) {
    if (isProjectModified()) {
      saveAndCloseProjet(new Runnable() {
        public void run() {
          ouvrir(_f);
        }
      });
      return;
    }

    closeProject();
   beforeLoading();
    
    final File f = CtuluLibFile.exists(_f) ? _f : chooseFile(false);
    if (f == null) {
      return; // Abandon utilisateur.
    }
    createProject(false);
    setGlassPaneStop();
    new CtuluRunnable(MdlResource.getS("Ouvrir"), MdlImplementation.this) {
      public boolean run(ProgressionInterface _proj) {
        project_.setOpening(true);
        Runnable r = null;
        try {
          try {
            FudaaSaveZipLoader loader = new FudaaSaveZipLoader(f);
            // Recup des infos pour utilisateur.
            BuInformationsSoftware is = FudaaSaveProject.getSoftwareInfos(loader);
            if (is != null) {
              FuLog.trace("File version : " + (is.version == null ? "Undefined" : is.version));
            }

            r = FudaaSaveLib.restoreFille(MdlImplementation.this, mdl2dFrame_, _proj, loader);
          } catch (final IOException _evt) {
            FuLog.error(_evt);

          }

          project_.setParamFile(f);

        } finally {
           afterLoading();
          final Runnable swingRun = r;
          BuLib.invokeLater(new Runnable() {
            public void run() {
              try {
                addInternalFrame(mdl2dFrame_);
                mdl2dFrame_.getCmdMng().clean();

                if (swingRun != null) {
                  swingRun.run();
                }

                // Met a jour la fenetre 1d
                FudaaSaveZipLoader loader = null;
                try {
                  loader = new FudaaSaveZipLoader(f);
                  mdl1dFrame_.restoreFrom(loader, null);
                  mdl1dFrame_.getCmdMng().clean();
                } catch (IOException _exc) {
                } finally {
                  if (loader != null) {
                    try {
                      loader.close();
                    } catch (IOException _exc) {
                    }
                  }
                }

                // le projet a ete install�: on enl�ve les flag de modification
                project_.setOpening(false);
                project_.setSaved();
                if (swingRun == null) {
                  MdlImplementation.this.warn(MdlResource.getS("Ouvrir"),
                          MdlResource.getS("Le projet n'a pas �t� ouvert!"));
                } else {
                  updateRecentFiles(project_.getParamsFile());
                }
              } finally {
                unsetGlassPaneStop();
                
              }
            }
          });

        }
        return r != null;
      }
    }.run();
  }

  /**
   * Action "Enregistrer" le projet.
   */
  public void save() {
    if (project_.getParamsFile() != null) {

      CtuluRunnable act = new CtuluRunnable(FudaaSaveLib.getActionSaveTitle(), this) {
        public boolean run(ProgressionInterface _proj) {
          try {
            setGlassPaneStop();
            setExitCanceled(true);
            if (FSigProjectPersistence.saveProject(MdlImplementation.this, project_, project_.getParamsFile(), _proj)) {
              updateRecentFiles(project_.getParamsFile());
              return true;
            }
            return false;
          } finally {
            unsetGlassPaneStop();
            setExitCanceled(false);
          }
        }
      };
      act.setAfterRunnable(getSaveSwingRunnable(project_.getParamsFile()), true);
      act.run();
    } else {
      saveAs();
    }
  }

  /**
   * Action "Enregistrer sous" le projet.
   */
  public void saveAs() {
    final File f = chooseNewFile(true);
    if (f == null) {
      return;
    }

    CtuluRunnable act = new CtuluRunnable(FudaaSaveLib.getActionSaveTitle(), this) {
      public boolean run(ProgressionInterface _proj) {
        try {
          setGlassPaneStop();
          setExitCanceled(true);
          if (FSigProjectPersistence.saveProject(MdlImplementation.this, project_, f, _proj)) {
            updateRecentFiles(f);
            return true;
          }
          return false;
        } finally {
          unsetGlassPaneStop();
          setExitCanceled(false);
        }
      }
    };
    act.setAfterRunnable(getSaveSwingRunnable(f), true);
    act.run();
  }

  protected boolean isProjectModified() {
    return project_ != null
            && (project_.getProjectState().isParamsModified() || project_.getProjectState().isUIModified());
  }

  public void importProject() {
    MdlProjectImportPanel pn = new MdlProjectImportPanel();
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(getFrame(), MdlResource.getS("Import d'un projet")))) {
      File f = pn.getFile();
      try {
        FudaaSaveZipLoader loader = new FudaaSaveZipLoader(f);
        String[] ignoredLayers = pn.getIgnoredLayers();
        for (String s : ignoredLayers) {
          loader.setOption(CtuluArkLoader.OPTION_LAYER_IGNORE + s, CtuluLibString.toString(true));
        }

        final CtuluTaskDelegate createTask = createTask(MdlResource.getS("Import d'un projet"));
// FIXME BM: Suppression de la tache dans un thread diff�rent, car l'appli se bloque
// Probable pb de deadlock.
//        createTask.start(new Runnable() {
//          public void run() {
        FudaaSaveLib.restoreAndLaunch(MdlImplementation.this, mdl2dFrame_, createTask.getStateReceiver(), loader);
//          }
//        });

        // Liberation du fichier.
        loader.close();
      } catch (final IOException _evt) {
        FuLog.error(_evt);

      }
    }
  }

  /**
   * Dans le thread swing....
   *
   * @param _relaunch l'exe a lancer apr�s: si l'utilisateur a accept� la fermeture du projet
   */
  public boolean saveAndCloseProjet(final Runnable _relaunch) {
    final Runnable r = new Runnable() {
      public void run() {
        closeProject();
        if (_relaunch != null) {
          _relaunch.run();
        }
      }
    };
    boolean save = false;
    if (isProjectModified()) {
      final int i = CtuluLibDialog.confirmExitIfProjectisModified(getFrame());
      // l'utilisateur veut annuler l'op�ration
      if (i == JOptionPane.CANCEL_OPTION) {
        FuLog.debug("FSI: close operation cancelled by user");
        return false;
      }
      save = (i == JOptionPane.OK_OPTION);
    }

    FuLog.debug("FSI: close operation accepted and save option= " + save);
    // pas de sauvegarde: on lance l'op�ration suivante:
    if (!save) {
      r.run();
    } else {
      // on initilialise le fichier de dest si n�cessaire
      setFileIfNeeded();
      if (project_.getParamsFile() == null) {
        return false;
      }

      CtuluRunnable act = new CtuluRunnable(FudaaSaveLib.getActionSaveTitle(), this) {
        public boolean run(ProgressionInterface _prog) {
          // le fichier peut etre null si l'utilisateur a refuse de pr�ciser un fichier
          // dans ce cas, on ne sauvegarde pas.
          if (project_.getParamsFile() != null) {
            try {
              setGlassPaneStop();
              setExitCanceled(true);
              FSigProjectPersistence.saveProject(MdlImplementation.this, project_,
                      project_.getParamsFile(), _prog);
              updateRecentFiles(project_.getParamsFile());
              return true;
            } finally {
              unsetGlassPaneStop();
              setExitCanceled(false);
            }
          } else {
            return false;
          }
        }
      };
      // le r sera lance apres et dans le thread swing
      act.setAfterRunnable(r, true);
      act.run();
      return false;
    }
    return true;
  }

  protected Runnable getSaveSwingRunnable(final File _paramFile/*, final JDialog _d*/) {
    return new Runnable() {
      public void run() {
        if (_paramFile != null) {
          project_.setParamFile(_paramFile);
        }
        project_.setSaved();
//        _d.dispose();
      }
    };
  }

  /**
   * Construit le menu projet.
   *
   * @return Le menu projet.
   */
  protected BuMenu buildProjectMenu() {
    BuMenu mn = new BuMenu(MdlResource.getS("Projet"), "mnPROJECT");
    mn.add(new MdlShow2DFrameAction(this));
    mn.add(new MdlShow1DFrameAction(this));

    mn.setVisible(false);
    return mn;
  }

  /**
   * Methode surcharg�e pour les panneau de pr�f�rence.
   */
  @Override
  protected void buildPreferences(final List<BuAbstractPreferencesPanel> _frAddTab) {

    _frAddTab.add(new FudaaPerformancePreferencesPanel(this));
    _frAddTab.add(new BuUserPreferencesPanel(this));
    _frAddTab.add(new BuLanguagePreferencesPanel(this));
    _frAddTab.add(new BuDesktopPreferencesPanel(this));
    _frAddTab.add(new FudaaStartupExitPreferencesPanel(true));
    _frAddTab.add(new FudaaLookPreferencesPanel(this));
    _frAddTab.add(new BuBrowserPreferencesPanel(this));
    _frAddTab.add(new EbliMiseEnPagePreferencesPanel());
    _frAddTab.add(new BuContainerPreferencesPanel(MdlResource.getS("Visuel"), MdlResource.getS("Cartographie"),
            new BuAbstractPreferencesComponent[]{
              new EbliGISPreferencesComponent(),
              new MdlLayerTreePreferencesComponent()
            }));
    _frAddTab.add(new BuContainerPreferencesPanel(MdlResource.getS("Export"),
            new BuAbstractPreferencesComponent[]{
              new CtuluTablePreferencesComponent(),
              new MascaretExportPreferencesComponent(),
              new ZesriExportPreferencesComponent()
            }));
    _frAddTab.add(new BuContainerPreferencesPanel(MdlResource.getS("Vue 1D"),
            new BuAbstractPreferencesComponent[]{
              new Mdl1dPreferencesComponent()
            }));
  }

  /**
   * Cr�ation du panneau des taches, dans la colonne de droite.
   */
  protected void buildTaskView() {
    final BuMainPanel mp = getMainPanel();
    final BuColumn lc = mp.getLeftColumn();
    lc.setFocusable(false);
    final BuColumn rc = mp.getRightColumn();
    rc.setFocusable(false);
    lc.setBorder(null);
    // rc.setBorder(new EmptyBorder(0,2,0,2));
    BuTaskView taches = new BuTaskView();
    taches.setToolTipText(MdlResource.getS("Les t�ches en cours"));
    final BuScrollPane sp = new BuScrollPane(taches);
    sp.setPreferredSize(new Dimension(150, 80));
    sp.setToolTipText(MdlResource.getS("Les t�ches en cours"));
    rc.addToggledComponent(MdlResource.getS("T�ches"), "TOGGLE_TACHE", BuResource.BU.getToolIcon("tache"), sp,
            true, this).setToolTipText(MdlResource.getS("Cacher/Afficher les t�ches"));
    mp.setTaskView(taches);
  }

  /**
   * Sauvegarde des pr�f�rences de l'appli � la sortie.
   *
   * Remarque importante : Certaines infos sauv�es par cette m�thode sont relues par d'autres applications Fudaa, qui risquent alors de s'afficher de
   * facon inattendue.
   */
  protected void savePreferencesAndTerminate() {
    final Point p = getFrame().getLocation();
    final Dimension d = getFrame().getSize();
    BuPreferences.BU.putIntegerProperty("window.x", p.x);
    BuPreferences.BU.putIntegerProperty("window.y", p.y);
    BuPreferences.BU.putIntegerProperty("window.w", d.width);
    BuPreferences.BU.putIntegerProperty("window.h", d.height);
    BuPreferences.BU.putIntegerProperty("leftcolumn.width", getMainPanel().getLeftColumn().getWidth());
    BuPreferences.BU.putIntegerProperty("rightcolumn.width", getMainPanel().getRightColumn().getWidth());
    BuPreferences.BU.writeIniFile();

    BuRegistry.unregister(this.getFrame());
  }

  /**
   * Maximisation syst�matique de la fenetre.
   *
   * @param _f La fenetre ajout�e.
   */
  @Override
  public void addInternalFrame(final JInternalFrame _f) {
    super.addInternalFrame(_f);
    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        try {
          _f.setMaximum(true);
        } catch (PropertyVetoException ex) {
          Logger.getLogger(MdlImplementation.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    });
  }

  /**
   * Mise en place de l'application ava,nt affichage.
   */
  @Override
  public void init() {
    super.init();

    TransferHandler transHdl = new TransferHandler() {
      @Override
      public boolean canImport(TransferSupport support) {
        for (DataFlavor df : support.getDataFlavors()) {
          if (df.equals(DataFlavor.javaFileListFlavor)) {
            return true;
          }
        }
        return false;
      }

      @Override
      public boolean importData(TransferSupport support) {
        if (canImport(support)) {
          try {
            List<File> files = (List<File>) support.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
            if (files.size() > 1) {
              error(MdlResource.getS("Un seul fichier projet est autoris� pour le transfert"));
              return false;
            }
            ouvrir(files.get(0));
            return true;
          } catch (UnsupportedFlavorException ufe) {
            ufe.printStackTrace();
          } catch (IOException ioe) {
            ioe.printStackTrace();
          }
        }
        return false;
      }
    };
//    ((JFrame) getFrame()).setTransferHandler(transHdl);

    // Pour forcer l'activation du command listener.
    getUndoCmdListener();

    removeUnusedActions();
    final BuMenuBar mb = getMainMenuBar();
    // on enleve le menu des look and feel : moche car tout n'est pas mis a jour
    final BuMenu mAide = (BuMenu) mb.getMenu("MENU_AIDE");
    addJava3DTest(mAide);

    // Les actions qu'on peut retrouver partout.
    EbliActionMap.getInstance().addAction(new MdlBiefImportAction(this));

    setEnabledForAction("QUITTER", true);
    setEnabledForAction("PREFERENCE", true);
    setEnabledForAction("CREER", true);
    setEnabledForAction("OUVRIR", true);
    setEnabledForAction("IMPORTER", true);
    setEnabledForAction("EXPORTER", true);

    BuMenuRecentFiles mr = (BuMenuRecentFiles) mb.getMenu("REOUVRIR");
    if (mr != null) {
      mr.setPreferences(MdlPreferences.MDL);
      mr.setResource(MdlResource.MDL);
      mr.setEnabled(true);
    }

    /*    setEnabledForAction("MAJ", true);
     setEnabledForAction("SEND_COMMENT", true);
     setEnabledForAction("LAUNCH_JAVAWS", true);
     final BuMenu mFichier = (BuMenu) b.getMenu("MENU_FICHIER");
     mFichier.addMenuItem(MdlResource.getS("Fermer toutes les applications"), "CLOSE_ALL", BuResource.BU
     .getIcon("fermer"), true, 0);
     final BuToolBar tb = getMainToolBar();
     if (!isSupervisor()) {

     tb.addToolButton(TrResource.getSupervisorName(), TrResource.getS("ouvrir le superviseur"), "SUPERVISEUR",
     TrResource.getSupervisorIcon(), true).setVisible(true);

     }*/
    // les menus exporter et importer sont construit dynamiquement
//    b.getMenu(getExporterCmd()).addItemListener(this);
    BuMenu mmImport = (BuMenu) mb.getMenu("IMPORTER");
    BuMenu mnExport = (BuMenu) mb.getMenu("EXPORTER");

//    menu.addMenuItem(FSigImageImportAction.getCommonTitle(), "IMPORT_IMAGE", FSigImageImportAction.getCommonImage(),
//        this).setEnabled(false);
    mmImport.addMenuItem(MdlResource.getS("Importer un projet"), "IMPORT_PROJECT", BuResource.BU.getMenuIcon("importer"), this)
            .setEnabled(false);
//    menu.addItemListener(this);
    setEnabledForAction("IMPORT_PROJECT", true);
    mmImport.setEnabled(true);

    BuMenuItem itExportData = new BuMenuItem();
    super.initExportDataButton(itExportData);
    mnExport.add(itExportData);

    BuMenu mnProject = buildProjectMenu();
    mb.addMenu(mnProject);

    // Ajout du menu des exe tools
    BuMenu mnEdition = (BuMenu) mb.getMenu("MENU_EDITION");
    mnEdition.addSeparator();
    addExeToolsMenu(mnEdition);

    /*    setEnabledForAction("RANGERICONES", true);
     if (!isSupervisor()) {
     final JComponent cp = getMainPanel().getMiddleComponent();
     if (cp instanceof JScrollPane) {
     ScrollPaneSelector.installScrollPaneSelector((JScrollPane) cp);
     }
     }*/
    buildTaskView();
  }

  @Override
  protected FSigManageExeTools getExeToolManager() {
    if (toolMng_ == null) {
      toolMng_ = new FSigManageExeTools(this);
    }
    return toolMng_;
  }

  /**
   * Suppression des commandes par d�faut dans Fudaa.
   */
  protected void removeUnusedActions() {
    /*  Issu de Fudaa-Prepro   
     final BuMenu r = (BuMenu) getMainMenuBar().getMenu("MENU_EDITION");
     if (r != null) {
     r.removeAll();
     r.addMenuItem(MdlResource.getS("D�faire"), "DEFAIRE", false, KeyEvent.VK_Z);
     r.addMenuItem(MdlResource.getS("Refaire"), "REFAIRE", false).setAccelerator(
     KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
     // normalement Ctrl-Y
     // r.addSeparator();
     //
     r.addMenuItem(MdlResource.getS("Copier"), "COPIER", false, KeyEvent.VK_C);
     r.addMenuItem(MdlResource.getS("Couper"), "COUPER", false, KeyEvent.VK_X);
     r.addMenuItem(MdlResource.getS("Coller"), "COLLER", false, KeyEvent.VK_V);
     // r.addMenuItem(MdlResource.getS("Dupliquer" ),"DUPLIQUER" ,false)
     // .setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V,KeyEvent.CTRL_MASK|KeyEvent.SHIFT_MASK));
     r.addSeparator();
     FSigLib.addSelectionAction(r, null);
     // r.addMenuItem(MdlResource.getS("Remplacer..." ),"REMPLACER"
     // ,false,KeyEvent.VK_R);
     r.addSeparator();
     r.addMenuItem(MdlResource.getS("Pr�f�rences"), "PREFERENCE", false, KeyEvent.VK_F2);
     addConsoleMenu(r);
     final BuToolBar tb = getMainToolBar();
     BuActionRemover.removeAction(tb, "COUPER");
     BuActionRemover.removeAction(tb, "COLLER");
     BuActionRemover.removeAction(tb, "COPIER");
     BuActionRemover.removeAction(tb, "DUPLIQUER");
     BuActionRemover.removeAction(tb, "RANGERICONES");
     // BuActionRemover.removeAction(tb, "TOUTSELECTIONNER");
     BuActionRemover.removeAction(tb, "REMPLACER");

     }*/
    /*
     * removeAction("COUPER"); removeAction("COLLER"); removeAction("COPIER"); removeAction("DUPLIQUER");
     * removeAction("TOUTSELECTIONNER"); removeAction("REMPLACER");
     */
    removeAction("ASSISTANT");
//    removeAction("ASTUCE");
    removeAction("POINTEURAIDE");
    removeAction("INDEX_THEMA");
    removeAction("INDEX_ALPHA");
    removeAction("PROPRIETE");
    removeAction("PLEINECRAN");
    removeAction("VISIBLE_LEFTCOLUMN");
  }

  @Override
  public void start() {
    super.start();
    super.addFrameListLeft();
    createProject(true);
  }
}
