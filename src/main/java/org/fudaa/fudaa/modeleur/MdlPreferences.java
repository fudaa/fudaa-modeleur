/*
 * @file         PrertPreferences.java
 * @creation     2002-08-30
 * @modification $Date: 2008-02-14 17:03:33 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.modeleur;

import org.fudaa.dodico.commun.DodicoPreferences;
import org.fudaa.fudaa.commun.FudaaPreferencesAbstract;

import com.memoire.bu.BuPreferences;

/**
 * Préférences pour le modeleur.
 * 
 * @author Bertrand Marchand
 * @version $Id$
 */
public final class MdlPreferences extends FudaaPreferencesAbstract {
  /**
   * Singleton.
   */
  public final static BuPreferences MDL;
  
  static {
    if (root_==null) {
      MDL= new MdlPreferences();
    }
    else
      MDL=BU;
  }

  private MdlPreferences() {
    super();
  }

  public void applyOn(final Object _o) {

  }
}
