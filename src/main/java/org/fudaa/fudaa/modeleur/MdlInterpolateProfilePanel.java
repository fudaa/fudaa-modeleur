/*
 * @creation 8 sept. 06
 * @modification $Date: 2008-10-07 16:14:47 +0200 (mar., 07 oct. 2008) $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.modeleur;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;

import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.calque.CalqueGISTreeModel;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesAbstract;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZCalqueGeometry;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.ZModeleGeometry;
import org.fudaa.ebli.calque.edition.ZModeleGeometryDefault;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.ebli.trace.TraceSurface;
import org.fudaa.ebli.trace.TraceSurfaceModel;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Polygon;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuLabel;

/**
 * Un panneau de saisie des géométries supports de projection suivant Z.
 * @author Bertrand Marchand, Emmanuel MARTIN
 * @version $Id$
 */
public class MdlInterpolateProfilePanel extends CtuluDialogPanel {
  
  private ZEbliCalquesPanel pn_;
  private GrBoite initZoom_;
  private JTree trLayers_;
  private ZCalqueGeometry cqTmp_;
  private GISZoneCollection support_;
  /** List des calques acceptant les profils avec pour clef leur nom. */
  private Map<String, ZCalqueAffichageDonneesInterface> calquesProfil_=new HashMap<String, ZCalqueAffichageDonneesInterface>();
  /** La comboBox contenant le calque de destination du profil. */
  private BuComboBox cbProfil_;
  
  public MdlInterpolateProfilePanel(ZEbliCalquesPanel _pn) {
    pn_=_pn;
    setLayout(new BuBorderLayout(5, 5));
    
    BuLabel lbTitle=new BuLabel(MdlResource.getS("Sélectionnez les semis"));
    add(lbTitle, BuBorderLayout.NORTH);

    CalqueGISTreeModel md=new CalqueGISTreeModel(null,_pn.getDonneesCalque());
    md.setMask(GISLib.MASK_MULTIPOINT);
    trLayers_=md.createView(true,true);
    
    trLayers_.addTreeSelectionListener(new TreeSelectionListener() {
      public void valueChanged(TreeSelectionEvent e) {
        treeSelectionChanged(getSelectedGeomInTree(),true);
      }
    });
    JScrollPane sp=new JScrollPane(trLayers_);
    sp.setPreferredSize(new Dimension(300,200));
    add(sp, BuBorderLayout.CENTER);
    
    // Liste des calques disponibles pour le profil résultant \\
    ZCalqueAffichageDonneesInterface[] calques=pn_.getScene().getAllLayers();
    for(int i=0;i<calques.length;i++){
      if(calques[i].modeleDonnees() instanceof ZModeleGeometry){
        Object value=((ZModeleGeometry)calques[i].modeleDonnees()).getGeomData().getFixedAttributValue(GISAttributeConstants.NATURE);
        if(value!=null&&value.equals(GISAttributeConstants.ATT_NATURE_PF))
          calquesProfil_.put(calques[i].getTitle(), calques[i]);
      }
    }
    // Remplissage de la comboBox
    cbProfil_=new BuComboBox();
    for(Map.Entry<String, ZCalqueAffichageDonneesInterface> entry:calquesProfil_.entrySet())
      cbProfil_.addItem(entry.getKey());
    // Selection par defaut
    if(calquesProfil_.containsKey(pn_.getScene().getCalqueActif().getTitle()))
        cbProfil_.setSelectedItem(pn_.getScene().getCalqueActif().getTitle());
    if(calquesProfil_.size()==0)
      setErrorText(MdlResource.getS("Aucun calque de profil n'a été trouvé, l'interpolation est impossible."));
    Container cont=new Container();
    cont.setLayout(new GridLayout(1, 2, 2, 2));
    cont.add(new BuLabel(MdlResource.getS("Calque de destination")+" : "));
    cont.add(cbProfil_);
    add(cont, BuBorderLayout.SOUTH);
  }
  
  public ZCalqueAffichageDonneesInterface getCalqueProfileDestination(){
    return calquesProfil_.get(cbProfil_.getSelectedItem());
  }
  
  /**
   * Retourne les géometries sélectionnées de l'arbre. Les géométries transportent le Z en chaque point si un Z existe.
   * @return Les géométries, de taille = 0 si rien de selectionné.
   */
  private Geometry[] getSelectedGeomInTree() {
    if (trLayers_.isSelectionEmpty()) {
      return null;
    }
    TreePath[] selpaths=trLayers_.getSelectionPaths();
    Geometry[] geoms=new Geometry[selpaths.length];
    for (int i=0; i<selpaths.length; i++) {
      final CalqueGISTreeModel.LayerNode node=(CalqueGISTreeModel.LayerNode)selpaths[i].getLastPathComponent();
      final ZCalqueAffichageDonneesAbstract cq = (ZCalqueAffichageDonneesAbstract) node.getUserObject();
      GISZoneCollection col=((ZModeleGeometry)cq.modeleDonnees()).getGeomData();
      // Pour le transport du Z.
      col.initZCoordinate(node.getIdxGeom());
      geoms[i]=col.getGeometry(node.getIdxGeom());
    }
    
    return geoms;
  }

  /**
   * Réaffichage de la fenetre 2D en cas de selection d'un objet.
   * @param _geoms Les géométries sélectionnées.
   * @param _zoom True si le zoom doit se faire sur les géometries sélectionnées.
   */
  private void treeSelectionChanged(final Geometry[] _geoms, final boolean _zoom) {
    if (pn_==null) {
      return;
    }
    if (_geoms == null || _geoms.length==0) {
      if (cqTmp_ != null) {
        cqTmp_.setVisible(false);
      }
      return;
    }

    ZModeleGeometryDefault mdl=new ZModeleGeometryDefault();
    support_=mdl.getGeomData();

    for (int i=0; i<_geoms.length; i++) {
      support_.addGeometry(_geoms[i], null, null);
    }
    // Ajout de l'enveloppe externe pour visualisation.
    Geometry g=support_.convexHull();
    if (g instanceof Polygon)
      support_.addGeometry(((Polygon)g).getExteriorRing(), null, null);
    else
      support_.addGeometry(g, null, null);
    
    if (cqTmp_ == null) {
      initZoom_ = pn_.getVueCalque().getViewBoite();
      cqTmp_ = new ZCalqueGeometry(mdl);
      cqTmp_.setDestructible(true);
      final TraceIconModel model = new TraceIconModel(TraceIcon.PLUS, 4, Color.RED);
      cqTmp_.setIconModel(0, model);
      cqTmp_.setIconModel(1, model);
      final TraceLigneModel ligne = new TraceLigneModel(TraceLigne.INVISIBLE, 2, Color.RED);
      cqTmp_.setLineModel(0, ligne);
      cqTmp_.setLineModel(1, ligne);
      final TraceSurfaceModel surfMdl=new TraceSurfaceModel(TraceSurface.UNIFORME,new Color(255,50,0,40),null);
      cqTmp_.setSurfaceModel(0,surfMdl);
      pn_.getVueCalque().getCalque().enPremier(cqTmp_);
      pn_.getCqInfos().enPremier();
    }
    cqTmp_.modele(mdl);
    cqTmp_.setVisible(true);
    
    if (_zoom) {
      BArbreCalqueModel.actionCenter(cqTmp_, pn_);
    }
  }

  @Override
  public boolean isDataValid() {
    if (trLayers_.getSelectionCount()==0) {
      setErrorText(MdlResource.getS("Vous devez sélectionner au moins 1 semis"));
      return false;
    }
    return true;
  }
  
  /**
   * Retourne le modele des géométries supports (les géométries séléectionnées dans l'arbre).
   * @return Le modèle.
   */
  public GISZoneCollection getSupportCollection() {
    support_.setAttributes(new GISAttributeInterface[]{GISAttributeConstants.BATHY}, null);
    support_.setAttributeIsZ(GISAttributeConstants.BATHY);
    support_.postImport(0);
    return support_;
  }

  /**
   * Fermeture du dialogue, et suppression du calque qui visualise les géométries sélectionnées.
   */
  private void close() {
    if (cqTmp_!=null) {
      cqTmp_.detruire();
      cqTmp_=null;
      // Suppression de l'enveloppe externe.
      support_.removeGeometries(new int[]{support_.getNbGeometries()-1}, null);
      pn_.getVueCalque().getCalque().repaint();
    }
    pn_.getVueCalque().changeRepere(this, initZoom_);
  }
  
  @Override
  public boolean cancel() {
    close();
    return super.cancel();
  }

  @Override
  public boolean ok() {
    close();
    return super.ok();
  }

}