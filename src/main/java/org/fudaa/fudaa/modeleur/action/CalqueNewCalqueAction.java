/*
 *  @creation     1 avr. 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.modeleur.action;

import java.awt.event.ActionEvent;

import javax.swing.Icon;

import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluNamedCommand;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.modeleur.layer.MdlLayerFactory;
import org.fudaa.fudaa.modeleur.layer.MdlLayerInterface;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 * Une action pour cr�er un nouveau calque.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class CalqueNewCalqueAction extends EbliActionSimple {

  /** Une op�ration de construction d'un nouveau calque */
  class OperationForLayerCreation implements CtuluNamedCommand {
    private final BGroupeCalque parent_;
    private final ZCalqueAffichageDonnees la_;

    /**
     * @param _parent le calque parent
     * @param _la le calque ajout�
     */
    OperationForLayerCreation(BGroupeCalque _parent, ZCalqueAffichageDonnees _la) {
      parent_ = _parent;
      la_ = _la;
    }

    public void redo() {
      parent_.enDernier(la_);
    }

    public void undo() {
      parent_.remove(la_);
    }
    
    public String getName() {
      return MdlResource.getS("Nouveau calque");
    }
  }
  
  FSigEditor editor_;
  BGroupeCalque parent_;
  int cqType_;

  /**
   * Construit l'action.
   * @param _title Label de l'action
   * @param _icon L'icone pour l'action.
   * @param _cqType Le type de calque (Voir {@link MdlLayerFactory})
   * @param _parent Le calque parent dans lequel sera cr�� le calque.
   * @param _editor L'�diteur.
   */
  public CalqueNewCalqueAction(String _title, Icon _icon, int _cqType, BGroupeCalque _parent, FSigEditor _editor) {
    super(_title, _icon, "CREER");
    editor_=_editor;
    parent_=_parent;
    cqType_=_cqType;
  }

  public void actionPerformed(final ActionEvent _e) {
    ZCalqueAffichageDonnees cq=MdlLayerFactory.getInstance().createLayer(cqType_, editor_);
    cq.setName(BGroupeCalque.findUniqueChildName(parent_, ((MdlLayerInterface)cq).getExtName()));
    cq.setTitle(parent_.findUniqueChildTitle(cq.getTitle()));
    CtuluCommand cmd=new OperationForLayerCreation(parent_,cq);
    cmd.redo();
    
    if (editor_.getMng()!=null) {
      editor_.getMng().addCmd(cmd);
    }
  }
}
