/*
 *  @creation     1 avr. 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.modeleur.action;

import java.awt.event.ActionEvent;

import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.modeleur.MdlImplementation;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Une action pour visualiser la fenetre 1D.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MdlShow1DFrameAction extends EbliActionSimple {
  MdlImplementation impl_;

  /**
   * @param _impl L'implementation deu modeleur.
   */
  public MdlShow1DFrameAction(MdlImplementation _impl) {
    super(MdlResource.getS("Vue 1D"), null, "VUE1D");
    impl_=_impl;
  }

  public void actionPerformed(final ActionEvent _e) {
    impl_.install1dFrame();
  }
}
