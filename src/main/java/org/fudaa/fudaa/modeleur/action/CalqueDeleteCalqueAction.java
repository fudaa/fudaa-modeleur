/*
 *  @creation     1 avr. 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.modeleur.action;

import java.awt.event.ActionEvent;

import javax.swing.tree.TreePath;

import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluNamedCommand;
import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.calque.action.TreeDeleteCalqueAction;
import org.fudaa.fudaa.modeleur.layer.MdlLayerInterface;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 * Une action pour supprimer un calque, avec gestion du undo/redo.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class CalqueDeleteCalqueAction extends TreeDeleteCalqueAction {

  /** Une op�ration de destruction d'un calque */
  private class OperationForLayerDestruction implements CtuluNamedCommand {
    private final BCalque parent_;
    private final ZCalqueAffichageDonnees la_;
    private final int idx_;

    /**
     * @param _parent le calque parent
     * @param _la le calque ajout�
     * @param _idx Position du calque.
     */
    OperationForLayerDestruction(BCalque _parent, ZCalqueAffichageDonnees _la, int _idx) {
      parent_ = _parent;
      la_ = _la;
      idx_=_idx;
    }

    public void redo() {
      la_.detruire();
    }

    public void undo() {
      parent_.add(la_,idx_);
    }
    
    public String getName() {
      return MdlResource.getS("D�truire calque");
    }
  }
  
  FSigEditor editor_;

  /**
   * Construit l'action.
   * @param _editor L'�diteur.
   * @param _mdl Le mod�le de l'arbre des calques.
   */
  public CalqueDeleteCalqueAction(FSigEditor _editor, BArbreCalqueModel _mdl) {
    super(_mdl);
    setDefaultToolTip(MdlResource.getS("Destruction du calque"));
    editor_=_editor;
  }

  public void actionPerformed(final ActionEvent _e) {
    if (treeModel_==null) return;
    
    // On conserve le traitement par d�faut pour tous les calques autres que modeleur.
    if (!(treeModel_.getSelectedCalque() instanceof MdlLayerInterface)) {
      super.actionPerformed(_e);
      return;
    }
    
    if (treeModel_.getSelection().length>1) {
      editor_.getUi().error(MdlResource.getS("Destruction non autoris�e"),
          MdlResource.getS("La destruction simultan�e de plusieurs calques n'est pas autoris�e."), false);
      return;
    }
    
    ZCalqueAffichageDonnees cqSel=(ZCalqueAffichageDonnees)treeModel_.getSelectedCalque();
    BGroupeCalque gc=(BGroupeCalque)cqSel.getParent();
    BCalque[] cqs=gc.getCalques();
    int nbexist=0;
    for (BCalque cq: cqs) {
      if (cq.getName().indexOf(((MdlLayerInterface)cqSel).getExtName())!=-1) nbexist++;
    }
    if (nbexist<=1) {
      editor_.getUi().error(MdlResource.getS("Destruction non autoris�e"),
          MdlResource.getS("La destruction n'est pas autoris�e.\nIl doit exister au moins 1 calque de ce type."), false);
      return;
    }
    if (cqSel.modeleDonnees().getNombre()!=0) {
      if (!editor_.getUi().question(MdlResource.getS("Confirmez la destruction"),
          MdlResource.getS("Le calque contient des g�om�tries.\nVoulez vous poursuivre la destruction ?")))
        return;
    }

    int ind=0;
    while (ind<cqs.length && cqs[ind]!=cqSel) ind++;
    CtuluCommand cmd=new OperationForLayerDestruction(gc,cqSel,ind);
    cmd.redo();
    cqSel.clearSelection();
    
    if (editor_.getMng()!=null) {
      editor_.getMng().addCmd(cmd);
    }
    final BCalque[] c = treeModel_.getSelection();
    if (c.length == 0) {
      return;
    }
    final TreePath[] parent = treeModel_.getSelectionParent();
    for (int i = 0; i < c.length; i++) {
      c[i].detruire();
    }
    treeModel_.getTreeSelectionModel().setSelectionPaths(parent);
  }

//  public void actionPerformed(final ActionEvent _e) {
//    ZCalqueAffichageDonnees cq=MdlLayerFactory.getInstance().createLayer(cqType_, editor_);
//    cq.setName(BGroupeCalque.findUniqueChildName(parent_, ((MdlLayerInterface)cq).getExtName()));
//    CtuluCommand cmd=new OperationForLayerCreation(parent_,cq);
//    cmd.redo();
//    
//    if (editor_.getMng()!=null) {
//      editor_.getMng().addCmd(cmd);
//    }
//  }
}
