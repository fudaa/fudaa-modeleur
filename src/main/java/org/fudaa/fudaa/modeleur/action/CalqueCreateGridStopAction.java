/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.action;

import com.memoire.bu.BuResource;
import java.awt.event.ActionEvent;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Permet d'arr�ter la triangulation en cours.
 *
 * @author Frederic Deniger
 */
public class CalqueCreateGridStopAction extends EbliActionSimple {

  private final CalqueCreateGridAction action;
  private boolean running;

  /**
   *
   * @param action l'action associ�e
   */
  public CalqueCreateGridStopAction(CalqueCreateGridAction action) {
    super(MdlResource.getS("Stopper le maillage"), BuResource.BU.getIcon("arreter"), "STOP_GRID");
    this.action = action;
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    action.stop();
  }

  @Override
  public void updateStateBeforeShow() {
    setEnabled(running);
  }

  protected void setRunning(boolean running) {
    this.running = running;
    setEnabled(running);
  }

  @Override
  public void setEnabled(boolean _newValue) {
    super.setEnabled(_newValue && running);
  }
}
