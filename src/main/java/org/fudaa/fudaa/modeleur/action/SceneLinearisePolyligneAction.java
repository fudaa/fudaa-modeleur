/*
 * @creation     2 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.action;

import java.awt.event.ActionEvent;

import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.modeleur.MdlSceneEditor;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Action permettant de rendre rectiligne un profil.
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class SceneLinearisePolyligneAction extends EbliActionSimple implements ZSelectionListener {
  protected MdlSceneEditor sceneEditor_;
  
  public SceneLinearisePolyligneAction(MdlSceneEditor _sceneEditor) {
    super(MdlResource.getS("Rendre rectiligne des polylignes"), null, "RECTILIGNE_POLYLIGNE");
    sceneEditor_=_sceneEditor;
    sceneEditor_.getScene().addSelectionListener(this);
  }

  public void actionPerformed(final ActionEvent _e) {
    sceneEditor_.linearisePolyligne();
  }

  public String getEnableCondition() {
    return MdlResource.getS("<p>En mode sommet, s�lectionner au moins deux points pour chaque polyligne<p>En mode global, s�lectionner une ou plusieurs polyligne(s).");
  }

  public void selectionChanged(ZSelectionEvent _evt) {
    ZScene scn=sceneEditor_.getScene();
    // Au moins une et seulement des polylignes de selectionn�es. Et si en mode
    // atomique, deux sommets au moins doivent �tre selectionn�s pour chacune d'entre
    // elles.
    int[] geomSelected=scn.getSelectionHelper().getSelectedIndexes();
    boolean b=geomSelected.length>0;
    int i=0;
    while (b&&i<geomSelected.length) {
      b=scn.getObject(geomSelected[i]) instanceof GISPolyligne;
      if (scn.isAtomicMode())
        b=b&&scn.getLayerSelectionMulti()!=null&&scn.getLayerSelectionMulti().getSelection(geomSelected[i]).getNbSelectedIndex()>=2;
      i++;
    }
    setEnabled(b);
  }
}
