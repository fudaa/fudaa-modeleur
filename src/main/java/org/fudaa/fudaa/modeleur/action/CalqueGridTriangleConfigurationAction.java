/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.action;

import com.memoire.bu.BuResource;
import java.awt.event.ActionEvent;
import java.io.File;
import org.apache.commons.lang.StringUtils;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.modeleur.MdlPreferences;
import org.fudaa.fudaa.modeleur.grid.CalqueCreateGridTriangleProcessor;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 *
 * @author Frederic Deniger
 */
public class CalqueGridTriangleConfigurationAction extends EbliActionSimple {

  public CalqueGridTriangleConfigurationAction() {
    super(MdlResource.getS("Configurer l'ex�cutable Triangle"), BuResource.BU.getIcon("configurer"), "CONFIGURE_TRIANGLE");
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    chooseExec();
  }

  /**
   *
   * @return true if the user accept the dialog
   */
  public static boolean chooseExec() {
    ChooseExePanel pn = new ChooseExePanel();
    if (pn.afficheModaleOk(null, MdlResource.getS("Configurer l'ex�cutable Triangle"))) {
      MdlPreferences.MDL.putStringProperty("grid.triangle.path", pn.getChoosenFile().getAbsolutePath());
      MdlPreferences.MDL.writeIniFile();
      return true;
    }
    return false;
  }

  public static class ChooseExePanel extends CtuluDialogPanel {

    CtuluFileChooserPanel chooseExe;

    public ChooseExePanel() {
      super(true);
      setHelpText(MdlResource.getS("Choisir le chemin vers l'ex�cutable"));
      chooseExe = addFileChooserPanel(this, MdlResource.getS("Chemin") + ": ", false, false);
      String triangleExe = CalqueCreateGridTriangleProcessor.getTriangleExe();
      if (StringUtils.isNotBlank(triangleExe)) {
        chooseExe.setFile(new File(triangleExe));
      }
      add(chooseExe);
      isDataValid();
    }

    public File getChoosenFile() {
      return chooseExe.getFile();
    }

    @Override
    public boolean isDataValid() {
      File choosenFile = getChoosenFile();
      if (choosenFile == null) {
        setErrorText(MdlResource.getS("Le chemin vers l'ex�cutable n'est pas d�fini"));
        return false;
      } else if (!choosenFile.exists()) {
        setErrorText(MdlResource.getS("Le chemin vers l'ex�cutable n'existe pas: {0}", choosenFile.getAbsolutePath()));
        return false;
      }
      return super.isDataValid();
    }
  }
}
