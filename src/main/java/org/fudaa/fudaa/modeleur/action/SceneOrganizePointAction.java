/*
 * @creation     22 janv. 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.action;

import java.awt.event.ActionEvent;

import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.modeleur.MdlSceneEditor;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Allow to reorganize geometries with criterion. The criterions can be x or y.
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class SceneOrganizePointAction extends EbliActionSimple implements ZSelectionListener {
  
  public static final String x="x";
  public static final String y="y";
  
  /** The criterion used */
  protected String criterion_;
  protected MdlSceneEditor sceneEditor_;

  /**
   * @param _sceneEditor
   * @param _criterion
   *          the criterion, can be x or y. Put directly the string "x" or
   *          SceneOrganizePoint.x. As you want.
   */
  public SceneOrganizePointAction(MdlSceneEditor _sceneEditor, String _criterion) {
    super(MdlResource.getS("R�organisation sur")+(_criterion==null?"":" "+_criterion), null, "REORGANIZE_POLYLIGNE");
    sceneEditor_=_sceneEditor;
    criterion_=_criterion;
    if(sceneEditor_==null||criterion_==null)
      throw new IllegalArgumentException(MdlResource.getS("Les param�tres ne doivent pas �tre null"));
    if(!criterion_.equals(x)&&!criterion_.equals(y))
      throw new IllegalArgumentException(MdlResource.getS("Le crit�re n'a pas �t� reconnu."));
    sceneEditor_.getScene().addSelectionListener(this);
  }

  public void selectionChanged(ZSelectionEvent _evt) {
    setEnabled(!sceneEditor_.getScene().isSelectionEmpty());
  }
  
  public String getEnableCondition() {
    return MdlResource.getS("<p>S�lectionner une ou plusieurs g�om�trie(s).");
  }
  
  public void actionPerformed(final ActionEvent _e) {
    sceneEditor_.organizePoint(criterion_==x ? 0:1);
  }
}
