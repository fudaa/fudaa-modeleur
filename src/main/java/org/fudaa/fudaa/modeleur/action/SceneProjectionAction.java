/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.action;

import java.awt.event.ActionEvent;

import javax.swing.Action;

import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.modeleur.MdlSceneEditor;

import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Une action pour projeter une g�om�trie sur un ou des semis de points. La projection consiste �
 * initialiser le Z de la g�om�trie project�e.
 * @author Bertrand Marchand
 */
public class SceneProjectionAction extends EbliActionSimple implements ZSelectionListener {
  MdlSceneEditor sceneEditor_;
  
  public SceneProjectionAction(MdlSceneEditor _sceneEditor) {
    super(MdlResource.getS("Projeter sur un semis"), null, "GIS_PROJECT");
//    setDefaultToolTip(MdlResource.getS("Projeter la g�om�trie sur un semis"));
    setSceneEditor(_sceneEditor);
    sceneEditor_.getScene().addSelectionListener(this);
  }

  public void actionPerformed(final ActionEvent _e) {
    sceneEditor_.projectSelectedObject();
  }

  /**
   * @param _sceneEditor L'editeur sp�cifique a la scene.
   */
  private void setSceneEditor(final MdlSceneEditor _sceneEditor) {
    sceneEditor_ = _sceneEditor;
  }

  public void updateForSelectionChanged() {
    ZScene scn=sceneEditor_.getScene();
//    SceneSelectionHelper hlp=sceneEditor_.getScene().getSelectionHelper();
//    int idGeom=-1;

    boolean b=true;
    // Si la selection n'est pas nulle
    b=b && !scn.isSelectionEmpty();
    // Si atomique
    String acname=b?(scn.isAtomicMode()?
        MdlResource.getS("Projeter les sommets sur un semis"):
        MdlResource.getS("Projeter les g�om�tries sur un semis")):
        MdlResource.getS("Projeter sur un semis");
    putValue(Action.NAME, acname);

    setEnabled(b);
  }

  public String getEnableCondition() {
    return MdlResource.getS("S�lectionner une g�om�trie ou des sommets");
  }

  /* (non-Javadoc)
   * @see org.fudaa.ebli.calque.ZSelectionListener#selectionChanged(org.fudaa.ebli.calque.ZSelectionEvent)
   */
  public void selectionChanged(ZSelectionEvent _evt) {
    updateForSelectionChanged();
  }
}
