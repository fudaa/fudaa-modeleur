/*
 *  @creation     1 avr. 2005
 *  @modification $Date: 2008-05-13 12:10:55 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.modeleur.action;

import com.memoire.bu.BuInternalFrame;
import org.fudaa.fudaa.modeleur.MdlCompareProfileAdapter;
import java.awt.event.ActionEvent;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ebli.calque.ZCalqueGeometry.SelectionMode;

import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.modeleur.FrameCompareProfile;
import org.fudaa.fudaa.modeleur.MdlCompareProfilePanel;
import org.fudaa.fudaa.modeleur.MdlVisuPanel;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Une action pour afficher une boite de dialogue pour comparer des profils,
 * et eventuellement les modifier.
 * 
 * @author marchand@deltacad.fr
 * @version $Id$
 */
public class SceneCompareProfileAction extends EbliActionSimple implements ZSelectionListener {

//  MdlSceneEditor sceneEditor_;
  MdlVisuPanel pnCalques_;
  /** Instanci� 1 seule fois pour conserver les options d'une ouverture � l'autre. */
  MdlCompareProfilePanel pnProfiles_;
    
  /**
   * @param _sceneEditor L'editeur sp�cifique a la scene.
   */
  public SceneCompareProfileAction(MdlVisuPanel _pn) {
    super(MdlResource.getS("Comparaison de profils"), null, "COMPARE_PROFILES");
    pnCalques_=_pn;
    pnCalques_.getScene().addSelectionListener(this);
//    setSceneEditor(_sceneEditor);
//    sceneEditor_.getScene().addSelectionListener(this);
    setEnabled(false);
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    ZScene scn=pnCalques_.getScene();
    
    // Construction du set profils depuis les profils saisis.
    int[] sceneIds=scn.getSelectionHelper().getSelectedIndexes();
    
    //ProfileAdapter[] profs=new ProfileAdapter[sceneIds.length];
    
    ZCalqueLigneBriseeEditable cqCommun=null;
    boolean isConsecutive=true;
    
    int idx=-1;
    for (int i=0; i<sceneIds.length; i++) {
      ZCalqueLigneBriseeEditable cq=(ZCalqueLigneBriseeEditable)scn.getLayerForId(sceneIds[i]);
      int idxGeom=scn.sceneId2LayerId(sceneIds[i]);
      // Premier passage
      if (cqCommun==null) {
        cqCommun=cq;
      }
      // Les g�om�tries ne sont pas toutes sur le m�me calque => Sortie
      else if (cqCommun!=cq) {
        cqCommun=null;
        isConsecutive=false;
        break;
      }
      // Les g�om�tries ne sont pas cons�cutives
      else if (idxGeom!=idx+1) {
        isConsecutive=false;
      }
      idx=idxGeom;
    }
    
    MdlCompareProfileAdapter[] profs;
    MdlCompareProfileSet profSet;
    int indsel;
    
    // Possibilit� de naviguer sur les profils => Set avec tous les profils du calque.
    if (isConsecutive) {
      GISZoneCollectionLigneBrisee zone=cqCommun.modeleDonnees().getGeomData();
      profs=new MdlCompareProfileAdapter[zone.getNumGeometries()];
      for (int i=0; i<zone.getNumGeometries(); i++) {
        profs[i]=new MdlCompareProfileAdapter(i, zone);
      }
      profSet=new MdlCompareProfileSet(profs);
      indsel=idx;
    }
    // Pas de possibilit� de naviguer sur les profils du calque 
    // => Le set n'est constitu� que des profils s�lectionn�s.
    else {
      profs=new MdlCompareProfileAdapter[sceneIds.length];
      
      for (int i=0; i<sceneIds.length; i++) {
        ZCalqueLigneBriseeEditable cq=(ZCalqueLigneBriseeEditable)scn.getLayerForId(sceneIds[i]);
        int idxGeom=scn.sceneId2LayerId(sceneIds[i]);
        profs[i]=new MdlCompareProfileAdapter(idxGeom, cq.modeleDonnees().getGeomData());
      }
      
      profSet=new MdlCompareProfileSet(profs);
      indsel=0;
    }
    
        if (pnProfiles_ == null) {
            pnProfiles_ = new MdlCompareProfilePanel(pnCalques_);
        }

        pnProfiles_.setProfileSet(profSet);
        pnProfiles_.setSelectedProfile(indsel);
        FrameCompareProfile internalFrame = new FrameCompareProfile(pnProfiles_);
        pnCalques_.getImpl().addInternalFrame(internalFrame);
    }

  /**
   * Autoris� si les g�om�tries sont sur des calques ZCalqueLigneBrisee.
   */
  public void updateForSelectionChanged() {
    ZScene scn=pnCalques_.getScene();
    boolean b=true;
    b&=!scn.isSelectionEmpty();
    b&=scn.getSelectionMode()==SelectionMode.NORMAL;
    b&=scn.getSelectionHelper().getSelectedIndexes().length>=1;
      
    int[] sceneIds=scn.getSelectionHelper().getSelectedIndexes();
    for (int i=0; i < sceneIds.length; i++) {
      int layerId=scn.sceneId2LayerId(sceneIds[i]);
      b&=scn.getLayerForId(sceneIds[i]) instanceof ZCalqueLigneBriseeEditable;
      if (!b) break;
      ZCalqueLigneBriseeEditable cq=(ZCalqueLigneBriseeEditable)scn.getLayerForId(sceneIds[i]);
      GISAttributeInterface attZ=cq.modeleDonnees().getGeomData().getAttributeIsZ();
      b&=attZ!=null && attZ.isAtomicValue();
      if (!b) break;
      b&=!cq.modeleDonnees().isGeometryFermee(layerId);
      if (!b) break;
    }

    super.setEnabled(b);
  }

  @Override
  public String getEnableCondition() {
    return MdlResource.getS("S�lectionner au moins une polyligne 3D");
  }

  /* (non-Javadoc)
   * @see org.fudaa.ebli.calque.ZSelectionListener#selectionChanged(org.fudaa.ebli.calque.ZSelectionEvent)
   */
  public void selectionChanged(ZSelectionEvent _evt) {
    updateForSelectionChanged();
  }
}
