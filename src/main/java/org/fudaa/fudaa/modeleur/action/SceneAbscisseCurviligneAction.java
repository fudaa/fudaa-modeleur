/*
 * @creation     20 nov. 2008
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.action;

import java.awt.Container;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.SwingUtilities;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ebli.calque.BCalqueInteraction;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.ZModeleGeometry;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.calque.edition.ZEditorDefault;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.fudaa.modeleur.layer.BPaletteAbscisseCurviligne;
import org.fudaa.fudaa.modeleur.layer.ZCalqueAbscisseCurviligneInteraction;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Point;

import com.memoire.bu.BuDesktop;

/**
 * Une action pour afficher une palette indiquant l'abscisse curviligne d'une polyligne.
 *
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class SceneAbscisseCurviligneAction extends EbliActionPaletteAbstract implements PropertyChangeListener, ZSelectionListener {

  private ZEbliCalquesPanel calquesPanel_;
  private ZScene scene_;
  // Calque ne fermant pas l'edition \\
  private BCalqueInteraction calqueZoom_;
  private BCalqueInteraction calqueDeplacementVue_;
  /**
   * Le calque contenant les informations de distance.
   */
  private ZCalqueAbscisseCurviligneInteraction calqueAbscisseCurviligne_;
  /**
   * La geometrie selectionn� sur laquelle le calcul de l'abcisse curviligne est effectu�.
   */
  private LineString selectedGeometry_;
  /**
   * La palette affichant la donn�e curviligne.
   */
  private BPaletteAbscisseCurviligne palette_;

  /**
   * Classe g�rant la disparition et la r�apparition du panel quand la fen�tre principale est r�duite.
   */
  class AbcisseFrameListener extends InternalFrameAdapter {

    public void internalFrameActivated(final InternalFrameEvent _e) {
      if (window_ != null) {
        window_.setVisible(true);
      }
    }

    public void internalFrameDeactivated(final InternalFrameEvent _e) {
      if (window_ != null) {
        window_.setVisible(false);
      }
    }
  }

  public SceneAbscisseCurviligneAction(final ZEditorDefault _editor) {
    super(MdlResource.getS("Calcul de l'abscisse curviligne"), null, "PALETTE_ABSCISSE_CURVILIGNE");
    scene_ = _editor.getSceneEditor().getScene();
    scene_.addSelectionListener(this);
    calquesPanel_ = _editor.getPanel();
    calquesPanel_.getController().addPropertyChangeListener("gele", this);
    calqueZoom_ = calquesPanel_.getController().getCalqueInteraction("cqAGRANDIR");
    calqueDeplacementVue_ = calquesPanel_.getController().getCalqueInteraction("cqDEPLACEMENT_VUE-I");
    calqueAbscisseCurviligne_ = new ZCalqueAbscisseCurviligneInteraction(this);
    calqueAbscisseCurviligne_.setGele(true);
    calquesPanel_.addCalqueInteraction(calqueAbscisseCurviligne_);
    calqueAbscisseCurviligne_.addPropertyChangeListener("gele", this);
    setEnabled(false);
  }

  public String getEnableCondition() {
    return MdlResource.getS("<p>S�lectionner une polyligne ou un polygone.</p>");
  }

  public void propertyChange(PropertyChangeEvent _evt) {
    // Cas o� un autre calque que celui du zoom, du deplacement de vue et d'edition
    // est d�gel� donc on ferme le pannel d'edition si il est ouvert.
    if (_evt.getSource() != calqueZoom_ && _evt.getSource() != calqueDeplacementVue_ && _evt.getSource() != calqueAbscisseCurviligne_
            && !((Boolean) _evt.getNewValue()).booleanValue() && isSelected()) {
      setSelected(false);
      calqueAbscisseCurviligne_.stopEdition();
      ((BPaletteAbscisseCurviligne) palette_).setAbcisseCurviligne(0);
      super.hideWindow();
    }
  }

  public void showWindow() {
    // Necessaire, sinon la palette est visualis�e en externe. Ne peut �tre fait avant, car le desktop n'est pas
    // encore existant.
    setDesktop((BuDesktop) SwingUtilities.getAncestorOfClass(BuDesktop.class, calquesPanel_));
    calquesPanel_.setCalqueInteractionActif(calqueAbscisseCurviligne_);
    super.showWindow();
  }

  public void hideWindow() {
    calquesPanel_.unsetCalqueInteractionActif(calqueAbscisseCurviligne_);
    calqueAbscisseCurviligne_.stopEdition();
    if (calquesPanel_.getController().getCqSelectionI() != null) {
      calquesPanel_.getController().getCqSelectionI().setGele(false);
    }
    ((BPaletteAbscisseCurviligne) palette_).setAbcisseCurviligne(0);
    super.hideWindow();
  }

  /* (non-Javadoc)
   * @see org.fudaa.ebli.calque.ZSelectionListener#selectionChanged(org.fudaa.ebli.calque.ZSelectionEvent)
   */
  public void selectionChanged(ZSelectionEvent _evt) {
    /*
     * Si une seule g�om�trie est selectionn�e et que cette g�om�trie est une
     * polyligne ou un polygone, alors l'action de calcul d'abcisses curvilignes
     * est activable et un ref�rence sur cette g�om�trie est concerv�e dans
     * 'selectedGeometry_'.
     */
    if (scene_.isOnlyOneObjectSelected() && scene_.sceneId2LayerId(scene_.getSelectionHelper().getUniqueSelectedIdx()) != -1) {
      int idxGeom = scene_.getSelectionHelper().getUniqueSelectedIdx();
      ZCalqueAffichageDonneesInterface calque = scene_.getLayerForId(idxGeom);
      if (!(calque.modeleDonnees() instanceof ZModeleGeometry)) {
        return;
      }
      Geometry geom = ((ZModeleGeometry) calque.modeleDonnees()).getGeomData().getGeometry(scene_.sceneId2LayerId(idxGeom));
      setEnabled(calque.isOnlyOneObjectSelected() && (geom instanceof GISPolyligne || geom instanceof GISPolygone));
      if (isEnabled()) {
        selectedGeometry_ = ((LineString) geom);
      } else {
        selectedGeometry_ = null;
      }
    } else {
      setEnabled(false);
      selectedGeometry_ = null;
    }
  }

  /**
   * Est appel� par le calque d'interaction pour indiquer le point cliqu�.
   *
   * @param _coord la coordonn� r�el.
   * @return la coordonn� projet� si le point click� est dans la tol�rance, null sinon.
   */
  public Coordinate coordinateClicked(Coordinate _coord, double tolerance) {
    Point ptClick = new GeometryFactory().createPoint(_coord);
    if (selectedGeometry_ != null && selectedGeometry_.distance(ptClick) < tolerance) {
      // Recherche de l'index du dernier point de la polyligne � prendre en
      // compte.
      boolean fini = false;
      int i = -1;
      CoordinateSequence coordSeq = selectedGeometry_.getCoordinateSequence();
      while (!fini && ++i < selectedGeometry_.getNumPoints() - 1) {
        fini = (GISGeometryFactory.INSTANCE
                .createLineString(new Coordinate[]{coordSeq.getCoordinate(i), coordSeq.getCoordinate(i + 1)})).distance(ptClick) < tolerance;
      }
      // Calcule de l'abscisse curviligne
      double valueCurviligne = 0;
      for (int j = 1; j <= i; j++) {
        valueCurviligne += coordSeq.getCoordinate(j).distance(coordSeq.getCoordinate(j - 1));
      }
      // Point projet�
      GrPoint pp = new GrSegment(new GrPoint(coordSeq.getCoordinate(i)), new GrPoint(coordSeq.getCoordinate(i + 1))).pointPlusProche(new GrPoint(_coord));
      Coordinate ppc = new Coordinate(pp.x_, pp.y_, pp.z_);
      // Mise � jour de l'abscisse curviligne
      valueCurviligne += coordSeq.getCoordinate(i).distance(ppc);
      ((BPaletteAbscisseCurviligne) palette_).setAbcisseCurviligne(valueCurviligne);
      return ppc;
    } else if (selectedGeometry_ != null) {
      ((BPaletteAbscisseCurviligne) palette_).setMessage(MdlResource.getS("Pas de valeur"));
    }
    return null;
  }

  @Override
  public JComponent buildContentPane() {
    if (palette_ == null) {
      // Cr�ation de l'internal frame affichant l'information curviligne
      palette_ = new BPaletteAbscisseCurviligne(calqueAbscisseCurviligne_, calquesPanel_.getController().getEbliFormatter(),
              calquesPanel_);
      // Ecoute de la fen�tre principale pour pouvoir se r�duire avec elle
      final Container c = SwingUtilities.getAncestorOfClass(JInternalFrame.class, calquesPanel_);
      if (c instanceof JInternalFrame) {
        ((JInternalFrame) c).addInternalFrameListener(new AbcisseFrameListener());
      }
    }
    return palette_.getComponent();
  }
}