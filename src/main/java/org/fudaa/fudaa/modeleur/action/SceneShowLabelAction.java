/*
 * @creation     14 nov. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.action;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;

import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueVisitor;
import org.fudaa.ebli.calque.ZCalqueGeometry;
import org.fudaa.ebli.commun.EbliActionChangeState;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Une action pour afficher les labels sur les sommets dans les calques.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class SceneShowLabelAction extends EbliActionChangeState implements TreeModelListener {
    
  protected BArbreCalqueModel treeModel_;
    
  public SceneShowLabelAction(BArbreCalqueModel _treeModel) {
      super(MdlResource.getS("Afficher les labels sur les sommets"), null, "SHOW_LABELS");
      treeModel_=_treeModel;
      treeModel_.addTreeModelListener(this);
      setEnabled(true);
      setSelected(true);
    }

    private void applyConf(){
      treeModel_.getRootCalque().apply(new BCalqueVisitor(){
        public boolean visit(BCalque _cq) {
          if(_cq instanceof ZCalqueGeometry)
            ((ZCalqueGeometry)_cq).setAttributForLabels(isSelected()?GISAttributeConstants.LABEL:null);
          return true;
        }
      });
    }
    
    /* (non-Javadoc)
     * @see javax.swing.event.TreeModelListener#treeNodesChanged(javax.swing.event.TreeModelEvent)
     */
    public void treeNodesChanged(TreeModelEvent e) {
    }

    /* (non-Javadoc)
     * @see javax.swing.event.TreeModelListener#treeNodesInserted(javax.swing.event.TreeModelEvent)
     */
    public void treeNodesInserted(TreeModelEvent e) {
      applyConf();
    }

    /* (non-Javadoc)
     * @see javax.swing.event.TreeModelListener#treeNodesRemoved(javax.swing.event.TreeModelEvent)
     */
    public void treeNodesRemoved(TreeModelEvent e) {
    }

    /* (non-Javadoc)
     * @see javax.swing.event.TreeModelListener#treeStructureChanged(javax.swing.event.TreeModelEvent)
     */
    public void treeStructureChanged(TreeModelEvent e) {
    }

    /* (non-Javadoc)
     * @see org.fudaa.ebli.commun.EbliActionChangeState#changeAction()
     */
    @Override
    public void changeAction() {
      applyConf();
    }
}
