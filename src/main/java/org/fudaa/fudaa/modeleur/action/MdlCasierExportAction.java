/*
 * @creation     26 nov. 2008
 * @modification $Date: 2009-03-06 15:58:09 +0100 (ven., 06 mars 2009) $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.action;

import java.awt.event.ActionEvent;
import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZCalqueGeometry;
import org.fudaa.ebli.calque.ZModeleGeometry;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.modeleur.MdlCasierExportPanel;
import org.fudaa.fudaa.modeleur.MdlCasierExporter;
import org.fudaa.fudaa.modeleur.MdlCasierExporterFilter;
import org.fudaa.fudaa.modeleur.MdlEditionManager;
import org.fudaa.fudaa.modeleur.resource.MdlResource;


/**
 * Une action pour exporter des casiers.
 * @author Bertrand Marchand
 */
public class MdlCasierExportAction extends EbliActionSimple {
  MdlEditionManager editor_;
  
  /** Le dialogue permettant l'export des géométries */
  MdlCasierExportPanel pnExport_;
  
  public MdlCasierExportAction(MdlEditionManager _editor) {
    super(MdlResource.getS("Exporter les casiers..."), MdlResource.MDL.getIcon("exporter-casier"), "EXPORT_CASIER");
    setDefaultToolTip(MdlResource.getS("Exporte les casiers de la vue 2D"));
    editor_=_editor;
  }

  public void actionPerformed(final ActionEvent _e) {
    // Les exporters de fichier casiers.
    MdlCasierExporter[] exporters=new MdlCasierExporter[2];
    exporters[0]=new MdlCasierExporter.Rubar();
    exporters[1]=new MdlCasierExporter.Mascaret();
    
    ZScene scn=editor_.getSupport();
    boolean bpreselect=
      !scn.isAtomicMode() && 
      !scn.isSelectionEmpty() && 
      containsOnlyCasiers();

    if (pnExport_==null)
      pnExport_=new MdlCasierExportPanel(exporters,editor_.getUi(),editor_.getPanel());
    
    pnExport_.update(bpreselect);
    
    if (pnExport_.afficheModaleOk(editor_.getFrame(), MdlResource.getS("Export de casiers"))) {
      
      // Récupération du modele des casiers a exporter.
      MdlCasierExporterFilter flt=new MdlCasierExporterFilter(editor_.getPanel().getArbreCalqueModel());
      flt.setTreatmentOnlySelectedGeometries(pnExport_.isExportOnSelectedGeometries());
      flt.setTreatmentOnlyOnVisibleLayers(pnExport_.isExportOnVisibleLayers());
      flt.setTreatmentOnlyOnSelectedLayers(pnExport_.isExportOnSelectedLayers());
      GISDataModel mdCasiers=flt.filter();
      
      // Exporter et fichier selectionnés.
      File f=pnExport_.getFile();
      MdlCasierExporter exporter=pnExport_.getSelectedExporter();
      
      // Export.
      CtuluIOOperationSynthese synt=exporter.export(f,scn,mdCasiers,pnExport_.getSelection());
      editor_.getUi().manageErrorOperationAndIsFatal(synt);
    }
  }

  public String getEnableCondition() {
    return MdlResource.getS("Sélectionner des casiers.");
  }
  
  /**
   * Controle que la selection ne contient que des contours de casiers.
   * @return True si la selection ne contient que des casiers ou selection vide.
   */
  public boolean containsOnlyCasiers() {
    ZScene scn=editor_.getSupport();
    
    for (int i=scn.getLayerSelection().getMinIndex(); i<=scn.getLayerSelection().getMinIndex(); i++) {
      if (!scn.getLayerSelection().isSelected(i)) continue;

      ZCalqueAffichageDonneesInterface calque=scn.getLayerForId(i);
      int idGeom=scn.sceneId2LayerId(i);

      if(!(calque instanceof ZCalqueGeometry)) return false;
      
      GISZoneCollection zone=((ZModeleGeometry)calque.modeleDonnees()).getGeomData();
      int iattNat=zone.getIndiceOf(GISAttributeConstants.NATURE);
      if (iattNat==-1||!zone.getValue(iattNat, idGeom).equals(GISAttributeConstants.ATT_NATURE_CA))
        return false;
    }
    return true;
  }
}
