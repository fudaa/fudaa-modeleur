/*
 *  @creation     1 avr. 2005
 *  @modification $Date: 2008-05-13 12:10:55 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.modeleur.action;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.HashMap;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.mascaret.io.MascaretGEO2dFileFormat;
import org.fudaa.dodico.rubar.io.RubarStCnFileFormat;

import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.modeleur.MdlImplementation;
import org.fudaa.fudaa.modeleur.modeleur1d.model.Bief;
import org.fudaa.fudaa.modeleur.modeleur1d.view.ImportBiefPanel;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Une action pour importer un bief, depuis le 2D ou le 1D.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MdlBiefImportAction extends EbliActionSimple {

  MdlImplementation impl_;

  /**
   * Un importateur d'un bief depuis un fichier.
   * @author Bertrand Marchand
   * @version $Id$
   */
  public interface Importer1d {

    /**
     * Import des donn�es d'un bief.
     * @param _f Le fichier d'�criture du bief.
     * @param _bief Le bief.
     * @param _name Le nom du bief.
     * @return La synth�se. CtuluIOOperationSynthese#getSource() permet de
     * r�cuperer le bief.
     */
    public CtuluIOOperationSynthese importer(File _f);

    /**
     * Retourne le file format associ� � l'exporter.
     * @return Le file format.
     */
    public FileFormatVersionInterface getFileFormat();

    /**
     * Une classe abstraite d'import depuis Mascaret.
     * @author Bertrand Marchand
     * @version $Id$
     */
    class Mascaret2d implements Importer1d {

      public CtuluIOOperationSynthese importer(File _file) {

        // Lecture du fichier
        FileReadOperationAbstract reader=getFileFormat().createReader();
        CtuluIOOperationSynthese synt=reader.read(_file, null);
        if (synt.containsSevereError()) {
          return synt;
        }

        // Le nom du bief.
        String name=(String)((Object[])synt.getSource())[0];
        // Les lignes bris�es, traces, profils, axe, rives, lim stockage.
        GISZoneCollectionLigneBrisee[] zones=(GISZoneCollectionLigneBrisee[])((Object[])synt.getSource())[1];
        Bief bief=new Bief(
            new ZModeleLigneBriseeEditable(zones[2]), // Axe
            new ZModeleLigneBriseeEditable(),         // Lignes cont
            new ZModeleLigneBriseeEditable(),         // Lignes dir
            new ZModeleLigneBriseeEditable(zones[4]), // Limites stock
            new ZModeleLigneBriseeEditable(zones[1]), // Profils
            new ZModeleLigneBriseeEditable(zones[3]));// Rives
        bief.setName(name);

        synt.setSource(bief);
        return synt;
      }

      public FileFormatVersionInterface getFileFormat() {
        return MascaretGEO2dFileFormat.getInstance();
      }
    }

    /**
     * Une classe abstraite d'import depuis Rubar.
     * @author Bertrand Marchand
     * @version $Id$
     */
    class Rubar implements Importer1d {

      public CtuluIOOperationSynthese importer(File _file) {

        // Lecture du fichier
        FileReadOperationAbstract reader=getFileFormat().createReader();
        CtuluIOOperationSynthese synt=reader.read(_file, null);
        if (synt.containsSevereError()) {
          return synt;
        }

        // Les lignes bris�es, profils, lignes directrices.
        GISZoneCollectionLigneBrisee[] zones=(GISZoneCollectionLigneBrisee[])synt.getSource();
        Bief bief=new Bief(
            new ZModeleLigneBriseeEditable(),         // Axe
            new ZModeleLigneBriseeEditable(),         // Lignes cont
            zones[1]!=null ? new ZModeleLigneBriseeEditable(zones[1]):new ZModeleLigneBriseeEditable(), // Lignes dir
            new ZModeleLigneBriseeEditable(),         // Limites stock
            zones[0]!=null ? new ZModeleLigneBriseeEditable(zones[0]):new ZModeleLigneBriseeEditable(), // Profils
            new ZModeleLigneBriseeEditable());        // Rives

        synt.setSource(bief);
        return synt;
      }

      public FileFormatVersionInterface getFileFormat() {
        return RubarStCnFileFormat.getInstance();
      }
    }
  }

  /**
   * Cr�ation de l'action.
   */
  public MdlBiefImportAction(MdlImplementation _impl) {
    super(MdlResource.getS("Importer"), MdlResource.MDL.getIcon("crystal_importer"), "IMPORTER_BIEF");
    setDefaultToolTip(MdlResource.getS("Importer un bief"));
    impl_=_impl;
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    // Les formats importables.
    HashMap<FileFormatVersionInterface, Importer1d> ff2Importer=new HashMap<FileFormatVersionInterface, Importer1d>();
    ff2Importer.put(MascaretGEO2dFileFormat.getInstance(), new Importer1d.Mascaret2d());
    ff2Importer.put(RubarStCnFileFormat.getInstance(), new Importer1d.Rubar());

    FileFormatVersionInterface[] ffs=ff2Importer.keySet().toArray(new FileFormatVersionInterface[0]);

    // Choix du fichier a exporter, donc du format.
    ImportBiefPanel pnImport=new ImportBiefPanel(impl_,ffs);
    if(!pnImport.run())
      return;

    File file=pnImport.getFile();
    FileFormatVersionInterface ff=null;
    for (int i=0; i<ffs.length; i++) {
      if (ffs[i].getFileFormat().createFileFilter().accept(file)) {
        ff=ffs[i];
        break;
      }
    }
    if (ff==null) return;

    // Import
    CtuluIOOperationSynthese result=ff2Importer.get(ff).importer(file);
    impl_.manageAnalyzeAndIsFatal(result.getAnalyze());
    
    if (result.containsSevereError())
      return;

    // Creation du bief
    Bief bief=(Bief)result.getSource();
    
    impl_.get2dFrame().getMdlVisuPanel().buildBiefLayers(bief,true);
    impl_.get2dFrame().getMdlVisuPanel().restaurer();
    impl_.get1dFrame().getController().updateFrom2d();
  }
}
