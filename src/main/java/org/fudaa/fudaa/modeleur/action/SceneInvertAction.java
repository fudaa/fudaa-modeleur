/*
 * @creation     14 nov. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.action;

import java.awt.event.ActionEvent;

import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.calque.ZScene.SceneSelectionHelper;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.modeleur.MdlSceneEditor;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dLine;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Action permettant le changement de sens des polylignes selectionnées.
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class SceneInvertAction extends EbliActionSimple implements ZSelectionListener {
  
  protected MdlSceneEditor sceneEditor_;
  
  public SceneInvertAction(MdlSceneEditor _sceneEditor){
    super(MdlResource.getS("Inverser l'orientation"), null, "INVERSER");
    sceneEditor_=_sceneEditor;
    sceneEditor_.getScene().addSelectionListener(this);
  }

  public void actionPerformed(final ActionEvent _e) {
    sceneEditor_.invertOrientationSelectedGeometries();
  }
  
  /**
   * Autorisée si 1 ou plusieurs géométries sont complètes.
   */
  public void updateForSelectionChanged() {
    ZScene scn=sceneEditor_.getScene();
    SceneSelectionHelper hlp=sceneEditor_.getScene().getSelectionHelper();
    
    boolean mdlLayer2dLine=true;
    int i=-1;
    int[] geom=hlp.getSelectedIndexes();
    while(mdlLayer2dLine&&++i<geom.length)
      mdlLayer2dLine=scn.getLayerForId(geom[i]) instanceof MdlLayer2dLine;
    
    super.setEnabled(!scn.isSelectionEmpty()&&!scn.isAtomicMode()&&mdlLayer2dLine);
  }

  public String getEnableCondition() {
    return MdlResource.getS("<p>En mode global, un ou plusieurs objet(s) de type polyligne ou polygone.</p>");
  }
  
  /* (non-Javadoc)
   * @see org.fudaa.ebli.calque.ZSelectionListener#selectionChanged(org.fudaa.ebli.calque.ZSelectionEvent)
   */
  public void selectionChanged(ZSelectionEvent _evt) {
    updateForSelectionChanged();
  }
}
