/*
 *  @creation     1 avr. 2005
 *  @modification $Date: 2008-05-13 12:10:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.modeleur.action;

import java.awt.event.ActionEvent;

import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.modeleur.MdlSceneEditor;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dLine;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dMultiPoint;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Une action pour d�placer un objet dans un autre calque. L'�tat actif/inactif de l'action d�pend du calque selectionn�.
 * @author Fred Deniger
 * @version $Id: SceneMoveInLayerAction.java,v 1.1.2.1 2008-05-13 12:10:53 bmarchan Exp $
 */
public class SceneMoveInLayerAction extends EbliActionSimple implements ZSelectionListener, TreeSelectionListener {

  MdlSceneEditor sceneEditor_;

  /**
   * @param _sceneEditor L'editeur sp�cifique a la scene.
   * @param _tree Le modele de l'arbre des calques
   */
  public SceneMoveInLayerAction(MdlSceneEditor _sceneEditor, BArbreCalqueModel _tree) {
    super(MdlResource.getS("D�placer dans le calque cible"), null/*EbliResource.EBLI.getToolIcon("draw-rotation")*/, "MOVE_IN_LAYER");
    setSceneEditor(_sceneEditor);
    sceneEditor_.getScene().addSelectionListener(this);
    _tree.addTreeSelectionListener(this);
  }

  public void actionPerformed(final ActionEvent _e) {
    sceneEditor_.moveInLayerSelectedGeometries();
  }

  /**
   * @param _sceneEditor L'editeur sp�cifique a la scene.
   */
  private void setSceneEditor(final MdlSceneEditor _sceneEditor) {
    sceneEditor_ = _sceneEditor;
  }

  /**
   * Autoris� si 2 sommets sont selectionn�s.
   */
  public void updateForSelectionChanged() {
    ZScene scn=sceneEditor_.getScene();
    boolean b=true;
    b&=!scn.isSelectionEmpty();
    b&=!scn.isAtomicMode();
    
    b&=(scn.getCalqueActif()!=null &&
        (scn.getCalqueActif() instanceof MdlLayer2dLine ||
         scn.getCalqueActif() instanceof MdlLayer2dMultiPoint));
    super.setEnabled(b);
  }

  public String getEnableCondition() {
    return MdlResource.getS("S�lectionner au moins une g�ometrie et un calque cible");
  }

  /* (non-Javadoc)
   * @see org.fudaa.ebli.calque.ZSelectionListener#selectionChanged(org.fudaa.ebli.calque.ZSelectionEvent)
   */
  public void selectionChanged(ZSelectionEvent _evt) {
    updateForSelectionChanged();
  }

  /* (non-Javadoc)
   * @see javax.swing.event.TreeSelectionListener#valueChanged(javax.swing.event.TreeSelectionEvent)
   */
  public void valueChanged(TreeSelectionEvent e) {
    updateForSelectionChanged();
  }
}
