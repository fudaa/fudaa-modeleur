/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.modeleur.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.fudaa.modeleur.MdlCompareProfileAdapter;
import org.fudaa.fudaa.modeleur.modeleur1d.model.ProfileSetI;
import org.fudaa.fudaa.modeleur.modeleur1d.model.ProfileSetListener;

/**
 * Un profil set pour comparaison de profils.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class MdlCompareProfileSet implements ProfileSetI {
  List<MdlCompareProfileAdapter> profs_;
  HashSet<ProfileSetListener> listeners_=new HashSet<ProfileSetListener>();
  int idxSelProf_;

  public MdlCompareProfileSet(MdlCompareProfileAdapter[] _profs) {
    profs_=new ArrayList<MdlCompareProfileAdapter>(Arrays.asList(_profs));
  }

  public int getNbProfil() {
    return profs_.size();
  }

  public MdlCompareProfileAdapter getProfil(int _idxProfil) {
    if (_idxProfil < 0 || _idxProfil >= profs_.size()) {
      return null;
    }
    return profs_.get(_idxProfil);
  }

  public void addListener(ProfileSetListener _listener) {
    listeners_.add(_listener);
  }

  public void removeListener(ProfileSetListener _listener) {
    listeners_.remove(_listener);
  }

  public void removeProfil(int _idxProfil, CtuluCommandContainer _cmd) {
    profs_.remove(_idxProfil);
    fireProfileRemoved(_idxProfil);
  }
  
  public void addProfil(MdlCompareProfileAdapter _prof, CtuluCommandContainer _cmd) {
    profs_.add(_prof);
    fireProfileAdded(profs_.size()-1);
  }

  /**
   * Notification des listener en cas de suppression de profil.
   * @param _idxProfil 
   */
  private void fireProfileRemoved(int _idxProfil) {
    for (ProfileSetListener l : listeners_) {
      l.profilRemoved(_idxProfil);
    }
  }

  /**
   * Notification des listener en cas d'ajout de profil.
   * @param _idxProfil 
   */
  private void fireProfileAdded(int _idxProfil) {
    for (ProfileSetListener l : listeners_) {
      l.profilAdded(_idxProfil);
    }
  }

  @Override
  public boolean isBief() {
    return false;
  }
}
