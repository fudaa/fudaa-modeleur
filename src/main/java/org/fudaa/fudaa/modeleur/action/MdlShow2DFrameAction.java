/*
 *  @creation     1 avr. 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.modeleur.action;

import java.awt.event.ActionEvent;

import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.modeleur.MdlImplementation;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Une action pour visualiser la fenetre 2D.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MdlShow2DFrameAction extends EbliActionSimple {
  MdlImplementation impl_;

  /**
   * @param _impl L'implementation deu modeleur.
   */
  public MdlShow2DFrameAction(MdlImplementation _impl) {
    super(MdlResource.getS("Vue 2D"), null, "VUE2D");
    impl_=_impl;
  }

  public void actionPerformed(final ActionEvent _e) {
    impl_.activateInternalFrame(impl_.get2dFrame());
  }
}
