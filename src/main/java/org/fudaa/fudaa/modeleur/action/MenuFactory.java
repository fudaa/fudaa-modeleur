package org.fudaa.fudaa.modeleur.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliComponentFactory;

import com.memoire.bu.BuMenu;

/**
 * Menu factory that syncronize menu availability with the state of action (enable, disable them when user click on it).
 * @author Adrien
 *
 */
public class MenuFactory {

	
	public static void addSynchronizedActionToMenu(final EbliActionInterface[] actions, final BuMenu menu) {
		 EbliComponentFactory.INSTANCE.addActionsToMenu(actions, menu);
		 
		 menu.addMenuListener(new MenuListener() {
		

		        @Override
		        public void menuSelected(MenuEvent e) {
		           doSyncrhonize(e);

		        }

		        @Override
		        public void menuDeselected(MenuEvent e) {
		        	doSyncrhonize(e);

		        }

		        @Override
		        public void menuCanceled(MenuEvent e) {
		            

		        }
		        public void doSyncrhonize(MenuEvent e) {
					for(EbliActionInterface action: actions) {
						if(action != null)
							action.updateStateBeforeShow();
					}
					
				}
		        
		    });
		 
		
		 
		 
	}

}
