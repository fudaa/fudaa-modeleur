/*
 GPL 2
 */
package org.fudaa.fudaa.modeleur.action;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.palette.BPalettePlageInterface;
import org.fudaa.fudaa.modeleur.MdlVisuPanel;
import org.fudaa.fudaa.modeleur.grid.AbstractCalqueCreateGridProcessor;
import org.fudaa.fudaa.modeleur.grid.CalqueCreateGridPoly2TriProcessor;
import org.fudaa.fudaa.modeleur.grid.CalqueCreateGridTriangleProcessor;
import org.fudaa.fudaa.modeleur.grid.CalqueGridSemisSynchronizeProcess;
import org.fudaa.fudaa.modeleur.grid.CalqueGridSemisSynchronizeProcess.Result;
import org.fudaa.fudaa.modeleur.grid.GridChooseToolAndContourConvexHullPanel;
import org.fudaa.fudaa.modeleur.grid.MdlCalqueMultiPointEditable;
import org.fudaa.fudaa.modeleur.grid.MdlElementModel;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dContour;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.fudaa.fudaa.sig.layer.FSigEditor;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.operation.valid.IsValidOp;

/**
 * L'action qui permet de trianguler un semis de point.
 *
 * @author Frederic Deniger
 */
public class CalqueCreateGridAction extends EbliActionSimple {

  private final MdlCalqueMultiPointEditable layer;
  private CalqueCreateGridStopAction stopAction;
  private AbstractCalqueCreateGridProcessor processor;
  private volatile boolean running;
  private volatile boolean stop;

  /**
   * @param layer la calque semis de points associ�e
   */
  public CalqueCreateGridAction(MdlCalqueMultiPointEditable layer) {
    super(MdlResource.getS("Mailler"), null, "PERFORM_GRID");
    this.layer = layer;

    stopAction = new CalqueCreateGridStopAction(this);
    stopAction.setEnabled(false);
  }

  private CtuluUI getUI() {
    return ((FSigEditor) layer.getEditor()).getUi();
  }

  public CalqueCreateGridStopAction getStopAction() {
    return stopAction;
  }

  @Override
  public void updateStateBeforeShow() {
    setEnabled(!running);
  }

  @Override
  public void actionPerformed(ActionEvent _e) {

    stop = false;
    setEnabled(false);
    running = true;
    stopAction.setRunning(true);
    final FSigEditor editor = (FSigEditor) layer.getEditor();
    MdlVisuPanel visuPanel = (MdlVisuPanel) editor.getPanel();
    //le panneau permet de choisir le triangulateur et le calque contour.
    final GridChooseToolAndContourConvexHullPanel panel = new GridChooseToolAndContourConvexHullPanel(visuPanel);
    boolean ok = panel.afficheModaleOk(editor.getUi().getParentComponent(), MdlResource.getS("Selectionner le calque de contour"));
    final MdlLayer2dContour finalContourLayer = (MdlLayer2dContour) panel.getSelectedContourLayer();
    if (ok) {
      layer.getLayerGridDelegate().clearGrid();
      ok = checkParameters(finalContourLayer, editor, panel);
    }
    if (!ok) {
      running = false;
      stopAction.setRunning(false);
      setEnabled(true);
      return;
    }
    //lancement de la triangulation dans un thread � part.
    final CtuluTaskDelegate createTask = editor.getUi().createTask(getTitle());
    createTask.start(new CreateGridRunnable(panel, createTask.getStateReceiver()));

  }

  @Override
  public void setEnabled(boolean _newValue) {
    super.setEnabled(_newValue && !running);
  }

  void stop() {
    stop = true;
    if (processor != null) {
      processor.stop();
    }
  }

  /**
   * Permet de v�rifer avant la triangulation que toutes les lignes sont valides.
   *
   * @param finalContourLayer
   * @param editor
   * @return
   */
  private boolean checkLines(final MdlLayer2dContour finalContourLayer, final FSigEditor editor) {
    boolean ok = true;
    int numGeometries = finalContourLayer.modeleDonnees().getGeomData().getNumGeometries();
    List<String> nonValideLines = new ArrayList<String>();
    GISAttributeModel model = finalContourLayer.modeleDonnees().getGeomData().getModel(GISAttributeConstants.TITRE);
    for (int i = 0; i < numGeometries; i++) {
      LineString line = (LineString) finalContourLayer.modeleDonnees().getGeomData().getGeometry(i);
      IsValidOp isValid = new IsValidOp(line);
      if (!isValid.isValid()) {
        nonValideLines.add((String) model.getObjectValueAt(i) + ". " + isValid.getValidationError().getMessage() + "; Point: " + isValid.getValidationError().getCoordinate());
      }
    }
    if (!nonValideLines.isEmpty()) {
      editor.getUi().error(MdlResource.getS("Contours non valides"), "<html><body>"
              + MdlResource.getS("Les lignes de contours suivantes ne sont pas valides") + ":<br><ul><li>" + StringUtils.join(nonValideLines, "</li><li>"), stop);
      ok = false;
    }
    return ok;
  }

  /**
   *
   * @param finalContourLayer
   * @param editor
   * @param panel
   * @return true si les param�tres d'entr�e sont correct ( ligne de contour correct et si Triangle est choisi, l'exe doit �tre configur�).
   */
  private boolean checkParameters(final MdlLayer2dContour finalContourLayer, final FSigEditor editor, final GridChooseToolAndContourConvexHullPanel panel) {
    boolean ok = true;
    if (finalContourLayer != null) {
      //on doit tester si les lignes sont valides.
      ok = checkLines(finalContourLayer, editor);
    }
    if (ok && panel.isTriangleSelected()) {
      if (!CalqueCreateGridTriangleProcessor.isTriangleExeConfigured()) {
        ok = CalqueGridTriangleConfigurationAction.chooseExec();
      }
      if (!CalqueCreateGridTriangleProcessor.isTriangleExeConfigured()) {
        ok = false;
      }
    }
    return ok;
  }

  /**
   * Runnable permettant de mailler le semis de point en arri�re plan et ajoute le calque si la triangulation a r�ussi.
   */
  private class CreateGridRunnable implements Runnable {

    private final GridChooseToolAndContourConvexHullPanel panel;
    private final ProgressionInterface progression;

    public CreateGridRunnable(GridChooseToolAndContourConvexHullPanel panel, ProgressionInterface progression) {
      this.panel = panel;
      this.progression = progression;
    }

    /**
     * Installe le calque maillage correspondant au model.
     *
     * @param model
     */
    private void installGridLayer(MdlElementModel model) {
      stopRunning();
      if (!stop) {
        if (layer.isTraceIsoLine() || layer.isTraceIsoSurface()) {
          if (layer.getPaletteCouleur() == null) {
            BPalettePlageInterface newPalette = layer.createPaletteCouleur();
            CtuluRange range = new CtuluRange();
            layer.getRange(range);
            ((BPalettePlage) newPalette).initPlages(10, range.min_, range.max_);
            layer.setPaletteCouleurPlages(newPalette);
          }
        }
        layer.getLayerGridDelegate().setGridLayer(model);
        ((FSigEditor) layer.getEditor()).getPanel().getArbreCalqueModel().setSelectionCalque(layer);
      }
    }
    private double eps = 1e-3;

    @Override
    public void run() {
      eps = panel.getEps();
      final LinearRing finalConvexHull = panel.getSelectedConvexHull();
      final MdlLayer2dContour finalContourLayer = (MdlLayer2dContour) panel.getSelectedContourLayer();
      if (panel.isTriangleSelected()) {
        processor = new CalqueCreateGridTriangleProcessor(layer.modeleDonnees(), getUI(), finalContourLayer, finalConvexHull);
      } else {
        processor = new CalqueCreateGridPoly2TriProcessor(layer.modeleDonnees(), getUI(), finalContourLayer, finalConvexHull);
      }
      processor.setEps(eps);
      processor.setContraintLines(panel.getSelectedContrainteLayer());
      processor.setUseHole(panel.isUseHoleIsSelected());
      processor.setAddPointOnFrontier(panel.isAddPointOnFrontierSelected());
      //TODO boolean 
      //on execute la triangulation et on lit le maillage
      final EfGridInterface grid = processor.process(progression);
      final MdlElementModel model = new MdlElementModel(grid);
      //si non null, on fait des calculs suppl�mentaires:
      if (grid != null) {
        CalqueGridSemisSynchronizeProcess synchProcess = new CalqueGridSemisSynchronizeProcess(processor.getPointsAdapter());
        synchProcess.setEps(eps);
        //recherche des relations entre les noeuds du maillage et le semis actuel:
        Result relations = synchProcess.getRelations(grid, progression);
        model.setPointsAdapter(processor.getPointsAdapter());
        model.setAltiIdxByGridPtIdx(relations.getRelations());
        model.setInitPointPaintedByMesh(relations.getInitPointPaintedByMesh());
        if (progression != null) {
          progression.setDesc(MdlResource.getS("Affichage"));
          progression.setProgression(0);
        }
        EventQueue.invokeLater(new Runnable() {
          @Override
          public void run() {
            installGridLayer(model);
            if (progression != null) {
              progression.reset();
            }
          }
        });
      } else {
        stopRunning();
        getUI().warn(MdlResource.getS("Construction de la triangulation"), getLongHelp(), false);
      }

    }

    public String getLongHelp() {
      return MdlResource.getS("<html><body>La triangulation a �chou�.<br>Si vous avez choisi un calque de contour, v�rifier la coh�rence des lignes ferm�es: "
              + "<br><ul><li>pas d'intersections</li><li>l'enveloppe externe contient les trous</li></ul>"
              + "<br>Si vous utiliser le triangulateur interne, il est pr�f�rable de d�finir une enveloppe externe"
              + "<br>Utiliser le triangulateur externe (Triangle) de pr�f�rence: plus stable et plus rapide");
    }

    public void stopRunning() {
      running = false;
      setEnabled(true);
      stopAction.setRunning(running);
    }
  }
}
