/*
 * @creation     26 nov. 2008
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.action;

import java.awt.event.ActionEvent;

import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZModeleGeometry;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.calque.ZScene.SceneSelectionHelper;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.modeleur.MdlSceneEditor;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Une action pour cr�er un profil par interpolation sur un nuage de points depuis une trace de profil.
 * @author Emmanuel MARTIN
 */
public class SceneProfilInterpolationAction extends EbliActionSimple implements ZSelectionListener {
  MdlSceneEditor sceneEditor_;
  
  public SceneProfilInterpolationAction(MdlSceneEditor _sceneEditor) {
    super(MdlResource.getS("Cr�er un profil depuis une trace"), null, "INTERPOLE_PROFIL");
    setDefaultToolTip(MdlResource.getS("Cr�e un profil par interpolation sur un nuage de points depuis une trace"));
    sceneEditor_=_sceneEditor;
    sceneEditor_.getScene().addSelectionListener(this);
  }

  public void actionPerformed(final ActionEvent _e) {
    sceneEditor_.interpolateProfile();
  }

  public void updateForSelectionChanged() {
    ZScene scn=sceneEditor_.getScene();
    SceneSelectionHelper hlp=sceneEditor_.getScene().getSelectionHelper();
    boolean b=false;
    // Si la selection est en mode globale et qu'une seule g�om�trie est selectionn�e.
    if(!scn.isAtomicMode()&&hlp.getUniqueSelectedIdx()!=-1){
      int idGeom=scn.sceneId2LayerId(hlp.getUniqueSelectedIdx());
      ZCalqueAffichageDonneesInterface calque=scn.getLayerForId(hlp.getUniqueSelectedIdx());
      // La nature de la g�om�trie est bien trace profil 
      if(calque.modeleDonnees() instanceof ZModeleGeometry){
        GISZoneCollection zone=((ZModeleGeometry) calque.modeleDonnees()).getGeomData();
        int iattNat=zone.getIndiceOf(GISAttributeConstants.NATURE);
        if (iattNat!=-1)
          b=zone.getValue(iattNat, idGeom).equals(GISAttributeConstants.ATT_NATURE_TP);
      }
    }
    setEnabled(b);
  }

  public String getEnableCondition() {
    return MdlResource.getS("S�lectionner une trace de profil.");
  }

  /* (non-Javadoc)
   * @see org.fudaa.ebli.calque.ZSelectionListener#selectionChanged(org.fudaa.ebli.calque.ZSelectionEvent)
   */
  public void selectionChanged(ZSelectionEvent _evt) {
    updateForSelectionChanged();
  }
}
