/*
 *  @creation     1 avr. 2005
 *  @modification $Date: 2008-05-13 12:10:55 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.modeleur.action;

import java.awt.event.ActionEvent;

import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.modeleur.MdlSceneEditor;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dLine;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dMultiPoint;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Une action pour faire une interpolation entre 2 sommet d'un objets GIS.
 * L'interpolation consiste a cr�er une nouvelle g�ometrie entre les 2 sommets,
 * avec le nombre de points indiqu�s.
 * 
 * @author Bertrand Marchand
 * @version $Id: SceneInterpolationAction.java,v 1.1.2.1 2008-05-13 12:10:55
 *          bmarchan Exp $
 */
public class SceneInterpolationAction extends EbliActionSimple implements ZSelectionListener {

  MdlSceneEditor sceneEditor_;

  /**
   * @param _sceneEditor L'editeur sp�cifique a la scene.
   */
  public SceneInterpolationAction(MdlSceneEditor _sceneEditor) {
    super(MdlResource.getS("Interpolation entre 2 sommets"), null, "INTERPOLER");
    setSceneEditor(_sceneEditor);
    sceneEditor_.getScene().addSelectionListener(this);
  }

  public void actionPerformed(final ActionEvent _e) {
    sceneEditor_.interpolateSelectedGeometries();
  }

  /**
   * @param _sceneEditor L'editeur sp�cifique a la scene.
   */
  private void setSceneEditor(final MdlSceneEditor _sceneEditor) {
    sceneEditor_ = _sceneEditor;
  }

  /**
   * Autoris� si 2 sommets sont selectionn�s.
   */
  public void updateForSelectionChanged() {
    ZScene scn=sceneEditor_.getScene();
    boolean b=true;
    b&=!scn.isSelectionEmpty();
    b&=scn.isAtomicMode();
    b&=scn.getLayerSelectionMulti().getNbSelectedItem()==2;
    
    b&=(scn.getCalqueActif()!=null &&
        (scn.getCalqueActif() instanceof MdlLayer2dLine ||
         scn.getCalqueActif() instanceof MdlLayer2dMultiPoint));
    super.setEnabled(b);
  }

  public String getEnableCondition() {
    return MdlResource.getS("S�lectionner 2 sommets quelconques et un calque cible");
  }

  /* (non-Javadoc)
   * @see org.fudaa.ebli.calque.ZSelectionListener#selectionChanged(org.fudaa.ebli.calque.ZSelectionEvent)
   */
  public void selectionChanged(ZSelectionEvent _evt) {
    updateForSelectionChanged();
  }
}
