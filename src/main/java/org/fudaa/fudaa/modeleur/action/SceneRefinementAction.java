/*
 *  @creation     1 avr. 2005
 *  @modification $Date: 2008-05-13 12:10:55 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.modeleur.action;

import java.awt.event.ActionEvent;

import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.calque.ZScene.SceneSelectionHelper;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.modeleur.MdlSceneEditor;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Une action pour raffiner entre 2 sommets ou la totalit� d'un objets GIS. Le raffinement consiste
 * � ajouter le nombre de points sp�cifi�s entre chaque sommets contenu entre les 2 sommets s�lectionn�s.
 * @author Bertrand Marchand
 * @version $Id: SceneInterpolationAction.java,v 1.1.2.1 2008-05-13 12:10:55 bmarchan Exp $
 */
public class SceneRefinementAction extends EbliActionSimple implements ZSelectionListener {

  MdlSceneEditor sceneEditor_;

  /**
   * @param _sceneEditor L'editeur sp�cifique a la scene.
   */
  public SceneRefinementAction(MdlSceneEditor _sceneEditor) {
    super(MdlResource.getS("Raffiner"), null, "RAFFINER");
    setSceneEditor(_sceneEditor);
    sceneEditor_.getScene().addSelectionListener(this);
  }

  public void actionPerformed(final ActionEvent _e) {
    sceneEditor_.refineSelectedGeometries();
  }

  /**
   * @param _sceneEditor L'editeur sp�cifique a la scene.
   */
  private void setSceneEditor(final MdlSceneEditor _sceneEditor) {
    sceneEditor_ = _sceneEditor;
  }

  /**
   * Autoris� si 2 sommets d'un m�me bloc sont selectionn�s ou 1 g�om�trie compl�te.
   */
  public void updateForSelectionChanged() {
    ZScene scn=sceneEditor_.getScene();
    SceneSelectionHelper hlp=sceneEditor_.getScene().getSelectionHelper();
    int idGeom=-1;

    boolean b=true;
    // Si selection n'est pas vide.
    b&=!scn.isSelectionEmpty();
    // Si le nombre d'atomiques est de 2 sur le m�me bloc
    b&=!scn.isAtomicMode() || (hlp.getNbAtomicSelected()==2 && (idGeom=hlp.getUniqueSelectedIdx())!=-1);
    // Si le nombre de selectionn�s est de 1 ou plus en mode global
//    b&=scn.isAtomicMode() || true;
    // Si la g�om�trie appartient a un calque polylignes.
//    b&=scn.getLayerForId(idGeom) instanceof MdlLayer2dLine;

    super.setEnabled(b);
  }

  public String getEnableCondition() {
    return MdlResource.getS("<p>En mode sommet, s�lectionner 2 sommets d'un m�me objet<p>En mode global, un ou plusieurs objet(s)");
  }
  
  /* (non-Javadoc)
   * @see org.fudaa.ebli.calque.ZSelectionListener#selectionChanged(org.fudaa.ebli.calque.ZSelectionEvent)
   */
  public void selectionChanged(ZSelectionEvent _evt) {
    updateForSelectionChanged();
  }
}
