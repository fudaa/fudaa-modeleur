/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.modeleur.modeleur1d.model;

import org.fudaa.ctulu.CtuluCommandContainer;

/**
 * Un set de profils. Ce set peut �tre un bief ou un ensemble.
 * @author bmarchan
 */
public interface ProfileSetI {
  
  /** Ajout d'un nouveau listener. */
  public void addListener(ProfileSetListener _listener);
  
  /** Supprime le listener. */
  public void removeListener(ProfileSetListener _listener);

  /**
   * @return -1 Si aucun selectionn�
   */
//  int getSelectedIndex();
  
  /**
   * @param _idx LE profil selectionn�. Peut etre -1 si aucun s�lectionn�.
   */
//  void setSelectedIndex(int _idx);
  
  /**
   * Retourne ne nombre de profils de ce bief.
   */
  int getNbProfil();

  /**
   * Retourne le nom du profil choisi par sont index.
   */
//  String getNomProfil(int _idxProfil);
  
  /**
   * @param _idxProfil L'index du profil dans le set.
   * @return null si index invalide.
   */
  ProfilContainerI getProfil(int _idxProfil);
  
  /** Supprime un profil. */
  public void removeProfil(int _idxProfil, CtuluCommandContainer _cmd);

  /**
   * Retourne vrai si un axe hydraulique existe
   */
//  boolean hasAxeHydraulique();
  
  /**
   * @return True si c'est un bief. Un bief est compos� de profils et de limites
   * sp�ciales.
   */
  boolean isBief();
}
