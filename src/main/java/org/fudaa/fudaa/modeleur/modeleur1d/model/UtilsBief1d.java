/*
 * @creation     12 f�vr. 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.model;

import java.util.Arrays;
import java.util.Comparator;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ebli.calque.ZModeleLigneBrisee;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Quelques fonctions concernant les biefs.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class UtilsBief1d {
  /**
   * Ajout les attributs manquant � la zone de l'axe hydraulique.
   */
  static public void normalizeAxeHydrauliqueAttributes(GISZoneCollection _zone) {
    if (_zone.getIndiceOf(GISAttributeConstants.CURVILIGNE_DECALAGE)==-1) {
      GISAttributeInterface[] atts=new GISAttributeInterface[_zone.getNbAttributes()+1];
      for (int k=0; k<_zone.getNbAttributes(); k++)
        atts[k]=_zone.getAttribute(k);
      atts[atts.length-1]=GISAttributeConstants.CURVILIGNE_DECALAGE;
      _zone.setAttributes(atts, null);
      int idxAttrDecCurv=_zone.getIndiceOf(GISAttributeConstants.CURVILIGNE_DECALAGE);
      for (int i=0; i<_zone.getNbGeometries(); i++)
        _zone.setAttributValue(idxAttrDecCurv, i, 0.0, null);
    }
  }

  /**
   * Ajout les attributs manquant � la zone des profils.
   */
/*  static public void normalizeProfilAttributes(GISZoneCollection _zone) {
    // Verification de la pr�sences des attributs
    int attToAdd=0;
    int idxIlsd=_zone.getIndiceOf(GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_DROITE);
    if (idxIlsd==-1)
      attToAdd++;
    int idxIlsg=_zone.getIndiceOf(GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_GAUCHE);
    if (idxIlsg==-1)
      attToAdd++;
    int idxRd=_zone.getIndiceOf(GISAttributeConstants.INTERSECTION_RIVE_DROITE);
    if (idxRd==-1)
      attToAdd++;
    int idxRg=_zone.getIndiceOf(GISAttributeConstants.INTERSECTION_RIVE_GAUCHE);
    if (idxRg==-1)
      attToAdd++;
    int idxLd=_zone.getIndiceOf(GISAttributeConstants.INTERSECTIONS_LIGNES_DIRECTRICES);
    if (idxLd==-1)
      attToAdd++;
    // Ajout des attributs manquant \\
    if (attToAdd>0) {
      GISAttributeInterface[] atts=new GISAttributeInterface[_zone.getNbAttributes()+attToAdd];
      int k=0;
      for (k=0; k<_zone.getNbAttributes(); k++)
        atts[k]=_zone.getAttribute(k);
      if (idxIlsd==-1)
        atts[k++]=GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_DROITE;
      if (idxIlsg==-1)
        atts[k++]=GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_GAUCHE;
      if (idxRd==-1)
        atts[k++]=GISAttributeConstants.INTERSECTION_RIVE_DROITE;
      if (idxRg==-1)
        atts[k++]=GISAttributeConstants.INTERSECTION_RIVE_GAUCHE;
      if (idxLd==-1)
        atts[k++]=GISAttributeConstants.INTERSECTIONS_LIGNES_DIRECTRICES;
      _zone.setAttributes(atts, null);
    }
  }*/
  
  /**
   * R�ordonne les profils dans le mod�le en fonction de leur abscisse curviligne. 
   * 
   * @param _profils Le modele contenant les profils
   * @param _idx L'indice du profil s�lectionn� (-1 si aucun profil selectionn�).
   * @param _cmd Le container de commande
   * @return Le nouvel indice de l'indice pass� en param�tre dans le mod�le (-1 si aucun profil selectionn�).
   */
  static public int orderProfils(ZModeleLigneBrisee _profils, int _idx, CtuluCommandContainer _cmd) {
    CtuluCommandComposite cmd=new CtuluCommandComposite(MdlResource.getS("R�ordonnancement des profils"));
    // R�ordonnancement des profils selon leur placement sur l'axe hydaulique
    // (abs curv) \\
    // Calcul des abscisses curvilignes
    Object[][] idxAbsCurv=new Object[_profils.getNombre()][];
    for (int j=0; j<_profils.getNombre(); j++) {
      GISZoneCollection zone=_profils.getGeomData();
      String value=(String)zone.getValue(zone.getIndiceOf(GISAttributeConstants.COMMENTAIRE_HYDRO), j);
      idxAbsCurv[j]=new Object[]{j, GISLib.getHydroCommentDouble(value, GISAttributeConstants.ATT_COMM_HYDRO_PK)};
    }
    // Tri en fonction des abcsisses curvilignes
    Arrays.sort((Object[])idxAbsCurv, new Comparator<Object>() {
      public int compare(Object o1, Object o2) {
        double absCurvO1=(Double)((Object[])o1)[1];
        double absCurvO2=(Double)((Object[])o2)[1];
        if (absCurvO1<absCurvO2)
          return -1;
        if (absCurvO1==absCurvO2)
          return 0;
        else
          return 1;
      }
    });
    int newIdx=_idx;
    // R�odonnancement en fonction du nouvel ordre
    for (int i=0; i<idxAbsCurv.length; i++) {
      if (i!=(Integer)idxAbsCurv[i][0]) {
        // Mise � jour de la table de correspondance
        boolean found=false;
        int k=i;
        while (!found&&++k<idxAbsCurv.length)
          found=(Integer)idxAbsCurv[k][0]==i;
        if (found)
          idxAbsCurv[k][0]=idxAbsCurv[i][0];
        // Mise � jour de newIdx
        if (newIdx==(Integer)idxAbsCurv[i][0])
          newIdx=i;
        else if (newIdx==i)
          newIdx=(Integer)idxAbsCurv[i][0];
        // Mise � jour de la gis
        _profils.getGeomData().switchGeometries((Integer)idxAbsCurv[i][0], i, cmd);
      }
    }
    if (_cmd!=null)
      _cmd.addCmd(cmd);
    return newIdx;
  }
}
