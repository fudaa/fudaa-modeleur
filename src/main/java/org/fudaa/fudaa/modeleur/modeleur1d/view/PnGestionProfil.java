/*
 * @creation     18 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.view;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.fudaa.modeleur.modeleur1d.controller.ProfileSetControllerI;
import org.fudaa.fudaa.modeleur.modeleur1d.model.ProfileSetI;
import org.fudaa.fudaa.modeleur.modeleur1d.model.ProfileSetListener;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import javax.swing.ListSelectionModel;

/**
 * Un panneau permettant de g�rer les profils.
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class PnGestionProfil extends JPanel {
  
  /** Le controller des profils. */
  protected ProfileSetControllerI controllerProfil_;
  /** Tableau d'affichage des biefs. */
  protected JTable tableProfils_;
  private TableProfilModel tbModel_;
  private boolean isNameEditable_=false;
  
  public PnGestionProfil(ProfileSetControllerI _controllerProfil) {
    this(_controllerProfil,true);
  }
  
  public PnGestionProfil(ProfileSetControllerI _controllerProfil, boolean _showButtons){
    controllerProfil_=_controllerProfil;
    setLayout(new BuBorderLayout(2,2));
    // Titre
    BuLabel lblTitre=new BuLabel("<html><b>"+MdlResource.getS("Profil")+"</b></html>");
    lblTitre.setHorizontalAlignment(BuLabel.CENTER);
    add(lblTitre, BuBorderLayout.NORTH);
    // Tableau de noms
    tableProfils_=new CtuluTable(tbModel_=new TableProfilModel()) {
      /* (non-Javadoc)
       * @see javax.swing.JTable#valueChanged(javax.swing.event.ListSelectionEvent)
       */
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) return;
          System.out.println(".valueChanged() !!");
        super.valueChanged(e);
        if (getSelectionModel().getMinSelectionIndex()!=-1)
          scrollRectToVisible(getCellRect(getSelectionModel().getMinSelectionIndex(), 0, false));
      }
    };
    tableProfils_.setSelectionModel(controllerProfil_.getProfilSelectionModel());
    tableProfils_.getColumnModel().getColumn(0).setCellRenderer(new DefaultTableCellRenderer(){
      @Override
      public JLabel getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row,
          int column) {
        JLabel c=(JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        c.setHorizontalAlignment(SwingUtilities.CENTER);
        return c;
      }
    });
    tableProfils_.getColumnModel().getColumn(0).setPreferredWidth(50);
    tableProfils_.getColumnModel().getColumn(1).setPreferredWidth(150);
    
    add(new JScrollPane(tableProfils_), BuBorderLayout.CENTER);
    // Boutons
    JPanel pnButtons=new JPanel();
    pnButtons.setLayout(new BuGridLayout(1, 5, 5));
    JButton btAjout=new BuButton(MdlResource.getS("Ajout"));
    btAjout.addActionListener(new ActionListener(){
      public void actionPerformed(ActionEvent e) {
        controllerProfil_.ajoutProfil();
      }
    });
    btAjout.setEnabled(false);
//    pnButtons.add(btAjout);
    JButton btSupprimer=new BuButton(MdlResource.getS("Supprimer"));
    btSupprimer.addActionListener(new ActionListener(){
      public void actionPerformed(ActionEvent e) {
        controllerProfil_.supprimerSelectedProfil();
      }
    });
    pnButtons.add(btSupprimer);
    JButton btFusionner=new BuButton(MdlResource.getS("Fusionner"));
    btFusionner.addActionListener(new ActionListener(){
      public void actionPerformed(ActionEvent e) {
        controllerProfil_.fusionnerProfil();
      }
    });
    btFusionner.setEnabled(false);
//    pnButtons.add(btFusionner);
    if (_showButtons) {
      add(pnButtons, BuBorderLayout.EAST);
      isNameEditable_=true;
    }
    setPreferredSize(new Dimension(200, 200));
  }
  
  public void setProfileSet(ProfileSetI _prf) {
    tbModel_.setProfilSet(_prf);
  }
  public JTable getTableProfils() {
      return tableProfils_;
  }
  /**
   * Surcharge du model du tableau pour prendre les informations directement dans le ProfilSet.
   */
  private class TableProfilModel extends DefaultTableModel implements ProfileSetListener {

    /** Raccourcis vers le biefContainer. */
    private ProfileSetI profilSet_;
    
    public TableProfilModel() {
      super(new String[]{MdlResource.getS("Index"),MdlResource.getS("Nom")}, 0);
    }

    public void setProfilSet(ProfileSetI _profSet) {
      if (profilSet_ == _profSet) {
        return;
      }

      tableProfils_.clearSelection();

      if (profilSet_ != null) {
        profilSet_.removeListener(this);
      }
      profilSet_ = _profSet;
      if (profilSet_ != null) {
        profilSet_.addListener(this);
      }

      fireTableDataChanged();
    }

    public Class<?> getColumnClass(int columnIndex) {
      if (columnIndex==0) return Integer.class;
      return String.class;
    }

    public int getRowCount() {
      if(profilSet_==null)
        return 0;
      return profilSet_.getNbProfil();
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
      if (columnIndex==0)
        return rowIndex+1;
      
      return profilSet_.getProfil(rowIndex).getName();
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
      if (columnIndex==0) return false;
      return isNameEditable_;
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {
      if (columnIndex==0) return;
      
      if(!getValueAt(rowIndex, columnIndex).equals(value))
        controllerProfil_.renameProfil(rowIndex, (String) value);
    }

    public void profilRemoved(int profil) {
      fireTableDataChanged();
    }

    public void profilAdded(int profil) {
      fireTableDataChanged();
    }
  }
}
