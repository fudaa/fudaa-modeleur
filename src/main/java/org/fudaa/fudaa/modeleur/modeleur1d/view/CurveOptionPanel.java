/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.modeleur.modeleur1d.view;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuVerticalLayout;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.fudaa.fudaa.modeleur.modeleur1d.view.VueCourbe.Alignment;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Un panneau d'options pour l'affichage de courbes.
 * @author marchand@deltacad.fr
 */
public class CurveOptionPanel extends JPanel {
  VueCourbe view_;
  
  public CurveOptionPanel(VueCourbe _view) {
    view_=_view;
    
    buildPanel();
  }
  
  private void buildPanel() {
    setLayout(new BuVerticalLayout(3,true,false));
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    
    JPanel pnXAlign=new JPanel();
    pnXAlign.setLayout(new BorderLayout(3,3));
    JLabel lbXAlign=new JLabel(MdlResource.getS("Alignement en X"));
    
    BuComboBox coXAlign=new BuComboBox();
    coXAlign.addItem(MdlResource.getS("Sans alignement"));
    coXAlign.addItem(MdlResource.getS("Abscisse min"));
    coXAlign.addItem(MdlResource.getS("Abscisse max"));
    coXAlign.addItem(MdlResource.getS("Point de Z min"));
    
    if (view_.getProfilSet().isBief()) {
      
      // Ajout des limites
      coXAlign.addItem(MdlResource.getS("Stockage gauche"));
      coXAlign.addItem(MdlResource.getS("Rive gauche"));
      coXAlign.addItem(MdlResource.getS("Rive droite"));
      coXAlign.addItem(MdlResource.getS("Stockage droit"));
      
      // Ajout de l'axe
      coXAlign.addItem(MdlResource.getS("Axe hydraulique"));
      
      // Ajout des lignes directrice
      String[] ldnames=view_.getProfilSet().getProfil(0).getNamesLignesDirectrices();
      for (int i=0; i<ldnames.length; i++) {
        coXAlign.addItem(MdlResource.getS("LD")+": "+ldnames[i]);
      }
    }
    
    coXAlign.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        int indsel=((BuComboBox)e.getSource()).getSelectedIndex();
        
        if (indsel<=1) {
          view_.setXAlignment(Alignment.LEFT, 0);
        }
        else if (indsel==2) {
          view_.setXAlignment(Alignment.RIGHT, 0);
        }
        else if (indsel==3) {
          view_.setXAlignment(Alignment.X_ZMIN,0);
        }
        else if (indsel==4) {
          view_.setXAlignment(Alignment.LEFT_STORE,0);
        }
        else if (indsel==5) {
          view_.setXAlignment(Alignment.LEFT_BANK,0);
        }
        else if (indsel==6) {
          view_.setXAlignment(Alignment.RIGHT_BANK,0);
        }
        else if (indsel==7) {
          view_.setXAlignment(Alignment.RIGHT_STORE,0);
        }
        else if (indsel==8) {
          view_.setXAlignment(Alignment.AXIS,0);
        }
        else if (indsel>=9) {
          view_.setXAlignment(Alignment.DIR_LINE,indsel-9);
        }
      }
    });
    
    pnXAlign.add(lbXAlign,BorderLayout.WEST);
    pnXAlign.add(coXAlign,BorderLayout.CENTER);
    
    add(pnXAlign);
    
    JPanel pnZAlign=new JPanel();
    pnZAlign.setLayout(new BorderLayout(3,3));
    JLabel lbZAlign=new JLabel(MdlResource.getS("Alignement en Z"));
    
    BuComboBox coZAlign=new BuComboBox();
    coZAlign.addItem(MdlResource.getS("Sans alignement"));
    coZAlign.addItem(MdlResource.getS("Sur le Z min"));
    coZAlign.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if (((BuComboBox)e.getSource()).getSelectedIndex()==0) {
          view_.setZAlignment(Alignment.Z_IDENT);
        }
        else
          view_.setZAlignment(Alignment.Z_MIN);
      }
    });
    
    pnZAlign.add(lbZAlign,BorderLayout.WEST);
    pnZAlign.add(coZAlign,BorderLayout.CENTER);
    
    add(pnZAlign);
    
    
  }
}
