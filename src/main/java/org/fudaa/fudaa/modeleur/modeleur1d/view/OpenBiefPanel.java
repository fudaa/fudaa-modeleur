/*
 * @creation     12 f�vr. 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.view;

import javax.swing.ButtonGroup;
import javax.swing.SwingUtilities;

import org.fudaa.ctulu.gui.CtuluDialogPanel;

import com.memoire.bu.BuLabel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * GUI de l'importeur de donn�es. Il se contente de faire apparaitre une fen�tre
 * modale (via la m�thode 'run') puis met � disposition les informations. Il ne
 * fait aucun traitement par lui m�me.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class OpenBiefPanel extends CtuluDialogPanel {

  private BuRadioButton rbtAxeHydrau_;
  private String nameBief_;

  public OpenBiefPanel(String _nameBief) {
    nameBief_=_nameBief;
    setLayout(new BuVerticalLayout(2, true, true));
    add(new BuLabel(MdlResource.getS("<html>Les informations de type 'PK' sont incompatibles avec l'axe hydraulique. Voulez vous privil�gier<br>l'axe ou les PK (les informations non s�lectionn�es seront d�truites)?")));
    rbtAxeHydrau_=new BuRadioButton(MdlResource.getS("Axe hydraulique"));
    rbtAxeHydrau_.setSelected(true);
    BuRadioButton rbtPK=new BuRadioButton(MdlResource.getS("Informations 'PK'"));
    ButtonGroup group=new ButtonGroup();
    group.add(rbtAxeHydrau_);
    group.add(rbtPK);
    rbtAxeHydrau_.setHorizontalAlignment(SwingUtilities.CENTER);
    rbtPK.setHorizontalAlignment(SwingUtilities.CENTER);
    add(rbtAxeHydrau_);
    add(rbtPK);
  }

  /**
   * Lance l'exporter dans une fen�tre.
   * 
   * @return vrai si l'utilisateur clic sur 'ok' � la fin, faux si il clic sur 'annuler'.
   */
  public boolean run() {
    return CtuluDialogPanel.isOkResponse(afficheModale(this, MdlResource.getS("Importation du bief '{0}'", nameBief_)));
  }

  @Override
  public boolean isDataValid() {
    return true;
  }

  /**
   * Retourne vrai si l'utilisateur � choisie l'axe hydraulique.
   */
  public boolean axeHydrauChosen() {
    return rbtAxeHydrau_.isSelected();
  }
}
