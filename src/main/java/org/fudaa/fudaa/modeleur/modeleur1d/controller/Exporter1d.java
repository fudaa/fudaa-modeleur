/*
 * @creation     18 f�vr. 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.controller;

import java.io.File;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.dodico.mascaret.io.MascaretGEO1dFileFormat;
import org.fudaa.dodico.mascaret.io.MascaretGEO2dFileFormat;
import org.fudaa.dodico.mascaret.io.MascaretGEOWriter;
import org.fudaa.dodico.rubar.io.RubarStCnFileFormat;
import org.fudaa.fudaa.modeleur.modeleur1d.model.Bief;
import org.fudaa.fudaa.modeleur.modeleur1d.model.UtilsProfil1d;

/**
 * Un exporteur des donn�es d'un bief vers des fichiers.
 * @author Bertrand Marchand
 * @version $Id$
 */
interface Exporter1d {
  
  /**
   * Export des donn�es d'un bief.
   * @param _f Le fichier d'�criture du bief.
   * @param _bief Le bief.
   * @return Les parametres.
   */
  public CtuluIOOperationSynthese export(File _f, Bief _bief);
  
  /**
   * Retourne le file format associ� � l'exporter.
   * @return Le file format.
   */
  public FileFormatVersionInterface getFileFormat();

  /**
   * Une classe abstraite d'exportation vers Mascaret. Gere les geometries pour les exportateurs
   * sp�cialis�s 1D et 2D.
   * @author Bertrand Marchand
   * @version $Id$
   */
  abstract class Mascaret implements Exporter1d {
    
    public CtuluIOOperationSynthese export(File _file, Bief _bief) {
      _bief.profils_.getGeomData().prepareExport();
      _bief.axeHydraulique_.getGeomData().prepareExport();

      GISZoneCollection[] zones=new GISZoneCollection[]{_bief.axeHydraulique_.getGeomData(), _bief.lignesDirectrices_.getGeomData(),
          _bief.limitesStockages_.getGeomData(), _bief.profils_.getGeomData(), _bief.rives_.getGeomData()};
      
      final CtuluAnalyze ana=new CtuluAnalyze();
      MascaretGEOWriter.FunctorSelectProfil functorSelectProfil=new MascaretGEOWriter.FunctorSelectProfil() {
        public boolean exportProfil(GISZoneCollection _zone, int _idxProfil) {
          return UtilsProfil1d.isProfilCorrect(_zone, _idxProfil,ana);
        }
      };
      
      // Ecriture du fichier
      FileWriteOperationAbstract writer=getFileFormat().createWriter();
      if(writer instanceof MascaretGEOWriter)
    	  ((MascaretGEOWriter)writer).setTolerateMultipleAxe(true);
      CtuluIOOperationSynthese synt=writer.write(new Object[]{_bief.getName(),zones,functorSelectProfil},_file, null);

      if (ana.containsWarnings()) {
        ana.merge(synt.getAnalyze());
        synt.setAnalyze(ana);
      }
      return synt;
    }
  }
  
  /**
   * Un exporter au format Mascaret 1D.
   * @author Bertrand Marchand
   * @version $Id$
   */
  class Mascaret1d extends Mascaret {
    public FileFormatVersionInterface getFileFormat() {
      return MascaretGEO1dFileFormat.getInstance();
    }
  }
  
  /**
   * Un exporter au format Mascaret 2D.
   * @author Bertrand Marchand
   * @version $Id$
   */
  class Mascaret2d extends Mascaret {
    public FileFormatVersionInterface getFileFormat() {
      return MascaretGEO2dFileFormat.getInstance();
    }
  }
  
  /**
   * Exporteur pour Rubar ST.
   * @author Bertrand Marchand
   * @version $Id$
   */
  class Rubar implements Exporter1d {
    
    public CtuluIOOperationSynthese export(File _file, Bief _bief) {
      GISDataModel[] params=new GISDataModel[2];
      // Remplit les Z coordonn�es avec la valeur de l'attribut Z
      _bief.profils_.getGeomData().prepareExport();
      _bief.lignesDirectrices_.getGeomData().prepareExport();
      
      params[0]=_bief.profils_.getGeomData();
      params[1]=_bief.lignesDirectrices_.getGeomData();

      // Ecriture du fichier
      FileWriteOperationAbstract writer=getFileFormat().createWriter();
      CtuluIOOperationSynthese synt=writer.write(params,_file, null);
      return synt;
    }

    public FileFormatVersionInterface getFileFormat() {
      return RubarStCnFileFormat.getInstance();
    }
  }
}
