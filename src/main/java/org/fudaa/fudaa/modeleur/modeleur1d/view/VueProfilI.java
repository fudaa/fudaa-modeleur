/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.modeleur.modeleur1d.view;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ebli.commun.EbliFormatterInterface;

/**
 * Une vue d'affichage de profils.
 * @author bmarchan
 */
public interface VueProfilI {

  /**
   * Efface la derni�re erreur affich�e.
   */
  void clearError();
  
  /**
   * Affichage d'une erreur
   * @param _message Le message affich� en erreur.
   */
  void showError(String _message);

  /**
   * Le manager de commande pour cette vue.
   * @return Le manager de commande.
   */
  CtuluCommandContainer getCommandContainer();
  
  /**
   * Le formatteur pour les champs r�els.
   * @return  Le formatteur.
   */
  EbliFormatterInterface getFormater();
}
