/*
 * @creation     10 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.model;

/**
 * Listener de profilContainer.
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public interface ProfilContainerListener {
  /**
   * Callback lors d'une modification dans le profilContainer.
   */
  public void profilContainerDataModified();
  
  /**
   * Callback lorsque le profil selectionn� � chang�.
   */
//  public void profilContainerSelectedChanged(int _idxOldProfil, int _idxNewProfil);
}
