/*
 * @creation     22 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.model;

/**
 * Les classes voullant r�agir aux modifications dans BiefCollection doivent
 * impl�menter cette interface.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public interface BiefSetListener {

  public void addedBief(Bief _bief, String _newName);
  public void renamedBief(Bief _bief, String _oldName, String _newName);
  public void removedBief(Bief _bief, String _oldName);
  
}
