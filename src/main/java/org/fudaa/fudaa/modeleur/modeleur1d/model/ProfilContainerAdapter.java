/*
 * @creation     10 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluNamedCommand;
import org.fudaa.ctulu.FastBitSet;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISAttributeModelIntegerList;
import org.fudaa.ctulu.gis.GISCoordinateSequenceContainerInterface;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ebli.geometrie.GrVecteur;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;

/**
 * Cette classe permet d'adapter une GISZoneCollection en un model manipulable
 * simplement. Cette simplification se fait en cachant toutes les informations
 * inutils et en ajoutant une information d'abcisse curviligne.
 * 
 * Chaque instance est associ�e � un index de profil sur la zone des profils
 * et cet index n'est plus modifi� par la suite.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class ProfilContainerAdapter implements ProfilContainerI {

  /**
   * Un command container pour le undo/redo.
   * Fonctionnement :
   * Deux �l�ments sont � prendre en compte lors du undo/redo :
   *  1) Il peut �tre n�c�ssaire de mettre a jour le z et/ou le curv.
   *  2) Le undo/redo peut �tre d�clanch� alors que le DataGeometryAdapter g�re actuellement une autre g�om�try.
   * Pour g�rer ces deux cas :
   *  1) Deux booleans sont pris par le constructeur indiquant si il faut mettre � jour les valeurs de z et/ou de curv.
   *  2) La zone et l'index de la g�om�trie g�r� au moment de l'action sont m�moris�s pour g�rer le cas 2. Dans le cas 
   *      o� un autre g�om�trie est g�r�e au moment du undo/redo aucun update n'est d�clanch�.
   * Ainsi m�me si la g�om�trie selectionn� change plein de fois, revient sur la selection pr�c�dente etc...La mise �
   * jour visuelle se fera correctement.
   * Information suppl�mentaire : pour fonctionner correctement le CtuluCommandContainer prit en param�tre contenant
   * le undo/redo ne doit pas d�pendre de DataGeometryAdapter mais seulement de la zone.
   */
  protected class DGCommandUndoRedo implements CtuluCommand, CtuluNamedCommand {
    private CtuluCommand cmd_;
    private boolean updateZ_;
    private boolean updateCurv_;
    private GISZoneCollectionLigneBrisee oldZone_;
    private int oldIdxGeom_;
    
    public DGCommandUndoRedo(CtuluCommand _cmd, boolean _updateZ, boolean _updateCurv){
      cmd_=_cmd;
      updateZ_=_updateZ;
      updateCurv_=_updateCurv;
      oldZone_=biefContainer_.getZoneProfils();
      oldIdxGeom_=idxProfilSelected_;
    }

    private void updateIfNeeded(){
      if(oldZone_==biefContainer_.getZoneProfils()&&oldIdxGeom_==idxProfilSelected_) {
        if(updateZ_) updateCacheZ();
        if(updateCurv_) updateCacheCurv();
        fireProfilContainerDataModified();
      }
    }
    
    public void redo() {
      cmd_.redo();
      updateIfNeeded();
    }

    public void undo() {
      cmd_.undo();
      updateIfNeeded();
    }

    public String getName() {
      if(cmd_ instanceof CtuluNamedCommand)
        return ((CtuluNamedCommand) cmd_).getName();
      else
        return null;
    }
  }
  protected class AddRemoveCommandUndoRedo implements CtuluCommand, CtuluNamedCommand {
    private CtuluCommand cmd_;
    private GISZoneCollectionLigneBrisee oldZone_;
    private int oldIdxGeom_;
    private int idxPoint_;
    private boolean add_;
    
    public AddRemoveCommandUndoRedo(CtuluCommand _cmd, boolean _add, int _idxPoint){
      cmd_=_cmd;
      oldZone_=biefContainer_.getZoneProfils();
      oldIdxGeom_=idxProfilSelected_;
      idxPoint_=_idxPoint;
      add_=_add;
    }

    private void updateIfNeeded(boolean _redo){
      if(oldZone_==biefContainer_.getZoneProfils()&&oldIdxGeom_==idxProfilSelected_) {
        updateCacheZ();
        updateCacheCurv();
        updateSpecialLines(idxPoint_, _redo ? add_:!add_);
        fireProfilContainerDataModified();
      }
    }
    
    public void redo() {
      cmd_.redo();
      updateIfNeeded(true);
    }

    public void undo() {
      cmd_.undo();
      updateIfNeeded(false);
    }

    public String getName() {
      if(cmd_ instanceof CtuluNamedCommand)
        return ((CtuluNamedCommand) cmd_).getName();
      else
        return null;
    }
  }
  
  /**
   * Marge d'acceptation avant de consid�rer un point comme �tant un point de
   * rupture. La valeur est l'angle maximal au dela duquel il sera lin�aris�.
   * Il est en radian.
   */
  static final double _tolerance=Math.PI-0.0001;//Math.PI/6;
  
  /** Le biefContainer contenant le bief contenant les profils utilis�s. */
  private BiefContainerAdapter biefContainer_;
  /** Le profil selectionn�. */
  private int idxProfilSelected_=-1;
  /** La liste contenant les abscisses en travers du profil selectionn� */
  private List<Double> curv_;
  // Caches \\
  private int idxZMax_;
  private int idxZMin_;
  /** D�calage de d�but d'axe hydraulique. */
  private double curviligneDecalage_;
  /** Les points de ruptures du profils sont contenus dans cet ensemble. */
  private Set<Integer> idxRuptures_=new HashSet<Integer>();

  public ProfilContainerAdapter(BiefContainerAdapter _biefContainer) {
    biefContainer_=_biefContainer;
//    biefContainer_.addBiefContainerListener(this);
  }

  public boolean isRupturePoint(int _idx) {
    if(_idx<0||_idx>=getNbPoint())
      // Message dev
      throw new IllegalArgumentException("Erreur prog : _idx n'appartient pas au profil.");
    return idxRuptures_.contains(_idx);
  }
  
  /**
   * Change le profil selectionn�.
   * @throws DataGeometryException 
   * @throws IllegalArgumentException 
   */
  public void setSelectedProfil(int _idxProfil) throws IllegalArgumentException, ProfilContainerException {
    if(_idxProfil!=-1&&!biefContainer_.hasProfil(_idxProfil))
      throw new IllegalArgumentException("Erreur prog : Profil : "+_idxProfil+" inconnu.");
    if(_idxProfil!=idxProfilSelected_) {
      idxProfilSelected_=_idxProfil;
      updateData();
    }
  }
  
  /**
   * Met � jour les valeurs zMin et zMax.
   * Si z est null, zMin et zMax sont mises � -1.
   */
  protected void updateCacheZ(){
    GISAttributeModel z=getZCollection();
    if(z!=null) {
      idxZMax_=0;
      idxZMin_=0;
      for (int i=1; i<z.getSize(); i++) {
        double val=(Double)z.getObjectValueAt(i);
        if (val>(Double)z.getObjectValueAt(idxZMax_))
          idxZMax_=i;
        else if (val<(Double)z.getObjectValueAt(idxZMin_))
          idxZMin_=i;
      }
    }
    else {
      idxZMax_=-1;
      idxZMin_=-1;
    }
  }
  
  /**
   * Met � jour les valeurs curv_.
   * Si curv_ ou zone_ sont null, rien n'est fait.
   */
  protected void updateCacheCurv(){
    if(curv_!=null&&biefContainer_.getZoneProfils()!=null) {
      CoordinateSequence seq=((GISCoordinateSequenceContainerInterface)biefContainer_.getZoneProfils().getGeometry(idxProfilSelected_)).getCoordinateSequence();
      curv_.clear();
      double curvPre=0;
      curv_.add((double)0);
      for (int i=1; i<seq.size(); i++) {
        double partialCurv=Math.sqrt(Math.pow(seq.getX(i)-seq.getX(i-1), 2)+Math.pow(seq.getY(i)-seq.getY(i-1), 2));
        curvPre=curvPre+partialCurv;
        curv_.add(curvPre);
      }
    }
  }
  
  /**
   * Permet de choisir la g�om�trie sur lequel l'instance va travailler.
   * Hypoth�se importante : les points de la g�om�tries sont correctement
   * ordonn�s (les points sont ordonn�s en ordre croissant de leur abscisse
   * curviligne). La veracit� de cette hypoth�se n'est PAS v�rifi� dans updateData.
   * 
   * @throws DataGeometryException 
   */
  protected void updateData() throws IllegalArgumentException, ProfilContainerException {
    if (idxProfilSelected_==-1) {
      curv_=null;
      curviligneDecalage_=0;
      idxRuptures_.clear();
    }
    else if (biefContainer_.getZoneProfils()!=null&&idxProfilSelected_>=0&&idxProfilSelected_<biefContainer_.getZoneProfils().getNbGeometries()&&biefContainer_.getZoneProfils().getAttributeIsZ()!=null) {
      try {
        if(biefContainer_.getZoneAxeHydraulique().getNumGeometries()==0)
          curviligneDecalage_=0;
        else {
          int idxAttrDecCurv=biefContainer_.getZoneAxeHydraulique().getIndiceOf(GISAttributeConstants.CURVILIGNE_DECALAGE);
          curviligneDecalage_=(Double) biefContainer_.getZoneAxeHydraulique().getValue(idxAttrDecCurv, 0);
        }
        CoordinateSequence seq=((GISCoordinateSequenceContainerInterface)biefContainer_.getZoneProfils().getGeometry(idxProfilSelected_)).getCoordinateSequence();
        // Verifie qu'on a bien au minimum deux points. \\
        if (seq.size()<2)
          throw new ProfilContainerException(MdlResource.getS("Le profil doit avoir au minimum 2 points."));
        // Verifie que deux points cons�cutifs ne sont pas confondus. \\
/*        for (int i=1; i<seq.size(); i++)
          if (seq.getX(i-1)==seq.getX(i)&&seq.getY(i-1)==seq.getY(i))
            throw new ProfilContainerException(MdlResource.getS("Au moins deux points dans le profil sont confondus."));
*/        
        // Verifie que le profil donn� ne poss�de pas des segments qui se coupent entre eux \\
        for(int i=1;i<seq.size();i++){
          // On passe les segments de longueur nulle.
          if (seq.getCoordinate(i-1).equals2D(seq.getCoordinate(i))) continue;
          
          Geometry seg1=GISGeometryFactory.INSTANCE.createLineString(new Coordinate[]{seq.getCoordinate(i-1), seq.getCoordinate(i)});
          boolean ok=true;
          
          // Valide : Un seul point d'intersection avec l'axe qui suit
          int j=i;
          while(ok && j+1<seq.size()) {
            // On passe les segments de longueur nulle.
            if (!seq.getCoordinate(j).equals2D(seq.getCoordinate(j+1))) {
              Geometry seg2=GISGeometryFactory.INSTANCE.createLineString(new Coordinate[]{seq.getCoordinate(j), seq.getCoordinate(j+1)});
              ok=seg1.intersection(seg2).getNumPoints()==1;
              j++;
              break;
            }
            j++;
          }
          
          // Valide : Aucun point d'intersection avec les axes qui suivent
          while(ok && j+1<seq.size()) {
            // On passe les segments de longueur nulle.
            if (!seq.getCoordinate(j).equals2D(seq.getCoordinate(j+1))) {
              Geometry seg2=GISGeometryFactory.INSTANCE.createLineString(new Coordinate[]{seq.getCoordinate(j), seq.getCoordinate(j+1)});
              ok=!seg1.intersects(seg2);
            }
            j++;
          }

          // Une intersection a �t� trouv�e.
          if(!ok)
            throw new ProfilContainerException(MdlResource.getS("Profil non conforme : Il se coupe lui m�me au sommet {0}",(j+1)));
        }
        
        /// D�termination des points de ruptures. \\\
        idxRuptures_.clear();
        int idx1=0;
        int idx2=1;
        int idx3=2;
        while(idx3<seq.size()) {
          if(idx1==idx2)
            idx2++;
          else if(idx2==idx3)
            idx3++;
          else if(seq.getCoordinate(idx1).equals(seq.getCoordinate(idx2)))
            idx2++;
          else if(seq.getCoordinate(idx2).equals(seq.getCoordinate(idx3)))
            idx3++;
          else {
            if(UtilsProfil1d.getAngle(seq.getCoordinate(idx1), seq.getCoordinate(idx2), seq.getCoordinate(idx3))<_tolerance)
              idxRuptures_.add(idx2);
            if(idx2+1<idx3)
              idx2++;
            else
              idx1=idx2;
          }
        }
        // Lin�arisation du profil
        int[] idxRuptures=new int[idxRuptures_.size()+2];
        idxRuptures[0]=0;
        Iterator<Integer> it=idxRuptures_.iterator();
        for (int i=0; i<idxRuptures_.size(); i++)
          idxRuptures[i+1]=it.next();
        idxRuptures[idxRuptures.length-1]=seq.size()-1;
        // On ne lin�arise plus. Pose des probl�me lors des undo, car les collections sont modifi�es. Ce n'est pas priori pas g�nant,
        // les points de ruptures semblent regler la totalit� du probl�me.
//        LibUtils.linearisePolyligne(biefContainer_.getZoneProfils(), idxProfilSelected_, idxRuptures, null);
        
        curv_=new ArrayList<Double>();
        // Calcul des acbscisses curvilignes => hypoth�se d'ordonnancement correcte des points \\
        updateCacheCurv();
        // Remplissage du cache Z \\
        updateCacheZ();

      }
      catch(ProfilContainerException _exp) {
        // Remise dans un �tat coh�rent
        idxProfilSelected_=-1;
        curv_=null;
        curviligneDecalage_=0;
        idxRuptures_.clear();
//        fireProfilContainerDataModified();
        // Propagation de l'exception
        throw _exp;
      }
    }
    else
      throw new IllegalArgumentException("Erreur prog : Les arguments sont invalides.");
    // Fin des verifications, tous est ok => accept�
//    fireProfilContainerDataModified();
  }

  public GISZoneCollection getGISZoneCollection(){
    return biefContainer_.getZoneProfils();
  }
  
  public int getIdxGeom(){
    return idxProfilSelected_;
  }
  
  public int getNbPoint() {
    if (idxProfilSelected_==-1)
      return 0;
    else
      return biefContainer_.getZoneProfils().getGeometry(idxProfilSelected_).getNumPoints();
  }

  public String getName() {
    if (idxProfilSelected_==-1) return null;
      return (String)getValueOf(GISAttributeConstants.TITRE);
  }

  /**
   * Retourne la collection correspondant au Z profil.
   */
  private GISAttributeModel getZCollection() {
    if (idxProfilSelected_==-1) return null;
    return (GISAttributeModel)getValueOf(biefContainer_.getZoneProfils().getAttributeIsZ());
  }
  
  /**
   * Retourne la valeur de l'attribut d'intersection demand�.
   */
  private Object getValueOf(GISAttributeInterface attr_) {
    int idxAtt=biefContainer_.getZoneProfils().getIndiceOf(attr_);
    return biefContainer_.getZoneProfils().getValue(idxAtt, idxProfilSelected_);
  }
  
  /**
   * Retourne la valeur de l'attribut d'intersection demand�.
   */
  private void setValueOf(GISAttributeInterface attr_, Object _val, CtuluCommandContainer _cmd) {
    int idxAtt=biefContainer_.getZoneProfils().getIndiceOf(attr_);
    biefContainer_.getZoneProfils().setAttributValue(idxAtt, idxProfilSelected_, _val, _cmd);
  }
  
  private CoordinateSequence getCoordSeq() {
    return ((GISCoordinateSequenceContainerInterface)biefContainer_.getZoneProfils().getGeometry(idxProfilSelected_))
        .getCoordinateSequence();
  }
  
  public double getAbsCurvRiveGauche() {
    if (idxProfilSelected_==-1)
      return -1;
    return getCurv((Integer)getValueOf(GISAttributeConstants.INTERSECTION_RIVE_GAUCHE));
  }

  public double getAbsCurvRiveDroite() {
    if (idxProfilSelected_==-1)
      return -1;
    return getCurv((Integer)getValueOf(GISAttributeConstants.INTERSECTION_RIVE_DROITE));
  }

  public double getAbsCurvLimiteStockageGauche() {
    if (idxProfilSelected_==-1)
      return -1;
    return getCurv((Integer)getValueOf(GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_GAUCHE));
  }
  
  public double getAbsCurvLimiteStockageDroite() {
    if (idxProfilSelected_==-1)
      return -1;
    return getCurv((Integer)getValueOf(GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_DROITE));
  }

  public double getAbsCurvProfilOnAxeHydraulique() {
    if (idxProfilSelected_==-1)
      return -1;
    GISZoneCollection zone=biefContainer_.getZoneProfils();
    String value=(String)zone.getValue(zone.getIndiceOf(GISAttributeConstants.COMMENTAIRE_HYDRO), idxProfilSelected_);
    return GISLib.getHydroCommentDouble(value, GISAttributeConstants.ATT_COMM_HYDRO_PK);
  }
  
  /**
   * Definit le PK du profil sur l'axe.
   */
  public void setAbsCurvProfilOnAxeHydraulique(double _abs, CtuluCommandContainer _cmd) {
    if (idxProfilSelected_==-1)
      return;
    GISZoneCollection zone=biefContainer_.getZoneProfils();
    String value=(String)zone.getValue(zone.getIndiceOf(GISAttributeConstants.COMMENTAIRE_HYDRO), idxProfilSelected_);
    String s=GISLib.setHydroCommentDouble(value, _abs, GISAttributeConstants.ATT_COMM_HYDRO_PK);
    zone.setAttributValue(zone.getIndiceOf(GISAttributeConstants.COMMENTAIRE_HYDRO), idxProfilSelected_, s, _cmd);
    fireProfilContainerDataModified();
  }
  
  public int getNbLignesDirectrices() {
    if (idxProfilSelected_==-1)
      return 0;
    return biefContainer_.getZoneLignesDirectrices().getNbGeometries();
  }
  
  public void setLigneDirectriceAt(int _idxLd, int _idxDestination, CtuluCommandContainer _cmd) throws ProfilContainerException {
    int[] allowed=getAllowedMoveOfLignesDirectricesTo(_idxDestination);
    if(allowed==null||!UtilsProfil1d.in(_idxLd, allowed))
      throw new ProfilContainerException(MdlResource.getS("Ce d�placement n'est pas permis"));
    class UndoRedoSetLigneDirectriceAt implements  CtuluCommand, CtuluNamedCommand {
      CtuluCommandComposite cmd_;
      public UndoRedoSetLigneDirectriceAt(CtuluCommandComposite _cmd) {
        cmd_=_cmd;
      }
      public String getName() {
        return MdlResource.getS("D�placement de ligne directrice");
      }
      public void redo() {
        cmd_.redo();
        fireProfilContainerDataModified();
      }
      public void undo() {
        cmd_.undo();
        fireProfilContainerDataModified();
      }
    }
    GISAttributeModelIntegerList lst=(GISAttributeModelIntegerList)getValueOf(GISAttributeConstants.INTERSECTIONS_LIGNES_DIRECTRICES);
    // Changement de la valeur
    CtuluCommandComposite cmd=new CtuluCommandComposite();
    lst.set(_idxLd, _idxDestination, cmd);
    fireProfilContainerDataModified();
    if(_cmd!=null)
      _cmd.addCmd(new UndoRedoSetLigneDirectriceAt(cmd));
  }
  
  public String getLigneDirectriceName(int _idx) {
    if (idxProfilSelected_==-1)
      return null;
    GISZoneCollection zoneLd=biefContainer_.getZoneLignesDirectrices();
    if(_idx<0||_idx>=zoneLd.getNumGeometries())
      return null;
    int idxTitre=zoneLd.getIndiceOf(GISAttributeConstants.TITRE);
    if(idxTitre==-1)
      return null;
    return (String) zoneLd.getValue(idxTitre, _idx);
  }
  
  public int[] getAllowedMoveOfLignesDirectricesTo(int _idxPoint) {
    if(idxProfilSelected_==-1||_idxPoint<0||_idxPoint>=getNbPoint())
      return null;
    // Recherche des lignes avants et apr�s.
    int[] namesLdPrevious=new int[0];
    int idx=_idxPoint;
    while (namesLdPrevious.length==0&&--idx>=0)
      namesLdPrevious=getIdxLignesDirectricesAt(idx);
    int[] namesLdNext=new int[0];
    idx=_idxPoint;
    while(namesLdNext.length==0&&++idx<getNbPoint())
      namesLdNext=getIdxLignesDirectricesAt(idx);
    // Agr�gation des r�sultats
    int[] result=new int[namesLdPrevious.length+namesLdNext.length];
    System.arraycopy(namesLdPrevious, 0, result, 0, namesLdPrevious.length);
    System.arraycopy(namesLdNext, 0, result, namesLdPrevious.length, namesLdNext.length);
    return result;
  }
  
  public int[] getIdxLignesDirectricesAt(int _idxPoint) {
    if (idxProfilSelected_==-1||_idxPoint<0||_idxPoint>=getNbPoint())
      return new int[0];
    // R�cup�ration des croisements entre les lignes directrices et le profil
    GISZoneCollection zone=biefContainer_.getZoneProfils();
    int idxAtt=zone.getIndiceOf(GISAttributeConstants.INTERSECTIONS_LIGNES_DIRECTRICES);
    GISAttributeModelIntegerList lst=(GISAttributeModelIntegerList) zone.getValue(idxAtt, idxProfilSelected_);
    // S�lection des index utils
    int nbIdx=0;
    for(int i=0;i<lst.getSize();i++)
      if(lst.getValue(i)==_idxPoint)
        nbIdx++;
    int[] result=new int[nbIdx];
    int j=0;
    for(int i=0;i<lst.getSize();i++)
      if(lst.getValue(i)==_idxPoint)
        result[j++]=i;
    return result;
  }

  public int getPointForDirLine(int _idxLine) {
    if (idxProfilSelected_ == -1 || _idxLine < 0 || _idxLine >= getNbLignesDirectrices()) {
      return -1;
    }
    
    GISZoneCollection zone=biefContainer_.getZoneProfils();
    int idxAtt=zone.getIndiceOf(GISAttributeConstants.INTERSECTIONS_LIGNES_DIRECTRICES);
    GISAttributeModelIntegerList lst=(GISAttributeModelIntegerList) zone.getValue(idxAtt, idxProfilSelected_);
    return lst.getValue(_idxLine);
  }
  
  /**
   * Definit la position de la ligne directrice, sans aucun controle de forme.
   * @param _idxLine L'index de ligne
   * @param _idxPoint L'index du point position.
   */
  private void setPointForDirLine(int _idxLine, int _idxPoint, CtuluCommandContainer _cmd) {
    if (idxProfilSelected_ == -1 || _idxLine < 0 || _idxLine >= getNbLignesDirectrices()) {
      return;
    }
    
    GISZoneCollection zone=biefContainer_.getZoneProfils();
    int idxAtt=zone.getIndiceOf(GISAttributeConstants.INTERSECTIONS_LIGNES_DIRECTRICES);
    GISAttributeModelIntegerList lst=(GISAttributeModelIntegerList) zone.getValue(idxAtt, idxProfilSelected_);
    lst.set(_idxLine,_idxPoint,_cmd);
  }
  
  public String[] getNamesLignesDirectricesAt(int _idxPoint) {
    if (idxProfilSelected_==-1||_idxPoint<0||_idxPoint>=getNbPoint())
      return new String[0];
    int[] idx=getIdxLignesDirectricesAt(_idxPoint);
    if(idx.length==0)
      return new String[0];
    // R�cup�rationd des noms a partir des index
    String[] result=new String[idx.length];
    for(int i=0;i<result.length;i++)
      result[i]= getLigneDirectriceName(idx[i]);
    return result;
  }
  
  public String[] getNamesLignesDirectrices() {
    if (idxProfilSelected_==-1)
      return null;
    GISZoneCollection zoneLd=biefContainer_.getZoneLignesDirectrices();
    int idxTitre=zoneLd.getIndiceOf(GISAttributeConstants.TITRE);
    if(idxTitre==-1)
      return null;
    String[] result=new String[zoneLd.getNbGeometries()];
    for(int i=0; i<result.length;i++)
      result[i]=(String) zoneLd.getValue(idxTitre, i);
    return result;
  }
  
  public double getAbsCurvAxeHydrauliqueOnProfil() {
    if (idxProfilSelected_==-1||biefContainer_.getZoneAxeHydraulique().getNbGeometries()==0)
      return -1;
    Geometry intersection=biefContainer_.getZoneAxeHydraulique().getGeometry(0).intersection(
        biefContainer_.getZoneProfils().getGeometry(idxProfilSelected_));
    if (intersection.getNumPoints()!=1)
      return -1;
    return UtilsProfil1d.abscisseCurviligne(getCoordSeq(), intersection.getCoordinate());
  }
  
  public void setLimiteGauche(int _idx, CtuluCommandContainer _cmd) throws ProfilContainerException {
    int idxRiveGauche=(Integer) getValueOf(GISAttributeConstants.INTERSECTION_RIVE_GAUCHE);
    if(_idx<=idxRiveGauche)
      setIndexIn(GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_GAUCHE, _idx, _cmd);
    else
      throw new ProfilContainerException(MdlResource.getS("La limite gauche doit rester � gauche de la rive gauche."));
  }

  public void setLimiteDroite(int _idx, CtuluCommandContainer _cmd) throws ProfilContainerException {
    int idxRiveDroite=(Integer) getValueOf(GISAttributeConstants.INTERSECTION_RIVE_DROITE);
    if(_idx>=idxRiveDroite)
      setIndexIn(GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_DROITE, _idx, _cmd);
    else
      throw new ProfilContainerException(MdlResource.getS("La limite droite doit rester � droite de la rive droite."));
  }

  public void setRiveGauche(int _idx, CtuluCommandContainer _cmd) throws ProfilContainerException {
    int idxLimiteGauche=(Integer) getValueOf(GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_GAUCHE);
    int idxRiveDroite=(Integer) getValueOf(GISAttributeConstants.INTERSECTION_RIVE_DROITE);
    if(_idx>=idxLimiteGauche&&_idx<idxRiveDroite)
      setIndexIn(GISAttributeConstants.INTERSECTION_RIVE_GAUCHE, _idx, _cmd);
    else
      throw new ProfilContainerException(MdlResource.getS("La rive gauche doit rester entre la limite gauche et la rive droite."));
  }

  public void setRiveDroite(int _idx, CtuluCommandContainer _cmd) throws ProfilContainerException {
    int idxLimiteDroite=(Integer) getValueOf(GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_DROITE);
    int idxRiveGauche=(Integer) getValueOf(GISAttributeConstants.INTERSECTION_RIVE_GAUCHE);
    if(_idx<=idxLimiteDroite&&_idx>idxRiveGauche)
      setIndexIn(GISAttributeConstants.INTERSECTION_RIVE_DROITE, _idx, _cmd);
    else
      throw new ProfilContainerException(MdlResource.getS("La rive droite doit rester entre la limite droite et la rive gauche."));
  }
  
  private void setIndexIn(GISAttributeInterface _attr, int _idx, CtuluCommandContainer _cmd) {
    if(idxProfilSelected_==-1||_idx<0||_idx>=getNbPoint())
      throw new IllegalArgumentException("Erreur prog : Cet index n'existe pas");
    class UndoRedoSetIndexIn implements  CtuluCommand, CtuluNamedCommand {
      GISAttributeInterface attr_;
      CtuluCommandComposite cmd_;
      public UndoRedoSetIndexIn(GISAttributeInterface _attr, CtuluCommandComposite _cmd) {
        attr_=_attr;
        cmd_=_cmd;
      }
      public String getName() {
        return MdlResource.getS("D�placement de {0}",attr_.getName());
      }
      public void redo() {
        cmd_.redo();
        fireProfilContainerDataModified();
      }
      public void undo() {
        cmd_.undo();
        fireProfilContainerDataModified();
      }
    }
    int idxAttr=biefContainer_.getZoneProfils().getIndiceOf(_attr);
    CtuluCommandComposite cmd=new CtuluCommandComposite();
    if(_idx!=(Integer) biefContainer_.getZoneProfils().getValue(idxAttr, idxProfilSelected_))
      biefContainer_.getZoneProfils().setAttributValue(idxAttr, idxProfilSelected_, _idx, cmd);
    if(_cmd!=null)
      _cmd.addCmd(new UndoRedoSetIndexIn(_attr, cmd));
    fireProfilContainerDataModified();
  }
  
  public boolean moveProfilOnAxeHydraulique(double _pk, CtuluCommandContainer _cmd) throws ProfilContainerException {
    double absZeroDecal=_pk-curviligneDecalage_;
    CtuluCommandComposite cmp=new CtuluCommandComposite();
    // Cas si un axe hydraulique existe \\
    if (biefContainer_.getZoneAxeHydraulique().getNumGeometries()>0) {
      CoordinateSequence axeHydrau=((GISCoordinateSequenceContainerInterface)biefContainer_.getZoneAxeHydraulique().getGeometry(0))
          .getCoordinateSequence();
      Geometry geomAxeHydrau=biefContainer_.getZoneAxeHydraulique().getGeometry(0);
      if (absZeroDecal<0||absZeroDecal>UtilsProfil1d.abscisseCurviligne(axeHydrau, axeHydrau.getCoordinate(axeHydrau.size()-1)))
        throw new IllegalArgumentException(MdlResource.getS("L'abscisse curviligne doit �tre entre {0} et {1} inclus.",
            curviligneDecalage_,(UtilsProfil1d.abscisseCurviligne(axeHydrau, axeHydrau.getCoordinate(axeHydrau.size()-1))+curviligneDecalage_)));
      // Calcul de la coordonn�e actuelle de croisement entre l'axe et le profil
      Geometry intersection=geomAxeHydrau.intersection(biefContainer_.getZoneProfils().getGeometry(idxProfilSelected_));
      if (intersection.getNumPoints()!=1)
        throw new ProfilContainerException(MdlResource.getS("Soit il n'y a pas de croisement avec l'axe hydraulique, soit il y en a plusieurs."));
      // Point de croisement actuel entre l'axe et le profil
      Coordinate oldCoordCroisement=intersection.getCoordinate();
      double oldAbscisseCurviligne=UtilsProfil1d.abscisseCurviligne(axeHydrau, oldCoordCroisement);
      
      if (!UtilsProfil1d.egal(absZeroDecal, oldAbscisseCurviligne)) {
        // Cr�ation d'une selection contenant la g�om�trie � modifier
        FastBitSet bs=new FastBitSet(biefContainer_.getZoneProfils().getNbGeometries());
        bs.set(idxProfilSelected_);
        CtuluListSelection selection=new CtuluListSelection(bs);
        // Calcul du future point de croisement entre l'axe et le profil
        Coordinate newCoordCroisement=UtilsProfil1d.getCoordinateXY(axeHydrau, absZeroDecal);
        // Application de la translation
        Coordinate move=UtilsProfil1d.vec(oldCoordCroisement, newCoordCroisement);
        // R�duction de la pr�cision pour �viter les probl�mes d'arrondis 
        move.x=((double) ((long) (move.x*10000.)))/10000;
        move.y=((double) ((long) (move.y*10000.)))/10000;
        biefContainer_.getModelProfils().moveGlobal(selection, move.x, move.y, 0, cmp);
        
        // On recup�re les 2 segments d'intersection...
        //... du profil de d�part avec l'axe
        int oldNextIdx=UtilsProfil1d.getNextIndex(axeHydrau, oldCoordCroisement);
        if (oldNextIdx==-1)
          oldNextIdx=axeHydrau.size()-1;
        //... du profil d'arriv�e avec l'axe.
        int newNextIdx=UtilsProfil1d.getNextIndex(axeHydrau, newCoordCroisement);
        if (newNextIdx==-1)
          newNextIdx=axeHydrau.size()-1;
        
        // Calcul de l'angle entre ces 2 segments, angle correspondant a la rotation a effectuer.
        GrVecteur v1=new GrVecteur(axeHydrau.getCoordinate(oldNextIdx).x-axeHydrau.getCoordinate(oldNextIdx-1).x,
                                   axeHydrau.getCoordinate(oldNextIdx).y-axeHydrau.getCoordinate(oldNextIdx-1).y,
                                   0);
        GrVecteur v2=new GrVecteur(axeHydrau.getCoordinate(newNextIdx).x-axeHydrau.getCoordinate(newNextIdx-1).x,
                                   axeHydrau.getCoordinate(newNextIdx).y-axeHydrau.getCoordinate(newNextIdx-1).y,
                                   0);
        double angrot=v1.getAngleXY(v2);
        
        // Application de la rotation
        biefContainer_.getModelProfils()
            .rotateGlobal(selection, angrot, newCoordCroisement.x, newCoordCroisement.y, cmp);
        // Verifie que le profil ne coupe pas deux fois l'axe hydraulique
        Geometry intersect=geomAxeHydrau.intersection(biefContainer_.getZoneProfils().getGeometry(idxProfilSelected_));
        if (intersect.getNumPoints()!=1) {
          cmp.undo(); // Annulation des modifications
          throw new ProfilContainerException(
              MdlResource.getS("D�placement non autoris� : Le profil coupe l'axe hydraulique en 2 endroits."));
        }
      }
    }
    // Modification bas�e sur l'attribut commentaire hydraulique. \\
    setAbsCurvProfilOnAxeHydraulique(_pk, cmp);
    GISZoneCollection zone=biefContainer_.getZoneProfils();
    int idxAttrComm=zone.getIndiceOf(GISAttributeConstants.COMMENTAIRE_HYDRO);
    // Gestion de l'undo/redo
    if (_cmd!=null)
      _cmd.addCmd(cmp.getSimplify());
    // Retourne vrai si il faut r�ordonnancer les profils
    if (zone.getNumGeometries()==1)
      return false;
    if (idxProfilSelected_==0) {
      if (GISLib.getHydroCommentDouble((String)zone.getValue(idxAttrComm, 1), GISAttributeConstants.ATT_COMM_HYDRO_PK)<_pk)
        return true;
    }
    else if (idxProfilSelected_<zone.getNumGeometries()-1) {
      if (GISLib.getHydroCommentDouble((String)zone.getValue(idxAttrComm, idxProfilSelected_-1), GISAttributeConstants.ATT_COMM_HYDRO_PK)>_pk
          ||GISLib.getHydroCommentDouble((String)zone.getValue(idxAttrComm, idxProfilSelected_+1), GISAttributeConstants.ATT_COMM_HYDRO_PK)<_pk)
        return true;
    }
    else if (idxProfilSelected_==zone.getNumGeometries()-1)
      if (GISLib.getHydroCommentDouble((String)zone.getValue(idxAttrComm, idxProfilSelected_-1), GISAttributeConstants.ATT_COMM_HYDRO_PK)>_pk)
        return true;
    return false;
  }
  
  public double getCurv(int _idxPoint) {
    if(curv_==null||_idxPoint<0||_idxPoint>=curv_.size())
      throw new IllegalArgumentException("Erreur prog : Cet index n'existe pas.");
    return curv_.get(_idxPoint);
  }

  public double getZ(int _idxPoint) {
    GISAttributeModel z=getZCollection();
    if (z==null||_idxPoint<0||_idxPoint>=z.getSize())
      throw new IllegalArgumentException("Erreur prog : Cet index n'existe pas.");
    return ((Double)z.getObjectValueAt(_idxPoint)).doubleValue();
  }
  
  public void setCurv(int _idxPoint, double _value, CtuluCommandContainer _cmd) throws ProfilContainerException {
    if (getCurv(_idxPoint)==_value) return;

    if(curv_==null||_idxPoint<0||_idxPoint>=curv_.size()) // Si curv_!=null alors z et index_ ont des valeurs coh�rentes
      throw new IllegalArgumentException("Erreur prog : Cet index n'existe pas.");
    // Extraction des coordonn�es de la g�om�trie pour pouvoir les modifier par la suite
    CoordinateSequence seq=((GISCoordinateSequenceContainerInterface)biefContainer_.getZoneProfils().getGeometry(idxProfilSelected_)).getCoordinateSequence();
    Coordinate[] coords=seq.toCoordinateArray().clone();
    /*
     * A propos de coords : le tableau retourn� est r�f�renc� (via une weak
     * reference) par le cache de la coordinate sequence dont il est issu. Donc,
     * pour �viter des probl�mes de point fantome, il faut le cloner. Cependant
     * les coordinantes qu'il contient doivent �galement �tre clon�es pour la
     * m�me raison. Pour ne pas trop perdre en performance, seuls les
     * coordonn�es modifi�es sont clon�es.
     */
//    // Les nouvelles valeurs du points � d�placer
//    double newX;
//    double newY;
//    // Delta pour savoir si des doubles sont �gaux
//    double tolerance=0.000001;
    
    // Cas o� l'index n'est pas sur un point de rupture \\
    if (!idxRuptures_.contains(_idxPoint)) {
      // Verifie que le point reste encadr� par le(s) m�me(s) point(s)
      if((_idxPoint==0&&_value>=curv_.get(1))||(_idxPoint==seq.size()-1&&_value<=curv_.get(seq.size()-2))||(_idxPoint!=0&&_idxPoint!=seq.size()-1&&(_value<=curv_.get(_idxPoint-1)||_value>=curv_.get(_idxPoint+1))))
        throw new ProfilContainerException(MdlResource.getS("Impossible de d�placer un point au del� des points l'encadrant."));
      
      // Le point reste encadr� par les deux m�me points => le signe de xa-xb et
      // de ya-yb ne change pas
      Coordinate newCoord=UtilsProfil1d.getCoordinateXY(seq, _value);
      newCoord.z=coords[_idxPoint].z;
      coords[_idxPoint]=newCoord;
      // Mise a jour de la table des valeurs curvilignes
      if (_idxPoint==0)
        for (int i=1; i<curv_.size(); i++)
          curv_.set(i, curv_.get(i)-_value);
      else
        curv_.set(_idxPoint, _value);
    }
    else
      throw new ProfilContainerException(MdlResource.getS("Point de rupture non d�pla�able suivant l'abscisse en travers"));
    
//    // Cas complexe : l'index est sur un point de rupture \\
//    else {
//      /* 
//       * Abstract :
//       * Pour trouver le point restectant la concervation de labcisse curviligne, est 
//       * utilis� par la suite une m�thode d'intersection de deux cercles.
//       * - Soient A, B, C, trois points non confondus. Soit la polyligne A, C, B.
//       * On veut construire C' tq cruv(C')=c et que curv(B) reste constant.
//       * Donc :
//       *  dist(AC)+dist(CB)=dist(AC')+dist(C'B)
//       * Ce probl�me peut se r�duire � la recherche de l'intersection de deux cercles :
//       * C1=cercle(centre=A, rayon=c-curv(A)) avec C2=cercle(centre=B, rayon=curv(B)-c)
//       * 
//       * Cas 1 : ya-yb != 0:
//       *  La r�solution de ce probl�me donne l'�quation :
//       *    x�[M�+1] + x[2yaM-2NM-2xa] + xa�+ya�+N�-rayonC1�-2yaN=0
//       *    avec N=(rayonC2�-rayonC1�-xb�+xa�-yb�+ya�)/(2(ya-yb))
//       *    et M=(xa-xb)/(ya-yb)
//       *  C'est une �quation du second degr�.
//       *  Pour la r�solution de l'�quation, nous nommerons :
//       *    equA=[M�+1]
//       *    equB=[2yaM-2NM-2xa]
//       *    equC=xa�+ya�+N�-rayonC1�-2yaN
//       *  La r�solution de l'�quation r�sultante donne deux solutions (c1' et c2').
//       *  Pour trouver celle qui nous interesse (si elle existe) la contrainte suivante doit �tre v�rifi�e :
//       *    vecteurNorm�(C, Proj(C, AB)) = vecteurNorm�(CX', Proj(CX', AB))
//       *    A noter que Proj(C1', AB)=Proj(C2', AB) de plus pour simplifier cette r�solution sera faite sur z=0.
//       * 
//       * Cas 2 : ya-yb = 0 et xa-xb != 0:
//       *  La r�solution de ce prol�me donne l'�quation :
//       *    y�-2yay+xa+K�-2xaK-rayonC1�=0
//       *    avec K=(rayonC2�-rayonC1�-xb�+xa�)/(2*(xa-xb))
//       *    # k est �galement la valeur de x des deux solutions potentielles
//       *  C'est un �quation du second degr�.
//       *  Pour la r�solution de l'aquation, nous nommerons :
//       *    equA=1
//       *    equB=2ya
//       *    equC=K�-2xaK-rayonC1�
//       *  La r�solution de l'�quation r�sultante donne deux solutions (c1' et c2').
//       *  Pour trouver celle qui nous interesse (si elle existe) la contrainte suivante doit �tre v�rifi�e :
//       *    vecteurNorm�(C, Proj(C, AB)) = vecteurNorm�(CX', Proj(CX', AB))
//       *    A noter que Proj(C1', AB)=Proj(C2', AB) de plus pour simplifier cette r�solution sera faite sur z=0.
//       * 
//       * Cas 3 : ya-yb = 0 et xa-xb=0:
//       *  quel qu'en soit le r�sultat, c'est incoh�rent pour notre contexte.
//       *  
//       * De plus on verifie que le r�sultat ne provoque pas de croisement avec les autres axes
//       * du profil.
//       * 
//       * Une fois ce point trouv�, on va �galement d�placer tous les points des deux axes adjacents,
//       * pour que le profil concerve sa forme g�n�rale. Ce r�aligement se faire en concervant les 
//       * abscisses curvilignes de chacun des points.
//       * Pour y parvenir le nouveau coefficient directeur des axes est calcul� et appliqu� au rapport de
//       * l'abcisse curviligne avec l'abscisse du point extr�mit� de l'axe.
//       * 
//       * Derni�re petite note pour la fin, le cas dit 'simple' pr�c�dent
//       * (d�placement d'un point qui n'est pas un point de rupture) n'est qu'un
//       * cas particulier de la r�solution ci dessus (la solution de l'�quation
//       * du second degr� serait unique). On pourrait donc l'enlever, mais je pense que
//       * s�parer ces deux cas ne fait pas de mal pour une �ventuelle relecture
//       * et/ou modification de ce code.
//       */
//      
//      // Application de la m�thode d�crite ci dessus \\
//      
//      // Valuation des variables d'entr�es n�c�ssaires
//      final int idxPtdeb=_idxPoint==idxRupture1_?0:idxRupture1_;
//      final int idxPtFin=_idxPoint==idxRupture2_||idxRupture2_==-1?seq.size()-1:idxRupture2_;
//      final Coordinate a=seq.getCoordinate(idxPtdeb);
//      final Coordinate b=seq.getCoordinate(idxPtFin);
//      final Coordinate c=seq.getCoordinate(_idxPoint);
//      final double rayonC1=_value-curv_.get(idxPtdeb);
//      final double rayonC2=curv_.get(idxPtFin)-_value;
//      // Solutions
//      double x1;
//      double y1;
//      double x2;
//      double y2;
//      // Cas 1
//      if(Math.abs(a.y-b.y)>tolerance){
//        // Variables interm�diaires
//        double n=(rayonC2*rayonC2-rayonC1*rayonC1-b.x*b.x+a.x*a.x-b.y*b.y+a.y*a.y)/(2*(a.y-b.y));
//        double m=(a.x-b.x)/(a.y-b.y);
//        double equA=m*m+1;
//        double equB=2*(a.y*m-n*m-a.x);
//        double equC=a.x*a.x+a.y*a.y+n*n-rayonC1*rayonC1-2*a.y*n;
//        // Variables de r�solution
//        double deltaTmp=equB*equB-4*equA*equC;
//        if(deltaTmp<0)
//          throw new ProfilContainerException(MdlResource.getS("Ce d�placement n'est pas possible sans violer la contrainte de constance de la longueur du profil."));
//        double delta=Math.sqrt(equB*equB-4*equA*equC);
//        x1=(-equB+delta)/(2*equA);
//        y1=n-x1*m;
//        x2=(-equB-delta)/(2*equA);
//        y2=n-x2*m;
//      }
//      // Cas 2
//      else if(Math.abs(a.x-b.x)>tolerance){
//        // Variables interm�diaires
//        double k=(rayonC2*rayonC2-rayonC1*rayonC1-b.x*b.x+a.x*a.x)/(2*(a.x-b.x));
//        double equA=1;
//        double equB=2*a.y;
//        double equC=k*k-2*a.x*k-rayonC1*rayonC1;
//        // Variables de r�solution
//        double deltaTmp=equB*equB-4*equA*equC;
//        if(deltaTmp<0)
//          throw new ProfilContainerException(MdlResource.getS("Ce d�placement n'est pas possible sans violer la contrainte de constance de la longueur du profil."));
//        double delta=Math.sqrt(equB*equB-4*equA*equC);
//        y1=(-equB+delta)/(2*equA);
//        x1=k;
//        y2=(-equB-delta)/(2*equA);
//        x2=k;
//      }
//      // Cas 3
//      else
//        throw new ProfilContainerException(MdlResource.getS("Ce d�placement n'est pas valide. Ce cas ne devrait jamais arriver. CSI_1"));
//      // Recherche du r�sultat nous interessant dans ce cas ci
//      Coordinate aZ0=new Coordinate(a.x, a.y, 0);
//      Coordinate bZ0=new Coordinate(b.x, b.y, 0);
//      Coordinate cZ0=new Coordinate(c.x, c.y, 0);
//      Coordinate vecReference=UtilsProfil1d.vec(cZ0, UtilsProfil1d.proj(cZ0, aZ0, bZ0));
//      Coordinate cProj=UtilsProfil1d.proj(new Coordinate(x1, y1, 0), aZ0, bZ0);
//      assert(UtilsProfil1d.egal(cProj, UtilsProfil1d.proj(new Coordinate(x2, y2, 0), aZ0, bZ0), 0.0001, false));
//      if(UtilsProfil1d.egal(vecReference, UtilsProfil1d.vec(new Coordinate(x1, y1, 0), cProj), 0.0001, true)){
//        newX=x1;
//        newY=y1;
//      }
//      else if(UtilsProfil1d.egal(vecReference, UtilsProfil1d.vec(new Coordinate(x2, y2, 0), cProj), 0.0001, true)) {
//        newX=x2;
//        newY=y2;
//      }
//      else
//        throw new ProfilContainerException(MdlResource.getS("Ce d�placement n'est pas valide. Ce cas ne devrait jamais arriver. CSI_2"));
//
//      // Verifie que le point trouv� ne coupe pas un autre axe du profil \\
//      boolean noCut=true;
//      Coordinate newP=new Coordinate(newX, newY);
//      GISPolyligne axe1=GISGeometryFactory.INSTANCE.createLineString(new Coordinate[]{a, newP});
//      GISPolyligne axe2=GISGeometryFactory.INSTANCE.createLineString(new Coordinate[]{newP, b});
//      if(idxPtdeb>0) {
//        /*
//         * Test de croisement entre les deux axes modifi�s et l'axe avant. l'axe
//         * a, newP a normalement un point d'intersectino (a).
//         */
//        GISPolyligne axeTest=GISGeometryFactory.INSTANCE.createLineString(new Coordinate[]{new Coordinate(coords[0]), new Coordinate(coords[idxPtdeb])});
//        noCut=axe1.intersection(axeTest).getNumPoints()==1;
//        noCut=noCut&&!axe2.intersects(axeTest);
//      }
//      if(idxPtFin<seq.size()-1) {
//        /*
//         * Test de croisement entre les deux axes modifi�s et l'axe avant. l'axe
//         * newP, b a normalement un point d'intersectino (b).
//         */
//        GISPolyligne axeTest=GISGeometryFactory.INSTANCE.createLineString(new Coordinate[]{new Coordinate(coords[idxPtFin]), new Coordinate(coords[coords.length-1])});
//        noCut=noCut&&axe2.intersection(axeTest).getNumPoints()==1;
//        noCut=noCut&&!axe1.intersects(axeTest);
//      }
//      // Traitement de l'�ventuelle d�tection d'un croisement
//      if(!noCut)
//        throw new ProfilContainerException(MdlResource.getS("Ce d�placement est impossible sans obtenir un profil dont des axes se coupent."));
//      // Enregistrement du nouveau point C' et de la nouvelle valeur curviligne
//      coords[_idxPoint]=new Coordinate(newX, newY, coords[_idxPoint].z);
//      // Mise a jour de la table des valeurs curvilignes
//      curv_.set(_idxPoint, _value);
//      
//      // R�alignement des points des axes adjacents \\
//      for(int inc=0;inc<2;inc++){
//        // D�termination des points de d�but et de fin d'axe
//        int idxDeb;
//        int idxFin;
//        if(inc==0) { // premi�re it�ration, axe : debut, C'
//          idxDeb=_idxPoint==idxRupture1_?0:idxRupture1_;
//          idxFin=_idxPoint;
//        }
//        else { // seconde it�ration, axe : C', fin
//          idxDeb=_idxPoint;
//          idxFin=_idxPoint==idxRupture2_||idxRupture2_==-1 ? seq.size()-1:idxRupture2_;
//        }
//        // R�alignement sur l'axe
//        if (Math.abs(coords[idxFin].x-coords[idxDeb].x)>tolerance) {
//          double sign=Math.signum(coords[idxFin].x-coords[idxDeb].x);
//          double newCoefDirecteur=(coords[idxFin].y-coords[idxDeb].y)/(coords[idxFin].x-coords[idxDeb].x);
//          for (int i=idxDeb+1; i<idxFin; i++) {
//            coords[i]=new Coordinate(0, 0, coords[i].z);
//            coords[i].x=coords[idxDeb].x+sign*(curv_.get(i)-curv_.get(idxDeb))/Math.sqrt(1+newCoefDirecteur*newCoefDirecteur);
//            coords[i].y=coords[idxDeb].y+newCoefDirecteur*(coords[i].x-coords[idxDeb].x);
//          }
//        }
//        else { // Cas du bout de profil vertical
//          double sign=Math.signum(coords[idxFin].y-coords[idxDeb].y);
//          for (int i=idxDeb+1; i<idxFin; i++)
//            coords[i]=new Coordinate(coords[idxDeb].x, coords[idxDeb].y+sign*(curv_.get(i)-curv_.get(idxDeb)),
//                coords[i].z);
//        }
//      }
//    }
    CtuluCommandComposite cmd=new CtuluCommandComposite(MdlResource.getS("Changement abs. curviligne"));
    // Enregistrement des nouvelles coordonn�es \\
    biefContainer_.getZoneProfils().setCoordinateSequence(idxProfilSelected_, GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory().create(coords), cmd);
    // Gestion du undo/redo \\
    if(_cmd!=null)
      _cmd.addCmd(new DGCommandUndoRedo(cmd, false, true));
    // Notification des changements de valeurs
    fireProfilContainerDataModified();
  }

  public void setZ(int _idxPoint, double _value, CtuluCommandContainer _cmd) throws ProfilContainerException {
    if (getZ(_idxPoint)==_value) return;
    
    GISAttributeModel z=getZCollection();
    if (z==null||_idxPoint<0||_idxPoint>=z.getSize())
      throw new IllegalArgumentException("Erreur prog : Cet index n'existe pas.");
    CtuluCommandComposite cmp=new CtuluCommandComposite(MdlResource.getS("Changement Z"));
    // Mise � jour du z \\
    // B.M. Il faut indiquer que l'attribut a chang�, pour que les g�om�tries volatiles soient remises a jour.
    z=((GISAttributeModel)z).createSubModel(new int[0]);
    z.setObject(_idxPoint, _value, null);
    int iattZ=biefContainer_.getZoneProfils().getIndiceOf(GISAttributeConstants.BATHY);
    biefContainer_.getZoneProfils().setAttributValue(iattZ, idxProfilSelected_, z, cmp);

    // Gestion du undo/redo \\
    if(_cmd!=null)
      _cmd.addCmd(new DGCommandUndoRedo(cmp, true, false));
    // Mise � jour de idxZMin_ et idxZMax_
    if (_idxPoint==idxZMax_||_idxPoint==idxZMin_)
      updateCacheZ();
    else if (_value>(Double)z.getObjectValueAt(idxZMax_))
      idxZMax_=_idxPoint;
    else if (_value<(Double)z.getObjectValueAt(idxZMin_))
      idxZMin_=_idxPoint;
    fireProfilContainerDataModified();
  }

  public double getCurvMax() {
    if(curv_==null)
      return 0;
    else
      return curv_.get(curv_.size()-1);
  }

  public double getCurvMin() {
    if(curv_==null)
      return 0;
    else
      return curv_.get(0); // toujours 0 de toute fa�on
  }

  public double getZMax() {
    GISAttributeModel z=getZCollection();
    if(z==null)
      return 0;
    else
      return (Double) z.getObjectValueAt(idxZMax_);
  }

  public double getZMin() {
    GISAttributeModel z=getZCollection();
    if(z==null)
      return 0;
    else
      return (Double) z.getObjectValueAt(idxZMin_);
  }
  
  public void setValues(int _idxPoint, double _valueCurv, double _valueZ, CtuluCommandContainer _cmd) throws ProfilContainerException{
    CtuluCommandComposite cmd=new CtuluCommandComposite(MdlResource.getS("D�placement d'un point"));
    setZ(_idxPoint, _valueZ, cmd);
    try {
      setCurv(_idxPoint, _valueCurv, cmd);
    }
    finally{
      if(_cmd!=null)
        _cmd.addCmd(cmd.getSimplify());
    }
  }
  
  public boolean isSpecialPoint(int _idxPoint) {
    boolean b;
    b= _idxPoint==(Integer)getValueOf(GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_DROITE);
    b|=_idxPoint==(Integer)getValueOf(GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_GAUCHE);
    b|=_idxPoint==(Integer)getValueOf(GISAttributeConstants.INTERSECTION_RIVE_DROITE);
    b|=_idxPoint==(Integer)getValueOf(GISAttributeConstants.INTERSECTION_RIVE_GAUCHE);
    b|=getIdxLignesDirectricesAt(_idxPoint).length!=0;
    return b;
  }
  
  /**
   * Met a jour les lignes remarquables apr�s suppression ou ajout d'un point.
   * @param _idxPoint L'index du point selectionn� pour ajout ou suppression.
   * @param _add True : Le point est ajout�. False : Supprim�.
   */
  public void updateSpecialLines(int _idxPoint, boolean _add) {
    int inc=_add ? 1:-1;
    
    int idxSD=(Integer)getValueOf(GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_DROITE);
    int idxSG=(Integer)getValueOf(GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_GAUCHE);
    int idxRD=(Integer)getValueOf(GISAttributeConstants.INTERSECTION_RIVE_DROITE);
    int idxRG=(Integer)getValueOf(GISAttributeConstants.INTERSECTION_RIVE_GAUCHE);
//    int[] idxLD=getIdxLignesDirectricesAt(_idxPoint);
    
    if (idxSD>=_idxPoint)
      setValueOf(GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_DROITE, idxSD+inc, null);
    if (idxSG>=_idxPoint)
      setValueOf(GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_GAUCHE, idxSG+inc, null);
    if (idxRD>=_idxPoint)
      setValueOf(GISAttributeConstants.INTERSECTION_RIVE_DROITE, idxRD+inc, null);
    if (idxRG>=_idxPoint)
      setValueOf(GISAttributeConstants.INTERSECTION_RIVE_GAUCHE, idxRG+inc, null);
    
    // Lignes directrices
    for (int i=0; i<getNbLignesDirectrices(); i++) {
      int idxLD=getPointForDirLine(i);
      if (idxLD>=_idxPoint)
        setPointForDirLine(i, idxLD+inc, null);
      
    }
  }
    
  public void remove(int _idxPoint, CtuluCommandContainer _cmd) throws ProfilContainerException {
    GISAttributeModel z=getZCollection();
    if(biefContainer_.getZoneProfils()==null||z==null||curv_==null||_idxPoint<0||_idxPoint>=curv_.size())
      throw new IllegalArgumentException("Erreur prog : Cet index n'existe pas.");
    if(curv_.size()==2)
      throw new ProfilContainerException(MdlResource.getS("Impossible d'enlever un point, il n'en reste que 2."));
    if (isSpecialPoint(_idxPoint))
      throw new ProfilContainerException(MdlResource.getS("Impossible de supprimer un point, c'est un point remarquable."));
    
    CtuluCommandComposite cmp=new CtuluCommandComposite(MdlResource.getS("Suppression d'un point"));
    // Suppression du point dans la zone
    biefContainer_.getBief(biefContainer_.getSelectedBief()).disableSynchroniser();
    biefContainer_.getZoneProfils().removeAtomics(idxProfilSelected_, new CtuluListSelection(new int[]{_idxPoint}), null, cmp);
    // Gestion du undo/redo \\
    // Mise � jour des informations curvilignes \\
    if (_idxPoint==0) {
      // Si _idxPoint est le premier point
      curv_.remove(0);
      for (int i=curv_.size()-1; i>=0; i--)
        curv_.set(i, curv_.get(i)-curv_.get(0));
    }
    else if (_idxPoint>0&&_idxPoint<curv_.size()-1) {
      // Cas g�n�ral si _idxPoint est entre le premier et le dernier
      // Attention curv_ contient toujours _idxPoint contrairement � seq
      CoordinateSequence seq=((GISCoordinateSequenceContainerInterface)biefContainer_.getZoneProfils().getGeometry(idxProfilSelected_)).getCoordinateSequence();
      double delta=curv_.get(_idxPoint-1)-curv_.get(_idxPoint+1)
          +Math.sqrt(Math.pow(seq.getCoordinate(_idxPoint-1).x-seq.getCoordinate(_idxPoint).x, 2)
                    +Math.pow(seq.getCoordinate(_idxPoint-1).y-seq.getCoordinate(_idxPoint).y, 2));
      curv_.remove(_idxPoint);
      for (int i=_idxPoint; i<curv_.size(); i++)
        curv_.set(i, curv_.get(i)+delta);
    }
    else
      // Si _idxPoint est le dernier
      curv_.remove(curv_.size()-1);
    // Mise � jour de idxZMin_ et idxZMax_
//    if (_idxPoint==idxZMax_||_idxPoint==idxZMin_)
    
    if(_cmd!=null)
      _cmd.addCmd(new AddRemoveCommandUndoRedo(cmp, false, _idxPoint));
    
    updateCacheZ();
    updateSpecialLines(_idxPoint, false);
    biefContainer_.getBief(biefContainer_.getSelectedBief()).enableSynchroniser();
    fireProfilContainerDataModified();
  }
  
  public void add(int _idxPoint, double _valueCurv, double _valueZ, CtuluCommandContainer _cmd) throws ProfilContainerException {
    CtuluCommandComposite cmp=new CtuluCommandComposite(MdlResource.getS("Ajout d'un point"));
    
    CoordinateSequence seq=getCoordSeq();
    Coordinate newCoord=UtilsProfil1d.getCoordinateXY(seq, _valueCurv);
    if (_idxPoint==-1) {
      _idxPoint=seq.size()-1;
    }
    else {
      _idxPoint--;
    }
    biefContainer_.getBief(biefContainer_.getSelectedBief()).disableSynchroniser();
    biefContainer_.getZoneProfils().addAtomic(idxProfilSelected_, _idxPoint, newCoord.x, newCoord.y, cmp);
    setZ(_idxPoint+1, _valueZ, cmp);
    updateCacheCurv();
    updateCacheZ();
    updateSpecialLines(_idxPoint+1, true);
    // Les g�om�tries doivent �tre remises � jour.
    biefContainer_.getBief(biefContainer_.getSelectedBief()).enableSynchroniser();
    
    if(_cmd!=null)
      _cmd.addCmd(new AddRemoveCommandUndoRedo(cmp, true, _idxPoint+1));
    
    fireProfilContainerDataModified();
  }
  
  // Gestion des listeners \\
  
  /** List de listener. */
  List<ProfilContainerListener> listeners_=new ArrayList<ProfilContainerListener>();
  
  public void addProfilContainerListener(ProfilContainerListener _listener) {
    if(_listener==null)
      throw new IllegalArgumentException("Erreur prog : _listener doit �tre non null");
    if(!listeners_.contains(_listener))
      listeners_.add(_listener);
  }

  public void removeProfilContainerListener(ProfilContainerListener _listener) {
    if(_listener==null)
      throw new IllegalArgumentException("Erreur prog : _listener doit �tre non null");
    if(listeners_.contains(_listener))
      listeners_.remove(_listener);
  }
  
  protected void fireProfilContainerDataModified(){
    for(ProfilContainerListener listener:listeners_)
      listener.profilContainerDataModified();
  }
  
  protected void fireProfilContainerSelectedChanged(int _idxOldProfil, int _idxNewProfil){
//    for(ProfilContainerListener listener:listeners_)
//      listener.profilContainerSelectedChanged(_idxOldProfil, _idxNewProfil);
  }
  
}
