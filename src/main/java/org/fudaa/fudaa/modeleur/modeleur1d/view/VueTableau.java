/*
 * @creation     8 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.view;

import java.awt.Component;
import java.awt.Dimension;

import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import org.fudaa.ebli.commun.EbliFormatterInterface;
import org.fudaa.fudaa.modeleur.modeleur1d.model.ProfilContainerI;
import org.fudaa.fudaa.modeleur.modeleur1d.model.ProfilContainerException;
import org.fudaa.fudaa.modeleur.modeleur1d.model.ProfilContainerListener;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuInsets;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuSpecificBar;
import com.memoire.bu.BuToolButton;
import java.awt.BorderLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Cette classe permet le visionnage des informations sous forme d'un tableau.
 * Chaque ligne repr�sente un point de la courbe, la colonne de gauche indique
 * l'abscisse curviligne et la colonne de droite indique la valeur bathym�tique
 * du point.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class VueTableau extends BuPanel {
  
  /**
   * Le model du tableau. Celui ci extrait les informations du DataGeometryAdapter.
   */
  protected class TableGeomModel extends DefaultTableModel implements ProfilContainerListener {

    protected ProfilContainerI data_;
    protected EbliFormatterInterface formater_;
    
    public TableGeomModel(EbliFormatterInterface _formater){
      super(new String[]{MdlResource.getS("Index"), MdlResource.getS("Abs travers"), "Z"}, 0);
      formater_=_formater;
//      if(_data==null)
//        throw new IllegalArgumentException("Erreur prog : _data ne doit pas �tre null.");
//      data_=_data;
//      data_.addProfilContainerListener(this);
    }
    
    public void profilContainerDataModified() {
      fireTableDataChanged();
    }
    
//    public void profilContainerSelectedChanged(int _idxOldProfil, int _idxNewProfil){
//      fireTableDataChanged();
//    }
    
    public boolean isCellEditable(int row, int column) {
      if(column==0)
        return false;
      else if (column==1)
        return !data_.isRupturePoint(row);
      else
        return true;
    }
    
    public Class<?> getColumnClass(int columnIndex) {
      if(columnIndex==0)
        return Integer.class;
      return Double.class;
    }

    public int getRowCount() {
      if(data_==null)
        return 0;
      else
        return data_.getNbPoint();
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
      if(columnIndex==0)
        return rowIndex+1;
      else if(columnIndex==1)
        if(formater_!=null)
          return formater_.getXYFormatter().format(data_.getCurv(rowIndex));
        else
          return data_.getCurv(rowIndex);
      else
        if(formater_!=null)
          return formater_.getXYFormatter().format(data_.getZ(rowIndex));
        else
          return data_.getZ(rowIndex);
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {
      controller_.clearError();
      try {
        if (columnIndex == 1) {
          data_.setCurv(rowIndex, (Double) value, controller_.getCommandContainer());
        }
        else {
          data_.setZ(rowIndex, (Double) value, controller_.getCommandContainer());
        }
      }
      catch (ProfilContainerException _exc) {
        controller_.showError(_exc.getMessage());
      }
    }
    
    public void removePoint(int[] _rowIndexes) {
      controller_.clearError();
      try {
        CtuluCommandComposite cmp=new CtuluCommandComposite(MdlResource.getS("Suppression de point(s)"));
        for (int i=_rowIndexes.length-1; i>=0; i--) {
          data_.remove(_rowIndexes[i], cmp);
        }
        if (controller_.getCommandContainer()!=null)
          controller_.getCommandContainer().addCmd(cmp);
      }
      catch (ProfilContainerException _exc) {
        controller_.showError(_exc.getMessage());
      }
    }
    
    /**
     * Ajoute un point avant la s�lection ou apr�s le dernier point si aucune s�lection.
     * @param _rowIndex Le point selectionn�. -1 si aucun s�lectionn�.
     */
    public void addPoint(int _rowIndex) {
      controller_.clearError();
      try {
        double curv;
        double z;
        if (_rowIndex==-1) {
          curv=data_.getCurvMax()+(data_.getCurvMax()-data_.getCurvMin())/20.;
          z=data_.getZ(data_.getNbPoint()-1);
        }
        else if (_rowIndex>0) {
          curv=(data_.getCurv(_rowIndex-1)+data_.getCurv(_rowIndex))/2.;
          z=(data_.getZ(_rowIndex-1)+data_.getZ(_rowIndex))/2.;
        }
        else {
          curv=data_.getCurvMin()-(data_.getCurvMax()-data_.getCurvMin())/20.;
          z=data_.getZ(0);
        }
        data_.add(_rowIndex, curv, z, controller_.getCommandContainer());
      }
      catch (ProfilContainerException _exc) {
        controller_.showError(_exc.getMessage());
      }
    }
  }
  
  /** Le controller de la fen�tre 1d. */
  private VueProfilI controller_;
  /** La vue du tableau. */
  private JTable table_;
  /** Le model du tableau affich� par le panel. */
  private TableGeomModel modelTable_;
  BuToolButton btAdd_;
  BuToolButton btRemove_;
  
  
  public VueTableau(VueProfilI _controller, boolean _showButtons){
    controller_=_controller;
    setLayout(new BuBorderLayout(2, 2));
    // Tableau
    modelTable_=new TableGeomModel(controller_.getFormater());
    table_=new CtuluTable(modelTable_) {
      /* (non-Javadoc)
       * @see javax.swing.JTable#valueChanged(javax.swing.event.ListSelectionEvent)
       */
      @Override
      public void valueChanged(ListSelectionEvent e) {
        super.valueChanged(e);
        if (getSelectionModel().getMaxSelectionIndex()!=-1)
          scrollRectToVisible(getCellRect(getSelectionModel().getMaxSelectionIndex(), 0, false));
      }
    };
    table_.getColumnModel().getColumn(0).setCellRenderer(new DefaultTableCellRenderer(){
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row,
          int column) {
        JLabel c=(JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        c.setHorizontalAlignment(SwingUtilities.CENTER);
        return c;
      }
    });
    table_.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      public void valueChanged(ListSelectionEvent e) {
        updateToolButtons();
      }
    });
    add(new BuScrollPane(table_), BuBorderLayout.CENTER);
    
    List<AbstractButton> actions=new ArrayList<AbstractButton>();
    buildToolButtons(actions);
    updateToolButtons();
    
    // Ajout des boutons � la barre
    BuSpecificBar speBar=new BuSpecificBar();
    speBar.addTools(actions.toArray(new JComponent[0]));
    if (_showButtons) add(speBar, BorderLayout.NORTH);
    
    // Taille
    setPreferredSize(new Dimension(200, 200));
  }

  public void buildToolButtons(List<AbstractButton> _list) {
    btAdd_ = new BuToolButton();
    btAdd_.setIcon(MdlResource.MDL.getToolIcon("node-add"));
    final Insets zeroInset = BuInsets.INSETS0000;
    btAdd_.setMargin(zeroInset);
    btAdd_.setToolTipText(MdlResource.getS("Ajouter un point (Avant la s�lection ou en dernier si aucune s�lection)"));
    btAdd_.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        // Pour inserer apr�s la s�lection
        int indsel=table_.getSelectedRow();
        modelTable_.addPoint(indsel);
        setSelection(new int[]{indsel==-1?table_.getRowCount()-1:indsel});
      }
    });
    _list.add(btAdd_);
    
    btRemove_ = new BuToolButton();
    btRemove_.setMargin(zeroInset);
    btRemove_.setIcon(MdlResource.MDL.getToolIcon("node-delete"));
    btRemove_.setActionCommand("REMOVE_POINTS");
    final KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK);
    btRemove_.setToolTipText(MdlResource.getS("Supprimer un point") + " ("
        + KeyEvent.getKeyModifiersText(ks.getModifiers()) + '+' + KeyEvent.getKeyText(ks.getKeyCode()) + ')');
    btRemove_.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        modelTable_.removePoint(table_.getSelectedRows());
      }
    });
    _list.add(btRemove_);
  }
  
  public void updateToolButtons() {
    btRemove_.setEnabled(table_.getSelectedRowCount()>0 && table_.getSelectedRowCount()<=table_.getRowCount()-2);
    btAdd_.setEnabled(modelTable_.getRowCount()>0 && table_.getSelectedRowCount()<=1);
  }
  
  public void setProfil(ProfilContainerI _prf) {
    if (modelTable_.data_==_prf) return;
    
    if (modelTable_.data_!=null) {
      modelTable_.data_.removeProfilContainerListener(modelTable_);
    }
    modelTable_.data_=_prf;
    
    if (modelTable_.data_!=null) {
      modelTable_.data_.addProfilContainerListener(modelTable_);
    }
    modelTable_.fireTableDataChanged();
    
    updateToolButtons();
  }
  
  /** Ajout un listener � la selection dans le tableau. */
  public void addSelectionListener(ListSelectionListener _listener){
    table_.getSelectionModel().addListSelectionListener(_listener);
  }
  
  /** Retourne un tableau contenant les index selectionn�s. */
  public int[] getSelection(){
    return table_.getSelectedRows();
  }
  
  /** Selectionne les points dont les indices sont pass�s en param�tre. */
  public void setSelection(int[] _idxSelection){
    table_.getSelectionModel().clearSelection();
    if(_idxSelection!=null) {
      for(int i=0;i<_idxSelection.length;i++)
        table_.getSelectionModel().addSelectionInterval(_idxSelection[i], _idxSelection[i]);
    }
  }
  
}
