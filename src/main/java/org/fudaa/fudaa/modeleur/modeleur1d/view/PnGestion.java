/*
 * @creation     18 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.view;

import java.awt.Container;

import javax.swing.JComponent;
import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuVerticalLayout;

/**
 * Le panneau contenant la liste des panneaux de gestions. Il est situ� dans la
 * colonne de droite de l'application.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class PnGestion extends JComponent implements ListDataListener {
  
  /** le model */
  private ListModel model_;
  /** Container contenant la visualisation des modules. */
  private Container container_;
  
  public PnGestion(ListModel _model){
    model_=_model;
    setLayout(new BuBorderLayout());
    container_=new Container();
    container_.setLayout(new BuVerticalLayout());
    add(container_, BuBorderLayout.CENTER);
    model_.addListDataListener(this);
    updateBox();
  }

  /**
   * R�g�n�re le contenu de la box.
   */
  protected void updateBox(){
    container_.removeAll();
    for(int i=0;i<model_.getSize();i++)
      if(model_.getElementAt(i) instanceof JComponent)
        container_.add((JComponent) model_.getElementAt(i));
  }
  
  public void contentsChanged(ListDataEvent e) {
    // TODO quelque chose de plus 'fin'
    updateBox();
  }

  public void intervalAdded(ListDataEvent e) {
    // TODO quelque chose de plus 'fin'
    updateBox();
  }

  public void intervalRemoved(ListDataEvent e) {
    // TODO quelque chose de plus 'fin'
    updateBox();
  }
}
