/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.modeleur.modeleur1d.controller;

import javax.swing.ListSelectionModel;

/**
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public interface ProfileSelectionModel extends ListSelectionModel {

  int getSelectedProfil();

  /**
   * L'index de profil selectionn�. Peut �tre egal � -1, pour aucune selection.
   * @param _idx L'index du profil selectionn�.
   */
  void setSelectedProfil(int _idx);
  
}
