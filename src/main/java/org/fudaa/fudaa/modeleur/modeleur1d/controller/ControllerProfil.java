/*
 * @creation     22 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.controller;

import javax.swing.DefaultListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.fudaa.modeleur.modeleur1d.controller.ControllerBief.BiefSelectionModel;
import org.fudaa.fudaa.modeleur.modeleur1d.model.BiefContainerAdapter;
import org.fudaa.fudaa.modeleur.modeleur1d.model.BiefContainerI;
import org.fudaa.fudaa.modeleur.modeleur1d.model.ProfileSetI;
import org.fudaa.fudaa.modeleur.modeleur1d.view.PnGestionProfil;

/**
 * Controller permettant de g�rer (cr�er, modifier, supprimer, selectionner) les profils.
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class ControllerProfil implements ProfileSetControllerI {

  /** Le controller principal. */
  protected Controller1d controller1d_;
  /** Le model commun de selection de profils. */
  private ProfilSelectionModel profilSelectionModel_;
  /** le conteneur du bief. */
  private BiefContainerAdapter biefContainer_;
  /** La vue du module de gestion des profils. */
  protected PnGestionProfil vueModuleGestionProfil_;
  
  public ControllerProfil(BiefSelectionModel _biefSelectionModel, BiefContainerAdapter _biefContainer, Controller1d _controller1d){
    controller1d_=_controller1d;
    biefContainer_=_biefContainer;
    profilSelectionModel_=new ProfilSelectionModel(_biefSelectionModel);
    vueModuleGestionProfil_=new PnGestionProfil(this);
  }
  
  public ProfilSelectionModel getProfilSelectionModel(){
    return profilSelectionModel_;
  }
  
  public BiefContainerI getBiefContainer() {
    return biefContainer_;
  }
  
  public ProfileSetI getProfilSet() {
    return biefContainer_;
  }
  
  /**
   * Retourne le Commande manager utilis� dans la partie 1d
   */
  public CtuluCommandContainer getCommandManager(){
    return controller1d_.getCommandContainer();
  }
  
  /**
   * Retourne la vue du module de gestion des profils.
   */
  public PnGestionProfil getVueModuleGestionProfil(){
    return vueModuleGestionProfil_;
  }
  
  /**
   * Cr�ation ou ajout d'un nouveau profil.
   */
  @Override
  public void ajoutProfil(){
    //TODO
  }
  
  /**
   * Supprime le profil selectionn�.
   */
  @Override
  public void supprimerSelectedProfil(){
    if(profilSelectionModel_.getSelectedProfil()!=-1)
      biefContainer_.removeProfil(profilSelectionModel_.getSelectedProfil(), controller1d_.getCommandContainer());
  }
  
  /**
   * Fusionne deux profils.
   */
  @Override
  public void fusionnerProfil(){
    
  }
  
  /**
   * Renommer un profil.
   */
  @Override
  public void renameProfil(int _idxProfil, String _newName) {
    biefContainer_.renameProfil(_idxProfil, _newName, controller1d_.getCommandContainer()); 
  }
  
  //Le model de selection concernant les biefs \\

  /**
   * Cette classe a pour objectif de g�rer avec le m�me model de selection tous
   * les endroits ayant besoin de savoir quels profils sont selectionn�s.
   */
  public class ProfilSelectionModel extends DefaultListSelectionModel implements ProfileSelectionModel, ListSelectionListener {
    
    /** La liste de selection des biefs. */
    private BiefSelectionModel biefSelectionModel_;
    
    public ProfilSelectionModel(BiefSelectionModel _biefSelectionModel){
      biefSelectionModel_=_biefSelectionModel;
      biefSelectionModel_.addListSelectionListener(this);
      super.setSelectionMode(SINGLE_SELECTION);
    }

    @Override
    public int getSelectedProfil(){
      return getMinSelectionIndex();
    }
    
    @Override
    public void setSelectedProfil(int _idx) {
      if (_idx==-1)
        clearSelection();
      else
        setSelectionInterval(_idx,_idx);
    }
    
    public void valueChanged(ListSelectionEvent e) {
      // Le bief selectionn� � chang�. \\
      clearSelection();
    }

    /**
     * Met � jour dataGeomAdapter_ pour qu'il tienne compte de la nouvelle selection.
     */
    private void updateProfil() {
//      controller1d_.clearError();
//      try {
//        profilContainerAdapter_.setSelectedProfil(getMinSelectionIndex());
//      }
//      catch (ProfilContainerException _exc) {
//        controller1d_.showError(_exc.getMessage());
//      }
    }
    
    public void addListSelectionListener(ListSelectionListener x) {
      super.addListSelectionListener(x);
      updateProfil();
    }

    public void addSelectionInterval(int index0, int index1) {
      super.addSelectionInterval(index0, index1);
      updateProfil();
    }

    public void clearSelection() {
      super.clearSelection();
      updateProfil();
    }

    public void insertIndexInterval(int index, int length, boolean before) {
      super.insertIndexInterval(index, length, before);
      updateProfil();
    }

    public void removeIndexInterval(int index0, int index1) {
      super.removeIndexInterval(index0, index1);
      updateProfil();
    }

    public void removeSelectionInterval(int index0, int index1) {
      super.removeSelectionInterval(index0, index1);
      updateProfil();
    }

    public void setAnchorSelectionIndex(int index) {
      super.setAnchorSelectionIndex(index);
      updateProfil();
    }

    public void setLeadSelectionIndex(int index) {
      super.setLeadSelectionIndex(index);
      updateProfil();
    }

    public void setSelectionInterval(int index0, int index1) {
      super.setSelectionInterval(index0, index1);
      updateProfil();
    }

    /**
     * Le mode de selection est bloqu� � SINGLE_SELECTION.
     */
    public void setSelectionMode(int selectionMode) {
      if(selectionMode!=SINGLE_SELECTION)
        throw new IllegalArgumentException("Erreur prog : Le mode est bloqu� � SINGLE_SELECTION");
    }
  }
}
