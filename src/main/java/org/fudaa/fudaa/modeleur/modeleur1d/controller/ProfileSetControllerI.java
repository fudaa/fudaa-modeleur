/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.modeleur.modeleur1d.controller;

import org.fudaa.fudaa.modeleur.modeleur1d.model.ProfileSetI;

/**
 * Un controller pour un set de profils
 * @author bmarchan
 */
public interface ProfileSetControllerI {

  /**
   * Cr�ation ou ajout d'un nouveau profil.
   */
  void ajoutProfil();

  /**
   * Fusionne deux profils.
   */
  void fusionnerProfil();

  /**
   * @return Le modele de selection de profil.
   */
  ProfileSelectionModel getProfilSelectionModel();

  /**
   * Renommer un profil.
   */
  void renameProfil(int _idxProfil, String _newName);

  /**
   * Supprime le profil selectionn�.
   */
  void supprimerSelectedProfil();
  
  /**
   * @return Le profilSet associ�.
   */
  ProfileSetI getProfilSet();
}
