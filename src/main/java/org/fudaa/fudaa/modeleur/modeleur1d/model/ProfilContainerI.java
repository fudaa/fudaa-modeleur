/*
 * @creation     10 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.model;

import org.fudaa.ctulu.CtuluCommandContainer;

/**
 * Une interface permettant d'acc�der aux informations d'un profil et
 * egalement de les modifier. Cette interface joue le role de controlleur, dans
 * le sens ou une modification ill�gale l�ve une exception.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public interface ProfilContainerI {
  
  /** Ajout d'un nouveau listener. */
  public void addProfilContainerListener(ProfilContainerListener _listener);
  
  /** Supprime le listener. */
  public void removeProfilContainerListener(ProfilContainerListener _listener);
  
  /** Retourne le nombre de points total de la geometry. */
  public int getNbPoint();

  /** Retourne l'abcisse curviligne du point indiqu� en param�tre. */
  public double getCurv(int _idxPoint);

  /** Retourne la valeur maximal de l'abcisse curviligne. */
  public double getCurvMax();
  
  /** Retourne la valeur minimal de l'abcisse curvilgne. */
  public double getCurvMin();
  
  /** Retourne la valeur de z du point indiqu� en param�tre. */
  public double getZ(int _idxPoint);
  
  /** Retourne la valeur maximal de z. */
  public double getZMax();

  /** Retourne la valeur minimal de z. */
  public double getZMin();
  
  /** Retourne l'abscisse curviligne de l'intersection avec la rive gauche. -1 si inexistant. */
  public double getAbsCurvRiveGauche();
  
  /** Retourne l'abscisse curviligne de l'intersection avec la rive droite. -1 si inexistant. */
  public double getAbsCurvRiveDroite();
  
  /** Retourne l'abscisse curviligne de l'intersection avec la limite de stockage gauche. -1 si inexistant. */
  public double getAbsCurvLimiteStockageGauche();
  
  /** Retourne l'abscisse curviligne de l'intersection avec la limite de stockage droite. -1 si inexistant. */
  public double getAbsCurvLimiteStockageDroite();
  
  /** Retourne l'abscisse curviligne de l'axe sur le profil (ne pas confondre avec getAbsCurvProfilOnAxeHydraulique). -1 si inexistant. */
  public double getAbsCurvAxeHydrauliqueOnProfil();
  
  /**
   * @return Le nom du profil
   */
  public String getName();
  
  /**
   * Enregistre l'abscisse curviligne du point indiqu� en param�tre.
   * Ce changement d'abscisse curviligne fonctionne sur tous les points.
   * Si un point extr�mit� est chang�, le point 2D est d�plac� sur son axe,
   * la longueur globale n'est pas conserv�e dans l'op�ration.
   * Si c'est un point de rupture le point 2D est d�plac� de telle mani�re
   * que la longueur globale soit conserv�e.
   * Pour les autres points, le point 2D est d�plac� sur son axe en conservant
   * la longueur globale.
   */
  public void setCurv(int _idxPoint, double _value, CtuluCommandContainer _cmd) throws ProfilContainerException;

  /** Enregistre la valeur de z du point indiqu� en param�tre. */
  public void setZ(int _idxPoint, double _value, CtuluCommandContainer _cmd) throws ProfilContainerException;
  
  /** Enregistre l'abcisse curviligne et la valeur de z du point indiqu� en param�tre. */
  public void setValues(int _idxPoint, double _valueCurv, double _valueZ, CtuluCommandContainer _cmd) throws ProfilContainerException;
  
  /** Supprime le point indiqu� en param�tre. */
  public void remove(int _idxPoint, CtuluCommandContainer _cmd) throws ProfilContainerException;
  
  /**
   * Insere un point avant le point d'indice donn� dans le profil.
   * @param _idxPoint L'indice du point d'insertion. Si -1, le point est ajout�
   *                  en dernier.
   * @param _valueCurv La valeur d'abscisse curviligne
   * @param _valueZ La valeur en Z
   * @param _cmd
   * @throws ProfilContainerException 
   */
  public void add(int _idxPoint, double _valueCurv, double _valueZ, CtuluCommandContainer _cmd) throws ProfilContainerException;
  
  /**
   * Retourne le PK du profil sur l'axe. Correspond a l'abscisse curviligne + d�calage de d�but de l'axe.
   */
  public double getAbsCurvProfilOnAxeHydraulique();
  
  /**
   * D�place le profil sur l'axe, en fonction d'un abscisse curviligne donn�.
   * Retourne vrai il est n�c�ssaire d'effectuer une r�ordonnancement des
   * profils sur l'axe hydraulique.
   * @param _pk Le PK du profil. Ne peut �tre ni n�gatif ni sup�rieur � l'abscisse curviligne total
   * de l'axe, dans ces cas une exception de type
   * {@link IllegalArgumentException} est lev�e.
   */
  public boolean moveProfilOnAxeHydraulique(double _pk, CtuluCommandContainer _cmd) throws ProfilContainerException;
  
  /** Place l'intersection du profil avec la limite de stockage gauche � l'index _idx. */
  public void setLimiteGauche(int _idx, CtuluCommandContainer _cmd) throws ProfilContainerException;

  /** Place l'intersection du profil avec la limite de stockage droite � l'index _idx. */
  public void setLimiteDroite(int _idx, CtuluCommandContainer _cmd) throws ProfilContainerException;

  /** Place l'intersection du profil avec la rive gauche � l'index _idx. */
  public void setRiveGauche(int _idx, CtuluCommandContainer _cmd) throws ProfilContainerException;

  /** Place l'intersection du profil avec la rive droite � l'index _idx. */
  public void setRiveDroite(int _idx, CtuluCommandContainer _cmd) throws ProfilContainerException;
  
  /** Retourne le nombre de lignes directrices croisant le profil. */
  public int getNbLignesDirectrices();
  
  /** Retourne les noms des lignes directrices. null si aucun profil n'est s�lectionn� ou si il n'y a pas de noms.*/
  public String[] getNamesLignesDirectrices();
  
  /** Retourne les index des lignes directrices associ�s � l'index pass� en param�tre. */
  public int[] getIdxLignesDirectricesAt(int _idxPoint);
  
  /**
   * @param _idxLine L'indice de ligne directrice
   * @return L'indice du point supportant la ligne directrice d'indice donn�. Peut
   * retourner -1 si la ligne directrice n'existe pas.
   */
  public int getPointForDirLine(int _idxLine);
  
  /** Retourne les noms des lignes directrices associ�s � l'index pass� en param�tre. */
  public String[] getNamesLignesDirectricesAt(int _idxPoint);
  
  /** Enregistre la ligne directrice _idxLd comme passant par le point _idxDestination. */ 
  public void setLigneDirectriceAt(int _idxLd, int _idxPoint, CtuluCommandContainer _cmd) throws ProfilContainerException;
  
  /** Retourne le nom de la ligne directrice. */
  public String getLigneDirectriceName(int _idx);
  
  /** Retourne les index des lignes directrices pouvant �tre d�plac�s au point indiqu�.*/
  public int[] getAllowedMoveOfLignesDirectricesTo(int _idxPoint);
  
  /** 
   * D�termine si le point indiqu� est un point de rupture. Un point de rupture 
   * ne peut pas �tre d�plac� en X, ni �tre supprim�.
   * @param _idx L'indice du point sur le profil.
   * @return True si le point indiqu� est un point de rupture.
   */
  public boolean isRupturePoint(int _idx);
}
