/*
 * @creation     9 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.controller;

import org.fudaa.fudaa.modeleur.modeleur1d.view.VueProfilI;
import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Observable;

import javax.swing.Action;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluListSelectionEvent;
import org.fudaa.ctulu.CtuluListSelectionListener;
import org.fudaa.ctulu.CtuluNamedCommand;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.mascaret.io.MascaretGEO1dFileFormat;
import org.fudaa.dodico.mascaret.io.MascaretGEO2dFileFormat;
import org.fudaa.dodico.rubar.io.RubarStCnFileFormat;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliFormatter;
import org.fudaa.ebli.commun.EbliFormatterInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.fudaa.modeleur.MdlImplementation;
import org.fudaa.fudaa.modeleur.modeleur1d.MdlFille1d;
import org.fudaa.fudaa.modeleur.modeleur1d.model.Bief;
import org.fudaa.fudaa.modeleur.modeleur1d.view.ExportBiefPanel;
import org.fudaa.fudaa.modeleur.modeleur1d.view.PnGestion;
import org.fudaa.fudaa.modeleur.modeleur1d.view.PnGestionAxeHydraulique;
import org.fudaa.fudaa.modeleur.modeleur1d.view.VueBief;
import org.fudaa.fudaa.modeleur.modeleur1d.view.VueCourbe;
import org.fudaa.fudaa.modeleur.modeleur1d.view.VueTableau;

import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuLabel;
import org.fudaa.fudaa.modeleur.modeleur1d.model.BiefContainerListener;
import org.fudaa.fudaa.modeleur.modeleur1d.model.ProfilContainerException;
import org.fudaa.fudaa.modeleur.modeleur1d.view.PnAbscisseProfil;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Cette classe a la charge et la responsabilit� de g�rer toutes les actions,
 * interactions et r�actions de la fen�tre 1d dans son ensemble.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class Controller1d extends InternalFrameAdapter implements ZSelectionListener, ListSelectionListener, CtuluListSelectionListener, VueProfilI {
  
  class SpecialObservable extends Observable {
    @Override
    protected synchronized void setChanged() {
      super.setChanged();
    }
  }
  
  // Les vues \\
  /** La vue d'un profil par tableau. */
  private VueTableau vueTableau_;
  /** La vue d'un profil via une courbe. */
  private VueCourbe vueCourbe_;
  /** Un label pour afficher les erreurs. */
  private BuLabel vueError_=new BuLabel();
  /** Un panneau pour la saisie de l'abscisse curviligne d'un profil. */
  private PnAbscisseProfil vueAbsProf_;
  /** La vue des modules contenu dans la colonne � droite. */
  private PnGestion vueContainerModules_;
  /** La frame contenant tous les widgets 1d. */
  private MdlFille1d frame1d_;
  
  // Donn�es sp�ciales \\ 
  /** Le lien avec le reste de l'application. */
  private MdlImplementation appli_;
  /** Indique si on doit �couter les �venements de selection. */
  private boolean listenEventSelection_=true;
  /** Formater */
  private EbliFormatterInterface formater_=new EbliFormatter();
  /** Le commande manager pour le undo/redo */
  private CtuluCommandManager mng_;
  
  /** La classe d�l�gu�e servant d'observable */
  private SpecialObservable delegateObs=new SpecialObservable();
  
  // Controllers \\
  /** Le controller des Biefs */
  private ControllerBief controllerBief_;
  private ControllerProfil controllerProfil_;
  
  // Models \\
  /** Le container des modules contenus � droite. */
  private DefaultListModel modelContainerModules_;
  
  public Controller1d(MdlImplementation _appli, MdlFille1d _frame1d){
    if(_appli==null||_frame1d==null)
      throw new IllegalArgumentException("Erreur prog : Les param�tres ne doivent pas �tre null.");
    appli_=_appli;
    frame1d_=_frame1d;
    frame1d_.addInternalFrameListener(this);
    mng_=new CtuluCommandManager();
    // Instanciation des Models \\
    modelContainerModules_=new DefaultListModel();
    // Instanciation des controllers \\
    controllerBief_=new ControllerBief(this);
    controllerProfil_=new ControllerProfil(controllerBief_.getBiefSelectionModel(), controllerBief_.getBiefContainer(), this);
    // Instanciation des Vues \\
    // Modules
    vueContainerModules_=new PnGestion(modelContainerModules_);
    addVueModule(controllerBief_.getVueModuleGestionBief());
    addVueModule(new PnGestionAxeHydraulique(controllerBief_,formater_));
    addVueModule(controllerProfil_.getVueModuleGestionProfil());
    
    controllerBief_.getBiefContainer().addBiefContainerListener(frame1d_);
    controllerBief_.getBiefContainer().addBiefContainerListener(new BiefContainerListener() {

      public void biefSelectedRenamed(String _oldName, String _newName) {
      }

      public void biefSelectedChanged(String _newName) {
        controllerProfil_.getVueModuleGestionProfil().setProfileSet(null);
        controllerProfil_.getVueModuleGestionProfil().setProfileSet(controllerBief_.getBiefContainer());
      }

      public void profilRenamed(int _idxProfil, String _oldName, String _newName) {
      }

      public void profilRemoved(int _idxProfil) {
      }

      public void profilAdded(int _idxProfil) {
      }
    });
    // Vue Bief
    controllerBief_.getVueBief().getScene().addSelectionListener(this);
//    controllerProfil_.getProfilContainer().addProfilContainerListener(controllerBief_.getVueBief());
    controllerProfil_.getProfilSelectionModel().addListSelectionListener(new ListSelectionListener() {
      public void valueChanged(ListSelectionEvent e) {
        int indsel=controllerProfil_.getProfilSelectionModel().getSelectedProfil();
        vueCourbe_.setSelectedProfile(indsel);
        controllerBief_.getVueBief().setSelectedProfile(indsel);
        vueTableau_.setProfil(controllerProfil_.getProfilSet().getProfil(indsel));
        vueAbsProf_.setProfile(controllerProfil_.getProfilSet().getProfil(indsel));
      }
    });
    // Vue tableau
    vueTableau_=new VueTableau(this, true);
    vueTableau_.addSelectionListener(this);
    // Vue courbe
    vueCourbe_=new VueCourbe(this, controllerProfil_.getBiefContainer());
    vueCourbe_.addSelectionListener(this);
    // Vue error
    vueError_.setForeground(Color.RED);
    vueError_.setHorizontalAlignment(JLabel.CENTER);
    // Vue abscisse curv profil
    vueAbsProf_=new PnAbscisseProfil(this, formater_);
    clearError();
  }
  
  // Op�rations sp�ciales n�c�ssitant l'intervention de plusieurs controller \\
  /**
   * Met l'abscisse curviligne du profil sur l'axe hydraulique � _value.
   * _value ne peut �tre ni n�gatif ni sup�rieur � l'abscisse curviligne total
   * de l'axe, dans ces cas une exception de type
   * {@link IllegalArgumentException} est lev�e.
   * @throws ProfilContainerException 
   */
  public void moveProfilOnAxeHydraulique(double _value) throws ProfilContainerException {
    CtuluCommandComposite cmp=new CtuluCommandComposite(MdlResource.getS("D�placement du profil sur l'axe"));
    boolean result=controllerProfil_.getProfilSet().getProfil(controllerProfil_.getProfilSelectionModel().getSelectedProfil()).moveProfilOnAxeHydraulique(_value, cmp);
    if (result) {
      orderProfils(cmp);
    }
    getCommandContainer().addCmd(cmp.getSimplify());
  }

  /**
   * R�ordonne les profils du bief selectionn�. La selection est mise � jour.
   */
  public void orderProfils(CtuluCommandContainer _cmd) {
    // Gestion de l'undo/Redo sur la profil selectionn�
    class UndoRedoChangeSelectionProfil implements CtuluCommand, CtuluNamedCommand {
      int oldIdx_;
      int newIdx_;
      CtuluCommandComposite cmd_;
      public UndoRedoChangeSelectionProfil(int _oldIdx, int _newIdx, CtuluCommandComposite _cmd) {
        oldIdx_=_oldIdx;
        newIdx_=_newIdx;
        cmd_=_cmd;
      }
      public void redo() {
        controllerProfil_.getProfilSelectionModel().setSelectedProfil(-1);
        controllerBief_.getSelectedBief().disableSynchroniser();
        cmd_.redo();
        if (newIdx_ != oldIdx_) {
          controllerBief_.biefContainerAdapter_.rebuildProfiles();
        }
        controllerBief_.getSelectedBief().enableSynchroniser();
        controllerProfil_.getProfilSelectionModel().setSelectedProfil(newIdx_);
      }
      public void undo() {
        controllerProfil_.getProfilSelectionModel().setSelectedProfil(-1);
        controllerBief_.getSelectedBief().disableSynchroniser();
        cmd_.undo();
        if (newIdx_ != oldIdx_) {
          controllerBief_.biefContainerAdapter_.rebuildProfiles();
        }
        controllerBief_.getSelectedBief().enableSynchroniser();
        controllerProfil_.getProfilSelectionModel().setSelectedProfil(oldIdx_);
      }
      public String getName() {
        return MdlResource.getS("R�ordonnancement des profils");
      }
    }
    CtuluCommandComposite cmd=new CtuluCommandComposite();
    int oldIdxProfil=controllerProfil_.getProfilSelectionModel().getSelectedProfil();
    controllerProfil_.getProfilSelectionModel().setSelectedProfil(-1);
    controllerBief_.getSelectedBief().disableSynchroniser();
    int newIdxProfil=controllerBief_.orderProfils(oldIdxProfil, cmd);
    if (newIdxProfil!=oldIdxProfil) {
      controllerBief_.biefContainerAdapter_.rebuildProfiles();
    }
    controllerBief_.getSelectedBief().enableSynchroniser();
    controllerProfil_.getProfilSelectionModel().setSelectedProfil(newIdxProfil);
    if(_cmd!=null)
      _cmd.addCmd(new UndoRedoChangeSelectionProfil(oldIdxProfil, newIdxProfil, cmd));
  }
  
  // Getters des vues \\
  
  public VueBief getVueBief(){
    return controllerBief_.getVueBief();
  }
  public VueTableau getVueTableau(){
    return vueTableau_;
  }
  public VueCourbe getVueCourbe(){
    return vueCourbe_;
  }
  public JComponent getVueError(){
    return vueError_;
  }
  public PnAbscisseProfil getVueAbsProfil(){
    return vueAbsProf_;
  }
  public PnGestion getVueContainerModules(){
    return vueContainerModules_;
  }
  
  // Autres getters \\
  
  public EbliFormatterInterface getFormater(){
    return formater_;
  }
  
  @Override
  public CtuluCommandManager getCommandContainer(){
    return mng_;
  }
  
  public MdlImplementation getImplementation(){
    return appli_;
  }
  
  public ControllerBief getControllerBief() {
    return controllerBief_;
  }
  
  public ControllerProfil getControllerProfil() {
    return controllerProfil_;
  }
  
  // Gestion de l'affichage des erreurs. \\
  
  @Override
  public void showError(String _message){
    if(_message==null||_message.length()==0)
      clearError();
    vueError_.setText(_message);
  }
  
  @Override
  public void clearError(){
    vueError_.setText(" ");
  }
  
  // Gestion du container de module \\

  /**
   * Ajoute le JComponent dans le container de module, c'est � dire dans la
   * colonne � droite de l'�cran.
   */
  public void addVueModule(JComponent _vueModule){
    modelContainerModules_.addElement(_vueModule);
  }

  /**
   * Supprime le JComponent du container de module, c'est � dire de la colonne �
   * droite de l'�cran.
   * 
   * @param _vueModule
   */
  public void removeVueModule(JComponent _vueModule){
    modelContainerModules_.removeElement(_vueModule);
  }

  // Gestion des actions \\
  
  /** Les action � la fen�tre 1d. */ 
  JComponent[] specificTools_;
  
  /**
   * Renvoie les actions sp�cifiques � la fen�tre 1d. Ils seront affiches dans la
   * tool bar de l'application.
   * 
   * @return specificTools_
   */
  public JComponent[] getToolsActions() {
    if (specificTools_==null) {
      final JDesktopPane j=frame1d_.getDesktopPane();
      BuDesktop buJ=null;
      if (j instanceof BuDesktop)
        buJ=(BuDesktop)j;
      List<EbliActionInterface> actionAbs=controllerBief_.getVueBief().getController().getActions();
      // Selection des actions
      String[] tmpAction=new String[]{"RECTANGLE_SELECTION", "POLYGON_SELECTION", "RESTORE", "ZOOM", "ZOOM_ON_SELECTED",
          "LAST_VIEW", "MOVE_VIEW", "CHANGE_REFERENCE", "NAVIGATE"};
      // info : Actions not kept : "CHOOSE_COLOR_PALET", "EDIT_LEGEND", "CONFIGURE", "INFOS", "TABLE"
      HashSet<String> actionsKept=new HashSet<String>();
      actionsKept.addAll(Arrays.asList(tmpAction));
      List<EbliActionInterface> actions=new ArrayList<EbliActionInterface>();
      for (int i=0; i<actionAbs.size(); i++)
        if (actionAbs.get(i)==null)
          actions.add(actionAbs.get(i));
        else if (actionsKept.contains(actionAbs.get(i).getValue(Action.ACTION_COMMAND_KEY)))
          actions.add(actionAbs.get(i));
      // Construction des boutons
      final List<?> l=EbliLib.updateToolButtons(actions, buJ);
      specificTools_=new JComponent[l.size()];
      l.toArray(specificTools_);
    }
    if (specificTools_==null)
      return null;
    final JComponent[] r=new JComponent[specificTools_.length];
    System.arraycopy(specificTools_, 0, r, 0, r.length);
    return r;
  }

  // Gestion de la selection entre les trois widgets (tableau, bief et courbe) \\
  
  /**
   * Changement de la selection dans le widget de bief.
   */
  public void selectionChanged(ZSelectionEvent _evt) {
    clearError();
    ZScene scene=controllerBief_.getVueBief().getScene();
    int sceneIdSelected=scene.getSelectionHelper().getUniqueSelectedIdx();
    boolean ok=true; // Indique si tout s'est bien pass�.
    if (sceneIdSelected==-1)
      controllerProfil_.getProfilSelectionModel().clearSelection();
    else {
      int idSelected=scene.sceneId2LayerId(sceneIdSelected);
      // Extraction des diff�rentes informations n�c�ssaires au changement de
      // courbe \\
      ZCalqueAffichageDonneesInterface calque=scene.getLayerForId(sceneIdSelected);
      if (calque.modeleDonnees() instanceof ZModeleLigneBriseeEditable) {
        GISZoneCollectionLigneBrisee zone=((ZModeleLigneBriseeEditable)calque.modeleDonnees()).getGeomData();
        if (zone.getIndiceOf(GISAttributeConstants.NATURE)!=-1) {
          if (zone.getValue(zone.getIndiceOf(GISAttributeConstants.NATURE), idSelected)==GISAttributeConstants.ATT_NATURE_PF) {
            controllerProfil_.getProfilSelectionModel().setSelectionInterval(idSelected, idSelected);
          }
          else
            // On a pas selectionn� un profil, on annule la selection
            ok=false;
        }
        else
          // Il n'y a pas d'attribut Nature, on annule la selection
          ok=false;
      }
      else
        // Le type de model ne correspond pas, on annule la selection
        ok=false;
    }
    if(!ok) {
      //dataGeomAdapter_.setData(null, -1);
      scene.clearSelection();
      controllerProfil_.getProfilSelectionModel().clearSelection();
      showError(MdlResource.getS("Seuls les profils sont s�lectionnables."));
    }
  }
  
  /**
   * Changement de la selection dans le widget du tableau.
   */
  public void valueChanged(ListSelectionEvent e) {
    if (listenEventSelection_) {
      listenEventSelection_=false;
      vueCourbe_.setSelection(vueTableau_.getSelection());
      listenEventSelection_=true;
    }
  }

  /**
   * Changement de la selection dans le widget de la courbe.
   */
  public void listeSelectionChanged(CtuluListSelectionEvent _e) {
    if (listenEventSelection_) {
      listenEventSelection_=false;
      vueTableau_.setSelection(vueCourbe_.getSelection());
      listenEventSelection_=true;
    }
  }
  
  /**
   * Export du bief selectionn�.
   */
  public void export(){
    Bief bief=controllerBief_.getSelectedBief();
    if(bief==null) {
      appli_.warn(MdlResource.getS("Attention"), MdlResource.getS("Il n'y a pas de bief s�lectionn�."));
      return;
    }

    // Les formats a exporter.
    HashMap<FileFormatVersionInterface, Exporter1d> ff2Exporter=new HashMap<FileFormatVersionInterface, Exporter1d>();
    ff2Exporter.put(MascaretGEO1dFileFormat.getInstance(), new Exporter1d.Mascaret1d());
    ff2Exporter.put(MascaretGEO2dFileFormat.getInstance(), new Exporter1d.Mascaret2d());
    ff2Exporter.put(RubarStCnFileFormat.getInstance(), new Exporter1d.Rubar());
    
    FileFormatVersionInterface[] ffs=ff2Exporter.keySet().toArray(new FileFormatVersionInterface[0]);
    // Choix du fichier a exporter, donc du format.
    ExportBiefPanel vueExport=new ExportBiefPanel(appli_,ffs);
    if(!vueExport.run())
      return;
    
    File file=vueExport.getFile();
    FileFormatVersionInterface ff=null;
    for (int i=0; i<ffs.length; i++) {
      if (ffs[i].getFileFormat().createFileFilter().accept(file)) {
        ff=ffs[i];
        break;
      }
    }
    if (ff==null) return;
    
    // Export.
    CtuluIOOperationSynthese result=ff2Exporter.get(ff).export(file,bief);
    appli_.manageErrorOperationAndIsFatal(result);
  }
  
  @Override
  public void internalFrameActivated(InternalFrameEvent e) {
    updateFrom2d();
  }
  
  @Override
  public void internalFrameDeactivated(InternalFrameEvent e) {
    mng_.clean();
    controllerBief_.disableSynchronisers();
  }

  public void updateFrom2d() {
    controllerBief_.updateFrom2d();
  }
  
  public Observable getObservable() {
    return delegateObs;
  }
  
  public void fireUIChanged() {
    delegateObs.setChanged();
    delegateObs.notifyObservers("UI");
  }
  
  public void fireDataChanged() {
    delegateObs.setChanged();
    delegateObs.notifyObservers("Params");
  }
}
