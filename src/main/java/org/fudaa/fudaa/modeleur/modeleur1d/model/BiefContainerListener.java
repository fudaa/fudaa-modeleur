/*
 * @creation     23 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.model;

/**
 * Interface permettant de ce maintenir � jour du contenu du BiefContainer
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public interface BiefContainerListener extends ProfileSetListener {
  public void biefSelectedRenamed(String _oldName, String _newName);
  public void biefSelectedChanged(String _newName);
//  public void profilRemoved(int _idxProfil);
  public void profilRenamed(int _idxProfil, String _oldName, String _newName);
//  public void profilAdded(int _idxProfil);
}
