/*
 * @creation     18 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.view;

import java.awt.BorderLayout;
import java.io.File;

import javax.swing.JPanel;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuVerticalLayout;

/**
 * Panneau pour importer des donn�es 1d. Il se contente de faire apparaitre une
 * fen�tre modale (via la m�thode 'run') puis met � disposition les
 * informations. Il ne fait aucun traitement par lui m�me.
 * 
 * @author marchand@deltacad.fr
 * @version $Id$
 */
public class ImportBiefPanel extends CtuluDialogPanel {

  private CtuluFileChooserPanel fileChooser_;
  private CtuluUI ui_;
  private BuFileFilter[] filters_;

  public ImportBiefPanel(CtuluUI _ui, FileFormatVersionInterface[] _ffs) {
    ui_=_ui;
    
    filters_=new BuFileFilter[_ffs.length];
    for (int i=0; i<_ffs.length; i++) {
      filters_[i]=_ffs[i].getFileFormat().createFileFilter();
    }

    setLayout(new BuVerticalLayout(5, true, true));
    final String title=MdlResource.getS("Fichier d'import");
    fileChooser_=new CtuluFileChooserPanel(title);
    fileChooser_.setAllFileFilter(false);
    fileChooser_.setWriteMode(true);
    fileChooser_.setFilter(filters_);

    BuLabel lbFile=new BuLabel(title);
    JPanel pnFile=new JPanel();
    pnFile.setLayout(new BorderLayout(3, 3));
    pnFile.add(lbFile, BorderLayout.WEST);
    pnFile.add(fileChooser_, BorderLayout.CENTER);
    add(pnFile);
  }

  /**
   * Lance l'exporter dans une fen�tre modale.
   * 
   * @return vrai si l'utilisateur clic sur 'ok' � la fin, faux si il clic sur 'annuler'.
   */
  public boolean run(){
    return CtuluDialogPanel.isOkResponse(afficheModale(ui_.getParentComponent(), MdlResource.getS("Importer un bief")));
  }

  @Override
  public boolean isDataValid() {
    if (!isFileOK())
      return false;
    return true;
  }

  private boolean isFileOK() {
    final File f=fileChooser_.getFile();
    if (f==null) {
      setErrorText(MdlResource.getS("Donnez un nom de fichier"));
      return false;
    }
    // Verification de l'extension
    boolean accept=false;
    for (BuFileFilter ff : filters_) {
      if (ff.accept(f)) {
        accept=true;
        break;
      }
    }
    if (!accept) {
      setErrorText(MdlResource.getS("Le fichier choisi a une extension inconnue"));
      return false;
    }

    if (CtuluLibFile.getExtension(CtuluLibFile.getSansExtension(f.getName()))!=null) {
      setErrorText(MdlResource.getS("Ne mettez pas plusieurs '.' dans le nom."));
      return false;
    }
    setErrorText(null);
    return true;
  }

  public File getFile() {
    return fileChooser_.getFile();
  }
}
