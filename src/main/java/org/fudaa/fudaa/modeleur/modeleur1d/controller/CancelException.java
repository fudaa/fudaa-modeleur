/*
 * @creation     12 f�vr. 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.controller;

/**
 * Exception lev�e quand quelque chose est annul� par l'utilisateur.
 * @author Emmanuel MARTIN
 * @version $Id:$
 */
public class CancelException extends Exception {
  public CancelException(String _message) {
    super(_message);
  }
}
