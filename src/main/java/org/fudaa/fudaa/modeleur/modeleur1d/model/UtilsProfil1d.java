/*
 * @creation     22 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.model;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISCoordinateSequenceContainerInterface;
import org.fudaa.ctulu.gis.GISCoordinateSequenceFactory;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;

/**
 * Quelques fonctions utilis�es dans le module modeleur1d.
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class UtilsProfil1d {

  /** Tol�rance pour les calcules de comparaison avec des doubles. */
  static private double tolerance_=0.0001;
  
  /**
   * Retourne vrai si le profil est correct.
   * @param _ana L'analyseur. Permet de retourner le pourquoi de la non conformit�. Peut �tre null.
   */
  static public boolean isProfilCorrect(final GISZoneCollection _zone, final int _idxProfil, final CtuluAnalyze _ana) {

    // Une classe pour afficher le message.
    class MessageHelper {
      public String getProfName(int _idxProfil) {
        int iattName=_zone.getIndiceOf(GISAttributeConstants.TITRE);
        return (String)_zone.getValue(iattName, _idxProfil);
      }
    }
        
    CoordinateSequence seq=((GISCoordinateSequenceContainerInterface)_zone.getGeometry(_idxProfil)).getCoordinateSequence();
    // Verifie qu'on a bien au minimum deux points. \\
    if (seq.size()<2) {
      if (_ana!=null)
        _ana.addWarn(MdlResource.getS("Profil '{0}' : Le nombre de points mini doit �tre de 2", 
            new MessageHelper().getProfName(_idxProfil)));
      return false;
    }
    // Verifie que deux points cons�cutifs ne sont pas confondus. \\
    for (int i=1; i<seq.size(); i++)
      if (seq.getX(i-1)==seq.getX(i)&&seq.getY(i-1)==seq.getY(i)&&seq.getOrdinate(i,2)==seq.getOrdinate(i-1,2)) {
        if (_ana!=null)
          _ana.addWarn(MdlResource.getS("Profil '{0}' : Au moins 2 points sont confondus au point {1}",
             new MessageHelper().getProfName(_idxProfil), i+1));
        return false;
      }
    // Verifie que le profil donn� ne poss�de pas des segments qui se coupent entre eux
    // eux \\
    for (int i=1; i<seq.size(); i++) {
      Geometry mainAxe=GISGeometryFactory.INSTANCE.createLineString(new Coordinate[]{seq.getCoordinate(i-1), seq.getCoordinate(i)});
      boolean ok=true;
      // Un seul point d'intersection avec le segment qui suit
      int j=i;
      if (j+1<seq.size()
          &&mainAxe.intersection(
              GISGeometryFactory.INSTANCE.createLineString(new Coordinate[]{seq.getCoordinate(j), seq.getCoordinate(j+1)}))
              .getNumPoints()>1)
        ok=false;
      j++;
      // Aucun point d'intersection avec les segments qui suivent
      while (ok&&++j+1<seq.size())
        ok=!mainAxe.intersects(GISGeometryFactory.INSTANCE.createLineString(new Coordinate[]{seq.getCoordinate(j),
            seq.getCoordinate(j+1)}));
      // Traitement d'une �ventuelle intersection
      if (!ok) {
        if (_ana!=null)
          _ana.addWarn(MdlResource.getS("Profil '{0}' : Le profil se coupe lui m�me au point {1}",
             new MessageHelper().getProfName(_idxProfil), j+1));
        return false;
      }
    }

    // / D�termination des points de ruptures et verification qu'il y en a
    // au maximun deux. \\\
    int nbPointsRupture=0;
    int idx1=0;
    int idx2=1;
    int idx3=2;
    int idxPointRupturePrecedent=-1;
    while(idx3<seq.size()) {
      if(idx1==idx2)
        idx2++;
      else if(idx2==idx3)
        idx3++;
      else if(seq.getCoordinate(idx1).equals(seq.getCoordinate(idx2)))
        idx2++;
      else if(seq.getCoordinate(idx2).equals(seq.getCoordinate(idx3)))
        idx3++;
      else {
        if(UtilsProfil1d.getAngle(seq.getCoordinate(idx1), seq.getCoordinate(idx2), seq.getCoordinate(idx3))<(Math.PI-0.0001))
          if(idxPointRupturePrecedent<0||!seq.getCoordinate(idxPointRupturePrecedent).equals(seq.getCoordinate(idx2)))
            nbPointsRupture++;
        if (nbPointsRupture>2) {
          if (_ana != null)
            _ana.addWarn(MdlResource.getS("Profil '{0}' : Le profil poss�de plus de 3 segments",
                new MessageHelper().getProfName(_idxProfil)));
          return false;
        }
        if(idx2+1<idx3)
          idx2++;
        else
          idx1=idx2;
      }
    }
    return true;
  }

  // Quelques fonctions m�th�matiques \\
  
  /**
   * retourne le vecteur _a, _b sous forme de Coordinate.
   */
  static public Coordinate vec(Coordinate _a, Coordinate _b){
    return new Coordinate(_b.x-_a.x, _b.y-_a.y, _b.z-_a.z);
  }

  /**
   * Retourne la projection de _a sur la droite _b_c.
   * Attention : Il s'agit bien d'une projection sur une
   * droite et non un segment de droite : le r�sultat
   * peut �tre en dehors de [_b, _c]
   */
  static public Coordinate proj(Coordinate _a, Coordinate _b, Coordinate _c){
    Coordinate vBC=vec(_b, _c);
    Coordinate vBA=vec(_b, _a);
    double normeBC=Math.sqrt(vBC.x*vBC.x+vBC.y*vBC.y+vBC.z*vBC.z);
    double produitScalaireBCBA=vBC.x*vBA.x+vBC.y*vBA.y+vBC.z*vBA.z;
    double rapportSurBC=produitScalaireBCBA/(normeBC*normeBC);
    return new Coordinate(vBC.x*rapportSurBC+_b.x, vBC.y*rapportSurBC+_b.y, vBC.z*rapportSurBC+_b.z);
  }
  
  /**
   * Normalise le vecteur pass� en param�tre.
   */
  static public Coordinate normalisation(Coordinate _a){
    double normeA=Math.sqrt(_a.x*_a.x+_a.y*_a.y+_a.z*_a.z);
    _a.x=_a.x/normeA;
    _a.y=_a.y/normeA;
    _a.z=_a.z/normeA;
    return _a;
  }

  /**
   * Compars les deux vecteurs avec la tol�rance donn�e. Le dernier param�tre
   * indique si un normalisation des deux vecteurs doit �tre faite avec la
   * comparaison. Les vecteurs ne sont pas modif�s dans l'op�ration. _testZ doit
   * �tre mit � vrai pour que la comparaison tienne compte du z.
   */
  static public boolean egal(Coordinate _a, Coordinate _b, double _tolerance, boolean _normalisation, boolean _testZ) {
    if(_normalisation){
      normalisation(_a);
      normalisation(_b);
    }
    Coordinate diff=vec(_a, _b);
    return Math.abs(diff.x)<_tolerance&&Math.abs(diff.y)<_tolerance&&(!_testZ||Math.abs(diff.z)<_tolerance);
  }
  
  /**
   * Comme 'egal' sauf que _tolerance _normalisation et _testZ sont valu�s.
   */
  static public boolean egalSansZ(Coordinate _a, Coordinate _b) {
    return egal(_a, _b, tolerance_, false, false);
  }
  
  /**
   * Comme 'egal' sans l'attribut _testZ qui est mit � vrai.
   */
  static public boolean egal(Coordinate _a, Coordinate _b, double _tolerance, boolean _normalisation) {
    return egal(_a, _b, _tolerance, _normalisation, true);
  }
  
  /**
   * Comme 'egal' sans le dernier param�tre qui n'a de sens que lorsque les
   * Coordinates sont des vecteurs.
   */
  static public boolean egal(Coordinate _a, Coordinate _b, double _tolerance) {
    return egal(_a, _b, _tolerance, false);
  }

  /**
   * Comme 'egal' sans la tolerance (la tolerance par defaut est utilis�) ni la
   * normalisation.
   */
  static public boolean egal(Coordinate _a, Coordinate _b) {
    return egal(_a, _b, tolerance_);
  }
  
  /**
   * Retourne la tol�rance utilis�e par defaut dans cette classe.
   */
  static public double getTolerance() {
    return tolerance_;
  }
  
  /**
   * Retourne vrai si les deux param�tres sont �gaux � une tol�rance pr�s.
   */
  static public boolean egal(double _d1, double _d2) {
    return Math.abs(_d1-_d2)<tolerance_;
  }
  
  /**
   * Retourne l'index du point pr�c�dent la coordonn�e indiqu�e sur la coordinateSequence donn�e.
   * Retourne -1 si la coordonn�e donn�e correspond � l'index 0.
   * Retourne -2 si la coordonn�e n'appartient pas � la g�om�trie.
   */
  static public int getPreviousIndex(CoordinateSequence _geom, Coordinate _point) {
    if(_point==null||_geom.size()<=0||egalSansZ(_point, _geom.getCoordinate(0)))
      return -1;
    boolean fini=false;
    int i=-1;
    GISPoint point=new GISPoint(_point);
    while (!fini&&++i<_geom.size()-1)
      fini=(GISGeometryFactory.INSTANCE
          .createLineString(new Coordinate[]{_geom.getCoordinate(i), _geom.getCoordinate(i+1)})).distance(point)<tolerance_;
    if(!fini)
      return -2;
    else
      return i;
  }
  
  /**
   * Retourne l'index du point suivant la coordonn�e indiqu�e sur la coordinateSequence donn�e.
   * Retourne -1 si la coordonn�e donn�e correspond au dernier l'index.
   * Retourne -2 si la coordonn�e n'appartient pas � la g�om�trie.
   */
  static public int getNextIndex(CoordinateSequence _geom, Coordinate _point) {
    if(_point==null||_geom.size()<=0||egalSansZ(_point, _geom.getCoordinate(_geom.size()-1)))
      return -1;
    boolean fini=false;
    int i=_geom.size();
    GISPoint point=new GISPoint(_point);
    while (!fini&&--i>0)
      fini=(GISGeometryFactory.INSTANCE
          .createLineString(new Coordinate[]{_geom.getCoordinate(i-1), _geom.getCoordinate(i)})).distance(point)<tolerance_;
    if(!fini)
      return -2;
    else
      return i;
  }

  /**
   * Retourne l'index de la coordonn�e pass�e en param�tre. Si le point n'existe
   * pas -1 est retourn�.
   */
  static public int getIndex(CoordinateSequence _geom, Coordinate _point) {
    if(_geom==null||_point==null)
      return -1;
    boolean found=false;
    int i=-1;
    while(!found&&++i<_geom.size())
      found=egalSansZ(_geom.getCoordinate(i), _point);
    if(found)
      return i;
    return -1;
  }
  
  /**
   * Retourne l'abscisse curviligne du point '_point' de la polyligne '_geom'.
   * Si la coordonn�e n'appartient pas � la geometry, -1 est retourn�.
   */
  static public double abscisseCurviligne(CoordinateSequence _geom, Coordinate _point){
    int i=getPreviousIndex(_geom, _point);
    if(i==-1)
      return 0;
    if(i==-2)
      return -1;
    // Calcule de l'abscisse curviligne
    double valueCurviligne=0;
    for (int j=1; j<=i; j++)
      valueCurviligne+=_geom.getCoordinate(j).distance(_geom.getCoordinate(j-1));
    // Mise � jour de l'abscisse curviligne
    valueCurviligne+=_geom.getCoordinate(i).distance(_point);
    return valueCurviligne;
  }
  
  /**
   * Retourne l'abscisse curviligne sur geom1 du croisement entre geom1 et geom2.
   * Si les geometries ne se croisent pas ou se croisent en plusieurs endroits, -1 est retourn�.
   */
  static public double abscisseCurviligne(CoordinateSequence _geom1, CoordinateSequence _geom2){
    Geometry intersection=GISGeometryFactory.INSTANCE.createLineString(_geom1).intersection(GISGeometryFactory.INSTANCE.createLineString(_geom2));

    // Pas d'intersection : Peut �tre en limite.
    if(intersection.getNumPoints()==0) {
      GrPolyligne pl=new GrPolyligne(_geom2);
      if (pl.distanceXY(new GrPoint(_geom1.getCoordinate(0)))<=tolerance_) {
        return 0;
      }
      else if (pl.distanceXY(new GrPoint(_geom1.getCoordinate(_geom1.size()-1)))<=tolerance_) {
        return abscisseCurviligne(_geom1, _geom1.getCoordinate(_geom1.size()-1));
      }
      // Vraiment trop loin => Erreur
      else
        return -1;
    }
    // Plus de 2 intersections => Erreur
    else if (intersection.getNumPoints()>1) {
      return -1;
    }
    else
      return abscisseCurviligne(_geom1, intersection.getCoordinate());
  }

  /**
   * Retourne la coordonn�e correspondant � 'abscisseCurv' sur '_geom'. La
   * valeur de z est mise � 0. _abscisseCurv peut �tre n�gatif : le point
   * retourn� sera avec le premier ou sup�rieur au max.
   */
  static public Coordinate getCoordinateXY(CoordinateSequence _geom, double _abscisseCurv) {
    // Recherche du point pr�c�dent '_abscisseCurv'
    int i=0;
    double oldAbsCurv=0;
    double currentAbsCurv=0;
    while(currentAbsCurv<=_abscisseCurv&&++i<_geom.size()) {
      oldAbsCurv=currentAbsCurv;
      currentAbsCurv+=_geom.getCoordinate(i-1).distance(_geom.getCoordinate(i));
    }
    // Index du point pr�c�dent _abscisseCurv
    if(i>0)
      i--;
    // Calcul de la coordonn�e \\
    int idx1;
    int idx2;
    double valCurv;
    double newX;
    double newY;
    if(i==0) { // Premier point
      idx1=i+1;
      idx2=i;
      valCurv=Math.abs(_abscisseCurv-abscisseCurviligne(_geom, _geom.getCoordinate(idx1)));
    }
    else if (i+1<_geom.size()) { // Point au milieu
      idx1=i;
      idx2=i+1;
      valCurv=_abscisseCurv-oldAbsCurv;
    }
    else { // Dernier point
      idx1=i-1;
      idx2=i;
      valCurv=_abscisseCurv-oldAbsCurv;
    }
    if(valCurv<tolerance_)
      return _geom.getCoordinate(i);
    // Calcul des nouvelles coordonn�es
    if (Math.abs(_geom.getX(idx2)-_geom.getX(idx1))>tolerance_) {
      // Extraction du signe de xa-xb
      double sign=Math.signum(_geom.getX(idx2)-_geom.getX(idx1));
      double coefDirecteur=(_geom.getY(idx2)-_geom.getY(idx1))/(_geom.getX(idx2)-_geom.getX(idx1));
      newX=_geom.getX(idx1)+sign*valCurv/Math.sqrt(1+coefDirecteur*coefDirecteur);
      newY=_geom.getY(idx1)+coefDirecteur*(newX-_geom.getX(idx1));
    }
    else { // Cas du bout de profil vertical
      // Extraction du signe de ya-yb
      double sign=Math.signum(_geom.getY(idx2)-_geom.getY(idx1));
      newX=_geom.getX(idx1);
      newY=_geom.getY(idx1)+sign*valCurv;
    }
    return new Coordinate(newX, newY, 0);
  }
  
  /**
   * Retourne la coordonn�e correspondant � 'abscisseCurv' sur '_geom'. La
   * valeur de z est mise � 0. _abscisseCurv peut �tre n�gatif : le point
   * retourn� sera avec le premier ou sup�rieur au max.
   */
  static public Coordinate getCoordinateXY(Coordinate[] _geom, double _abscisseCurv) {
    return getCoordinateXY(new GISCoordinateSequenceFactory().create(_geom), _abscisseCurv);
  }

  /**
   * Retourne l'angle form� par les segments _a_b et _b_c. _a, _b et _c doivent
   * �tre non confondus sous peine d'avoir un {@link IllegalArgumentException}.
   * L'angle est toujours positif.
   * L'ordonn� z n'est pas prise en compte.
   * L'angle retourn� est en radian.
   */
  static public double getAngle(Coordinate _a, Coordinate _b, Coordinate _c) {
    Coordinate a=new Coordinate(_a.x, _a.y, 0);
    Coordinate b=new Coordinate(_b.x, _b.y, 0);
    Coordinate c=new Coordinate(_c.x, _c.y, 0);
    if(egal(a, b)||egal(b, c)||egal(a, c))
      throw new IllegalArgumentException(MdlResource.getS("Les trois points doivent �tre non confondus."));
    // Projection de b sur ac
    Coordinate b2=proj(b, a, c);
    // Calcul des angles interm�diaires
    double angleCBB2=Math.asin(b2.distance(c)/c.distance(b));
    double angleB2BA=Math.asin(b2.distance(a)/a.distance(b));
    if(angleCBB2==Double.NaN||angleB2BA==Double.NaN)
      return Math.PI;
    // Selon que B2 appartient ou non � ac, la d�termination de l'angle final diff�re.
    double angle;
    int position=getPositionXY(b2, a, c);
    if(position==-1)
      angle=angleCBB2-angleB2BA;
    else if(position==1)
      angle=angleB2BA-angleCBB2;
    else
      angle=angleCBB2+angleB2BA;
    return angle;
  }
  
  /**
   * Soit _a, _b, _c align�s et non confondus. Retourne -1 si _a avant _b_c, 0
   * si _a est entre _b_c (inclu), +1 sinon. Si _a, _b et _c ne sont pas
   * align�s, {@link IllegalArgumentException} est lev�.
   */
  static public int getPositionXY(Coordinate _a, Coordinate _b, Coordinate _c) {
    if(!egal(proj(_a, _b, _c), _a)||egal(_a, _b)||egal(_b, _c)||egal(_a, _c))
      throw new IllegalArgumentException(MdlResource.getS("Les trois points doivent �tre align�s et non confondus."));
    Coordinate vecBC=vec(_b, _c);
    double k;
    if(vecBC.x!=0)
      k=(_a.x-_b.x)/vecBC.x;
    else
      k=(_a.y-_b.y)/vecBC.y;
    if(k<0)
      return -1;
    else if(k>1)
      return 1;
    else
      return 0;
  }
  
  /**
   * Retourne vrai si _el is dans _coll. _coll n'est pas consid�r� comme tri�e.
   */
  static public boolean in(int _in, int[] _coll) {
    boolean found=false;
    int i=-1;
    while(!found&&++i<_coll.length)
      found=_coll[i]==_in;
    return found;
  }
  
  /**
   * Retourne les donn�es de la g�om�tries de _zone.
   * retourne null si _idxGeom est invalide.
   */
  static public Object[] getData(int _idxGeom, GISZoneCollection _zone) {
    Object[] data=null;
    if (_idxGeom>=0&&_idxGeom<_zone.getNbGeometries()) {
      GISAttributeModel[] models=_zone.getModels();
      data=new Object[models.length];
      for (int i=0; i<data.length; i++)
        data[i]=models[i].getObjectValueAt(_idxGeom);
    }
    return data;
  }
  
}
