/*
 * @creation     16 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.model;

/**
 * Exception lev�e par les classes impl�mentant DataGeometry pour indiqu� une
 * impossibilit� dans le traitement demand�. Un message d'erreur renseigne sur
 * le probl�me.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class ProfilContainerException extends Exception {

  public ProfilContainerException(String _message) {
    super(_message);
  }
}
