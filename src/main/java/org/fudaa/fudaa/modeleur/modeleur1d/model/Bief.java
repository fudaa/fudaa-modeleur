/*
 * @creation     22 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.model;

import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeModelIntegerList;
import org.fudaa.ctulu.gis.GISCoordinateSequenceFactory;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ebli.calque.ZModelGeometryListener;
import org.fudaa.ebli.calque.ZModeleLigneBrisee;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.fudaa.modeleur.layer.MdlModel1dAxe;
import org.fudaa.fudaa.modeleur.layer.MdlModel1dBank;
import org.fudaa.fudaa.modeleur.layer.MdlModel1dLimiteStockage;
import org.fudaa.fudaa.modeleur.layer.MdlModel2dConstraintLine;
import org.fudaa.fudaa.modeleur.layer.MdlModel2dDirectionLine;
import org.fudaa.fudaa.modeleur.layer.MdlModel2dProfile;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;

/**
 * Contient les informations relatives � un bief.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class Bief {
  public ZModeleLigneBriseeEditable axeHydraulique_;
  public ZModeleLigneBriseeEditable profils_;
  public ZModeleLigneBriseeEditable rives_;
  public ZModeleLigneBriseeEditable limitesStockages_;
  public ZModeleLigneBriseeEditable lignesDirectrices_;
  public ZModeleLigneBriseeEditable lignesContraints_;
  private String biefName_;

  /** Le synchroniser de gis. */
  private GisZoneSynchroniser gisSynchroniser_=new GisZoneSynchroniser();

  /**
   * Les models de bases sont instanci�s, le synchronizer n'est pas lanc�.
   * Attention le modele d'axe hydraulique ne contient pas d'axe hydraulique. Il
   * est donc invalide.
   */
  public Bief() {
    axeHydraulique_=new MdlModel1dAxe(null);
    UtilsBief1d.normalizeAxeHydrauliqueAttributes(axeHydraulique_.getGeomData());
    profils_=new MdlModel2dProfile(null, null);
    MdlModel2dProfile.normalizeZone(profils_.getGeomData(), null, true);
    rives_=new MdlModel1dBank(null, null);
    limitesStockages_=new MdlModel1dLimiteStockage(null, null);
    lignesContraints_=new MdlModel2dConstraintLine(null, null);
    lignesDirectrices_=new MdlModel2dDirectionLine(null, null);
  }

  /**
   * Constructeur, avec le nom.
   * @param _name Le nom du bief.
   */
  public Bief(String _name) {
    this();
    biefName_=_name;
  }

  /**
   * Value simplement les attributs publiques avec ceux pass�s en param�tres. Ne
   * lance pas le synchoniser.
   * 
   * @param _axeHydraulique
   * @param _lignesContraints
   * @param _lignesDirectrices
   * @param _limitesStockages
   * @param _profils
   * @param _rives
   */
  public Bief(ZModeleLigneBriseeEditable _axeHydraulique, ZModeleLigneBriseeEditable _lignesContraints, ZModeleLigneBriseeEditable _lignesDirectrices,
      ZModeleLigneBriseeEditable _limitesStockages, ZModeleLigneBriseeEditable _profils, ZModeleLigneBriseeEditable _rives) {
    axeHydraulique_=_axeHydraulique;
    lignesContraints_=_lignesContraints;
    lignesDirectrices_=_lignesDirectrices;
    limitesStockages_=_limitesStockages;
    profils_=_profils;
    rives_=_rives;

    // Normalize les attributs attendus si certains ne sont pas pr�sents.
    MdlModel2dProfile.normalizeZone(profils_.getGeomData(), null, true);
    MdlModel1dAxe.normalizeZone(axeHydraulique_.getGeomData(), null);
    MdlModel1dBank.normalizeZone(rives_.getGeomData(), null);
    MdlModel1dLimiteStockage.normalizeZone(limitesStockages_.getGeomData(), null);
    MdlModel2dConstraintLine.normalizeZone(lignesContraints_.getGeomData(), null);
    MdlModel2dDirectionLine.normalizeZone(lignesDirectrices_.getGeomData(), null);
  }

  /**
   * Definit le nom du bief
   * @param _name Le nom
   */
  public void setName(String _name) {
    biefName_=_name;
  }

  /**
   * @return Le nom du bief, ou null si non d�finit.
   */
  public String getName() {
    return biefName_;
  }

  /**
   * Retourne vrai si le synchroniser est activ�.
   */
  public boolean isSynchronizerActived() {
    return gisSynchroniser_.isActived();
  }

  /**
   * Active le synchroniser.
   */
  public void enableSynchroniser() {
    gisSynchroniser_.enable();
  }

  /**
   * D�sactive le synchroniser.
   */
  public void disableSynchroniser() {
    gisSynchroniser_.disable();
  }

  /**
   * Indique au synchroniser que les models du bief ont potentiellement chang�.
   * L'appelle � cette m�thode active automatiquement le synchroniser. Note :
   * Une fois activ� le synchroniser met automatiquement � jour les gis. Cette
   * m�thode n'est utile que si les instances des models ont �t� manuellement
   * chang�, sinon le synchroniser continuea sa synchronisation sur les anciens
   * models.
   */
  public void updateSynchroniser() {
    gisSynchroniser_.enable();
  }

  /**
   * Cette classe g�re la g�n�ration et r�g�n�ration des g�om�tries volatiles
   * (comme les rives, les limites de stockages et des lignes directrices).
   */
  protected class GisZoneSynchroniser implements ZModelGeometryListener {
    /** Le model de profil �cout�. */
    private ZModeleLigneBrisee profilsListen_;
    /** Le model de rives modifi�. */
    private ZModeleLigneBrisee rivesModified_;
    /** Le model de limites de stockages modifi�. */
    private ZModeleLigneBrisee limitesStockagsesModified_;
    /** Le model de lignes directrices modifi�. */
    private ZModeleLigneBrisee lignesDirectricesModified_;
    // Les index des attributs \\
    private int idxAttRiveGauche_=-1;
    private int idxAttRiveDroite_=-1;
    private int idxAttlsGauche_=-1;
    private int idxAttlsDroite_=-1;
    private int idxAttlignesDirectrices_=-1;

    /**
     * Active/met � jour le synchroniser.
     */
    public void enable() {
      // Met tout au propre
      disable();
      // R�g�n�re tout
      if (profils_!=null) {
        profils_.addModelListener(this);
        profilsListen_=profils_;
        rivesModified_=rives_;
        limitesStockagsesModified_=limitesStockages_;
        lignesDirectricesModified_=lignesDirectrices_;
        idxAttRiveGauche_=profilsListen_.getGeomData().getIndiceOf(GISAttributeConstants.INTERSECTION_RIVE_GAUCHE);
        idxAttRiveDroite_=profilsListen_.getGeomData().getIndiceOf(GISAttributeConstants.INTERSECTION_RIVE_DROITE);
        idxAttlsGauche_=profilsListen_.getGeomData().getIndiceOf(GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_GAUCHE);
        idxAttlsDroite_=profilsListen_.getGeomData().getIndiceOf(GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_DROITE);
        idxAttlignesDirectrices_=profilsListen_.getGeomData().getIndiceOf(GISAttributeConstants.INTERSECTIONS_LIGNES_DIRECTRICES);
        // Normalisation de la position des g�om�tries et cr�ation des
        // g�om�tries manquantes \\
        /*
         * Dans les zones de rives et de limites de stockages, les g�om�tries
         * gauches sont � la position 0 et les g�om�tries droites sont � la
         * position 1.
         */
        // Rives
        if (rivesModified_.getNombre()==0) {
          rivesModified_.getGeomData().addGeometry(new GISPolyligne(), null, null);
          rivesModified_.getGeomData().addGeometry(new GISPolyligne(), null, null);
        }
        else if (rivesModified_.getNombre()==1) {
          rivesModified_.getGeomData().addGeometry(new GISPolyligne(), null, null);
          if (position(rivesModified_.getGeomData().getGeometry(0))==1)
            rivesModified_.getGeomData().switchGeometries(0, 1, null);
        }
        else if (rivesModified_.getNombre()==2) {
          int pos=position(rivesModified_.getGeomData().getGeometry(0));
          if (pos==0)
            if (position(rivesModified_.getGeomData().getGeometry(1))==-1)
              rivesModified_.getGeomData().switchGeometries(0, 1, null);
          if (pos==1)
            rivesModified_.getGeomData().switchGeometries(0, 1, null);
        }
        else
          throw new IllegalArgumentException(MdlResource.getS("Le nombre de rives est sup�rieur � 2."));
        // Limites de stockages
        if (limitesStockagsesModified_.getNombre()==0) {
          limitesStockagsesModified_.getGeomData().addGeometry(new GISPolyligne(), null, null);
          limitesStockagsesModified_.getGeomData().addGeometry(new GISPolyligne(), null, null);
        }
        else if (limitesStockagsesModified_.getNombre()==1) {
          limitesStockagsesModified_.getGeomData().addGeometry(new GISPolyligne(), null, null);
          if (position(limitesStockagsesModified_.getGeomData().getGeometry(0))==1)
            limitesStockagsesModified_.getGeomData().switchGeometries(0, 1, null);
        }
        else if (limitesStockagsesModified_.getNombre()==2) {
          int pos=position(limitesStockagsesModified_.getGeomData().getGeometry(0));
          if (pos==0)
            if (position(limitesStockagsesModified_.getGeomData().getGeometry(1))==-1)
              limitesStockagsesModified_.getGeomData().switchGeometries(0, 1, null);
          if (pos==1)
            limitesStockagsesModified_.getGeomData().switchGeometries(0, 1, null);
        }
        else
          throw new IllegalArgumentException(MdlResource.getS("Le nombre de limites de stockage est sup�rieur � 2."));
        regenerateAll();
      }
    }

    /**
     * D�termine la position de la courbe par rapport � l'axe hydraulique en se
     * basant sur les profils. Retourne -1 si � gauche, 0 si ind�terminable, 1
     * si � droite.
     */
    private int position(Geometry _geom) {
      if (axeHydraulique_.getNombre()==0)
        return 0;
      // Recherche d'intersection de _geom avec un profil
      boolean found=false;
      int i=-1;
      Coordinate coordIntersection=null;
      while (!found&&++i<profilsListen_.getNombre()) {
        Geometry inter=_geom.intersection((Geometry)profilsListen_.getObject(i));
        if (inter.getNumPoints()==1) {
          found=true;
          coordIntersection=inter.getCoordinate();
        }
      }
      if (!found)
        return 0;
      // Calcul de l'abscisse curviligne de l'axe hydraulique sur le profil.
      Geometry axeHydraulique=(Geometry)axeHydraulique_.getObject(0);
      Geometry intersection=((Geometry)profilsListen_.getObject(i)).intersection(axeHydraulique);
      // D�termination de la droite
      if (intersection.getNumPoints()==1) {
        CoordinateSequence seqProfil=profilsListen_.getGeomData().getCoordinateSequence(i);
        double abscisseCurvIntersectionAxe=UtilsProfil1d.abscisseCurviligne(seqProfil, intersection.getCoordinate());
        if (abscisseCurvIntersectionAxe>UtilsProfil1d.abscisseCurviligne(seqProfil, coordIntersection))
          return 1;
        else
          return -1;
      }
      return 0;
    }

    /**
     * D�active le synchroniser.
     */
    public void disable() {
      // indique si les g�om�tries volatiles doivent �tre d�truites
      if (profilsListen_!=null) {
        profilsListen_.removeModelListener(this);
        profilsListen_=null;
        rivesModified_=null;
        limitesStockagsesModified_=null;
        lignesDirectricesModified_=null;
        idxAttRiveGauche_=-1;
        idxAttRiveDroite_=-1;
        idxAttlsGauche_=-1;
        idxAttlsDroite_=-1;
        idxAttlignesDirectrices_=-1;
      }
    }

    /**
     * D�truit les g�om�tries volatiles
     */
    private void destroyGeometries() {
      // Suppression des anciennes g�om�tries \\
      // Rives
      rivesModified_.getGeomData().removeGeometries(new int[]{0, 1}, null);
      // Limites stockages
      limitesStockagsesModified_.getGeomData().removeGeometries(new int[]{0, 1}, null);
      // Lignes directrices
      int[] idx=new int[lignesDirectricesModified_.getNombre()];
      for (int i=0; i<idx.length; i++)
        idx[i]=i;
      lignesDirectricesModified_.getGeomData().removeGeometries(idx, null);
    }

    /**
     * Retourne vrai si activ�.
     */
    public boolean isActived() {
      return profilsListen_!=null;
    }

    public void attributeAction(Object _source, int att, GISAttributeInterface _att, int _action) {
    }

    public void attributeValueChangeAction(Object _source, int att, GISAttributeInterface _att, int _idxGeom, Object value) {
      if (_att==GISAttributeConstants.INTERSECTION_RIVE_GAUCHE)
        updateGeom(_idxGeom, rivesModified_, 0, idxAttRiveGauche_);
      else if (_att==GISAttributeConstants.INTERSECTION_RIVE_DROITE)
        updateGeom(_idxGeom, rivesModified_, 1, idxAttRiveDroite_);
      else if (_att==GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_GAUCHE)
        updateGeom(_idxGeom, limitesStockagsesModified_, 0, idxAttlsGauche_);
      else if (_att==GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_DROITE)
        updateGeom(_idxGeom, limitesStockagsesModified_, 1, idxAttlsDroite_);
      else if (_att==GISAttributeConstants.BATHY)
        for(int i=0;i<lignesDirectricesModified_.getGeomData().getNumGeometries();i++)
          updateLd(i);
      else if (_att==GISAttributeConstants.INTERSECTIONS_LIGNES_DIRECTRICES)
        /*
         * Attention subtilit� : Quand l'attribut est
         * GISAttributeConstants.INTERSECTIONS_LIGNES_DIRECTRICES c'est que
         * l'�v�nement vient de la liste des index d'intersection avec une ligne
         * directrice m�moris�e dans chaque profil. Or pour cette liste
         * l'_idxGeom est en r�alit� son index modifi�. Vu que les lignes
         * directrices sont ordonn�es dans cette attribut, l'information qu'on
         * re�oit sous le nom _idxGeom est en faite l'index de la
         * ligneDirectrice modifi�e.
         * Un cas particulier existe : quand on switch deux g�om�tries, la 
         * liste des index d'intersection de l'un est remplac� par celle de
         * l'autre. Dans ce cas ci l'attribut modifi� est bien 
         * INTERSECTIONS_LIGNES_DIRECTRICES, mais le _idxGeom est celui de la
         * g�om�trie. Le moyen de les diff�rencier est que dans ce second
         * cas value est une liste.
         */
        if(value instanceof GISAttributeModelIntegerList)
          // Mise � jour de tous les lignes directrices
          for(int i=0;i<lignesDirectricesModified_.getGeomData().getNumGeometries();i++)
            updateLd(i);
        else
          updateLd(_idxGeom);
    }

    public void geometryAction(Object _source, int _idxGeom, Geometry _geom, int _action) {
      // La g�om�trie d'un profil a �t� modifi�e, on remet a jour les g�om�tries volatiles.
      if (_action==ZModelGeometryListener.GEOMETRY_ACTION_MODIFY) {
        updateGeom(_idxGeom, rivesModified_, 0, idxAttRiveGauche_);
        updateGeom(_idxGeom, rivesModified_, 1, idxAttRiveDroite_);
        updateGeom(_idxGeom, limitesStockagsesModified_, 0, idxAttlsGauche_);
        updateGeom(_idxGeom, limitesStockagsesModified_, 1, idxAttlsDroite_);
        for (int i=0; i<lignesDirectrices_.getNombre(); i++)
          updateLd(i);
      }
    }

    /**
     * R�g�n�re toutes les g�om�tries volatiles.
     */
    private void regenerateAll() {
      GISZoneCollection zone=profilsListen_.getGeomData();
      // Pour que les coordonn�es qui sont conserv�es pour les g�om�tries volatiles soient correctement initialis�es en Z.
      zone.prepareExport();
      
      // Cr�ation des nouvelles g�om�tries \\
      // Rives
      Coordinate[] riveGauche=new Coordinate[profilsListen_.getNombre()];
      Coordinate[] riveDroite=new Coordinate[profilsListen_.getNombre()];
      for (int i=0; i<profilsListen_.getNombre(); i++) {
        riveGauche[i]=zone.getCoordinateSequence(i).getCoordinate((Integer)zone.getValue(idxAttRiveGauche_, i));
        riveDroite[i]=zone.getCoordinateSequence(i).getCoordinate((Integer)zone.getValue(idxAttRiveDroite_, i));
      }
      // Limites de stockages
      Coordinate[] lsGauche=new Coordinate[profilsListen_.getNombre()];
      Coordinate[] lsDroite=new Coordinate[profilsListen_.getNombre()];
      for (int i=0; i<profilsListen_.getNombre(); i++) {
        lsGauche[i]=zone.getCoordinateSequence(i).getCoordinate((Integer)zone.getValue(idxAttlsGauche_, i));
        lsDroite[i]=zone.getCoordinateSequence(i).getCoordinate((Integer)zone.getValue(idxAttlsDroite_, i));
      }
      // Lignes directrices
      Coordinate[][] ld=new Coordinate[lignesDirectricesModified_.getNombre()][];
      for (int i=0; i<ld.length; i++)
        ld[i]=new Coordinate[profilsListen_.getNombre()];
      for (int i=0; i<profilsListen_.getNombre(); i++) {
        GISAttributeModelIntegerList lst=(GISAttributeModelIntegerList)profilsListen_.getGeomData().getValue(
            idxAttlignesDirectrices_, i);
        for (int j=0; j<lignesDirectricesModified_.getNombre(); j++)
          ld[j][i]=profilsListen_.getGeomData().getCoordinateSequence(i).getCoordinate(lst.getValue(j));
      }

      // Ajout des nouvelles g�o�mtries \\
      // Extraction des anciennes donn�es
      Object[] dataRiveGauche=UtilsProfil1d.getData(0, rivesModified_.getGeomData());
      Object[] dataRiveDroite=UtilsProfil1d.getData(1, rivesModified_.getGeomData());
      Object[] dataLsGauche=UtilsProfil1d.getData(0, rivesModified_.getGeomData());
      Object[] dataLsDroite=UtilsProfil1d.getData(1, rivesModified_.getGeomData());
      Object[][] dataLignesDirectrices=new Object[lignesDirectricesModified_.getNombre()][];
      for (int i=0; i<lignesDirectricesModified_.getNombre(); i++)
        dataLignesDirectrices[i]=UtilsProfil1d.getData(i, lignesDirectricesModified_.getGeomData());
      // Destruction des g�om�tries
      destroyGeometries();
      // Ajout des geometries \\
      // Rives
      if (riveGauche.length>1&&riveGauche[0]!=null&&rivesModified_!=null&&riveDroite.length>1&&riveDroite[0]!=null
          &&rivesModified_!=null) {
        rivesModified_.getGeomData().addGeometry(new GISPolyligne(new GISCoordinateSequenceFactory().create(riveGauche)),
            dataRiveGauche, null);
        rivesModified_.getGeomData().addGeometry(new GISPolyligne(new GISCoordinateSequenceFactory().create(riveDroite)),
            dataRiveDroite, null);
      }
      // Limites de stockages
      if (lsGauche.length>1&&lsGauche[0]!=null&&limitesStockagsesModified_!=null&&lsDroite.length>1&&lsDroite[0]!=null
          &&limitesStockagsesModified_!=null) {
        limitesStockagsesModified_.getGeomData().addGeometry(new GISPolyligne(new GISCoordinateSequenceFactory().create(lsGauche)),
            dataLsGauche, null);
        limitesStockagsesModified_.getGeomData().addGeometry(new GISPolyligne(new GISCoordinateSequenceFactory().create(lsDroite)),
            dataLsDroite, null);
      }
      // Lignes directrices
      for (int i=0; i<ld.length; i++)
        if (ld[i].length>1)
          lignesDirectricesModified_.getGeomData().addGeometry(new GISPolyligne(new GISCoordinateSequenceFactory().create(ld[i])),
              dataLignesDirectrices[i], null);
      lignesDirectrices_.getGeomData().postImport(0);
    }

    /**
     * Met � jour la geometrie volatile indiqu�. Le param�tre pass� est l'index
     * du point � mettre � jour (qui correspond �galement � la g�om�trie
     * modifi�).
     */
    private void updateGeom(int _idxPoint, ZModeleLigneBrisee _model, int _idxGeomVolatile, int _idxAtt) {
      if (_idxGeomVolatile==-1||_idxAtt==-1||_model.getNombre()<=_idxGeomVolatile)
        return;
      CoordinateSequence seqGeomV=_model.getGeomData().getCoordinateSequence(_idxGeomVolatile);
      CoordinateSequence seqProf=profilsListen_.getGeomData().getCoordinateSequence(_idxPoint);
      Coordinate coord=seqProf.getCoordinate((Integer)profilsListen_.getGeomData().getValue(_idxAtt, _idxPoint));
      if (!UtilsProfil1d.egal(seqGeomV.getCoordinate(_idxPoint), coord)) {
        seqGeomV.setOrdinate(_idxPoint, 0, coord.x);
        seqGeomV.setOrdinate(_idxPoint, 1, coord.y);
        seqGeomV.setOrdinate(_idxPoint, 2, coord.z);
        _model.getGeomData().setCoordinateSequence(_idxGeomVolatile, seqGeomV, null);
      }
    }

    /**
     * Remet � jour les coordonn�es d'une ligne directrices depuis celles des profils.
     * si _idxGeom=-1, rien n'est fait
     * 
     * @param _idxGeom
     *          indique la ligne directrice ayant chang�.
     */
    private void updateLd(int _idxGeom) {
      if (_idxGeom==-1)
        return;
      boolean binitZ=false;
      CoordinateSequence seqLd=lignesDirectricesModified_.getGeomData().getCoordinateSequence(_idxGeom);
      for (int i=0; i<profilsListen_.getNombre(); i++) {
        GISAttributeModelIntegerList lstIdx=(GISAttributeModelIntegerList)profilsListen_.getGeomData().getValue(
            idxAttlignesDirectrices_, i);
        
        // La construction des lignes directrices se fait a partir des coordonn�es.
        profilsListen_.getGeomData().initZCoordinate(i);
        CoordinateSequence seqProfil=profilsListen_.getGeomData().getCoordinateSequence(i);
        Coordinate coodProfil=seqProfil.getCoordinate(lstIdx.getValue(_idxGeom));
        // Test si c'est bien un point � mettre � jour
        if (!UtilsProfil1d.egal(coodProfil, seqLd.getCoordinate(i))) {
          binitZ=true;
          seqLd.setOrdinate(i, 0, coodProfil.x);
          seqLd.setOrdinate(i, 1, coodProfil.y);
          seqLd.setOrdinate(i, 2, coodProfil.z);
          lignesDirectricesModified_.getGeomData().setCoordinateSequence(_idxGeom, seqLd, null);
        }
      }
      if (binitZ)
        lignesDirectricesModified_.getGeomData().initZAttribute(_idxGeom);
    }
  }

}
