/*
 * @creation     19 janv. 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.view;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.fudaa.fudaa.modeleur.modeleur1d.controller.ControllerBief;
import org.fudaa.fudaa.modeleur.modeleur1d.model.BiefContainerI;
import org.fudaa.fudaa.modeleur.modeleur1d.model.BiefContainerListener;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuCharValidator;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import org.fudaa.ebli.commun.EbliFormatterInterface;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Un panneau permettant de modifier les propri�t�s de l'axe hydraulique.
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class PnGestionAxeHydraulique extends BuPanel{

  /** Le controller des biefs. */
  protected ControllerBief controllerBief_;
  /** D�calage curviligne */
  protected BuTextField tfDecalageCurviligne_;
  /** Label d�calage abscisse curviligne */
  protected BuLabel lbDecalageCurviligne_;
  /** Le formatteur pour le d�calage curviligne. */
  protected EbliFormatterInterface formatter_;
  
  public PnGestionAxeHydraulique(ControllerBief _controllerBief, EbliFormatterInterface _formatter){
    formatter_=_formatter;
    controllerBief_=_controllerBief;
    setLayout(new BuBorderLayout());
    // Mise � jour lors du changement de bief selectionn�
    controllerBief_.getBiefContainer().addBiefContainerListener(new BiefContainerListener() {
      public void biefSelectedChanged(String name) {
        biefHasChanged();
      }
      public void biefSelectedRenamed(String name, String name2) {}
      public void profilAdded(int profil) {}
      public void profilRemoved(int profil) {}
      public void profilRenamed(int profil, String name, String name2) {}
//      @Override
//      public void profilSelectionChanged(int _oldProfil, int _idxProfil) {}
    });
    // Titre
    BuLabel lblTitre=new BuLabel("<html><b>"+MdlResource.getS("Axe hydraulique")+"</b></html>");
    lblTitre.setHorizontalAlignment(BuLabel.CENTER);
    add(lblTitre, BuBorderLayout.NORTH);
    // Body \\
    Container body=new Container();
    body.setLayout(new BuGridLayout(2, 2, 2));
    // D�calage curviligne
    body.add(lbDecalageCurviligne_=new BuLabel(MdlResource.getS("D�calage abscisse")+": "));
    tfDecalageCurviligne_=new BuTextField();
    tfDecalageCurviligne_.setCharValidator(BuCharValidator.DOUBLE);
    tfDecalageCurviligne_.addActionListener(new ActionListener(){
      public void actionPerformed(ActionEvent e) {
        try {
          double oldvalue=controllerBief_.getBiefContainer().getDecalageAxeHydraulique();
          double newvalue=Double.parseDouble(tfDecalageCurviligne_.getText());
          if (oldvalue!=newvalue)
            controllerBief_.setDebutAbscisseAxe(newvalue);
        }
        catch(NumberFormatException _ex) {}
      }
    });
    body.add(tfDecalageCurviligne_);
    add(body, BuBorderLayout.CENTER);
  }
  
  /**
   * Met a jour le panneau en fonction du bief selectionn�.
   */
  private void biefHasChanged() {
    BiefContainerI bief=controllerBief_.getBiefContainer();
    if (bief.hasAxeHydraulique()) {
      lbDecalageCurviligne_.setEnabled(true);
      tfDecalageCurviligne_.setEnabled(true);
      tfDecalageCurviligne_.setText(formatter_.getXYFormatter().format(bief.getDecalageAxeHydraulique()));
    }
    else {
      lbDecalageCurviligne_.setEnabled(false);
      tfDecalageCurviligne_.setEnabled(false);
      tfDecalageCurviligne_.setText("");
    }
  }
}
