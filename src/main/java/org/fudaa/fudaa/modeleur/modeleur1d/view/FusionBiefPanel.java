/*
 * @creation     20 janv. 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.view;

import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.SwingUtilities;

import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ebli.commun.EbliFormatterInterface;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuCharValidator;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Petite fen�tre demandant des informations suppl�mentaire � l'utilisateur
 * concernant la fusion de deux biefs.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class FusionBiefPanel extends CtuluDialogPanel {
  
  /* Donn�es de base */
  private String name1_;
  private double absCurv1_;
  private double length1_;
  private String name2_;
  private double absCurv2_;
  private double length2_;
  /* Donn�es de l'interface */
  private BuTextField tfAbsCurv1_;
  private BuTextField tfAbsCurv2_;
  private CtuluUI ui_;
  
  public FusionBiefPanel(CtuluUI _ui, EbliFormatterInterface _formater, String _name1, double _absCurv1, double _length1, String _name2, double _absCurv2, double _length2) {
    ui_=_ui;
    name1_=_name1;
    absCurv1_=_absCurv1;
    length1_=_length1;
    name2_=_name2;
    absCurv2_=_absCurv2;
    length2_=_length2;
    
    // Construction de la fen�tre \\
    setLayout(new BuBorderLayout());
    // Titres
    Container head=new Container();
    head.setLayout(new BuGridLayout(1));
    BuLabel titre1=new BuLabel(MdlResource.getS("Les biefs se superposent."));
    titre1.setHorizontalAlignment(SwingUtilities.CENTER);
    head.add(titre1);
    BuLabel titre2=new BuLabel(MdlResource.getS("D�finissez de nouvelles valeurs de d�calage d'abscisses pour corriger ce probl�me."));
    titre2.setHorizontalAlignment(SwingUtilities.CENTER);
    head.add(titre2);
    add(head, BuBorderLayout.NORTH);
    // Corps
    BuPanel body=new BuPanel();
    body.setLayout(new GridLayout(3, 2, 2, 2));
    BuLabel name1=new BuLabel(name1_);
    name1.setHorizontalAlignment(SwingUtilities.CENTER);
    body.add(name1);
    BuLabel name2=new BuLabel(name2_);
    name2.setHorizontalAlignment(SwingUtilities.CENTER);
    body.add(name2);
    tfAbsCurv1_=new BuTextField(_formater.getXYFormatter().format(absCurv1_));
    tfAbsCurv1_.setCharValidator(BuCharValidator.DOUBLE);
    tfAbsCurv2_=new BuTextField(_formater.getXYFormatter().format(absCurv2_));
    tfAbsCurv2_.setCharValidator(BuCharValidator.DOUBLE);
    body.add(tfAbsCurv1_);
    body.add(tfAbsCurv2_);
    body.add(new BuLabel(MdlResource.getS("taille : {0}", _formater.getXYFormatter().format(length1_))));
    body.add(new BuLabel(MdlResource.getS("taille : {0}", _formater.getXYFormatter().format(length2_))));
    add(body, BuBorderLayout.CENTER);
  }

  @Override
  public boolean isDataValid() {
    absCurv1_=Double.parseDouble(tfAbsCurv1_.getText());
    absCurv2_=Double.parseDouble(tfAbsCurv2_.getText());
    if(absCurv1_<=absCurv2_&&absCurv1_+length1_>absCurv2_) {
      setErrorText(MdlResource.getS("{0} est toujours chevauch� par {1}.", name1_, name2_));
      return false;
    }
    else if(absCurv2_<absCurv1_&&absCurv2_+length2_>absCurv1_) {
      setErrorText(MdlResource.getS("{0} est toujours chevauch� par {1}.", name2_, name1_));
      return false;
    }
    return true;
  }
  
  /**
   * Lance l'exporter dans une fen�tre modale.
   * 
   * @return vrai si l'utilisateur clic sur 'ok' � la fin, faux si il clic sur 'annuler'.
   */
  public boolean run(){
    return CtuluDialogPanel.isOkResponse(afficheModale(ui_.getParentComponent(), MdlResource.getS("Fusion de deux biefs")));
  }
  
  /**
   * Retourne l'abscisse curviligne de l'axe 1.
   */
  public double getAbsCurvAxe1() {
    return absCurv1_;
  }
  
  /**
   * Retourne l'abscisse curviligne de l'axe 2.
   */
  public double getAbsCurvAxe2() {
    return absCurv2_;
  }
  
}
