/*
 * @creation     20 janv. 08
 * @modification $Date: 2008-12-05 15:33:27 +0100 (ven., 05 déc. 2008) $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JComponent;
import javax.swing.JSplitPane;

import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluExportDataInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.CtuluUndoRedoInterface;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluFilleWithComponent;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.impression.EbliFilleImprimable;
import org.fudaa.fudaa.commun.save.FilleSaver;
import org.fudaa.fudaa.commun.save.FudaaSavable;
import org.fudaa.fudaa.commun.save.FudaaSaveZipLoader;
import org.fudaa.fudaa.modeleur.MdlImplementation;
import org.fudaa.fudaa.modeleur.modeleur1d.controller.Controller1d;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Query;
import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuUndoRedoInterface;
import com.memoire.fu.FuLog;
import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JPanel;
import org.fudaa.ctulu.CtuluArkSaver;
import org.fudaa.fudaa.modeleur.modeleur1d.model.BiefContainerListener;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * La fenetre interne vue 2D des donn�es du modeleur. Elle construit le composant arbre de
 * calques {@link org.fudaa.ebli.calque.BArbreCalque}. La plupart des traitements est
 * d�l�gu�e au composant {@link org.fudaa.ebli.calque.ZEbliCalquesPanel} encapsul�. 
 * 
 * @author fred deniger
 * @author bertrand marchand
 * @author Emmanuel Martin
 * @version $Id$
 */
public class MdlFille1d extends EbliFilleImprimable implements BuUndoRedoInterface, CtuluFilleWithComponent,
    CtuluUndoRedoInterface, CtuluExportDataInterface, FudaaSavable, BiefContainerListener {

  /** Le controller 1d */
  private Controller1d controller_;
  /** L'implementation */
  private MdlImplementation impl_=null;
  
  public MdlFille1d(MdlImplementation _appli) {
    super("", true, false, true, true, _appli, null);
    impl_=_appli;

    // Configuration de la fen�tre 1d \\
    setName("mdlFille1d");
    setTitle(null);
    setClosable(true);
    controller_=new Controller1d(_appli, this);
    // Construction des vues contenues dans la fen�tre. \\
    setLayout(new BuBorderLayout());
    JPanel pnTableau=new JPanel();
    pnTableau.setLayout(new BorderLayout());
    pnTableau.add(controller_.getVueTableau(),BorderLayout.CENTER);
    pnTableau.add(controller_.getVueAbsProfil(),BorderLayout.SOUTH);
    add(new JSplitPane(JSplitPane.VERTICAL_SPLIT, controller_.getVueBief(), new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, pnTableau, controller_.getVueCourbe())), BuBorderLayout.CENTER);
    add(controller_.getVueError(), BuBorderLayout.SOUTH);
    setPreferredSize(new Dimension(500, 500));
    pack();
  }

  @Override
  public void biefSelectedRenamed(String _oldName, String _newName) {
    setTitle(_newName);
  }

  @Override
  public void biefSelectedChanged(String _newName) {
    setTitle(_newName);
  }

  /** Unused */
  @Override
  public void profilRemoved(int _idxProfil) {
  }

  /** Unused */
  @Override
  public void profilRenamed(int _idxProfil, String _oldName, String _newName) {
  }

  /** Unused */
  @Override
  public void profilAdded(int _idxProfil) {
  }

  /** Unused */
//  @Override
//  public void profilSelectionChanged(int _oldProfil, int _idxProfil) {
//  }

  /* (non-Javadoc)
   * @see org.fudaa.ebli.impression.EbliPageable#print(java.awt.Graphics, java.awt.print.PageFormat, int)
   */
  public int print(Graphics _g, PageFormat _format, int _page) {
    // TODO Auto-generated method stub
    return 0;
  }

  /* (non-Javadoc)
   * @see java.awt.print.Pageable#getNumberOfPages()
   */
  public int getNumberOfPages() {
    // TODO Auto-generated method stub
    return 0;
  }

  public final String[] getEnabledActions() {
    return new String[] { CtuluExportDataInterface.EXPORT_CMD };
  }
  
  public JComponent[] getSpecificTools() {
    return controller_.getToolsActions();
  }
  
  // Gestion du container de module � droite de l'�cran \\
  
  @Override
  public JComponent createPanelComponent() {
    return controller_.getVueContainerModules();
  }

  @Override
  public Class<?> getComponentClass() {
    return controller_.getVueContainerModules().getClass();
  }

  @Override
  public String getComponentTitle() {
    return MdlResource.getS("Modules");
  }
  
  public Controller1d getController() {
    return controller_;
  }

  @Override
  public void majComponent(Object _o) {
  }

  // Gestion du undo/redo \\
  
  public void redo() {
    controller_.getCommandContainer().redo();
  }

  public void undo() {
    controller_.getCommandContainer().undo();
  }

  public void clearCmd(CtuluCommandManager _source) {
  }

  public CtuluCommandManager getCmdMng() {
    return controller_.getCommandContainer();
  }

  public void setActive(boolean _active) {    
  }

  // Gestion de l'export des donn�es \\
  
  public void startExport(CtuluUI _impl) {
    controller_.export();
  }

  /**
   * Modifie le titre pour int�grer le nom du bief courant.
   * @param _biefName Le nom du bief.
   */
  @Override
  public void setTitle(String _biefName) {
    String titre=MdlResource.getS("Vue 1D");
    if (_biefName!=null) {
      titre+=" - "+_biefName;
    }
    super.setTitle(titre);
  }

  /**
   * Recupere l'objet de la base stockant les propri�t�s pour cette fenetre.
   * @param _db La base.
   * @return Les propri�t�s pour la fenetre.
   */
  private FilleSaver findDataInDb(final ObjectContainer _db) {
    if (_db == null) {
      return null;
    }
    final Query q = _db.query();
    q.constrain(FilleSaver.class);
    q.descend("name_").constrain(getName());
    final ObjectSet set = q.execute();

    FilleSaver saveData=null;
    if (set.size() == 1) {
      saveData = (FilleSaver) set.next();
    }
    return saveData;
  }

  /* (non-Javadoc)
   * @see org.fudaa.fudaa.commun.save.FudaaSavable#saveIn(com.db4o.ObjectContainer, org.fudaa.ctulu.ProgressionInterface)
   */
  @Override
  public void saveIn(ObjectContainer _db, ProgressionInterface _prog) {
    if (_db == null) {
      return;
    }
    FilleSaver saveData=findDataInDb(_db);
    if (saveData==null)
      saveData = new FilleSaver();
    
    saveData.setStandardData(this,impl_.getMainPanel().getDesktop());
    saveData.setSpecificProps(fillSpecificProps());
    _db.set(saveData);
    _db.commit();
  }

  /* (non-Javadoc)
   * @see org.fudaa.fudaa.commun.save.FudaaSavable#saveIn(org.fudaa.fudaa.commun.save.FudaaSaveZipWriter, org.fudaa.ctulu.ProgressionInterface)
   */
  @Override
  public void saveIn(CtuluArkSaver _writer, ProgressionInterface _prog) {
    try {
      saveIn(_writer.getDb(), _prog);
    } catch (final IOException _evt) {
      FuLog.error(_evt);
    }
  }
  
  /**
   * Restaure la fenetre (donn�es et propri�t�s) depuis le fichier projet.
   * @param _loader Le fichier projet.
   * @param _prog La progression de la tache.
   */
  public void restoreFrom(FudaaSaveZipLoader _loader, ProgressionInterface _prog) {
    restoreFrom(_loader.getDb(), _prog);
  }

  /**
   * Restaure les propri�t�s de la fenetre, depuis la bd contenue dans le fichier projet.
   * @param _db La bd.
   * @param _prog La progression de la tache.
   */
  public void restoreFrom(ObjectContainer _db, ProgressionInterface _prog) {
    FilleSaver saveData=findDataInDb(_db);
    if (saveData==null) return;
    
    saveData.restoreStandardData(this,impl_.getMainPanel().getDesktop());
    // Si les propri�t�s restitu�es indiquent que la fenetre est visible, alors elle est ajout�e au desktop.
    if (isVisible()) impl_.addInternalFrame(this);
    // La restauration est faite apr�s l'ajout de la fenetre sur le Desktop. En effet, c'est l'activation
    // de la fenetre qui charge les donn�es 1d depuis le 2D.
    restoreSpecificProps(saveData.getSpecificProps());
  }
  
  /**
   * Remplit le tableau des propri�t�s sp�cifiques de cette fenetre
   * @return Les propri�t�s
   */
  private Properties fillSpecificProps() {
    Properties props=new Properties();

    // Taille des vues
    Dimension vuesize=controller_.getVueBief().getSize();
    props.setProperty("viewBief.width",Integer.toString(vuesize.width));
    props.setProperty("viewBief.height",Integer.toString(vuesize.height));
    vuesize=controller_.getVueTableau().getSize();
    props.setProperty("viewTableau.width",Integer.toString(vuesize.width));
    props.setProperty("viewTableau.height",Integer.toString(vuesize.height));
    
    // Viewport du bief
    final GrBoite b = controller_.getVueBief().getVueCalque().getViewBoite();
    props.setProperty("viewBief.xmin",Double.toString(b.o_.x_));
    props.setProperty("viewBief.ymin",Double.toString(b.o_.y_));
    props.setProperty("viewBief.xmax",Double.toString(b.e_.x_));
    props.setProperty("viewBief.ymax",Double.toString(b.e_.y_));
    
    // Bief selectionne
    int idx=controller_.getControllerBief().getBiefSelectionModel().getSelectedBief();
    props.setProperty("selectedBief",Integer.toString(idx));
    
    // Profil selectionne
    idx=controller_.getControllerProfil().getProfilSelectionModel().getSelectedProfil();
    props.setProperty("selectedProfil",Integer.toString(idx));
    
    return props;
  }
  
  /**
   * Restaure les propri�t�s sp�cifiques de la fenetre.
   * @param _props Les propri�t�s
   */
  private void restoreSpecificProps(final Properties _props) {
    if (_props==null) return;
    
//    SwingUtilities.invokeLater(new Runnable() {
//      public void run() {
        // Taille des vues
        try {
          int w=Integer.parseInt(_props.getProperty("viewBief.width"));
          int h=Integer.parseInt(_props.getProperty("viewBief.height"));
          Container c=controller_.getVueBief();
          while (c!=null && !(c instanceof JSplitPane)) c=c.getParent();
          if (c!=null)
            ((JSplitPane)c).setDividerLocation(h);
        }
        catch (NumberFormatException _exc) {}
        try {
          int w=Integer.parseInt(_props.getProperty("viewTableau.width"));
          int h=Integer.parseInt(_props.getProperty("viewTableau.height"));
          Container c=controller_.getVueTableau();
          while (c!=null && !(c instanceof JSplitPane)) c=c.getParent();
          if (c!=null)
            ((JSplitPane)c).setDividerLocation(w);
        }
        catch (NumberFormatException _exc) {}
    
        // Viewport du bief
        try {
          double xmin=Double.parseDouble(_props.getProperty("viewBief.xmin", "null"));
          double ymin=Double.parseDouble(_props.getProperty("viewBief.ymin", "null"));
          double xmax=Double.parseDouble(_props.getProperty("viewBief.xmax", "null"));
          double ymax=Double.parseDouble(_props.getProperty("viewBief.ymax", "null"));
          GrBoite bt=new GrBoite();
          bt.ajuste(xmin, ymin, 0);
          bt.ajuste(xmax, ymax, 0);
          controller_.getVueBief().getVueCalque().changeRepere(this, bt);
        }
        catch (NumberFormatException _exc) {}
        
        // Bief selectionne
        try {
          int idx=Integer.parseInt(_props.getProperty("selectedBief", "-1"));
          controller_.getControllerBief().getBiefSelectionModel().setSelectedBief(idx);
        }
        catch (NumberFormatException _exc) {}
        
        // Profil selectionne
        try {
          int idx=Integer.parseInt(_props.getProperty("selectedProfil", "-1"));
          controller_.getControllerProfil().getProfilSelectionModel().setSelectedProfil(idx);
        }
        catch (NumberFormatException _exc) {}
//      }
//    });
  }
}
