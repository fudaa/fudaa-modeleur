/*
 * @creation     11 f�vr. 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.fudaa.modeleur.modeleur1d.model.Bief;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.fudaa.fudaa.sig.layer.FSigLayerLineEditable;

/**
 * Cette classe s'occupe de g�rer l'importation de donn�es depuis le modeleur 2d
 * vers le modeleur 1d. Cette importation peut-�tre faite bief par bief. Une
 * hypoth�se est prise dans cette classe : le calque pass� en param�tre, de m�me
 * pour tous ses fils, ne sont pas modifi�s � l'ext�rieur de cette classe
 * pendant toute la dur�e de sa vie.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class BiefsBuilder {

  /** La racine de l'arbre du 2d contenant les biefs 2d. */
  private BCalque rootCalque_;
  /** La liste de nom. */
  private Map<String, Integer> names_=new HashMap<String, Integer>();
  /** Le controleur 1d pour les messages. */
  Controller1d controller1d_;

  /**
   * 
   * @param _rootCalque Le calque racine des biefs.
   */
  public BiefsBuilder(BCalque _rootCalque, Controller1d _cntrl) {
    if (_rootCalque==null)
      throw new IllegalArgumentException("Erreur prog : _rootCalque ne doit pas �tre null.");
    controller1d_=_cntrl;
    rootCalque_=_rootCalque;
    // G�n�ration des noms \\
    /*
     * Les noms g�n�r�s sont les noms donn�s dans le 2d. Si 2 biefs portent le
     * m�me nom, le nom en 2d est modifi�. Dans ce
     * cas le nom prend la forme : nuero#nom.
     */
    BCalque[] calques=rootCalque_.getCalques();
    // Extraction pour chacun des calques de son titre \\
    for (int i=0; i<calques.length; i++) {
      if(names_.containsKey(calques[i].getTitle())) {
        // Ajout d'un num�ro � la fin du nom du calque \\
        int j=2;
        while(names_.containsKey(calques[i].getTitle()+"("+j+")"))
          j++;
        calques[i].setTitle(calques[i].getTitle()+"("+j+")");
      }
      // Ajout simple du nom \\
      names_.put(calques[i].getTitle(), i);
    }
  }

  /**
   * Les noms retourn�s ne sont pas n�c�ssairement les noms donn�es dans le 2d.
   * La diff�rence apparait quand plusieurs biefs porte le m�me nom. Dans ce cas
   * le nom prend la forme : nuero#nom.
   * 
   * @return un tableau de String contenant les noms des biefs importables.
   */
  private String[] getBiefsNames() {
    return names_.keySet().toArray(new String[0]);
  }
  
  /**
   * Import le bief d�fini par _name.
   * 
   * @param _nameBief
   *          le nom du bief � importer
   * @return le bief construit.
   * @throws CancelException 
   * @throws IllegalArgumentException
   *           si _name ne correspond � aucun bief.
   */
  private Bief importBief(String _nameBief) throws CancelException {
    if (!names_.containsKey(_nameBief))
      throw new IllegalArgumentException("Erreur prog : "+_nameBief+" ne correspond a aucun bief.");
    // Extraction des modeles du bief selectionn�. \\
    List<ZModeleLigneBriseeEditable> modelsBief=new ArrayList<ZModeleLigneBriseeEditable>();
    // Iteration sur chaque calque
    BCalque[] sousCalques=rootCalque_.getCalques()[names_.get(_nameBief)].getCalques();
    for (int j=0; j<sousCalques.length; j++)
      if (sousCalques[j] instanceof FSigLayerLineEditable)
        modelsBief.add((ZModeleLigneBriseeEditable)((FSigLayerLineEditable)sousCalques[j]).modeleDonnees());
    // Importation du bief \\
    Bief bief=new BiefBuilder(_nameBief, modelsBief.toArray(new ZModeleLigneBriseeEditable[0])).getBief();
    bief.setName(_nameBief);
    return bief;
  }
  
  /**
   * Importe tous les biefs disponibles. En cas d'erreur, le bief n'est pas
   * import�, on passe au suivant.
   *
   * @return Map<String:NomDuBief, Bief:leBief>
   */
  public Map<String, Bief> importAllBiefs() {
    String[] names=getBiefsNames();
    Map<String, Bief> result=new HashMap<String, Bief>();
    for(String name: names)
      try {
        result.put(name, importBief(name));
      }
      // Erreurs de donn�es
      catch (IllegalArgumentException _exc) {
        controller1d_.getImplementation().error(MdlResource.getS("Erreur"), MdlResource.getS("Import du bief {0} depuis le 2d", name)+" :\n"+_exc.getMessage());
      }
      // Interruption de l'utilisateur
      catch (CancelException _exc) { }
    return result;
  }
}
