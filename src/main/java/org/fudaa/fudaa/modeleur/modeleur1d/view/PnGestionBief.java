/*
 * @creation     18 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.view;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import org.fudaa.fudaa.modeleur.modeleur1d.controller.ControllerBief;
import org.fudaa.fudaa.modeleur.modeleur1d.model.BiefSet;
import org.fudaa.fudaa.modeleur.modeleur1d.model.BiefSetListener;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.commun.EbliActionMap;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.fudaa.modeleur.modeleur1d.model.Bief;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Un panneau permettant de g�rer les biefs.
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class PnGestionBief extends BuPanel {
  
  /** Le controller des biefs. */
  protected ControllerBief controllerBief_;
  /** Tableau d'affichage des biefs. */
  protected JTable tableBiefs_;
  /** Bouton d'ouverture d'un bief. */
  protected BuButton btOuvrir_;
  /** bouton de fusion de bief. */
  protected BuButton btFusionner_;
  
  public PnGestionBief(ControllerBief _controllerBief){
    controllerBief_=_controllerBief;
    setLayout(new BuBorderLayout(2,2));
    // Titre
    BuLabel lblTitre=new BuLabel("<html><b>"+MdlResource.getS("Bief")+"</b></html>");
    lblTitre.setHorizontalAlignment(BuLabel.CENTER);
    add(lblTitre, BuBorderLayout.NORTH);
    // Tableau de noms
    tableBiefs_=new CtuluTable(new TableBiefModel());
    JScrollPane pnBiefs=new JScrollPane(tableBiefs_);
    pnBiefs.setPreferredSize(new Dimension(0,150));
    add(pnBiefs, BuBorderLayout.CENTER);
    // Boutons \\
    // Ouvrir
    JPanel pnButtons=new JPanel();
    pnButtons.setLayout(new BuGridLayout(1, 5, 5));
    btOuvrir_=new BuButton(MdlResource.getS("Ouvrir"));
    btOuvrir_.addActionListener(new ActionListener(){
      public void actionPerformed(ActionEvent e) {
        controllerBief_.getBiefSelectionModel().setSelectedBief(tableBiefs_.getSelectionModel().getMinSelectionIndex());
      }
    });
    tableBiefs_.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      public void valueChanged(ListSelectionEvent e) {
        ListSelectionModel sel=tableBiefs_.getSelectionModel();
        if(sel.getMaxSelectionIndex()!=sel.getMinSelectionIndex()||sel.getMaxSelectionIndex()==-1)
          btOuvrir_.setEnabled(false);
        else
          btOuvrir_.setEnabled(true);
      }
    });
    btOuvrir_.setEnabled(false);
    pnButtons.add(btOuvrir_);
    // Supprimer
    JButton btSupprimer=new BuButton(MdlResource.getS("Supprimer"));
    btSupprimer.addActionListener(new ActionListener(){
      public void actionPerformed(ActionEvent e) {
        // R�cup�ration des biefs selectionn�s
        List<Integer> idxSupp=new ArrayList<Integer>();
        for(int i=tableBiefs_.getModel().getRowCount()-1;i>=0;i--)
          if(tableBiefs_.getSelectionModel().isSelectedIndex(i))
            idxSupp.add(i);
        int[] idx=new int[idxSupp.size()];
        for(int i=0;i<idx.length;i++)
          idx[i]=idxSupp.get(i);
        // Suppression des biefs
        controllerBief_.removeBief(idx);
      }
    });
    pnButtons.add(btSupprimer);
    // Fusionner
    btFusionner_=new BuButton(MdlResource.getS("Fusionner"));
    btFusionner_.addActionListener(new ActionListener(){
      public void actionPerformed(ActionEvent e) {
        ListSelectionModel sel=tableBiefs_.getSelectionModel();
        controllerBief_.fusionnerBiefs(sel.getMinSelectionIndex(), sel.getMaxSelectionIndex());
      }
    });
    tableBiefs_.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      public void valueChanged(ListSelectionEvent e) {
        int nbSel=0;
        for(int i=tableBiefs_.getModel().getRowCount();i>=0;i--)
          if(tableBiefs_.getSelectionModel().isSelectedIndex(i))
            nbSel++;
        if(nbSel!=2)
          btFusionner_.setEnabled(false);
        else
          btFusionner_.setEnabled(true);
      }
    });
    btFusionner_.setEnabled(false);
    pnButtons.add(btFusionner_);
    // Dupliquer
    JButton btDupliquer=new BuButton(MdlResource.getS("Dupliquer"));
    btDupliquer.addActionListener(new ActionListener(){
      public void actionPerformed(ActionEvent e) {
        controllerBief_.dupliquerSelectedBief();
      }
    });
    btDupliquer.setEnabled(false);
    btDupliquer.setVisible(false);
    pnButtons.add(btDupliquer);

    // Importer
    pnButtons.add(EbliActionMap.getInstance().getAction("IMPORTER_BIEF").buildButton(EbliComponentFactory.INSTANCE));
    
    add(pnButtons, BuBorderLayout.EAST);
  }

  /**
   * Surcharge du model du tableau pour prendre les informations directement dans le BiefSet.
   */
  private class TableBiefModel extends DefaultTableModel implements BiefSetListener {

    /** Raccourcis vers le biefSet. */
    private BiefSet biefSet_;
    
    public TableBiefModel() {
      super(new String[]{MdlResource.getS("Nom")}, 0);
      biefSet_=controllerBief_.getBiefSet();
      biefSet_.addBiefSetListener(this);
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
      return String.class;
    }

    @Override
    public int getRowCount() {
      if(biefSet_==null)
        return 0;
      return biefSet_.getNbBief();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      return biefSet_.getBiefName(rowIndex);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      return true;
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
      if(!getValueAt(rowIndex, columnIndex).equals(value))
        controllerBief_.renameBief((String) getValueAt(rowIndex, columnIndex), (String) value);
    }

    public void addedBief(Bief _bief, String name) {
      fireTableDataChanged();
    }

    public void removedBief(Bief _bief, String name) {
      fireTableDataChanged();
    }

    public void renamedBief(Bief _bief, String name, String name2) {
      fireTableDataChanged();
    }
    
  }
}
