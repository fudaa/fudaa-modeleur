package org.fudaa.fudaa.modeleur.modeleur1d;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;

import org.fudaa.fudaa.modeleur.MdlPreferences;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.jdesktop.swingx.VerticalLayout;

import com.memoire.bu.BuAbstractPreferencesComponent;
import com.memoire.bu.BuLabel;

/**
 * Un composant pour les pr�f�rences 1d du modeleur.
 * @author marchand@deltacad.fr
 */
public class Mdl1dPreferencesComponent extends BuAbstractPreferencesComponent {
  public static final String PROFILS_AUTOMATIC_REORIENTATION="mdl.1d.profils.automaticreorientation";
  private JCheckBox cbReorient_;

  public Mdl1dPreferencesComponent() {
    super(MdlPreferences.MDL);
    
    cbReorient_=new JCheckBox(MdlResource.getS("R�orientation automatique des profils"));
    BuLabel lbHelp=new BuLabel(MdlResource.getS("<html>La r�orientation tient compte de l'ordre des profils suivant leur abscisse et de la pr�sence ou non d'un axe hydraulique</html>"));
    lbHelp.setForeground(MdlResource.HELP_FORGROUND_COLOR);
    lbHelp.setFont(MdlResource.HELP_FONT);
    setLayout(new VerticalLayout(2));
    add(cbReorient_);
    add(lbHelp);
    
    updateComponent();
    
    cbReorient_.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        setSavabled(true);
        setModified(true);
      }
    });
  }
  
  @Override
  public String getTitle() {
    return MdlResource.getS("Synchronisation 1D");
  }
  
  @Override
  public boolean isPreferencesValidable() {
    return true;
  }
  
  @Override
  public boolean isPreferencesCancelable() {
    return true;
  }
  
  /**
   * Mise a jour des composants � partir des info du fichier.
   */
  @Override
  protected void updateComponent() {
    boolean breorient=options_.getBooleanProperty(PROFILS_AUTOMATIC_REORIENTATION, true);
    cbReorient_.setSelected(breorient);
  }
  
  /**
   * Remplit les propri�t�s a partir des valeurs des composants.
   */
  @Override
  protected void updateProperties() {
    options_.putBooleanProperty(PROFILS_AUTOMATIC_REORIENTATION,cbReorient_.isSelected());
  }
}
