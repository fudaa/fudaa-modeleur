/*
 * @creation     22 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.controller;

import java.util.Arrays;
import java.util.Map;

import javax.swing.DefaultListSelectionModel;
import javax.swing.event.ListSelectionListener;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.FastBitSet;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISCoordinateSequenceFactory;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.fudaa.modeleur.modeleur1d.model.Bief;
import org.fudaa.fudaa.modeleur.modeleur1d.model.BiefContainerAdapter;
import org.fudaa.fudaa.modeleur.modeleur1d.model.BiefSet;
import org.fudaa.fudaa.modeleur.modeleur1d.model.BiefSetListener;
import org.fudaa.fudaa.modeleur.modeleur1d.model.UtilsBief1d;
import org.fudaa.fudaa.modeleur.modeleur1d.model.UtilsProfil1d;
import org.fudaa.fudaa.modeleur.modeleur1d.view.FusionBiefPanel;
import org.fudaa.fudaa.modeleur.modeleur1d.view.PnGestionBief;
import org.fudaa.fudaa.modeleur.modeleur1d.view.VueBief;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;

/**
 * Controller permettant de g�rer (cr�er, modifier, supprimer, selectionner) les biefs.
 * Le controlleur bief sert aussi a synchroniser la partie 2d pour les op�rations
 * faites sur le 1d.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class ControllerBief implements BiefSetListener {

  /** Le controller principal. */
  protected Controller1d controller1d_;
  /** Le nom du bief selectionn�. */
  protected String nomSelectedBief_=null;
  /** l'ensemble de biefs. */
  protected BiefSet biefSet_;
  /** Le container de bief. */
  BiefContainerAdapter biefContainerAdapter_;
  /** Le model de selection des biefs. */
  protected BiefSelectionModel biefSelectionModel_;
  /** La vue du module de gestion des biefs. */
  protected PnGestionBief vueModuleGestionBief_;
  /** La vue sur les biefs. */
  protected VueBief vueBief_;
  /** La synchronisation se fait depuis le 2d -> 1d : Pas de modifications des biefs 2d. */
  protected boolean bsyncFrom2d_=false;
  
  public ControllerBief(Controller1d _controller1d) {
    biefSet_=new BiefSet();
    biefSet_.addBiefSetListener(this);
    
    controller1d_=_controller1d;
    biefContainerAdapter_=new BiefContainerAdapter(biefSet_);
    biefSelectionModel_=new BiefSelectionModel();
    vueModuleGestionBief_=new PnGestionBief(this);
    vueBief_=new VueBief(controller1d_.getImplementation(), this);
  }
  
  /**
   * Se r�g�n�re par rapport aux 2d.
   */
  public void updateFrom2d(){
    try {
      bsyncFrom2d_=true;
      removeAllBiefs(false);
      importBiefsFrom2d(controller1d_.getImplementation().get2dFrame().getVisuPanel().getArbreCalqueModel().getRootCalque().getCalqueParNom("gcBiefs"));
    }
    catch(IllegalArgumentException _exp) {
      controller1d_.getImplementation().error(MdlResource.getS("Erreur dans l'import depuis le 2d"), _exp.getMessage());
    }
    finally {
      bsyncFrom2d_=false;
    }
  }
  
  /**
   * D�sactive tous les synchroniseurs.
   */
  public void disableSynchronisers() {
    String[] names=biefSet_.getBiefNames();
    for(int i=0;i<names.length;i++)
      biefSet_.getBief(names[i]).disableSynchroniser();
  }

  /**
   * Decale les abscisses des profils du bief en fonction de l'abscisse donn� pour le d�but de l'axe hydraulique
   */
  public void setDebutAbscisseAxe(double _abs) {
    if (!getBiefContainer().hasAxeHydraulique()) return;
    double decal=_abs-getBiefContainer().getDecalageAxeHydraulique();
    if (decal==0) return;

    CtuluCommandComposite cmp=new CtuluCommandComposite(MdlResource.getS("Modifier abscisse curviligne de l'axe"));

    // Changement de l'attribut PK sur axe.
    GISZoneCollection zaxe=getBiefContainer().getZoneAxeHydraulique();
    if (zaxe.getNumGeometries()>0) {
      int idxAttr=zaxe.getIndiceOf(GISAttributeConstants.CURVILIGNE_DECALAGE);
      zaxe.setAttributValue(idxAttr, 0, _abs, cmp);
    }

    // D�calage des profils
    GISZoneCollectionLigneBrisee zone=getBiefContainer().getZoneProfils();
    for (int i=0; i<zone.getNbGeometries(); i++) {
      String value=(String)zone.getValue(zone.getIndiceOf(GISAttributeConstants.COMMENTAIRE_HYDRO), i);
      double oldval=GISLib.getHydroCommentDouble(value, GISAttributeConstants.ATT_COMM_HYDRO_PK);
      String s=GISLib.setHydroCommentDouble(value, oldval+decal, GISAttributeConstants.ATT_COMM_HYDRO_PK);
      zone.setAttributValue(zone.getIndiceOf(GISAttributeConstants.COMMENTAIRE_HYDRO), i, s, cmp);
    }

    if (getCommandManager()!=null)
      getCommandManager().addCmd(cmp.getSimplify());
  }
  
  /**
   * R�ordonne les profils du bief selectionn�. retourne le nouvel indice de
   * l'indice pass� en param�tre (-1 si inutilis�).
   */
  public int orderProfils(int _idx, CtuluCommandContainer _cmd) {
    return UtilsBief1d.orderProfils(biefSet_.getBief(biefSelectionModel_.getSelectedName()).profils_, _idx, _cmd);
  }
  
  /**
   * Importe les biefs du 2d vers le 1d.
   */
  private void importBiefsFrom2d(BCalque _rootCalque) {
    if (_rootCalque==null)
      return;
    int selectedValue=biefSelectionModel_.getMinSelectionIndex();
    // Suppression des biefs pr�c�dents
    biefSet_.removeAllBiefs(null);
    // Importation
    Map<String, Bief> biefs=new BiefsBuilder(_rootCalque, getController1d()).importAllBiefs();
    for(Map.Entry<String, Bief> entry:biefs.entrySet())
      biefSet_.addBief(entry.getKey(), entry.getValue(), null);
    // Ajout d'une selection
    if(biefSet_.getNbBief()>selectedValue&&selectedValue!=-1)
      biefSelectionModel_.addSelectionInterval(selectedValue, selectedValue);
    else if(biefSet_.getNbBief()>0&&selectedValue==-1)
      biefSelectionModel_.addSelectionInterval(0, 0);
  }
  
  /**
   * Retourne le bief container.
   */
  public BiefContainerAdapter getBiefContainer(){
    return biefContainerAdapter_;
  }
  
  /**
   * Retourne le Commande manager utilis� dans la partie 1d
   */
  public CtuluCommandContainer getCommandManager(){
    return controller1d_.getCommandContainer();
  }
  
  /**
   * Retourne la vue du module de gestion des biefs.
   */
  public PnGestionBief getVueModuleGestionBief(){
    return vueModuleGestionBief_;
  }
  
  /**
   * Retourne la vue sur les biefs
   */
  public VueBief getVueBief() {
    return vueBief_;
  }
  
  /**
   * Supprime les biefs donn�es en param�tre.
   * Par defaut avec undoRedo.
   */
  public void removeBief(int[] _idx){
    removeBief(_idx, true);
  }
  
  /**
   * Supprime les biefs donn�es en param�tre.
   *  Si _undoRedo est � vrai, l'action pourra �tre annul�e.
   */
  public void removeBief(int[] _idx, boolean _undoRedo){
    Arrays.sort(_idx);
    CtuluCommandComposite cmd=new CtuluCommandComposite(MdlResource.getS("Suppression de biefs"));
    for(int i=_idx.length-1;i>=0;i--) {
      if(biefSelectionModel_.getSelectedBief()==_idx[i])
        biefSelectionModel_.clearSelection();
      biefSet_.removeBief(biefSet_.getBiefName(_idx[i]), cmd);
    }
    if(_undoRedo)
      getCommandManager().addCmd(cmd.getSimplify());
  }
  
  /**
   * Supprime tous les biefs.
   * Par defaut avec undoRedo.
   */
  public void removeAllBiefs() {
    removeAllBiefs(true);
  }
  
  /**
   * Supprime tous les biefs. Si _undoRedo est � vrai, l'action pourra �tre annul�e.
   */
  public void removeAllBiefs(boolean _undoRedo) {
    int[] idx=new int[biefSet_.getNbBief()];
    for(int i=0;i<idx.length;i++)
      idx[i]=i;
    removeBief(idx, _undoRedo);
  }
  
  /**
   * Supprime le bief selectionn�.
   */
  public void removeSelectedBief() {
    int idxBief=biefSelectionModel_.getSelectedBief();
    if(idxBief!=-1) {
      biefSelectionModel_.clearSelection();
      biefSet_.removeBief(biefSet_.getBiefName(idxBief), getCommandManager());
    }
  }

  public Controller1d getController1d() {
    return controller1d_;
  }
  /**
   * Fusion de deux biefs.
   */
  public void fusionnerBiefs(int _idxBief1, int _idxBief2) {
    try {
      // D�termine quel bief est le premier et lequel est le second
      // (curvilignement parlant)

      // Pr�sence ou non d'un axe hydro.
      boolean hasAxe=false;
      // Le decalage du debut des axes.
      double decalBief1;
      double decalBief2;
      // Les offset de d�calage pour les biefs s'ils ont �t� modifi�s par
      // l'utilisateur.
      double offsetBief1=0;
      double offsetBief2=0;
      // Longueurs de chaque axe (ou pk fin - pk debut si le bief n'a pas d'axe)
      double longBief1;
      double longBief2;

      String name1=biefSet_.getBiefName(_idxBief1);
      String name2=biefSet_.getBiefName(_idxBief2);
      Bief bief1=biefSet_.getBief(name1);
      Bief bief2=biefSet_.getBief(name2);
      GISZoneCollection zone1=biefSet_.getBief(name1).profils_.getGeomData();
      GISZoneCollection zone2=biefSet_.getBief(name2).profils_.getGeomData();

      // Verifications \\
      
      if (bief1==null||bief2==null)
        // Erreur prog
        throw new IllegalArgumentException("Erreur prog : Au moins un des index ne correspond pas � un bief.");
      if (bief1.lignesDirectrices_.getNombre()!=bief2.lignesDirectrices_.getNombre())
        throw new IllegalArgumentException(MdlResource.getS("Il doit y avoir le m�me nombre de lignes directrices dans les deux biefs."));
      if (bief1.axeHydraulique_.getNombre()!=bief2.axeHydraulique_.getNombre())
        throw new IllegalArgumentException(MdlResource.getS("Les 2 biefs doivent avoir la m�me pr�sence d'axe hydraulique."));

      GISZoneCollection zoneAxeHydrau1=bief1.axeHydraulique_.getGeomData();
      GISZoneCollection zoneAxeHydrau2=bief2.axeHydraulique_.getGeomData();
      hasAxe=zoneAxeHydrau1.getNumGeometries()>0;

      // R�cup�ration du d�calage d'abscisse \\

      if (hasAxe) {
        decalBief1=(Double)zoneAxeHydrau1.getValue(zoneAxeHydrau1.getIndiceOf(GISAttributeConstants.CURVILIGNE_DECALAGE), 0);
        decalBief2=(Double)zoneAxeHydrau2.getValue(zoneAxeHydrau2.getIndiceOf(GISAttributeConstants.CURVILIGNE_DECALAGE), 0);
      }
      else {
        decalBief1=GISLib.getHydroCommentDouble((String)zone1.getValue(zone1.getIndiceOf(GISAttributeConstants.COMMENTAIRE_HYDRO),
            0), GISAttributeConstants.ATT_COMM_HYDRO_PK);
        decalBief2=GISLib.getHydroCommentDouble((String)zone2.getValue(zone2.getIndiceOf(GISAttributeConstants.COMMENTAIRE_HYDRO),
            0), GISAttributeConstants.ATT_COMM_HYDRO_PK);
      }

      // R�cup�ration de la longueur des biefs \\

      if (hasAxe) {
        CoordinateSequence seqAxe1=biefSet_.getBief(name1).axeHydraulique_.getGeomData().getCoordinateSequence(0);
        longBief1=UtilsProfil1d.abscisseCurviligne(seqAxe1, seqAxe1.getCoordinate(seqAxe1.size()-1));
        CoordinateSequence seqAxe2=biefSet_.getBief(name2).axeHydraulique_.getGeomData().getCoordinateSequence(0);
        longBief2=UtilsProfil1d.abscisseCurviligne(seqAxe2, seqAxe2.getCoordinate(seqAxe2.size()-1));
      }

      // Pas d'axe hydro => On calcule les longueurs a partir des PK profils.
      else {
        longBief1=GISLib.getHydroCommentDouble((String)zone1.getValue(zone1.getIndiceOf(GISAttributeConstants.COMMENTAIRE_HYDRO),
            zone1.getNumGeometries()-1), GISAttributeConstants.ATT_COMM_HYDRO_PK)
            -decalBief1;
        longBief2=GISLib.getHydroCommentDouble((String)zone2.getValue(zone2.getIndiceOf(GISAttributeConstants.COMMENTAIRE_HYDRO),
            zone2.getNumGeometries()-1), GISAttributeConstants.ATT_COMM_HYDRO_PK)
            -decalBief2;
      }

      // Inversion des biefs si n�c�ssaire
      if (decalBief1>decalBief2) {
        int idxTmp=_idxBief1;
        _idxBief1=_idxBief2;
        _idxBief2=idxTmp;
        double valTmp=decalBief1;
        decalBief1=decalBief2;
        decalBief2=valTmp;

        name1=biefSet_.getBiefName(_idxBief1);
        name2=biefSet_.getBiefName(_idxBief2);
        bief1=biefSet_.getBief(name1);
        bief2=biefSet_.getBief(name2);
        zone1=bief1.profils_.getGeomData();
        zone2=bief2.profils_.getGeomData();
        zoneAxeHydrau1=bief1.axeHydraulique_.getGeomData();
        zoneAxeHydrau2=bief2.axeHydraulique_.getGeomData();
      }

      // Superposition des biefs => On demande un d�calage a l'utilisateur.
      if (decalBief1+longBief1>decalBief2) {
        FusionBiefPanel vueFusionBief=new FusionBiefPanel(controller1d_.getImplementation(),controller1d_.getFormater(), name1, decalBief1, longBief1, name2, decalBief2,
            longBief2);
        if (!vueFusionBief.run())
          return;

        offsetBief1=vueFusionBief.getAbsCurvAxe1()-decalBief1;
        offsetBief2=vueFusionBief.getAbsCurvAxe2()-decalBief2;
      }

      // Inversion des biefs si n�c�ssaire
      if (decalBief1+offsetBief1>decalBief2+offsetBief2) {
        int idxTmp=_idxBief1;
        _idxBief1=_idxBief2;
        _idxBief2=idxTmp;
        double valTmp=decalBief1;
        decalBief1=decalBief2;
        decalBief2=valTmp;
        valTmp=offsetBief1;
        offsetBief1=offsetBief2;
        offsetBief2=valTmp;

        name1=biefSet_.getBiefName(_idxBief1);
        name2=biefSet_.getBiefName(_idxBief2);
        bief1=biefSet_.getBief(name1);
        bief2=biefSet_.getBief(name2);
        zone1=bief1.profils_.getGeomData();
        zone2=bief2.profils_.getGeomData();
        zoneAxeHydrau1=bief1.axeHydraulique_.getGeomData();
        zoneAxeHydrau2=bief2.axeHydraulique_.getGeomData();
      }

      // Extraction/Cr�ation des axes hydrauliques  \\
        
      CoordinateSequence seqAxeHydraulique1;
      CoordinateSequence seqAxeHydraulique2;
      boolean fakeAH=false;

      // On recupere les axes.
      if (hasAxe) {
        seqAxeHydraulique1=zoneAxeHydrau1.getCoordinateSequence(0);
        seqAxeHydraulique2=zoneAxeHydrau2.getCoordinateSequence(0);
      }

      // Pas d'axe hydraulique => Construction de 2 faux axes hydrauliques.
      else {
        fakeAH=true;

        if (bief1.profils_.getNombre()>=2) {
          CoordinateSequence prof1=bief1.profils_.getGeomData().getCoordinateSequence(0);
          CoordinateSequence prof2=bief1.profils_.getGeomData().getCoordinateSequence(bief1.profils_.getNombre()-1);
          seqAxeHydraulique1=new GISCoordinateSequenceFactory().create(new Coordinate[]{prof1.getCoordinate(prof1.size()/2),
              prof2.getCoordinate(prof2.size()/2)});
        }
        else if (bief1.profils_.getNombre()==1) {
          CoordinateSequence prof1=bief1.profils_.getGeomData().getCoordinateSequence(0);
          Coordinate coord1=prof1.getCoordinate(prof1.size()/2);
          Coordinate coord2=new Coordinate(coord1.x-10, coord1.y-1, 0);
          seqAxeHydraulique1=new GISCoordinateSequenceFactory().create(new Coordinate[]{coord1, coord2});
        }
        else
          return;
        
        if (bief2.profils_.getNombre()>=2) {
          CoordinateSequence prof1=bief2.profils_.getGeomData().getCoordinateSequence(0);
          CoordinateSequence prof2=bief2.profils_.getGeomData().getCoordinateSequence(bief2.profils_.getNombre()-1);
          seqAxeHydraulique2=new GISCoordinateSequenceFactory().create(new Coordinate[]{prof1.getCoordinate(prof1.size()/2),
              prof2.getCoordinate(prof2.size()/2)});
        }
        else if (bief2.profils_.getNombre()==1) {
          CoordinateSequence prof1=bief2.profils_.getGeomData().getCoordinateSequence(0);
          Coordinate coord1=prof1.getCoordinate(prof1.size()/2);
          Coordinate coord2=new Coordinate(coord1.x-10, coord1.y-1, 0);
          seqAxeHydraulique2=new GISCoordinateSequenceFactory().create(new Coordinate[]{coord1, coord2});
        }
        else
          return;
      }
      

      Coordinate debutAxe2=UtilsProfil1d.getCoordinateXY(seqAxeHydraulique1, (decalBief2+offsetBief2)-(decalBief1+offsetBief1));
      Coordinate move=UtilsProfil1d.vec(seqAxeHydraulique2.getCoordinate(0), debutAxe2);

      // Construction du nouveau bief
      Bief newBief=new Bief();
      String biefName=name1+"_"+name2;
      int k=1;
      while (biefSet_.getBief(biefName)!=null)
        biefName=name1+"_"+name2+"("+Integer.toString(++k)+")";
      newBief.setName(biefName);

      // Racourcis
      GISZoneCollection zoneAxeHydraulique=newBief.axeHydraulique_.getGeomData();
      GISZoneCollection zoneProfils=newBief.profils_.getGeomData();
      GISZoneCollection bief1ZoneProfil=bief1.profils_.getGeomData();
      GISZoneCollection bief2ZoneProfil=bief2.profils_.getGeomData();

      // Ajout de g�om�tries au nouveau bief \\

      // Ajout des profils du bief 1, en d�calant eventuellement les PK.
      int idxAttrComm=zoneProfils.getIndiceOf(GISAttributeConstants.COMMENTAIRE_HYDRO);
      for (int i=0; i<bief1.profils_.getNombre(); i++) {
        int idxGeom=zoneProfils.addGeometry(bief1ZoneProfil.getGeometry(i), UtilsProfil1d.getData(i, bief1ZoneProfil), null);
          String comm=(String)zoneProfils.getValue(idxAttrComm, idxGeom);
          double newPK=GISLib.getHydroCommentDouble(comm, GISAttributeConstants.ATT_COMM_HYDRO_PK)+offsetBief1;
          String newValue=GISLib.setHydroCommentDouble(comm, newPK, GISAttributeConstants.ATT_COMM_HYDRO_PK);
          zoneProfils.setAttributValue(idxAttrComm, idxGeom, newValue, null);
      }
      // Ajout des profil du bief 2, en d�calant eventuellement les PK.
      for (int i=0; i<bief2.profils_.getNombre(); i++) {
        int idxGeom=zoneProfils.addGeometry(bief2ZoneProfil.getGeometry(i), UtilsProfil1d.getData(i, bief2ZoneProfil), null);
          String comm=(String)zoneProfils.getValue(idxAttrComm, idxGeom);
          double newPK=GISLib.getHydroCommentDouble(comm, GISAttributeConstants.ATT_COMM_HYDRO_PK)+offsetBief2;
          String newValue=GISLib.setHydroCommentDouble(comm, newPK, GISAttributeConstants.ATT_COMM_HYDRO_PK);
          zoneProfils.setAttributValue(idxAttrComm, idxGeom, newValue, null);
      }
      
      // Ajout de l'axe hydraulique du second bief (utile pour la translation)
      if (!fakeAH)
        zoneAxeHydraulique.addGeometry(zoneAxeHydrau2.getGeometry(0), UtilsProfil1d.getData(0, zoneAxeHydrau2), null);

      // Translation du second bief \\
      // Cr�ation d'une selection contenant le second axe hydraulique
      if (!fakeAH) {
        FastBitSet bs=new FastBitSet(1);
        bs.set(0);
        CtuluListSelection selection=new CtuluListSelection(bs);
        // Application de la translation sur le second axe hydraulique
        ((ZModeleLigneBriseeEditable)newBief.axeHydraulique_).moveGlobal(selection, move.x, move.y, 0, null);
      }
      // Cr�ation d'une selection contenant les profils du second axe
      // hydraulique
      FastBitSet bs=new FastBitSet(zoneProfils.getNbGeometries());
      bs.set(bief1.profils_.getNombre(), zoneProfils.getNbGeometries());
      CtuluListSelection selection=new CtuluListSelection(bs);
      // Application de la translation sur les profils du second axe hydraulique
      ((ZModeleLigneBriseeEditable)newBief.profils_).moveGlobal(selection, move.x, move.y, 0, null);

      // Fusion des deux axes Hydrauliques \\
      if (!fakeAH) {
        String newTitre=agregeTitres(0, zoneAxeHydrau1, zoneAxeHydrau2, -1, -1);
        seqAxeHydraulique2=zoneAxeHydraulique.getCoordinateSequence(0);
        Coordinate[] coords=new Coordinate[seqAxeHydraulique1.size()+seqAxeHydraulique2.size()];
        int i=0;
        for (; i<seqAxeHydraulique1.size(); i++)
          coords[i]=seqAxeHydraulique1.getCoordinate(i);
        for (; i<seqAxeHydraulique2.size()+seqAxeHydraulique1.size(); i++)
          coords[i]=seqAxeHydraulique2.getCoordinate(i-seqAxeHydraulique1.size());
        zoneAxeHydraulique.addGeometry(new GISPolyligne(new GISCoordinateSequenceFactory().create(coords)), null, null);
        zoneAxeHydraulique.setAttributValue(zoneAxeHydraulique.getIndiceOf(GISAttributeConstants.TITRE), 1, newTitre, null);
        zoneAxeHydraulique.setAttributValue(zoneAxeHydrau1.getIndiceOf(GISAttributeConstants.CURVILIGNE_DECALAGE), 1,
            decalBief1, null);
        // suppression de l'axe hydraulique 2 du nouveau bief
        zoneAxeHydraulique.removeGeometries(new int[]{0}, null);
      }

      // Traitement des g�om�tries volatiles \\
      // Rives
      calculateData(bief1.rives_.getGeomData(), bief2.rives_.getGeomData(), newBief.rives_.getGeomData());
      // Limite de stockage
      calculateData(bief1.limitesStockages_.getGeomData(), bief2.limitesStockages_.getGeomData(), newBief.limitesStockages_
          .getGeomData());
      // Lignes directrices
      calculateData(bief1.lignesDirectrices_.getGeomData(), bief2.lignesDirectrices_.getGeomData(), newBief.lignesDirectrices_
          .getGeomData());

      // Ajout du nouveau bief \\
      newBief.enableSynchroniser();
      biefSet_.addBief(biefName, newBief, getCommandManager());
    }
    catch (IllegalArgumentException _e) {
      controller1d_.getImplementation().error(MdlResource.getS("Erreur lors de la fusion"), _e.getMessage());
    }
  }

  /**
   * Extrait les valeurs de titre des g�om�tries et en cr�e une nouvelle
   * g�om�trie avec ce titre dans _zone3. La nouvelle g�om�trie n'a pas de
   * sommets.
   */
  private void calculateData(GISZoneCollection _zone1, GISZoneCollection _zone2, GISZoneCollection _zone3) {
    int idxAttrZone1=_zone1.getIndiceOf(GISAttributeConstants.TITRE);
    int idxAttrZone2=_zone2.getIndiceOf(GISAttributeConstants.TITRE);
    int idxAttrZone3=_zone3.getIndiceOf(GISAttributeConstants.TITRE);
    for(int i=0;i<_zone1.getNbGeometries();i++) {
      String newTitre=agregeTitres(i, _zone1, _zone2, idxAttrZone1, idxAttrZone2);
      // Cr�ation de la gom�trie (elle sera correctement g�n�r� par le synchronizer du bief)
      int idxGeom=_zone3.addGeometry(new GISPolyligne(), null, null);
      // Valuation de l'attribut nom
      _zone3.setAttributValue(idxAttrZone3, idxGeom, newTitre, null);
    }
  }
  
  /**
   * Agregation de deux titres.
   * Si les _idxAttrX sont � -1 il seront d�termin� par la m�thode.
   */
  private String agregeTitres(int _idxGeom, GISZoneCollection _zone1, GISZoneCollection _zone2, int _idxAttr1, int _idxAttr2) {
    if(_idxAttr1==-1)
      _idxAttr1=_zone1.getIndiceOf(GISAttributeConstants.TITRE);
    if(_idxAttr2==-1)
      _idxAttr2=_zone1.getIndiceOf(GISAttributeConstants.TITRE);
    String titre1=(String) _zone1.getValue(_idxAttr1, _idxGeom);
    String titre2=(String) _zone2.getValue(_idxAttr2, _idxGeom);
    String newTitre;
    // Calcule du nouveau nom
    if(titre1.equals(titre2))
      newTitre=titre1;
    else
      newTitre=titre1+'_'+titre2;
    return newTitre;
  }
  
  /**
   * Duplique le bief selectionn�.
   */
  public void dupliquerSelectedBief() {
    //TODO
  }
  
  /**
   * Renomme un bief.
   */
  public void renameBief(String _currentName, String _newName) {
    biefSet_.renameBief(_currentName, _newName, controller1d_.getCommandContainer()); 
  }
  
  // Gestion de la selection de bief \\
  
  /**
   * Retourne vrai si un bief est selectionn�.
   */
  public boolean isBiefSelected() {
    return biefSelectionModel_.getSelectedName()!=null;
  }
  
  /**
   * Retourne le nom du bief selectionn�.
   */
  public String getSelectedBiefName(){
    return biefSelectionModel_.getSelectedName();
  }

  /**
   * Retourne une ListSelectionModel permettant de g�rer la selection des biefs.
   */
  public BiefSelectionModel getBiefSelectionModel() {
    return biefSelectionModel_;
  }

  /**
   * Retourne le biefContainer, c'est � dire le conteneur de tous les biefs.
   * Attention, pour la suppression de biefs, mieux vaut passer par le
   * controller, sinon des incoh�rences avec la selection peuvent apparaitre.
   */
  public BiefSet getBiefSet(){
    return biefSet_;
  }
  
  /**
   * Retourne le Bief selectionn�.
   */
  public Bief getSelectedBief(){
    return biefSet_.getBief(getSelectedBiefName());
  }

  // Synchronisation du 2D
  
  public void addedBief(Bief _bief, String _newName) {
    if (!bsyncFrom2d_)
      controller1d_.getImplementation().get2dFrame().getMdlVisuPanel().buildBiefLayers(_bief, true);
  }

  public void renamedBief(Bief _bief, String _oldName, String _newName) {
      controller1d_.getImplementation().get2dFrame().getMdlVisuPanel().renameBief(_bief, _oldName, _newName);
  }

  public void removedBief(Bief _bief, String _oldName) {
    if (!bsyncFrom2d_)
      controller1d_.getImplementation().get2dFrame().getMdlVisuPanel().removeBief(_bief);
  }
  
  // Le model de selection concernant les biefs \\

  /**
   * Cette classe a pour objectif de g�rer avec le m�me model de selection tous
   * les endroits ayant besoin de savoir quel bief est selectionn�.
   */
  public class BiefSelectionModel extends DefaultListSelectionModel {

    public BiefSelectionModel(){
      super.setSelectionMode(SINGLE_SELECTION);
    }

    /**
     * Retourne le nom du bief selectionn�.
     */
    public String getSelectedName() {
      if(getMinSelectionIndex()==-1)
        return null;
      return biefSet_.getBiefName(getMinSelectionIndex());
    }
    
    /**
     * Retourne le bief selectionn�.
     */
    public int getSelectedBief() {
      return getMinSelectionIndex();
    }
    
    /**
     * Selectionne un bief d'indice donn�.
     */
    public void setSelectedBief(int _idx) {
      setSelectionInterval(_idx,_idx);
    }
        
    /**
     * Met � jour dataGeomAdapter_ pour qu'il tienne compte de la nouvelle selection.
     */
    private void updateBief() {
      if(getMinSelectionIndex()==-1)
        biefContainerAdapter_.setSelectedBief(null);
      else
        biefContainerAdapter_.setSelectedBief(biefSet_.getBiefName(getMinSelectionIndex()));
    }
    
    public void addListSelectionListener(ListSelectionListener x) {
      super.addListSelectionListener(x);
      updateBief();
    }

    public void addSelectionInterval(int index0, int index1) {
      super.addSelectionInterval(index0, index1);
      updateBief();
    }

    public void clearSelection() {
      super.clearSelection();
      updateBief();
    }

    public void insertIndexInterval(int index, int length, boolean before) {
      super.insertIndexInterval(index, length, before);
      updateBief();
    }

    public void removeIndexInterval(int index0, int index1) {
      super.removeIndexInterval(index0, index1);
      updateBief();
    }

    public void removeSelectionInterval(int index0, int index1) {
      super.removeSelectionInterval(index0, index1);
      updateBief();
    }

    public void setAnchorSelectionIndex(int index) {
      super.setAnchorSelectionIndex(index);
      updateBief();
    }

    public void setLeadSelectionIndex(int index) {
      super.setLeadSelectionIndex(index);
      updateBief();
    }

    public void setSelectionInterval(int index0, int index1) {
      super.setSelectionInterval(index0, index1);
      updateBief();
    }
    
    /**
     * Le mode de selection est bloqu� � SINGLE_SELECTION.
     */
    public void setSelectionMode(int selectionMode) {
      if(selectionMode!=SINGLE_SELECTION)
        throw new IllegalArgumentException("Erreur prog : Le mode est bloqu� � SINGLE_SELECTION");
    }
  }
}
