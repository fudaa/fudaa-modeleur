/*
 * @creation     8 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.view;

import java.awt.Dimension;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.edition.ZEditorDefault;
import org.fudaa.fudaa.modeleur.MdlImplementation;
import org.fudaa.fudaa.modeleur.layer.MdlLayer1dAxe;
import org.fudaa.fudaa.modeleur.layer.MdlLayer1dBank;
import org.fudaa.fudaa.modeleur.layer.MdlLayer1dLimiteStockage;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dDirectionLine;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dProfile;
import org.fudaa.fudaa.modeleur.modeleur1d.controller.ControllerBief;
import org.fudaa.fudaa.modeleur.modeleur1d.model.Bief;
import org.fudaa.fudaa.sig.layer.FSigEditor;


/**
 * Cette classe permet le visionnage du bief sur lequel la fenetre 1D travaille.
 * La vue est suivant l'axe Z.
 * Il est possible de selectionner les profils pour indiquer lequel doit �tre 
 * �dit� via la vue par graphe 1D.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class VueBief extends ZEbliCalquesPanel implements ListSelectionListener/*, ProfilContainerListener*/ {
  
  /** Le controller de la fen�tre 1d. */
  protected ControllerBief controllerBief_;
  /** Le calque des profils, pour mise a jour du profil selectionn�. */
  MdlLayer2dProfile profils_;
  
  public VueBief(MdlImplementation _impl, ControllerBief _controllerBief) {
    super(_impl);
    controllerBief_=_controllerBief;
    controllerBief_.getBiefSelectionModel().addListSelectionListener(this);
    // N�c�ssaire � l'instanciation de calques
    gisEditor_=new FSigEditor(this);
    gisEditor_.setUi(_impl);
    getScene().setRestrictedToCalqueActif(false); // Sans le mode selection n'est pas possible
    // Simplement pour bloquer l'�dition lors d'un double clic
    getController().getCqSelectionI().setEditor(new ZEditorDefault(this) {
      public String edit() {return null;}
    });
    // Pas d'accrochage.
    getController().getCqSelectionI().setCatchingEnabled(false);
//    getController().setEbliFormatter(new EbliFormatter());
    setPreferredSize(new Dimension(200, 200));
    generateCalques();
  }

  /**
   * G�n�re un arbre de calques pour afficher le contenu du bief. Et ajoute ce
   * calque � l'instance (en ayant pr�c�dement supprim� les pr�c�dents).
   */
  private void generateCalques() {
    if(controllerBief_.getSelectedBief()==null) {
      removeAllCalqueDonnees();
      validate();
      return;
    }
    Bief bief=controllerBief_.getSelectedBief();
    BGroupeCalque groupeCalque=new BGroupeCalque();
    // Ajout du calque d'axe hydraulique
    MdlLayer1dAxe axeHydraulique=new MdlLayer1dAxe((FSigEditor)gisEditor_);
    axeHydraulique.setSelectable(false);
    axeHydraulique.setShowLineOrientation(true);
    axeHydraulique.modele(bief.axeHydraulique_);
    groupeCalque.add(axeHydraulique);
    // Ajout du calque de lignes directrices
    MdlLayer2dDirectionLine lignesDirectrices=new MdlLayer2dDirectionLine((FSigEditor)gisEditor_);
    lignesDirectrices.setSelectable(false);
    lignesDirectrices.modele(bief.lignesDirectrices_);
    groupeCalque.add(lignesDirectrices);
    // Ajout du calque de limites de stockage
    MdlLayer1dLimiteStockage limitesStockage=new MdlLayer1dLimiteStockage((FSigEditor)gisEditor_);
    limitesStockage.setSelectable(false);
    limitesStockage.modele(bief.limitesStockages_);
    groupeCalque.add(limitesStockage);
    // Ajout du calque de profils
    profils_=new MdlLayer2dProfile((FSigEditor)gisEditor_);
    profils_.modele(bief.profils_);
    groupeCalque.add(profils_);
    // Ajout du calque de rives
    MdlLayer1dBank rives=new MdlLayer1dBank((FSigEditor)gisEditor_);
    rives.setSelectable(false);
    rives.modele(bief.rives_);
    groupeCalque.add(rives);
    // Fin
    removeAllCalqueDonnees();
    addCalque(groupeCalque);
    validate();
    restaurer();
  }

  public void valueChanged(ListSelectionEvent e) {
    generateCalques();
  }

//  @Override
//  public void profilContainerSelectedChanged(int _idxOldProfil, int _idxNewProfil){
//    if (_idxNewProfil==-1)
//      profils_.clearSelection();
//    else
//      profils_.setSelection(new int[]{_idxNewProfil});
//  }
  
  public void setSelectedProfile(int _idxNewProfil) {
    if (_idxNewProfil==-1)
      profils_.clearSelection();
    else
      profils_.setSelection(new int[]{_idxNewProfil});
  }

//  public void profilContainerDataModified() {}
}
