/*
 * @creation     19 janv. 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.view;

import com.memoire.bu.BuLabel;
import com.memoire.bu.BuTextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import org.fudaa.ebli.commun.EbliFormatterInterface;
import org.fudaa.fudaa.modeleur.modeleur1d.controller.Controller1d;
import org.fudaa.fudaa.modeleur.modeleur1d.model.ProfilContainerException;
import org.fudaa.fudaa.modeleur.modeleur1d.model.ProfilContainerI;
import org.fudaa.fudaa.modeleur.modeleur1d.model.ProfilContainerListener;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Un panneau permettant de modifier l'abscisse curviligne du profil sur l'axe hydraulique.
 * 
 * @author marchand@deltacad.fr
 * @version $Id$
 */
public class PnAbscisseProfil extends Box implements ProfilContainerListener {

  /** Le controller des biefs. */
  protected Controller1d controller_;
  /** L'abscisse curviligne du profil sur l'axe hydraulique. */
  private BuTextField tfAxeHydraulique_=BuTextField.createDoubleField();
  /** Le profil courant */
  protected ProfilContainerI data_;
  protected EbliFormatterInterface formater_;
  
  public PnAbscisseProfil(Controller1d _controller1d, EbliFormatterInterface _formatter){
    super(BoxLayout.X_AXIS);
    
    controller_=_controller1d;
    formater_=_formatter;
    setProfile(null);
    
    // Abscisse curviligne du profil sur l'axe hydraulique
    tfAxeHydraulique_.addActionListener(new ActionListener(){
      public void actionPerformed(ActionEvent e) {
        if (data_==null) return;

        controller_.clearError();
        double oldValue=data_.getAbsCurvProfilOnAxeHydraulique();
        try {
          double newValue=((Double)tfAxeHydraulique_.getValue()).doubleValue();
          if (oldValue!=newValue)
            controller_.moveProfilOnAxeHydraulique(newValue);
        }
        catch(NumberFormatException _exc) {
          tfAxeHydraulique_.setText(controller_.getFormater().getXYFormatter().format(oldValue));
        }
        catch(IllegalArgumentException _exc) {
          controller_.showError(_exc.getMessage());
        }
        catch (ProfilContainerException _exc) {
          controller_.showError(_exc.getMessage());
        }
      }
    });
    add(new BuLabel(MdlResource.getS("Abscisse sur l'axe")+" "));
    add(tfAxeHydraulique_);
  }

  /**
   * Met � jour l'affichage de l'abscisse curviligne du profil sur l'axe hydraulique.
   */
  private void updateAbscisseCurvAxeHydraulique() {
    double abscCurv=data_!=null ? data_.getAbsCurvProfilOnAxeHydraulique():-1;
    if (abscCurv == -1) {
      tfAxeHydraulique_.setText(" - ");
    }
    else {
      tfAxeHydraulique_.setText(formater_.getXYFormatter().format(abscCurv));
    }
    tfAxeHydraulique_.setEnabled(abscCurv != -1);
  }

  public void profilContainerDataModified() {
    updateAbscisseCurvAxeHydraulique();
  }

  
  public void setProfile(ProfilContainerI _prf) {
    if (data_==_prf) return;
    
    if (data_!=null)
      data_.removeProfilContainerListener(this);
    
    data_=_prf;
    
     if (data_!=null)
      data_.addProfilContainerListener(this);
     
     updateAbscisseCurvAxeHydraulique();
  }
//  @Override
//  public void profilContainerSelectedChanged(int _idxOldProfil, int _idxNewProfil) {
//    updateAbscisseCurvAxeHydraulique();
//  }
}
