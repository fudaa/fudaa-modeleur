/*
 * @creation     8 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.view;

import com.memoire.bu.BuSpecificBar;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JComponent;
import javax.swing.JPanel;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneListener;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.fudaa.modeleur.action.MdlCompareProfileSet;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dProfile;
import org.fudaa.fudaa.modeleur.layer.MdlModel2dProfile;
import org.fudaa.fudaa.modeleur.modeleur1d.controller.ControllerBief;

/**
 * Ce panneau permet le visionnage d'un set de profils en 2D. La vue est suivant l'axe Z.
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class VueProfileSet extends JPanel /*implements ProfilContainerListener*/ {

  /**
   * Le controller de la fen�tre 1d.
   */
  protected ControllerBief controllerBief_;
  /**
   * Le calque des profils, pour mise a jour du profil selectionn�.
   */
  List<MdlLayer2dProfile> layers_ = new ArrayList<MdlLayer2dProfile>();
//  MdlLayer2dProfile profils_;
  List<GISZoneCollectionLigneBrisee> zones_ = new ArrayList<GISZoneCollectionLigneBrisee>();
  /**
   * La liste des listeners de zones
   */
  List<GISZoneListener> zonesListeners_ = new ArrayList<GISZoneListener>();
  /**
   * Le profil set associ�
   */
  MdlCompareProfileSet profSet;
  /**
   * Le composant des calques
   */
  ZEbliCalquesPanel pnLayers_;
  /**
   * Le profil selectionn�
   */
  int selProfilId = -1;
  /**
   * Les listeners de selection chang�e
   */
  Set<ListSelectionListener> listeners_ = new HashSet<ListSelectionListener>();

  /**
   * Un modele de layer sp�cifique pour ne pas visualiser les profils des zones qui ne sont pas concern�s par la vue.
   */
  class MdlProfileLayerModel extends MdlModel2dProfile {

    HashSet<Integer> idxVisibles = new HashSet<Integer>();
    HashMap<Integer, Integer> idxgeom2idxprof = new HashMap<Integer, Integer>();

    public MdlProfileLayerModel(GISZoneCollectionLigneBrisee _zone) {
      super(_zone, null, null);
    }

    public void addVisibleProfile(int _idxGeom) {
      idxVisibles.add(_idxGeom);
    }

    public void setProfileId(int _idxGeom, int _idxProf) {
      idxgeom2idxprof.put(_idxGeom, _idxProf);
    }

    public int getProfileId(int _idxGeom) {
      return idxgeom2idxprof.get(_idxGeom);
    }

    @Override
    public boolean isGeometryVisible(int _idxGeom) {
      if (idxVisibles.contains(_idxGeom)) {
        return super.isGeometryVisible(_idxGeom);
      }
      return false;
    }
  }

  public VueProfileSet(CtuluUI _impl) {
    setLayout(new BorderLayout());

    pnLayers_ = new ZEbliCalquesPanel(_impl);
    pnLayers_.getScene().setRestrictedToCalqueActif(false);
    pnLayers_.getScene().addSelectionListener(new ZSelectionListener() {
      public void selectionChanged(ZSelectionEvent _evt) {
        int idScene = pnLayers_.getScene().getLayerSelection().getMinIndex();
        if (idScene == -1) {
          selProfilId = -1;
        } else {
          MdlProfileLayerModel mdl = (MdlProfileLayerModel) pnLayers_.getScene().getLayerForId(idScene).modeleDonnees();
          int idGeom = pnLayers_.getScene().sceneId2LayerId(idScene);
          selProfilId = mdl.getProfileId(idGeom);
        }
        fireSelectionChanged();
      }
    });
    List<JComponent> buttons = EbliLib.updateToolButtons(pnLayers_.getController().getActions(), null);

    BuSpecificBar tb = new BuSpecificBar();
    tb.addTools(buttons.toArray(new JComponent[0]));

    add(tb, BorderLayout.NORTH);
    add(pnLayers_, BorderLayout.CENTER);

    setPreferredSize(new Dimension(200, 200));
  }

  public void addSelectionListener(ListSelectionListener _l) {
    listeners_.add(_l);
  }

  public void removeSelectionListener(ListSelectionListener _l) {
    listeners_.remove(_l);
  }

  private void fireSelectionChanged() {
    for (ListSelectionListener l : listeners_) {
      l.valueChanged(new ListSelectionEvent(this, getSelectedProfile(), getSelectedProfile(), false));
    }
  }

  public void restaurer() {
    pnLayers_.restaurer();
  }

  /**
   * G�n�re un arbre de calques pour afficher le contenu du bief. Et ajoute ce calque � l'instance (en ayant pr�c�dement supprim� les pr�c�dents).
   */
  private void updateView() {
    // On construit autant de calques qu'il y a de zones. Les profils n'appartenant
    // pas au profil set sont rendus invisibles.
    zones_.clear();
    zonesListeners_.clear();
    layers_.clear();

    pnLayers_.removeAllCalqueDonnees();
    if (profSet == null) {
      return;
    }

    List<MdlProfileLayerModel> mdls = new ArrayList<MdlProfileLayerModel>();

    for (int i = 0; i < profSet.getNbProfil(); i++) {
      GISZoneCollectionLigneBrisee zone = profSet.getProfil(i).getZone();
      int index = zones_.indexOf(zone);
      MdlProfileLayerModel mdl;
      if (index == -1) {
        zonesListeners_.add(zone.getListener());
        mdl = new MdlProfileLayerModel(zone);
        zones_.add(zone);
        mdls.add(mdl);
      } else {
        mdl = mdls.get(index);
      }
      mdl.addVisibleProfile(profSet.getProfil(i).getGeomId());
      mdl.setProfileId(profSet.getProfil(i).getGeomId(), i);
    }

    for (int i = 0; i < mdls.size(); i++) {
      MdlLayer2dProfile layer = new MdlLayer2dProfile(null);
      layer.modele(mdls.get(i));
      pnLayers_.addCalque(layer);
      layers_.add(layer);
    }
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        pnLayers_.restaurer();
      }
    });

  }

  /**
   * Restaure les listeners de zone, puisqu'un seul listener peut �tre donn� par zone.
   */
  public void restoreZoneListeners() {
    for (int i = 0; i < zonesListeners_.size(); i++) {
      zones_.get(i).setListener(zonesListeners_.get(i));
    }
  }

  public void setProfilSet(MdlCompareProfileSet _profSet) {
    if (profSet == _profSet) {
      return;
    }

    if (profSet != null) {
      restoreZoneListeners();
    }

    profSet = _profSet;
    selProfilId = -1;

    updateView();
  }

  public void setSelectedProfile(int _idxProfil) {
    if (_idxProfil == selProfilId || profSet == null) {
      return;
    }

    selProfilId = _idxProfil;

    if (selProfilId != -1) {
      GISZoneCollectionLigneBrisee zone = profSet.getProfil(_idxProfil).getZone();
      int geomId = profSet.getProfil(_idxProfil).getGeomId();
      int zoneId = zones_.indexOf(zone);
      MdlLayer2dProfile layer = layers_.get(zoneId);
      int sceneId = pnLayers_.getScene().layerId2SceneId(layer, geomId);
      pnLayers_.getScene().setSelection(new int[]{sceneId});
    } else {
      pnLayers_.getScene().clearSelection();
    }
  }

  public int getSelectedProfile() {
    return selProfilId;
  }
}
