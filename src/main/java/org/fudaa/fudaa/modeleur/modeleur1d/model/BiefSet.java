/*
 * @creation     22 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluNamedCommand;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Ce model a pour objectif de contenir les diff�rents donn�es relavites aux
 * g�om�tries manipul�es dans tout le 1d.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class BiefSet {
  
  /** Dictionnaire contenant les diff�rents biefs. */
  private Map<String, Bief> biefs_=new HashMap<String, Bief>();
  /**
   * List des noms des Biefs. Cette attribut existe pour avoir une list ordonn�e
   * a retourner (notamment pour les tableau). La hashmap ne garantissant pas de
   * retourner ses clefs toujours dans le m�me ordre (ce qui pose probl�me pour
   * la gestion de la selection.
   */
  private List<String> listNoms_=new ArrayList<String>();
  /** Liste des listeners de cette instance. */
  private List<BiefSetListener> listeners_=new ArrayList<BiefSetListener>();
  
  public BiefSet(){}

  // Gestion des biefs \\
  
  /**
   * Ajout un nouveau bief. Le nom donn� doit �tre unique par rapport au bief
   * pr�c�dement ajout� sous peine de recevoir une {@link IllegalArgumentException}.
   */
  public void addBief(String _nom, Bief _bief, CtuluCommandContainer _cmd) {
    if(_nom==null||_bief==null)
      throw new IllegalArgumentException("Erreur prog : Ni _nom ni _bief ne peuvent �tre null.");
    if(biefs_.containsKey(_nom))
      throw new IllegalArgumentException(MdlResource.getS("Ce nom de bief est d�j� utilis�."));
    class AddBiefCtuluCommand implements CtuluNamedCommand {
      public String nom_;
      public Bief bief_;
      public AddBiefCtuluCommand(String _nom, Bief _bief){nom_=_nom;bief_=_bief;}
      public void redo() {addBief(nom_, bief_, null);}
      public void undo() {removeBief(nom_, null);}
      public String getName() {return MdlResource.getS("Ajouter bief");}
    };
    biefs_.put(_nom, _bief);
    listNoms_.add(_nom);
    if(_cmd!=null)
      _cmd.addCmd(new AddBiefCtuluCommand(_nom, _bief));
    fireAddedBief(_bief, _nom);
  }
  
  /**
   * Supprime tous les biefs du container.
   */
  public void removeAllBiefs(CtuluCommandContainer _cmd) {
    CtuluCommandComposite cmd=new CtuluCommandComposite(MdlResource.getS("Suppression de tous les biefs"));
    String[] names=getBiefNames();
    for(int i=0;i<names.length;i++)
      removeBief(names[i], cmd);
    if(_cmd!=null)
      _cmd.addCmd(cmd.getSimplify());
  }
  
  /**
   * Supprime le bief dont le nom est donn� en param�tre.
   */
  public void removeBief(String _nomBief, CtuluCommandContainer _cmd) {
    if(_nomBief==null)
      throw new IllegalArgumentException("Erreur prog : _nomBief ne peut �tre null");
    if(!biefs_.containsKey(_nomBief))
      throw new IllegalArgumentException("Erreur prog : "+_nomBief+" n'existe pas.");
    class removeBiefCtuluCommand implements CtuluCommand, CtuluNamedCommand {
      public String nom_;
      public Bief bief_;
      public boolean isSyn_;
      public removeBiefCtuluCommand(String _nom, Bief _bief, boolean _isSyn) {
        nom_=_nom;bief_=_bief;isSyn_=_isSyn;
      }
      public void redo() {
        removeBief(nom_, null);
        bief_.disableSynchroniser();
      }
      public void undo() {
        addBief(nom_, bief_, null);
        if(isSyn_)
          bief_.enableSynchroniser();
      }
      public String getName() {return MdlResource.getS("Supprimer bief");}
    };
    Bief bief=biefs_.get(_nomBief);
    boolean isSynchronise=bief.isSynchronizerActived();
    bief.disableSynchroniser();
    biefs_.remove(_nomBief);
    listNoms_.remove(_nomBief);
    if(_cmd!=null)
      _cmd.addCmd(new removeBiefCtuluCommand(_nomBief, bief, isSynchronise));
    fireRemovedBief(bief, _nomBief);
  }
  
  /**
   * Renomme un bief.
   */
  public void renameBief(String _oldName, String _newName, CtuluCommandContainer _cmd) {
    if(_oldName==null||_newName==null)
      throw new IllegalArgumentException("Erreur prog : Ni _ancienNom ni _nouveauNom ne peuvent �tre null.");
    if(!biefs_.containsKey(_oldName))
      throw new IllegalArgumentException("Erreur prog : "+_oldName+" n'existe pas");
    if(_oldName.equals(_newName))
      return;
    if(biefs_.containsKey(_newName))
      throw new IllegalArgumentException(MdlResource.getS("Le bief {0} existe d�j�", _newName));
    class renameBiefCtuluCommand implements CtuluCommand, CtuluNamedCommand {
      public String oldName_;
      public String newName_;
      public renameBiefCtuluCommand(String _oldName, String _newName){oldName_=_oldName;newName_=_newName;}
      public void redo() {renameBief(oldName_, newName_, null);}
      public void undo() {renameBief(newName_, oldName_, null);}
      public String getName() {return MdlResource.getS("Renommer profil");}
    };
    biefs_.get(_oldName).setName(_newName);
    biefs_.put(_newName, biefs_.get(_oldName));
    listNoms_.set(listNoms_.indexOf(_oldName), _newName);
    Bief bief=biefs_.remove(_oldName);
    if(_cmd!=null)
      _cmd.addCmd(new renameBiefCtuluCommand(_oldName, _newName));
    fireRenamedBief(bief, _oldName, _newName);
  }

  // Gestion des listeners \\
  
  /**
   * Ajoute un listener r�agissant au modification des donn�es.
   */
  public void addBiefSetListener(BiefSetListener _listener) {
    if(_listener!=null&&!listeners_.contains(_listener))
      listeners_.add(_listener);
  }
  
  /**
   * Supprime le listener de la liste.
   */
  public void removeBiefSetListener(BiefSetListener _listener) {
    if(_listener!=null&&listeners_.contains(_listener))
      listeners_.remove(_listener);
  }
  
  protected void fireAddedBief(Bief _bief, String _newName) {
    for(BiefSetListener listener: listeners_)
      listener.addedBief(_bief,_newName);
  }
  protected void fireRenamedBief(Bief _bief, String _oldName, String _newName) {
    for(BiefSetListener listener: listeners_)
      listener.renamedBief(_bief, _oldName, _newName);
  }
  protected void fireRemovedBief(Bief _bief, String _oldName) {
    for(BiefSetListener listener: listeners_)
      listener.removedBief(_bief, _oldName);
  }
  
  // R�cup�ration des donn�es \\
  
  /**
   * Retourne la liste des models du bief indiqu� par un nom.
   */
  public Bief getBief(String _nomBief){
    return biefs_.get(_nomBief);
  }
  
  /**
   * Retourne la liste des noms de bief pr�sent dans l'instance
   */
  public String[] getBiefNames(){
    Object[] o=listNoms_.toArray();
    String[] result=new String[o.length];
    for(int i=0;i<result.length;i++)
      result[i]=(String) o[i];
    return result;
  }

  /**
   * Retourne le nom d'un bief caract�ris� par son num�ro dans la liste des
   * noms. Ce num�ro peut chang� � chaque ajout/supression de biefs.
   */
  public String getBiefName(int _idxName) {
    return listNoms_.get(_idxName);
  }
  
  /**
   * Retourne vrai le le nom donn� correspond � un bief.
   */
  public boolean hasBief(String _nomBief){
    return biefs_.containsKey(_nomBief);
  }
  
  /**
   * Retourne le nombre de Bief.
   */
  public int getNbBief(){
    return listNoms_.size();
  }
  
}
