/*
 * @creation     23 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.modeleur1d.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluNamedCommand;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Cette classe permet d'adapter une BiefColleciton en un model manipulable
 * simplement. Il n'affiche que le bief selectionn�.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class BiefContainerAdapter implements BiefContainerI, BiefSetListener {

  /** Le container de tous les biefs. */
  private BiefSet biefSet_;
  /** Le nom du bief selectionn�. */
  private String nomBiefSelected_;
  /** Liste des listeners de cette instance. */
  private Set<BiefContainerListener> biefListeners_=new HashSet<BiefContainerListener>();
  /** Listeners profilSet */
  private Set<ProfileSetListener> profSetListeners_=new HashSet<ProfileSetListener>();
  /** Les profils reconstruits a chaque changement de bief. */
  private List<ProfilContainerAdapter> profiles=new ArrayList<ProfilContainerAdapter>();

  public BiefContainerAdapter(BiefSet _biefSet) {
    biefSet_=_biefSet;
    biefSet_.addBiefSetListener(this);
  }

  public boolean hasProfil(int _idxProfil) {
    if(_idxProfil>=0&&getZoneProfils()!=null&&_idxProfil<getZoneProfils().getNumGeometries())
      return true;
    return false;
  }
  
  public boolean hasAxeHydraulique() {
    GISZoneCollectionLigneBrisee zone=getZoneAxeHydraulique();
    return zone!=null && zone.getNbGeometries()>0;
  }
  
  public double getDecalageAxeHydraulique() {
    GISZoneCollection zone=getZoneAxeHydraulique();
    int idxAttr=zone.getIndiceOf(GISAttributeConstants.CURVILIGNE_DECALAGE);
    return ((Double)zone.getValue(idxAttr, 0)).doubleValue();
  }
  
  /**
   * Selectionner un des biefs. Pour n'en selectionner aucun, _nomBief doit
   * valloir null.
   */
  public void setSelectedBief(String _nomBief) {
    if(_nomBief!=null&&!biefSet_.hasBief(_nomBief))
      throw new IllegalArgumentException("Bief : "+_nomBief+" inconnu.");
    if(_nomBief == null ? nomBiefSelected_ != null : !_nomBief.equals(nomBiefSelected_)) {
      nomBiefSelected_=_nomBief;
      rebuildProfiles();
      fireBiefSelectedChanged(_nomBief);
    }
  }
  
  public String getSelectedBief() {
    return nomBiefSelected_;
  }
  
  public Bief getBief(String _name) {
    return biefSet_.getBief(_name);
  }
  
  /**
   * Reconstruit les profils du bief.
   */
  public void rebuildProfiles() {
    profiles.clear();
    for (int i=0; i<getNbProfil(); i++) {
      ProfilContainerAdapter pf=new ProfilContainerAdapter(this);
      try {
        pf.setSelectedProfil(i);
      }
      catch (Exception ex) {}
      profiles.add(pf);
    }
  }

  public GISZoneCollectionLigneBrisee getZoneAxeHydraulique() {
    if(nomBiefSelected_==null)
      return null;
    return (GISZoneCollectionLigneBrisee)biefSet_.getBief(nomBiefSelected_).axeHydraulique_.getGeomData();
  }

  public GISZoneCollectionLigneBrisee getZoneLignesDirectrices() {
    if(nomBiefSelected_==null)
      return null;
    return (GISZoneCollectionLigneBrisee)biefSet_.getBief(nomBiefSelected_).lignesDirectrices_.getGeomData();
  }

  public GISZoneCollectionLigneBrisee getZoneLimitesStockages() {
    if(nomBiefSelected_==null)
      return null;
    return (GISZoneCollectionLigneBrisee)biefSet_.getBief(nomBiefSelected_).limitesStockages_.getGeomData();
  }

  public GISZoneCollectionLigneBrisee getZoneProfils() {
    if(nomBiefSelected_==null)
      return null;
    return (GISZoneCollectionLigneBrisee)biefSet_.getBief(nomBiefSelected_).profils_.getGeomData();
  }

  public ZModeleLigneBriseeEditable getModelProfils() {
    if(nomBiefSelected_==null)
      return null;
    return (ZModeleLigneBriseeEditable)biefSet_.getBief(nomBiefSelected_).profils_;
  }
  
  public GISZoneCollectionLigneBrisee getZoneRives() {
    if(nomBiefSelected_==null)
      return null;
    return (GISZoneCollectionLigneBrisee)biefSet_.getBief(nomBiefSelected_).rives_.getGeomData();
  }

  public GISZoneCollectionLigneBrisee[] getZones() {
    if(nomBiefSelected_==null)
      return null;
    return new GISZoneCollectionLigneBrisee[]{getZoneAxeHydraulique(), getZoneLignesDirectrices(), getZoneLimitesStockages(),
        getZoneProfils(), getZoneRives()};
  }

  // Raccourcis de gestion des profils \\
  
  public int getNbProfil() {
    if(nomBiefSelected_==null)
      return 0;
    return getZoneProfils().getNbGeometries();
  }

  public String getNomProfil(int _idxProfil) {
    if(_idxProfil<0||_idxProfil>=getZoneProfils().getNbGeometries())
      throw new IllegalArgumentException("Erreur prog : Le profil "+_idxProfil+" n'existe pas");
    return (String) getZoneProfils().getValue(getZoneProfils().getIndiceOf(GISAttributeConstants.TITRE), _idxProfil);
  }

  public void removeProfil(int _idxProfil, CtuluCommandContainer _cmd) {
    if(_idxProfil<0||_idxProfil>=getZoneProfils().getNbGeometries())
      throw new IllegalArgumentException("Erreur prog : Le profil "+_idxProfil+" n'existe pas");
    class RemoveProfilCtuluCommand implements CtuluCommand, CtuluNamedCommand{
      private int idxProfil_;
      private CtuluCommandComposite cmd_;
      public RemoveProfilCtuluCommand(int _idxProfil, CtuluCommandComposite _cmd) {
        idxProfil_=_idxProfil;cmd_=_cmd;
      }
      public void redo() {
        cmd_.redo();
        fireProfilAdded(idxProfil_);
      }
      public void undo() {
        cmd_.undo();
        fireProfilRemoved(idxProfil_);
      }
      public String getName() {return MdlResource.getS("Supprimer profil");}
    }
    CtuluCommandComposite cmdComp=new CtuluCommandComposite();
    getBief(getSelectedBief()).disableSynchroniser();
    getZoneProfils().removeGeometries(new int[]{_idxProfil}, cmdComp);
    getBief(getSelectedBief()).enableSynchroniser();
    rebuildProfiles();
    if(_cmd!=null)
      _cmd.addCmd(new RemoveProfilCtuluCommand(_idxProfil, cmdComp));
    fireProfilRemoved(_idxProfil);
  }

  public void renameProfil(int _idxProfil, String _newName, CtuluCommandContainer _cmd) {
    if(_newName==null) return;
    
    if(_idxProfil<0||_idxProfil>=getZoneProfils().getNbGeometries())
      throw new IllegalArgumentException("Erreur prog : Le profil "+_idxProfil+" n'existe pas");

    class RenameProfilCtuluCommand implements CtuluCommand, CtuluNamedCommand {
      private int idxProfil_;
      private String oldName_;
      private String newName_;
      public RenameProfilCtuluCommand(int _idxProfil, String _oldName, String _newName){
        idxProfil_=_idxProfil;oldName_=_oldName;newName_=_newName;
      }
      public void redo() {renameProfil(idxProfil_, newName_, null);}
      public void undo() {renameProfil(idxProfil_, oldName_, null);}
      public String getName() {return MdlResource.getS("Renommer profil");}
    }
    String oldName=(String) getZoneProfils().getValue(getZoneProfils().getIndiceOf(GISAttributeConstants.TITRE), _idxProfil);
    getZoneProfils().setAttributValue(getZoneProfils().getIndiceOf(GISAttributeConstants.TITRE), _idxProfil, _newName, null);
    if(_cmd!=null)
      _cmd.addCmd(new RenameProfilCtuluCommand(_idxProfil, oldName, _newName));
    fireProfilRenamed(_idxProfil, oldName, _newName);
  }
  
  // R�action aux modifications dans le BiefCollection \\
  
  public void addedBief(Bief _bief, String _name) {}

  public void removedBief(Bief _bief, String _name) {
    if(_name==nomBiefSelected_) {
      nomBiefSelected_=null;
      fireBiefSelectedChanged(null);
    }
  }

  public void renamedBief(Bief _bief, String _oldName, String _newName) {
    if(_oldName==nomBiefSelected_) {
      nomBiefSelected_=_newName;
      fireBiefSelectedRenamed(_oldName, _newName);
    }
  }
  
  // Gestion des listeners \\
  
  public void addListener(ProfileSetListener _listener) {
    profSetListeners_.add(_listener);
  }
  
  public void removeListener(ProfileSetListener _listener) {
    profSetListeners_.remove(_listener);
  }
  
  /**
   * Ajoute un listener r�agissant au modification des donn�es.
   */
  public void addBiefContainerListener(BiefContainerListener _listener) {
    biefListeners_.add(_listener);
    profSetListeners_.add(_listener);
  }
  
  /**
   * Supprime le listener de la liste.
   */
  public void removeBiefContainerListener(BiefContainerListener _listener) {
    biefListeners_.remove(_listener);
    profSetListeners_.remove(_listener);
  }
  
  protected void fireBiefSelectedChanged(String _newName) {
    for(BiefContainerListener listener: biefListeners_)
      listener.biefSelectedChanged(_newName);
  }
  protected void fireBiefSelectedRenamed(String _oldName, String _newName) {
    for(BiefContainerListener listener: biefListeners_)
      listener.biefSelectedRenamed(_oldName, _newName);
  }
  protected void fireProfilRemoved(int _idxProfil){
    for(ProfileSetListener listener: profSetListeners_)
      listener.profilRemoved(_idxProfil);
  }
  protected void fireProfilAdded(int _idxProfil) {
    for(ProfileSetListener listener: profSetListeners_)
      listener.profilAdded(_idxProfil);
  }
  protected void fireProfilRenamed(int _idxProfil, String _oldName, String _newName){
    for(BiefContainerListener listener: biefListeners_)
      listener.profilRenamed(_idxProfil, _oldName, _newName);
  }
  
  @Override
  public ProfilContainerI getProfil(int _idxProfil) {
    if (_idxProfil<0 || _idxProfil>=profiles.size())
      return null;
    
    ProfilContainerAdapter pf=profiles.get(_idxProfil);
    return pf;
  }

  @Override
  public boolean isBief() {
    return true;
  }
}
