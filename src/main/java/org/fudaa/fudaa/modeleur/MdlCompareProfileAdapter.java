/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.modeleur;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISCoordinateSequenceContainerInterface;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.fudaa.modeleur.modeleur1d.model.ProfilContainerException;
import org.fudaa.fudaa.modeleur.modeleur1d.model.ProfilContainerI;
import org.fudaa.fudaa.modeleur.modeleur1d.model.ProfilContainerListener;
import org.fudaa.fudaa.modeleur.modeleur1d.model.UtilsProfil1d;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;

/**
 * Un adapteur de profil depuis un index de g�om�trie et d'une collection.
 * @author marchand@deltacad.fr
 */
public class MdlCompareProfileAdapter implements ProfilContainerI {
  final int idxGeom_;
  final GISZoneCollectionLigneBrisee zone_;
  final List<Double> curv_=new ArrayList<Double>();
  private int idxZMax_;
  private int idxZMin_;
  HashSet<ProfilContainerListener> listeners_=new HashSet<ProfilContainerListener>();

  public MdlCompareProfileAdapter(int _idxGeom, GISZoneCollectionLigneBrisee _zone) {
    idxGeom_=_idxGeom;
    zone_=_zone;
    updateCacheCurv();
    updateCacheZ();
  }
  
  /**
   * @return La zone supportant la g�om�trie. Important pour les modifications de g�om�trie.
   */
  public GISZoneCollectionLigneBrisee getZone() {
    return zone_;
  }
  
  /**
   * @return L'index de la g�om�trie sur la zone.
   */
  public int getGeomId() {
    return idxGeom_;
  }

  public void addProfilContainerListener(ProfilContainerListener _listener) {
    listeners_.add(_listener);
  }

  public void removeProfilContainerListener(ProfilContainerListener _listener) {
    listeners_.remove(_listener);
  }

  public int getNbPoint() {
    return zone_.getGeometry(idxGeom_).getNumPoints();
  }

  public double getCurv(int _idxPoint) {
    if (curv_ == null || _idxPoint < 0 || _idxPoint >= curv_.size()) {
      throw new IllegalArgumentException("Erreur prog : Cet index n'existe pas.");
    }
    return curv_.get(_idxPoint);
  }

  public double getCurvMax() {
    if (curv_ == null) {
      return 0;
    }
    else {
      return curv_.get(curv_.size() - 1);
    }
  }

  public double getCurvMin() {
    if (curv_ == null) {
      return 0;
    }
    else {
      return curv_.get(0); // toujours 0 de toute fa�on
    }
  }

  public double getZ(int _idxPoint) {
    GISAttributeModel z=getZCollection();
    if (z == null || _idxPoint < 0 || _idxPoint >= z.getSize()) {
      throw new IllegalArgumentException("Erreur prog : Cet index n'existe pas.");
    }
    return ((Double) z.getObjectValueAt(_idxPoint)).doubleValue();
  }

  public double getZMax() {
    GISAttributeModel z=getZCollection();
    if (z == null) {
      return 0;
    }
    else {
      return (Double) z.getObjectValueAt(idxZMax_);
    }
  }

  public double getZMin() {
    GISAttributeModel z=getZCollection();
    if (z == null) {
      return 0;
    }
    else {
      return (Double) z.getObjectValueAt(idxZMin_);
    }
  }

  public double getAbsCurvRiveGauche() {
    return -1;
  }

  public double getAbsCurvRiveDroite() {
    return -1;
  }

  public double getAbsCurvLimiteStockageGauche() {
    return -1;
  }

  public double getAbsCurvLimiteStockageDroite() {
    return -1;
  }

  public double getAbsCurvAxeHydrauliqueOnProfil() {
    return -1;
  }

  public double getAbsCurvProfilOnAxeHydraulique() {
    String value=(String) zone_.getValue(zone_.getIndiceOf(GISAttributeConstants.COMMENTAIRE_HYDRO), idxGeom_);
    return GISLib.getHydroCommentDouble(value, GISAttributeConstants.ATT_COMM_HYDRO_PK);
  }

  public int getNbLignesDirectrices() {
    return 0;
    //      throw new UnsupportedOperationException("Not supported yet.");
  }

  public String[] getNamesLignesDirectrices() {
    return new String[0];
    //      throw new UnsupportedOperationException("Not supported yet.");
  }

  public int[] getIdxLignesDirectricesAt(int _idxPoint) {
    //      throw new UnsupportedOperationException("Not supported yet.");
    return new int[0];
  }

  public String[] getNamesLignesDirectricesAt(int _idxPoint) {
    //      throw new UnsupportedOperationException("Not supported yet.");
    return new String[0];
  }

  public String getLigneDirectriceName(int _idx) {
    //      throw new UnsupportedOperationException("Not supported yet.");
    return null;
  }

  public int[] getAllowedMoveOfLignesDirectricesTo(int _idxPoint) {
    //      throw new UnsupportedOperationException("Not supported yet.");
    return new int[0];
  }

  public boolean isRupturePoint(int _idx) {
    //      throw new UnsupportedOperationException("Not supported yet.");
    return false;
  }
  
  public void fireDataModified() {
    for (ProfilContainerListener l : listeners_) {
      l.profilContainerDataModified();
    }
  }

  public void setCurv(int _idxPoint, double _value, CtuluCommandContainer _cmd) throws ProfilContainerException {
    // Pas de d�placement au dela des points encadrants.
    if ((_idxPoint>0 && curv_.get(_idxPoint-1)>=_value) ||
        (_idxPoint<curv_.size()-1 && curv_.get(_idxPoint+1)<=_value)) {
      throw new ProfilContainerException(MdlResource.getS("Impossible de d�placer un point au del� des points l'encadrant."));
    }
    
    CoordinateSequence seq=(CoordinateSequence)zone_.getCoordinateSequence(idxGeom_).clone();
    Coordinate newCoord=UtilsProfil1d.getCoordinateXY(seq, _value);
    seq.setOrdinate(_idxPoint, 0, newCoord.x);
    seq.setOrdinate(_idxPoint, 1, newCoord.y);
    zone_.setCoordinateSequence(idxGeom_, seq, _cmd);
    
    updateCacheCurv();
    fireDataModified();
  }

  public void setZ(int _idxPoint, double _value, CtuluCommandContainer _cmd) {
    // On ne fait pas de controle sur Z
    GISAttributeModel z=getZCollection();
    z.setObject(_idxPoint, _value, _cmd);
    updateCacheZ();
    fireDataModified();
  }

  public void setValues(int _idxPoint, double _valueCurv, double _valueZ, CtuluCommandContainer _cmd) throws ProfilContainerException {
    // Peut d�clencher une exception
    setCurv(_idxPoint, _valueCurv, _cmd);
    setZ(_idxPoint, _valueZ, _cmd);
  }

  public void remove(int _idxPoint, CtuluCommandContainer _cmd) throws ProfilContainerException {
    // Pas de controle sp�cifique.
    CtuluListSelection sel=new CtuluListSelection(new int[]{_idxPoint});
    zone_.removeAtomics(idxGeom_, sel, null, _cmd);
    
    updateCacheCurv();
    updateCacheZ();
    fireDataModified();
  }
  
  public void add(int _idxPoint, double _valueCurv, double _valueZ, CtuluCommandContainer _cmd) throws ProfilContainerException {
    CtuluCommandComposite cmp=new CtuluCommandComposite(MdlResource.getS("Ajout d'un point"));
    
    CoordinateSequence seq=(CoordinateSequence)zone_.getCoordinateSequence(idxGeom_).clone();
    Coordinate newCoord=UtilsProfil1d.getCoordinateXY(seq, _valueCurv);
    if (_idxPoint==-1) {
      _idxPoint=seq.size()-1;
    }
    else {
      _idxPoint--;
    }
    zone_.addAtomic(idxGeom_, _idxPoint, newCoord.x, newCoord.y, cmp);
    updateCacheCurv();
    setZ(_idxPoint+1, _valueZ, cmp);
    
    if (_cmd!=null)
      _cmd.addCmd(cmp);
  }

  public boolean moveProfilOnAxeHydraulique(double _pk, CtuluCommandContainer _cmd) throws ProfilContainerException {
    return false;
  }

  public void setLimiteGauche(int _idx, CtuluCommandContainer _cmd) {
  }

  public void setLimiteDroite(int _idx, CtuluCommandContainer _cmd) {
  }

  public void setRiveGauche(int _idx, CtuluCommandContainer _cmd) {
  }

  public void setRiveDroite(int _idx, CtuluCommandContainer _cmd) {
  }

  public void setLigneDirectriceAt(int _idxLd, int _idxPoint, CtuluCommandContainer _cmd) {
  }

  /**
   * Met � jour les valeurs curv_.
   * Si curv_ ou zone_ sont null, rien n'est fait.
   */
  protected void updateCacheCurv() {
    if (curv_ != null) {
      CoordinateSequence seq=((GISCoordinateSequenceContainerInterface) zone_.getGeometry(idxGeom_)).getCoordinateSequence();
      curv_.clear();
      double curvPre=0;
      curv_.add((double) 0);
      for (int i=1; i < seq.size(); i++) {
        double partialCurv=Math.sqrt(Math.pow(seq.getX(i) - seq.getX(i - 1), 2) + Math.pow(seq.getY(i) - seq.getY(i - 1), 2));
        curvPre=curvPre + partialCurv;
        curv_.add(curvPre);
      }
    }
  }

  /**
   * Retourne la collection correspondant au Z profil.
   */
  private GISAttributeModel getZCollection() {
    if (idxGeom_ == -1) {
      return null;
    }
    return (GISAttributeModel) getValueOf(zone_.getAttributeIsZ());
  }

  /**
   * Retourne la valeur de l'attribut d'intersection demand�.
   */
  private Object getValueOf(GISAttributeInterface attr_) {
    int idxAtt=zone_.getIndiceOf(attr_);
    return zone_.getValue(idxAtt, idxGeom_);
  }

  /**
   * Met � jour les valeurs zMin et zMax.
   * Si z est null, zMin et zMax sont mises � -1.
   */
  protected void updateCacheZ() {
    GISAttributeModel z=getZCollection();
    if (z != null) {
      idxZMax_=0;
      idxZMin_=0;
      for (int i=1; i < z.getSize(); i++) {
        double val=(Double) z.getObjectValueAt(i);
        if (val > (Double) z.getObjectValueAt(idxZMax_)) {
          idxZMax_=i;
        }
        else if (val < (Double) z.getObjectValueAt(idxZMin_)) {
          idxZMin_=i;
        }
      }
    }
    else {
      idxZMax_=-1;
      idxZMin_=-1;
    }
  }

  public String getName() {
    GISAttributeModel att=zone_.getModel(GISAttributeConstants.TITRE);
    return (String) att.getObjectValueAt(idxGeom_);
  }

  public int getPointForDirLine(int _idxLine) {
    return -1;
  }
  
}
