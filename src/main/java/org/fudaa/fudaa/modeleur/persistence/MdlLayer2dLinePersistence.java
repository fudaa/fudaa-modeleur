/*
 * @creation     20 janv. 08
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.persistence;

import org.fudaa.ctulu.CtuluArkLoader;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISDataModelZoneAdapter;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.exporter.GISDataStoreZoneExporter;
import org.fudaa.ctulu.gis.exporter.GISGMLZoneExporter;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueSaverInterface;
import org.fudaa.ebli.calque.BCalqueSaverTargetInterface;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.fudaa.modeleur.MdlVisuPanel;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dLine;
import org.fudaa.fudaa.sig.layer.FSigEditor;
import org.fudaa.fudaa.sig.persistence.FSigLayerLinePersistence;

import com.memoire.fu.FuLog;

/**
 * Classe mere de gestion de la persistance pour tous les calques contenant des lignes.
 * @author Bertrand Marchand
 * @version $Id$
 * @see MdlLayer2dLine
 */
public abstract class MdlLayer2dLinePersistence extends FSigLayerLinePersistence {


  /**
   * Dans tous les cas, l'appel a cette m�thode cr�e un nouveau calque.
   */
  @Override
  protected BCalque findCalque(final BCalqueSaverInterface _cqName, final BCalqueSaverTargetInterface _pn, final BCalque _parent) {
    MdlVisuPanel pn=(MdlVisuPanel)_pn;

    MdlLayer2dLine cq=createNewLayer(pn.getEditor());
    cq.setName(BGroupeCalque.findUniqueChildName(_parent, cq.getExtName()));
    _parent.enDernier(cq);

    return cq;
  }
  
  /**
   * Cree un calque du type correct, suivante le gestionnaire de persistence.
   */
  protected abstract MdlLayer2dLine createNewLayer(FSigEditor _editor);

  /**
   * Le calque n'est pas r�cup�r� si l'option ignorer est active.
   */
  @Override
  public String restoreFrom(CtuluArkLoader _loader, BCalqueSaverTargetInterface _parentPanel, BCalque _parentCalque, String _parentDirEntry, String _entryName, ProgressionInterface _proj) {
    String cqName=_loader.getLayerProperty(_parentDirEntry,_entryName, "name");
    if (CtuluLibString.toBoolean(_loader.getOption(CtuluArkLoader.OPTION_LAYER_IGNORE+cqName))) return null;
    
    return super.restoreFrom(_loader, _parentPanel, _parentCalque, _parentDirEntry, _entryName, _proj);
  }
  
  @Override
  public String restore(final BCalqueSaverInterface _saver, final BCalqueSaverTargetInterface _pn,
      final BCalque _parent, final ProgressionInterface _prog) {
    String cqName;
    if ((cqName=super.restore(_saver, _pn, _parent, _prog))==null) return null;
    
    GISZoneCollectionLigneBrisee zone = (GISZoneCollectionLigneBrisee)_saver.getUI().get(FSigLayerLinePersistence.getGeomId());
    final MdlLayer2dLine thisCalque = (MdlLayer2dLine)_parent.getCalqueParNom(cqName);
    if (thisCalque==null) {
      FuLog.warning("MDL: bad layer" + cqName);
      return null;
    }

    if (zone.getNbAttributes() != ((ZModeleLigneBriseeEditable)thisCalque.modeleDonnees()).getGeomData().getNbAttributes()) {
      zone.setAttributes(((ZModeleLigneBriseeEditable)thisCalque.modeleDonnees()).getGeomData().getAttributes(), null);
    }
    if (zone.getNumGeometries() > 0) {
      ((ZModeleLigneBriseeEditable)thisCalque.modeleDonnees()).getGeomData().addAll(new GISDataModelZoneAdapter(zone, null), null, true);
    }
    
    return cqName;
  }

//  /**
//   * La methode est surcharg�e, le calque {@link FSIGLayerLinePersistence} ne restaure pas les objets
//   * GIS s'ils ne sont inclus dans un groupe de calque GIS.
//   */
//  protected boolean restoreFromSpecific(final BCalqueSaverInterface _saver, final CtuluArkLoader _loader,
//      final BCalqueSaverTargetInterface _parentPanel, final BCalque _parentCalque, final String _parentDirEntry, final String _entryName,
//      final ProgressionInterface _proj) {
//    if (!super.restoreFromSpecific(_saver, _loader, _parentPanel, _parentCalque, _parentDirEntry, _entryName,_proj)) return false;
//    
//    GISZoneCollectionLigneBrisee zone = (GISZoneCollectionLigneBrisee) _saver.getUI().get(FSigLayerLinePersistence.getGeomId());
//    final MdlLayer2dLine thisCalque = (MdlLayer2dLine)_parentCalque.getCalqueParNom(_saver.getLayerName());
//    if (thisCalque==null) {
//      FuLog.warning("MDL: bad layer" + _saver.getLayerName());
//      return false;
//    }
//
//    if (zone.getNbAttributes() != thisCalque.getModele().getGeomData().getNbAttributes()) {
//      zone.setAttributes(thisCalque.getModele().getGeomData().getAttributes(), null);
//    }
//    if (zone.getNumGeometries() > 0) {
//      // Remet le Z atomique.
//      thisCalque.getModele().getGeomData().addAll(new GISDataModelZoneAdapter(zone, null), null, true);
//    }
//    return true;
//  }

  protected GISDataStoreZoneExporter createExporter() {
    final GISDataStoreZoneExporter res = super.createExporter();
    // Devrait �tre mis par d�faut.
    res.setUseIdAsName(true);
    return res;
  }
}
