/*
 * @creation     20 janv. 08
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.persistence;

import org.fudaa.fudaa.modeleur.layer.MdlLayer1dAxe;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dLine;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 * Gestion de la persistance d'un calque contenant des axes hydrauliques.
 * 
 * @author Bertrand Marchand
 * @version $Id$
 * @see MdlLayer1dAxe
 */
public class MdlLayer1dAxePersistence extends MdlLayer2dLinePersistence {
  
  @Override
  protected MdlLayer2dLine createNewLayer(FSigEditor _editor) {
    return new MdlLayer1dAxe(_editor);
  }
}
