/*
 * @creation     20 janv. 08
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.persistence;

import java.io.InputStream;

import org.apache.commons.lang.ArrayUtils;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluArkLoader;
import org.fudaa.ctulu.CtuluArkSaver;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISDataModelZoneAdapter;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionMultiPoint;
import org.fudaa.ctulu.gis.exporter.GISDataStoreZoneExporter;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.dodico.ef.io.triangle.TriangleNodeAdapter;
import org.fudaa.dodico.fortran.DodicoArraysBinaryFileSaver;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueSaverInterface;
import org.fudaa.ebli.calque.BCalqueSaverTargetInterface;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.edition.ZModeleMultiPointEditable;
import org.fudaa.fudaa.modeleur.MdlVisuPanel;
import org.fudaa.fudaa.modeleur.grid.LayerGridController;
import org.fudaa.fudaa.modeleur.grid.MdlElementLayer;
import org.fudaa.fudaa.modeleur.grid.MdlElementModel;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dMultiPoint;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dProfile;
import org.fudaa.fudaa.sig.layer.FSigEditor;
import org.fudaa.fudaa.sig.persistence.FSigLayerLinePersistence;

import com.memoire.fu.FuLog;

/**
 * Classe mere de gestion de la persistance pour tous les calques de multipoints.
 *
 * @author Bertrand Marchand
 * @version $Id$
 * @see MdlLayer2dProfile
 */
public abstract class MdlLayer2dMultiPointPersistence extends FSigLayerLinePersistence {

  public static final String PAINTED_EXT = ".painted";
  /**
   * correspondance entre points du maillage et point du semis
   */
  public static final String CORRESPONDANCE_EXT = ".mapping";
  /**
   * points du semis support�s par un unique point du maillage
   */
  public static final String CORRESPONDANCE_PAINTED_EXT = ".painted.mapping";
  public static final String GRID_X_EXT = ".grid.x";
  public static final String GRID_Y_EXT = ".grid.y";
  public static final String GRID_IPOBO_EXT = ".grid.ipobo";
  public static final String GRID_ELT_EXT = ".grid.elt";
  public static final String KEY_ELTMODEL = "ELT_MODEL";

  @Override
  protected GISZoneCollection createInitCollection() {
    return new GISZoneCollectionMultiPoint();
  }

  /**
   * Dans tous les cas, l'appel a cette m�thode cr�e un nouveau calque.
   */
  @Override
  protected BCalque findCalque(final BCalqueSaverInterface _cqName, final BCalqueSaverTargetInterface _pn, final BCalque _parent) {
    MdlVisuPanel pn = (MdlVisuPanel) _pn;

    MdlLayer2dMultiPoint cq = createNewLayer(pn.getEditor());
    cq.setName(BGroupeCalque.findUniqueChildName(_parent, cq.getExtName()));
    _parent.enDernier(cq);
    return cq;
  }

  /**
   * Cree un calque du type correct, suivante le gestionnaire de persistence.
   */
  protected abstract MdlLayer2dMultiPoint createNewLayer(FSigEditor _editor);

  @Override
  public BCalqueSaverInterface saveIn(BCalque _cqToSave, CtuluArkSaver _saver, String _parentDirEntry, String _parentDirIndice) {
    MdlLayer2dMultiPoint layerToSave = (MdlLayer2dMultiPoint) _cqToSave;
    BCalqueSaverInterface res = super.saveIn(_cqToSave, _saver, _parentDirEntry, _parentDirIndice);
    MdlElementLayer gridLayer = layerToSave.getLayerGridDelegate().getGridLayer();
    //on sauvegarde les donn�es de ce calque:
    if (gridLayer != null) {

      LayerGridController layerGridDelegate = layerToSave.getLayerGridDelegate();
      //on sauvegarde le maillage
      saveGrid(layerGridDelegate, _cqToSave, _parentDirEntry, res, _saver);
      //les correspondances maillage <-> semis de point
      saveCorrespondance(layerGridDelegate, _cqToSave, _parentDirEntry, res, _saver);
      //les �l�ments cach�s
      saveUserHiddenMeshes(layerGridDelegate, _cqToSave, _parentDirEntry, res, _saver);

    }
    return res;
  }

  /**
   * on recharge ici le maillage et les donn�es si trouv�es.
   *
   * @param _saver
   * @param _loader
   * @param _parentPanel
   * @param _parentCalque
   * @param _parentDirEntry
   * @param _entryName
   * @param _proj
   * @return
   */
  @Override
  protected boolean restoreFromSpecific(BCalqueSaverInterface _saver, CtuluArkLoader _loader, BCalqueSaverTargetInterface _parentPanel, BCalque _parentCalque, String _parentDirEntry, String _entryName, ProgressionInterface _proj) {
    boolean ok = super.restoreFromSpecific(_saver, _loader, _parentPanel, _parentCalque, _parentDirEntry, _entryName, _proj);
    EfGrid grid = readGrid(_loader, _parentDirEntry, _entryName, _proj);
    if (grid != null) {
      InputStream reader = _loader.getReader(_parentDirEntry, _entryName + CORRESPONDANCE_EXT);
      if (reader != null) {
        DodicoArraysBinaryFileSaver saver = new DodicoArraysBinaryFileSaver();
        int[][] correspondance = saver.load(reader);
        InputStream paintedReader = _loader.getReader(_parentDirEntry, _entryName + CORRESPONDANCE_PAINTED_EXT);
        int[] gisNodePaintedInIso = null;
        if (paintedReader != null) {
          gisNodePaintedInIso = saver.loadSimpleArray(paintedReader);
        }
        if (correspondance != null && gisNodePaintedInIso != null) {
          reader = _loader.getReader(_parentDirEntry, _entryName + PAINTED_EXT);
          MdlElementModel model = new MdlElementModel(grid);
          model.setAltiIdxByGridPtIdx(correspondance);
          model.setInitPointPaintedByMesh(new CtuluListSelection(gisNodePaintedInIso));
          boolean[] userVisible = saver.loadBoolean(reader);
          if (ArrayUtils.isNotEmpty(userVisible)) {
            model.setUserVisible(userVisible);
          }
          _saver.getUI().put(KEY_ELTMODEL, model);
        }
      }
    }
    return ok;
  }

  /**
   * Le calque n'est pas r�cup�r� si l'option ignorer est active.
   */
  @Override
  public String restoreFrom(CtuluArkLoader _loader, BCalqueSaverTargetInterface _parentPanel, BCalque _parentCalque, String _parentDirEntry, String _entryName, ProgressionInterface _proj) {
    String cqName = _loader.getLayerProperty(_parentDirEntry, _entryName, "name");
    if (CtuluLibString.toBoolean(_loader.getOption(CtuluArkLoader.OPTION_LAYER_IGNORE + cqName))) {
      return null;
    }
    return super.restoreFrom(_loader, _parentPanel, _parentCalque, _parentDirEntry, _entryName, _proj);
  }

  @Override
  public String restore(final BCalqueSaverInterface _saver, final BCalqueSaverTargetInterface _pn,
          final BCalque _parent, final ProgressionInterface _prog) {
    String cqName;
    if ((cqName = super.restore(_saver, _pn, _parent, _prog)) == null) {
      return null;
    }

    GISZoneCollectionMultiPoint zone = (GISZoneCollectionMultiPoint) _saver.getUI().get(FSigLayerLinePersistence.getGeomId());
    final MdlLayer2dMultiPoint thisCalque = (MdlLayer2dMultiPoint) _parent.getCalqueParNom(cqName);
    if (thisCalque == null) {
      FuLog.warning("MDL: bad layer" + cqName);
      return null;
    }

    if (zone.getNbAttributes() != ((ZModeleMultiPointEditable) thisCalque.modeleDonnees()).getGeomData().getNbAttributes()) {
      zone.setAttributes(((ZModeleMultiPointEditable) thisCalque.modeleDonnees()).getGeomData().getAttributes(), null);
    }
    if (zone.getNumGeometries() > 0) {
      ((ZModeleMultiPointEditable) thisCalque.modeleDonnees()).getGeomData().addAll(new GISDataModelZoneAdapter(zone, null), null, true);
    }
    MdlElementModel model = (MdlElementModel) _saver.getUI().get(KEY_ELTMODEL);
    GISZoneCollection points = (GISZoneCollection) thisCalque.modeleDonnees().getGeomData();
    points.updateListeners();
    if (model != null) {
      model.setPointsAdapter(new TriangleNodeAdapter(points));
      thisCalque.getLayerGridDelegate().setGridLayer(model);
      //on r�initialise les propri�t�s por
      thisCalque.initFrom(_saver.getUI());

    }
    return cqName;
  }

  protected GISDataStoreZoneExporter createExporter() {
    final GISDataStoreZoneExporter res = super.createExporter();
    // Devrait �tre mis par d�faut.
    res.setUseIdAsName(true);
    return res;
  }

  public void saveGrid(LayerGridController layerGridDelegate, BCalque _cqToSave, String _parentDirEntry, BCalqueSaverInterface res, CtuluArkSaver _saver) {
    //le maillage:
    final EfGridInterface grid = layerGridDelegate.getExportGridData().getGrid();
    final int ptsNb = grid.getPtsNb();
    final int nbElt = grid.getEltNb();
    int[] ipobo = grid.getFrontiers().getArray();
    double[] x = new double[ptsNb];
    double[] y = new double[ptsNb];
    int[][] elt = new int[nbElt][];
    for (int idxPt = 0; idxPt < ptsNb; idxPt++) {
      x[idxPt] = grid.getPtX(idxPt);
      y[idxPt] = grid.getPtY(idxPt);
    }
    for (int idxElt = 0; idxElt < nbElt; idxElt++) {
      EfElement element = grid.getElement(idxElt);
      elt[idxElt] = element.getIndices();
    }


    try {
      DodicoArraysBinaryFileSaver saver = new DodicoArraysBinaryFileSaver();
      String entry = getEntryBase(_cqToSave, _parentDirEntry, res.getId()) + GRID_X_EXT;
      _saver.startEntry(entry);
      saver.saveDouble(_saver.getOutStream(), x);
      _saver.getOutStream().flush();
      entry = getEntryBase(_cqToSave, _parentDirEntry, res.getId()) + GRID_Y_EXT;
      _saver.startEntry(entry);
      saver.saveDouble(_saver.getOutStream(), y);
      _saver.getOutStream().flush();
      entry = getEntryBase(_cqToSave, _parentDirEntry, res.getId()) + GRID_ELT_EXT;
      _saver.startEntry(entry);
      saver.save(_saver.getOutStream(), elt);
      _saver.getOutStream().flush();
      entry = getEntryBase(_cqToSave, _parentDirEntry, res.getId()) + GRID_IPOBO_EXT;
      _saver.startEntry(entry);
      saver.saveSimpleArray(_saver.getOutStream(), ipobo);
      _saver.getOutStream().flush();

    } catch (final Exception _evt) {
      FuLog.error(_evt);
    }
  }

  public void saveCorrespondance(LayerGridController layerGridDelegate, BCalque _cqToSave, String _parentDirEntry, BCalqueSaverInterface res, CtuluArkSaver _saver) {
    //le maillage:
    MdlElementModel layerModel = layerGridDelegate.getGridLayer().modele();
    final String entryCorrespondance = getEntryBase(_cqToSave, _parentDirEntry, res.getId()) + CORRESPONDANCE_EXT;
    try {
      _saver.startEntry(entryCorrespondance);
      int[][] corr = layerModel.getCorrespondance();
      DodicoArraysBinaryFileSaver saver = new DodicoArraysBinaryFileSaver();
      saver.save(_saver.getOutStream(), corr);
      _saver.getOutStream().flush();
      final String entryPainted = getEntryBase(_cqToSave, _parentDirEntry, res.getId()) + CORRESPONDANCE_PAINTED_EXT;
      _saver.startEntry(entryPainted);
      final int[] selectedIndex = layerModel.getGisNodePaintedInIso().getSelectedIndex();
      saver.saveSimpleArray(_saver.getOutStream(), selectedIndex);
      _saver.getOutStream().flush();
    } catch (final Exception _evt) {
      FuLog.error(_evt);
    }
  }

  public void saveUserHiddenMeshes(LayerGridController layerGridDelegate, BCalque _cqToSave, String _parentDirEntry, BCalqueSaverInterface res, CtuluArkSaver _saver) {
    //le maillage:
    boolean[] painted = layerGridDelegate.getGridLayer().modele().getUserEltPainted();
    if (painted != null) {
      final String entry = getEntryBase(_cqToSave, _parentDirEntry, res.getId()) + PAINTED_EXT;
      try {
        _saver.startEntry(entry);
        DodicoArraysBinaryFileSaver saver = new DodicoArraysBinaryFileSaver();
        saver.saveBoolean(_saver.getOutStream(), painted);
        _saver.getOutStream().flush();
      } catch (final Exception _evt) {
        FuLog.error(_evt);
      }
    }

  }

  public EfGrid readGrid(CtuluArkLoader _loader, String _parentDirEntry, String _entryName, ProgressionInterface _proj) {
    EfGrid grid = null;
    InputStream reader = _loader.getReader(_parentDirEntry, _entryName + GRID_X_EXT);
    if (reader != null) {
      DodicoArraysBinaryFileSaver saver = new DodicoArraysBinaryFileSaver();
      double[] x = saver.loadDouble(reader);
      reader = _loader.getReader(_parentDirEntry, _entryName + GRID_Y_EXT);
      if (reader != null) {
        double[] y = saver.loadDouble(reader);
        reader = _loader.getReader(_parentDirEntry, _entryName + GRID_ELT_EXT);
        int[][] elt = saver.load(reader);
        if (x != null && y != null && elt != null) {
          EfNode[] nodes = new EfNode[x.length];
          EfElement[] elts = new EfElement[elt.length];
          for (int i = 0; i < nodes.length; i++) {
            nodes[i] = new EfNode(x[i], y[i], 0);
          }
          for (int i = 0; i < elts.length; i++) {
            elts[i] = new EfElement(elt[i]);
          }
          grid = new EfGrid(nodes, elts);
          reader = _loader.getReader(_parentDirEntry, _entryName + GRID_IPOBO_EXT);
          int[] ipobo = saver.loadSimpleArray(reader);
          if (ipobo != null) {
            grid.computeBordFast(ipobo, _proj, new CtuluAnalyze());
          } else {
            grid.computeBord(_proj, new CtuluAnalyze());
          }
        }




      }

    }
    return grid;
  }
}
