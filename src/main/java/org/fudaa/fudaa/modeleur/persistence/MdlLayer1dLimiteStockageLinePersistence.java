/*
 * @creation     28 janv. 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.persistence;

import org.fudaa.fudaa.modeleur.layer.MdlLayer1dLimiteStockage;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dLine;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 * Gestion de la persistance d'un calque contenant des limites de stockage.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class MdlLayer1dLimiteStockageLinePersistence extends MdlLayer2dLinePersistence {

  @Override
  protected MdlLayer2dLine createNewLayer(FSigEditor _editor) {
    return new MdlLayer1dLimiteStockage(_editor);
  }
}
