/*
 * @creation     20 janv. 08
 * @modification $Date: 2008-12-01 12:02:10 +0100 (lun., 01 déc. 2008) $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.persistence;

import org.fudaa.fudaa.modeleur.layer.MdlLayer2dCasier;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 * Gestion de la persistance d'un calque contenant des casiers.
 * @author Bertrand Marchand
 * @version $Id$
 * @see MdlLayer2dCasier
 */
public class MdlLayer2dCasierPersistence extends MdlLayer2dLinePersistence {
  
  /**
   * Cree un calque du type correct, suivante le gestionnaire de persistence.
   */
  protected MdlLayer2dCasier createNewLayer(FSigEditor _editor) {
    return new MdlLayer2dCasier(_editor);
  }
}
