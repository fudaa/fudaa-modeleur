/*
 * @creation     29 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.persistence;

import org.fudaa.fudaa.modeleur.layer.MdlLayer1dBank;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dLine;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dProfile;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 * Gestion de la persistance pour un calque de lignes de rive.
 * @author Emmanuel MARTIN
 * @version $Id$
 * @see MdlLayer2dProfile
 */
public class MdlLayer2dBankPersistence extends MdlLayer2dLinePersistence {

  /* (non-Javadoc)
   * @see org.fudaa.fudaa.modeleur.persistence.MdlLayer2dLinePersistence#createNewLayer(org.fudaa.fudaa.sig.layer.FSigEditor)
   */
  @Override
  protected MdlLayer2dLine createNewLayer(FSigEditor _editor) {
    return new MdlLayer1dBank(_editor);
  }

}
