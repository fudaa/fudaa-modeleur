/*
 * @creation     20 janv. 08
 * @modification $Date: 2008/05/13 12:10:58 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.persistence;

import org.fudaa.fudaa.modeleur.layer.MdlLayer2dConstraintLine;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dLine;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dProfile;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 * Gestion de la persistance d'un calque contenant des lignes de contraintes.
 * @author Bertrand Marchand
 * @version $Id: MdlLayer1dAxePersistence.java 4212 2008-11-12 16:24:11Z bmarchan $
 * @see MdlLayer2dProfile
 */
public class MdlLayer2dConstraintLinePersistence extends MdlLayer2dLinePersistence {
  
  @Override
  protected MdlLayer2dLine createNewLayer(FSigEditor _editor) {
    return new MdlLayer2dConstraintLine(_editor);
  }
}
