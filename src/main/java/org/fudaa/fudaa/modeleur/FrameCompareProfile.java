package org.fudaa.fudaa.modeleur;

import com.memoire.bu.BuInternalFrame;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import static org.fudaa.ctulu.CtuluLibImage.PARAMS_FILL_BACKGROUND_BOOLEAN;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Internal frame manageable by the mdl implementation that is resizable and printable for report need.
 *
 * @author Adrien Hadoux
 */
public class FrameCompareProfile extends BuInternalFrame implements CtuluImageProducer {

    final MdlCompareProfilePanel panel_;

    public FrameCompareProfile(final MdlCompareProfilePanel panel) {
        super(MdlResource.getS("Comparaison de profils"), true, true, true, true);
        panel_ = panel;
        this.setSize(new Dimension(800, 600));
        this.setPreferredSize(new Dimension(800, 600));
        this.getContentPane().add(panel_);
        panel_.pnMain.setDividerLocation(0.2);
        panel_.pnLeft.setDividerLocation(0.5);
    }

    @Override
    public BufferedImage produceImage(Map _params) {
        return produceImage(panel_.getWidth(), panel_.getHeight(), _params);
    }

    @Override
    public BufferedImage produceImage(int _w, int _h, Map _params) {
        HashMap mapGraphe = new HashMap();
        mapGraphe.put(PARAMS_FILL_BACKGROUND_BOOLEAN, false);
        //-- compute image size--//
        BufferedImage imageGraphe = panel_.getViewCurve().getGraphe().produceImage(mapGraphe);
        BufferedImage imgLegende = null;
        //-- compute image  legende --//
        if (panel_.isCompareCurves()) {
            imgLegende = panel_.getPanelCompareCurves().produceImage(_params);
        }

        _w = imageGraphe.getWidth();
        _h = imageGraphe.getHeight();

        if (imgLegende != null) {
            _w = _w + imgLegende.getWidth() + 20;
            _h = Math.max(_h, imgLegende.getHeight());
        }
        BufferedImage bi = new BufferedImage(_w, _h, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = bi.createGraphics();

        g.setColor(Color.WHITE);
        g.fillRect(0, 0, _w, _h);
        g.drawImage(imageGraphe, 0, 0, null);

        if (imgLegende != null) {
            g.drawImage(imgLegende, imageGraphe.getWidth() + 10, 0, null);
        }
        return bi;

    }

    @Override
    public Dimension getDefaultImageDimension() {
        return panel_.getSize();
    }

}
