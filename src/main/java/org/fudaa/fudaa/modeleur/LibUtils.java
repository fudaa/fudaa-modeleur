/*
 * @creation     30 janv. 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.collection.CtuluCollection;
import org.fudaa.ctulu.gis.GISAttributeDouble;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrVecteur;
import org.locationtech.jts.geom.Coordinate;

/**
 * Quelques m�thodes utiles et ind�pendantes.
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class LibUtils {
  
  
  public static int getAttributeZPosition(GISZoneCollection zone){
    GISAttributeDouble attributeIsZ = zone.getAttributeIsZ();
    return zone==null?-1:zone.getIndiceOf(attributeIsZ);
  }

  /**
   * Rend rectiligne tout ou partie d'une polyligne.<p>
   * 
   * Si plus de deux points sont selectionn�s dans la g�om�trie (exemple idx :
   * 0, 3 et 4), on iterre sur les points selectionn�s deux � deux (donc 0 et 3
   * puis 3 et 4) pour chacune de ces it�rations on projete sur le segment les
   * points dont les indices sont entre les deux trait�s (premi�re it�ration :
   * projection du point 2, aucun pour la seconde). si la projection d'un point
   * n'appartient pas au segment, il est ignor�.
   * 
   * @param _zone La zone contenant la ligne a lin�ariser
   * @param _idxGeom L'index de la ligne dans la zone.
   * @param _idxPoints Les sommets entre lequels lin�ariser. Doit contenir au moins deux index de point.
   * @param _cmd Le manager de commande.
   */
  static public void linearisePolyligne(GISZoneCollectionLigneBrisee _zone, int _idxGeom, int[] _idxPoints, CtuluCommandContainer _cmd) {
    // Messages destin�s aux d�veloppeurs
    if(_zone==null||_idxPoints==null)
      throw new IllegalArgumentException("Error prog : _zone et _idxPoints ne doivent pas �tre null.");
    if(_idxGeom<0||_idxGeom>=_zone.getNbGeometries())
      throw new IllegalArgumentException("Error prog : _idxGeom n'appartient pas � _zone.");
    if(_idxPoints.length<2)
      throw new IllegalArgumentException("Error prog : Il doit y avoir au moins deux points selectionn�s.");
    for(int i=0;i<_idxPoints.length;i++)
      if(_idxPoints[i]<0||_idxPoints[i]>=_zone.getGeometry(_idxGeom).getNumPoints())
        throw new IllegalArgumentException("Error prog : Au moins un des index de _idxPoints est invalide.");
    
    /** Class contenant les infromations relatives � une coordonn�s projet�e. */
    class PointProj implements Comparable<PointProj> {
      public Coordinate c; // Coordonn� projet�e
      public double indiceCurviligne; // Indice Curviligne (k tq k*VecDirecteur=Vec(PtOrigine, c)) 
      public List<Object> data=new ArrayList<Object>(); // valeur des attributs atomiques
      /** M�thode pour utiliser les alorithmes de tris fournis par java. */
      public int compareTo(PointProj o) {
        if (indiceCurviligne<o.indiceCurviligne)
          return -1;
        else if (indiceCurviligne==o.indiceCurviligne)
          return 0;
        else
          return 1;
      }
    }
    CtuluCommandComposite cmd=new CtuluCommandComposite();
    // Pour chaque point selectionn�
    GISPolyligne polyligne=(GISPolyligne)_zone.getGeometry(_idxGeom);
    List<Coordinate> newCoords=new ArrayList<Coordinate>(polyligne.getNumPoints());
    for (int j=0; j<polyligne.getNumPoints(); j++)
      newCoords.add(polyligne.getCoordinateN(j));

    // Tableau contenant les valeurs d'attributs de la g�om�trie (les atomiques
    // sont dupliqu�s)
    Object[] newData=new Object[_zone.getNbAttributes()];
    for (int j=0; j<_zone.getNbAttributes(); j++) 
      if (_zone.getAttribute(j).isAtomicValue()) {
        Object[] values=((GISAttributeModel)_zone.getDataModel(j).getObjectValueAt(_idxGeom)).getObjectValues();
        List<Object> lst=new ArrayList<Object>(values.length);
        for (int k=0; k<values.length; k++)
          lst.add(values[k]);
        newData[j]=lst;
      }
      else
        newData[j]=_zone.getDataModel(j).getObjectValueAt(_idxGeom);
    /*
     * Si plus d'un point est selectionn� dans la g�om�trie (exemple idx :
     * 0, 3 et 4), on iterre sur les points selectionn�s deux � deux (donc 0
     * et 3 puis 3 et 4) pour chacune de ces it�rations on projete sur le
     * segment les points dont les indices sont entre les deux trait�s
     * (premi�re it�ration : projection du point 2, aucun pour la seconde).
     * si la projection d'un point n'appartient pas au segment, il est
     * ignor�.
     * Sinon on utilise le premier et le dernier point de la
     * polyligne comme destination de la projection. pt1 et pt2 sont ces
     * deux points de r�f�rence.
     */
    // Couples contients les couples d'indices � traiter.
    Arrays.sort(_idxPoints);
    int[][] couples;
    couples=new int[_idxPoints.length-1][];
    for (int j=0; j<couples.length; j++)
      couples[j]=new int[]{_idxPoints[j], _idxPoints[j+1]};
    // Iterration sur chacun des couples d'index
    for (int j=couples.length-1; j>=0; j--) {
      int idx1=couples[j][0];
      int idx2=couples[j][1];
      assert (idx1<idx2);
      GrPoint pt1=new GrPoint(newCoords.get(idx1));
      GrPoint pt2=new GrPoint(newCoords.get(idx2));

      // Vecteur de la droite de destination
      GrVecteur vecP1P2=new GrVecteur(pt2.x_-pt1.x_, pt2.y_-pt1.y_, pt2.z_-pt1.z_);
      // Construction de l'ensemble des points modifi�s \\
      List<PointProj> lstNewPtsCurv=new ArrayList<PointProj>();
      for (int k=1; k<idx2-idx1; k++) {
        GrPoint a=new GrPoint(polyligne.getCoordinateSequence().getCoordinate(k+idx1));
        // Projection
        GrVecteur vecP1A=new GrVecteur(a.x_-pt1.x_, a.y_-pt1.y_, a.z_-pt1.z_);
        GrPoint p=pt1.addition(vecP1A.projection(vecP1P2));
        Coordinate cp=new Coordinate(p.x_, p.y_, p.z_);
        // Remplissage des informations relatives au point calcul�
        PointProj pointProj=new PointProj();
        pointProj.c=cp;
        pointProj.indiceCurviligne=new GrVecteur(cp.x-pt1.x_, cp.y-pt1.y_, cp.z-pt1.z_).division(vecP1P2);
        pointProj.indiceCurviligne=((double) ((long) (pointProj.indiceCurviligne*10000)))/10000.;
        if (pointProj.indiceCurviligne>=0&&pointProj.indiceCurviligne<=1) {
          for (int l=0; l<_zone.getNbAttributes(); l++)
            if (_zone.getAttribute(l).isAtomicValue())
              pointProj.data.add(((CtuluCollection)_zone.getValue(l, _idxGeom)).getObjectValueAt(k+idx1));
          lstNewPtsCurv.add(pointProj);
        }
      }
      PointProj[] newPtsCurv=new PointProj[lstNewPtsCurv.size()];
      newPtsCurv=lstNewPtsCurv.toArray(newPtsCurv);
      // Tri des points en fonction de leur abcisse curviligne \\
      Arrays.sort(newPtsCurv);
      // Enregistrement des coordonn�s et suppression des points disparus
      for (int k=newPtsCurv.length-1; k>=0; k--)
        newCoords.set(k+idx1+1, newPtsCurv[k].c);
      for (int k=idx2-1; k>idx1+newPtsCurv.length; k--)
        newCoords.remove(k);
      // R�cup�ration des values des attributs atomiques
      int nemeAttAtomique=0;
      for (int k=0; k<_zone.getNbAttributes(); k++)
        if (_zone.getAttribute(k).isAtomicValue()) {
          List<Object> lst=(List<Object>)newData[k];
          for (int l=newPtsCurv.length-1; l>=0; l--)
            lst.set(l+idx1+1, newPtsCurv[l].data.get(nemeAttAtomique));
          for (int l=idx2-1; l>idx1+newPtsCurv.length; l--)
            lst.remove(l+idx1);
          nemeAttAtomique++;
        }
    }
    // Remplacement de l'ancienne g�om�trie \\
    _zone.setGeometry(_idxGeom, GISGeometryFactory.INSTANCE.createLineString(newCoords.toArray(new Coordinate[0])), cmd);
    for (int j=0; j<_zone.getNbAttributes(); j++) {
      GISAttributeInterface attr=_zone.getAttribute(j);
      if (attr.isAtomicValue()) {
        Object[] values=((List<Object>)newData[j]).toArray();
        _zone.setAttributValue(j, _idxGeom, attr.createDataForGeom(values, values.length), cmd);
      }
      else
        _zone.setAttributValue(j, _idxGeom, newData[j], cmd);
    }
    // Mise � jour du undo/redo
    if (_cmd!=null)
      _cmd.addCmd(cmd.getSimplify());
  } 
}
