/*
 * @creation     20 janv. 08
 * @modification $Date: 2008/05/13 12:10:42 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.layer;

import java.awt.Color;

import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ebli.calque.BCalquePersistenceInterface;
import org.fudaa.ebli.calque.edition.ZModeleMultiPointEditable;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.modeleur.persistence.MdlLayer2dCloudPersistence;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 * Un calque pour le stockage et la manipulation des nuages de points.
 *
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MdlLayer2dCloud extends MdlLayer2dMultiPoint {

  public final static String EXT_NAME = "cloud";

  /**
   * Constructeur. Utilise un mod�le de donn�es et un editeur.
   *
   * @param _model Modele
   * @param _editor Editeur.
   */
  private MdlLayer2dCloud(ZModeleMultiPointEditable _model, final FSigEditor _editor) {
    super(_model, _editor);
    setName(getExtName());
    setIconModel(0, new TraceIconModel(TraceIcon.CARRE_PLEIN, 2, new Color(0, 190, 0)));
    setTitle(MdlResource.getS("Semis de points"));
    setAttributForLabels(GISAttributeConstants.LABEL);
  }

  public MdlLayer2dCloud(FSigEditor _editor) {
    this(new MdlModel2dCloud(_editor, _editor == null ? null : _editor.getMng()), _editor);
  }

//  public static MdlLayer2dCloud createNew(FSigEditor _editor) {
//    MdlLayer2dCloud cq=new MdlLayer2dCloud(new MdlModel2dCloud(_editor,_editor.getMng()), _editor);
//    return cq;
//  }
  /**
   * Le manager de persistence. Celui par defaut ne permet par une restauration des lignes de la g�ometrie, qui sont restitu�es uniquement si le
   * groupe est de type ZCalqueEditionGroup.
   */
  public BCalquePersistenceInterface getPersistenceMng() {
    return new MdlLayer2dCloudPersistence();
  }

  /**
   * Retourne le nom par defaut du calque.
   *
   * @return Le nom.
   */
  public String getExtName() {
    return EXT_NAME;
  }
}
