/*
 * @creation     20 janv. 08
 * @modification $Date: 2008/05/13 12:10:43 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.layer;

import java.awt.Color;

import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ebli.calque.BCalquePersistenceInterface;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.modeleur.persistence.MdlLayer2dZonePersistence;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 * Un calque pour le stockage et la manipulation des zones g�om�triques 2d.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MdlLayer2dZone extends MdlLayer2dLine {
  public final static String EXT_NAME="zone";
  
  /**
   * Constructeur. Utilise un mod�le de donn�es et un editeur.
   * @param _model Modele
   * @param _editor Editeur.
   */
  private MdlLayer2dZone(ZModeleLigneBriseeEditable _model,  final FSigEditor _editor) {
    super(_model,_editor);
    setLineModel(0, new TraceLigneModel(TraceLigne.TIRETE, 2f, new Color(0,153,153)));
    setLineModelOuvert(getLineModel(0));
    setIconModel(0, new TraceIconModel(TraceIcon.CARRE_PLEIN, 2, new Color(0,153,153).darker()));
    setIconModel(1, new TraceIconModel(getIconModel(0)));
    setName(getExtName());
    setTitle(MdlResource.getS("Zones"));
    setAttributForLabels(GISAttributeConstants.LABEL);
  }
  
  public MdlLayer2dZone(FSigEditor _editor) {
    this(new MdlModel2dZone(_editor,_editor==null ? null:_editor.getMng()),_editor);
  }

//  public static MdlLayer2dZone createNew(FSigEditor _editor) {
//    MdlLayer2dZone cq=new MdlLayer2dZone(new MdlModel2dZone(_editor,_editor.getMng()), _editor);
//    return cq;
//  }

  public BCalquePersistenceInterface getPersistenceMng() {
    return new MdlLayer2dZonePersistence();
  }

  /**
   * Retourne le nom par defaut du calque.
   * @return Le nom.
   */
  public String getExtName() {
    return EXT_NAME;
  }
  
  public boolean canAddForme(int _typeForme) {
    return _typeForme==DeForme.POLYGONE | _typeForme==DeForme.RECTANGLE | _typeForme==DeForme.ELLIPSE;
  }
}
