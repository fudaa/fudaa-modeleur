/*
 * @creation     20 janv. 08
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.layer;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.gis.GISAttribute;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ebli.calque.ZModelGeometryListener;

/**
 * Un mod�le de calque pour le calque 1D des traces de profils. Les traces sont des polylignes XY.
 * 
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MdlModel1dTrace extends MdlModel2dLine {

  /**
   * Construction d'un modele de trace avec pile de commandes.
   * @param _cmd La pile de commandes pour le undo/redo.
   */
  public MdlModel1dTrace(final ZModelGeometryListener _listener, final CtuluCommandContainer _cmd) {
    super(_listener);
    GISAttribute[] attrs=new GISAttribute[]{
        GISAttributeConstants.TITRE,
        GISAttributeConstants.ETAT_GEOM,
        GISAttributeConstants.HISTORY,
        GISAttributeConstants.NATURE,
        GISAttributeConstants.VISIBILITE,
        GISAttributeConstants.LABEL
    };
    // Pas de container de commande pour cette op�ration, sinon conserv� en undo/redo.
    getGeomData().setAttributes(attrs, null);
    getGeomData().setAttributeIsZ(null);
    getGeomData().setFixedAttributeValue(GISAttributeConstants.NATURE, GISAttributeConstants.ATT_NATURE_TP);
  }
}
