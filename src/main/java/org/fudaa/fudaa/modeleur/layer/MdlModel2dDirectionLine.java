/*
 * @creation     20 janv. 08
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.layer;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.gis.GISAttribute;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ebli.calque.ZModelGeometryListener;

/**
 * Un mod�le de calque pour le calque 2D des lignes directrices. Les lignes sont des polylignes XYZ, non
 * ferm�es.
 * 
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MdlModel2dDirectionLine extends MdlModel2dLine {

  /**
   * Construction d'un modele avec pile de commandes.
   * @param _cmd La pile de commandes pour le undo/redo.
   */
  public MdlModel2dDirectionLine(final ZModelGeometryListener _listener, final CtuluCommandContainer _cmd) {
    super(_listener);
    // Pas de container de commande pour cette op�ration, sinon conserv� en undo/redo.
    normalizeZone(getGeomData(), null);
  }

  /**
   * Normalise une zone pour etre en accord avec ce mod�le.
   * @param _zone La zone
   * @param _cmd La pile de commandes pour le undo/redo.
   * @param _forBief True : Modele pour un bief.
   */
  public static void normalizeZone(GISZoneCollectionLigneBrisee _zone, CtuluCommandContainer _cmd) {
    GISAttribute[] attrs=new GISAttribute[]{
      GISAttributeConstants.BATHY,
      GISAttributeConstants.ETAT_GEOM,
      GISAttributeConstants.TITRE,
      GISAttributeConstants.HISTORY,
      GISAttributeConstants.NATURE,
      GISAttributeConstants.VISIBILITE,
      GISAttributeConstants.LABEL
    };

    _zone.setAttributes(attrs, _cmd);
    _zone.setAttributeIsZ(GISAttributeConstants.BATHY);
    _zone.setFixedAttributeValue(GISAttributeConstants.NATURE, GISAttributeConstants.ATT_NATURE_LD);
  }
}
