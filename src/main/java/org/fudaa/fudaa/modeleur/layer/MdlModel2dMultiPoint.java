/*
 * @creation     20 janv. 08
 * @modification $Date: 2008-05-13 12:10:39 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.layer;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISMultiPoint;
import org.fudaa.ctulu.gis.GISZoneCollectionMultiPoint;
import org.fudaa.ebli.calque.ZModelGeometryListener;
import org.fudaa.ebli.calque.edition.ZModeleMultiPointEditable;
import org.locationtech.jts.geom.Coordinate;

/**
 * Un mod�le de calque pour un calque contenant des multipoints.
 * 
 * @author Bertrand Marchand
 * @version $Id: MdlModel2dMultiPoint.java,v 1.1.2.1 2008-05-13 12:10:39 bmarchan Exp $
 */
public class MdlModel2dMultiPoint extends ZModeleMultiPointEditable {

  /**
   * Construction d'un modele de profil avec pile de commandes.
   */
  public MdlModel2dMultiPoint(final ZModelGeometryListener _listener) {
    super(new GISZoneCollectionMultiPoint(null));
    geometries_.setListener(this);
    addModelListener(_listener);
  }

  /**
   * Interpole entre 2 points, suivant un nombre de points donn�s.
   * @param _coords Les 2 coordonn�es
   * @param _dist La distance entre 2 points cons�cutifs.
   * @param _useBorderPoints vrai si la ligne int�pol�e doit contenir les coordonn�es fournies.
   * @return true si modif ok.
   */
  public boolean interpolate(Coordinate[] _coords, double _dist, boolean _useBorderPoints, final CtuluCommandContainer _cmd){
    double distTot=
      Math.sqrt((_coords[1].x-_coords[0].x)*(_coords[1].x-_coords[0].x)+(_coords[1].y-_coords[0].y)*(_coords[1].y-_coords[0].y));
    int nbpts=(int)Math.round(distTot/_dist)-1;
    Coordinate[] coords;
    if (_useBorderPoints) {
      coords=new Coordinate[_coords.length+nbpts];
      for (int i=1; i<coords.length-1; i++)
        coords[i]=new Coordinate(
            _coords[0].x+(_coords[1].x-_coords[0].x)*i*_dist/distTot,
            _coords[0].y+(_coords[1].y-_coords[0].y)*i*_dist/distTot,
            _coords[0].z+(_coords[1].z-_coords[0].z)*i*_dist/distTot
        );
      coords[0]=(Coordinate)_coords[0].clone();
      coords[coords.length-1]=(Coordinate)_coords[1].clone();
    }
    else {
      coords=new Coordinate[nbpts];
      for (int i=0; i<coords.length; i++)
        coords[i]=new Coordinate(
            _coords[0].x+(_coords[1].x-_coords[0].x)*(i+1)*_dist/distTot,
            _coords[0].y+(_coords[1].y-_coords[0].y)*(i+1)*_dist/distTot,
            _coords[0].z+(_coords[1].z-_coords[0].z)*(i+1)*_dist/distTot
        );
    }
    if (coords.length>=2) {
      GISMultiPoint poly=(GISMultiPoint)GISGeometryFactory.INSTANCE.createMultiPoint(coords);
      int idx=geometries_.addGeometry(poly, null, _cmd);
      // Modification de l'etat de la g�ometrie en modifi�e
      if (idx!=-1)
        setGeomModif(idx, _cmd);
      return true;
    }
    else
      return false;
  }
}
