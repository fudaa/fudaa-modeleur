/*
 * @creation     20 janv. 08
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.layer;

import java.awt.Color;

import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ebli.calque.BCalquePersistenceInterface;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.modeleur.persistence.MdlLayer2dContourPersistence;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 * Un calque pour le stockage et la manipulation des contours d'�tude 2d.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MdlLayer2dContour extends MdlLayer2dLine {
  public final static String EXT_NAME="contour";
  
  /**
   * Constructeur. Utilise un mod�le de donn�es et un editeur.
   * @param _model Modele
   * @param _editor Editeur.
   */
  private MdlLayer2dContour(ZModeleLigneBriseeEditable _model,  final FSigEditor _editor) {
    super(_model,_editor);
    setLineModel(0, new TraceLigneModel(TraceLigne.TIRETE, 2f, new Color(153,92,0)));
    setLineModelOuvert(getLineModel(0));
    setIconModel(0, new TraceIconModel(TraceIcon.CARRE_PLEIN, 2, new Color(153,92,0).darker()));
    setIconModel(1, new TraceIconModel(getIconModel(0)));
    setName(getExtName());
    setTitle(MdlResource.getS("Contours d'�tude"));
    setAttributForLabels(GISAttributeConstants.LABEL);
  }
  
  public MdlLayer2dContour(FSigEditor _editor) {
    this(new MdlModel2dContour(_editor,_editor==null ? null:_editor.getMng()),_editor);
  }
  
//  public static MdlLayer2dContour createNew(FSigEditor _editor) {
//    MdlLayer2dContour cq=new MdlLayer2dContour(new MdlModel2dContour(_editor,_editor.getMng()), _editor);
//    return cq;
//  }

  public BCalquePersistenceInterface getPersistenceMng() {
    return new MdlLayer2dContourPersistence();
  }

  /**
   * Retourne le nom par defaut du calque.
   * @return Le nom.
   */
  public String getExtName() {
    return EXT_NAME;
  }
  
  public boolean canAddForme(int _typeForme) {
    return _typeForme==DeForme.POLYGONE | _typeForme==DeForme.RECTANGLE | _typeForme==DeForme.ELLIPSE;
  }
}
