/*
 * @creation     20 janv. 08
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.layer;

import java.util.Arrays;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISCoordinateSequenceFactory;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ebli.calque.ZModelGeometryListener;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.LineString;

import gnu.trove.TIntArrayList;

/**
 * Un mod�le de calque pour un calque 2D contenant des lignes.
 *
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MdlModel2dLine extends ZModeleLigneBriseeEditable {

  /**
   * Construction d'un modele de profil avec pile de commandes.
   */
  public MdlModel2dLine(final ZModelGeometryListener _listener) {
    this(_listener, new GISZoneCollectionLigneBrisee(null));
  }

  /**
   * Construction d'un modele de profil avec pile de commandes.
   *
   * @param _listener Un listener du modele.
   * @param _zone La zone utilis�e par le mod�le.
   */
  public MdlModel2dLine(final ZModelGeometryListener _listener, final GISZoneCollectionLigneBrisee _zone) {
    super(_zone);
    geometries_.setListener(this);
    addModelListener(_listener);
  }

  /**
   * Interpole entre 2 points, suivant un nombre de points donn�s.
   *
   * @param _coords Les 2 coordonn�es
   * @param _dist La distance entre 2 points cons�cutifs.
   * @param _useBorderPoints vrai si la ligne int�pol�e doit contenir les coordonn�es fournies.
   * @param _cmd Le container de commandes.
   * @return true si modif ok.
   */
  public boolean interpolate(Coordinate[] _coords, double _dist, boolean _useBorderPoints, final CtuluCommandContainer _cmd) {
    double distTot =
            Math.sqrt((_coords[1].x - _coords[0].x) * (_coords[1].x - _coords[0].x) + (_coords[1].y - _coords[0].y) * (_coords[1].y - _coords[0].y));
    int nbpts = (int) Math.round(distTot / _dist) - 1;
    Coordinate[] coords;
    if (_useBorderPoints) {
      coords = new Coordinate[_coords.length + nbpts];
      for (int i = 1; i < coords.length - 1; i++) {
        coords[i] = new Coordinate(
                _coords[0].x + (_coords[1].x - _coords[0].x) * i * _dist / distTot,
                _coords[0].y + (_coords[1].y - _coords[0].y) * i * _dist / distTot,
                _coords[0].z + (_coords[1].z - _coords[0].z) * i * _dist / distTot);
      }
      coords[0] = (Coordinate) _coords[0].clone();
      coords[coords.length - 1] = (Coordinate) _coords[1].clone();
    } else {
      coords = new Coordinate[nbpts];
      for (int i = 0; i < coords.length; i++) {
        coords[i] = new Coordinate(
                _coords[0].x + (_coords[1].x - _coords[0].x) * (i + 1) * _dist / distTot,
                _coords[0].y + (_coords[1].y - _coords[0].y) * (i + 1) * _dist / distTot,
                _coords[0].z + (_coords[1].z - _coords[0].z) * (i + 1) * _dist / distTot);
      }
    }
    if (coords.length >= 2) {
      GISPolyligne poly = (GISPolyligne) GISGeometryFactory.INSTANCE.createLineString(coords);
      CtuluCommandComposite cmd = new CtuluCommandComposite();
      int idx = getGeomData().addGeometry(poly, null, cmd);
      if (idx != -1) {
        setGeomModif(idx, cmd);
      }
      if (_cmd != null) {
        _cmd.addCmd(cmd.getSimplify());
      }
      return true;
    } else {
      return false;
    }
  }

  /**
   * Decime une ligne. La m�thode utilis�e pour d�cimer peut �tre :<p> 0 : Suivant un nombre de points a supprimer entre 2 points conserv�s.<br> 1 :
   * Suivant une distance minimale.
   *
   * @param _idxGeom La g�om�trie a d�cimer.
   * @param _idxdeb L'indice de d�but de d�cimation.
   * @param _idxfin L'indice de fin de d�cimation.
   * @param _meth La m�thode.
   * @param _nbpts Le nombre de points.
   * @param _dstmin La distance minimale.
   * @param _cmd Le container de commandes.
   * @return le nombre de points enlev�
   */
  public int decimate(int _idxGeom, int _idxdeb, int _idxfin, int _meth, int _nbpts, double _dstmin, CtuluCommandContainer _cmd) {
    TIntArrayList list = new TIntArrayList();

    // Methode par nombre de points.
    if (_meth == 0) {
      int idx = _idxdeb + 1 + _nbpts;
      for (int i = _idxdeb + 1; i <= _idxfin - 1; i++) {
        if (i < idx) {
          list.add(i);
        } else {
          idx = i + 1 + _nbpts;
        }
      }
    } // Methode par distance mini.
    else if (_meth == 1) {
      LineString geom = (LineString) getGeomData().getGeometry(_idxGeom);
      CoordinateSequence seq = geom.getCoordinateSequence();
      double dst = 0;
      for (int i = _idxdeb + 1; i <= _idxfin - 1; i++) {
        dst += Math.sqrt((seq.getX(i) - seq.getX(i - 1)) * (seq.getX(i) - seq.getX(i - 1))
                + (seq.getY(i) - seq.getY(i - 1)) * (seq.getY(i) - seq.getY(i - 1)));
        if (dst < _dstmin) {
          list.add(i);
        } else {
          dst = 0;
        }
      }
    }

    CtuluListSelectionInterface ids = new CtuluListSelection(list.toNativeArray());
    CtuluCommandComposite cmd = new CtuluCommandComposite();
    getGeomData().removeAtomics(_idxGeom, ids, null, cmd); // L'UI est null, le cas ou moins de 2 points ne peut pas arriver.
    setGeomModif(_idxGeom, cmd); // Modification de l'etat de la g�om�trie
    if (_cmd != null) {
      _cmd.addCmd(cmd.getSimplify());
    }
    return ids.getNbSelectedIndex();
  }

  /**
   * Raffine une ligne. La m�thode utilis�e pour raffiner peut �tre :<p> 0 : Suivant un nombre de points a ajouter entre 2 points.<br> 1 : Suivant une
   * distance maximale.
   *
   * @param _idxGeom La g�om�trie a raffiner.
   * @param _idxdeb L'indice de d�but de raffinement.
   * @param _idxfin L'indice de fin de raffinement.
   * @param _meth La m�thode.
   * @param _nbpts Le nombre de points.
   * @param _dstmax La distance maximale.
   * @param _cmd Le container de commandes.
   * @return le nombre de points ajout�s
   */
  public int refine(int _idxGeom, int _idxdeb, int _idxfin, int _meth, int _nbpts, double _dstmax, CtuluCommandContainer _cmd) {
    LineString geom = (LineString) getGeomData().getGeometry(_idxGeom);
    CoordinateSequence seq = GISLib.refine(geom.getCoordinateSequence(), _idxdeb, _idxfin, _meth, _nbpts, _dstmax);

    GISPolyligne newgeom = (GISPolyligne) GISGeometryFactory.INSTANCE.createLineString(seq);
    CtuluCommandComposite cmd = new CtuluCommandComposite();
    getGeomData().setGeometry(_idxGeom, newgeom, cmd);
    setGeomModif(_idxGeom, cmd); // Modification de l'etat de la g�om�trie
    if (_cmd != null) {
      _cmd.addCmd(cmd.getSimplify());
    }
    return seq.size() - geom.getNumPoints();
  }

  /**
   * Sort the points of selected polylignes. This sorting is done on the _icoord.
   *
   * @param _icoord 0: x, 1: y.
   */
  public void organizePoints(int _idxGeom, int _icoord, CtuluCommandContainer _cmd) {

    /**
     * This class contain needed data to sort points.
     */
    class SortableIndex implements Comparable<SortableIndex> {

      public double coord;
      private int ind;

      public SortableIndex(double _coord, int _ind) {
        ind = _ind;
        coord = _coord;
      }

      public int compareTo(SortableIndex o) {
        if (coord < o.coord) {
          return -1;
        } else if (coord == o.coord) {
          return 0;
        } else {
          return 1;
        }
      }
    }

    CtuluCommandComposite cmp = new CtuluCommandComposite(MdlResource.getS("R�organisation de la g�om�trie"));

    GISZoneCollection zone = getGeomData();
    CoordinateSequence oldseq = zone.getCoordinateSequence(_idxGeom);
    // Construction de la structure de tri
    SortableIndex[] points = new SortableIndex[oldseq.size()];
    for (int j = 0; j < points.length; j++) {
      points[j] = new SortableIndex(oldseq.getOrdinate(j, _icoord), j);
    }
    Arrays.sort(points);

    // Remplacement de la g�om�trie r�ordonn�e.
    Coordinate[] coords = new Coordinate[points.length];
    for (int j = 0; j < points.length; j++) {
      coords[j] = oldseq.getCoordinateCopy(points[j].ind);
    }
    zone.setCoordinateSequence(_idxGeom, new GISCoordinateSequenceFactory().create(coords), cmp);

    // R�ordonnancement des attributs atomiques.
    for (int iatt = 0; iatt < zone.getNbAttributes(); iatt++) {
      if (zone.getAttribute(iatt).isAtomicValue()) {
        GISAttributeModel oldvalues = (GISAttributeModel) zone.getDataModel(iatt).getObjectValueAt(_idxGeom);
        GISAttributeModel newvalues = (GISAttributeModel) zone.getDataModel(iatt).getAttribute().createDataForGeom(oldvalues, oldvalues.getSize());
        for (int j = 0; j < oldvalues.getSize(); j++) {
          newvalues.setObject(j, oldvalues.getObjectValueAt(points[j].ind), null);
        }
        zone.setAttributValue(iatt, _idxGeom, newvalues, cmp);
      }
    }

    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
  }
}
