/*
 * @creation     20 janv. 08
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.layer;


import org.fudaa.ebli.calque.BCalquePersistenceInterface;
import org.fudaa.ebli.calque.edition.ZModeleMultiPointEditable;
import org.fudaa.fudaa.modeleur.grid.MdlCalqueMultiPointEditable;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 * Une classe m�re a implementer pour tous les calques stockant et manipulant des multipoints.
 *
 * @author Bertrand Marchand
 * @version $Id$
 */
public abstract class MdlLayer2dMultiPoint extends MdlCalqueMultiPointEditable implements MdlLayerInterface {

 

  /**
   * Constructeur. Utilise un mod�le de donn�es et un editeur.
   *
   * @param _model Modele
   * @param _editor Editeur.
   */
  public MdlLayer2dMultiPoint(ZModeleMultiPointEditable _model, final FSigEditor _editor) {
    super(_model, _editor);
    setDestructible(true);
    
  }

  @Override
  public abstract BCalquePersistenceInterface getPersistenceMng();

  

  

  @Override
  public boolean canSetSelectable() {
    return true;
  }
}
