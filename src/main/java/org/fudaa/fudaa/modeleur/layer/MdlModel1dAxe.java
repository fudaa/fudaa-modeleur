/*
 * @creation     20 janv. 08
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.layer;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.gis.GISAttribute;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ebli.calque.ZModelGeometryListener;

/**
 * Un mod�le de calque pour le calque 1D des axes hydrauliques. Les axes sont des polylignes XYZ.
 * 
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MdlModel1dAxe extends MdlModel2dLine {

  /**
   * Construction d'un modele d'axes.
   */
  public MdlModel1dAxe(final ZModelGeometryListener _listener) {
    super(_listener);
    // Pas de container de commande pour cette op�ration, sinon conserv� en undo/redo.
    normalizeZone(getGeomData(), null);
  }

  /**
   * Normalise une zone pour etre en accord avec ce mod�le.
   * @param _zone La zone
   * @param _cmd La pile de commandes pour le undo/redo.
   * @param _forBief True : Modele pour un bief.
   */
  public static void normalizeZone(GISZoneCollectionLigneBrisee _zone, CtuluCommandContainer _cmd) {
    GISAttribute[] attrs=new GISAttribute[]{
      GISAttributeConstants.ETAT_GEOM,
      GISAttributeConstants.TITRE,
      GISAttributeConstants.HISTORY,
      GISAttributeConstants.NATURE,
      GISAttributeConstants.VISIBILITE,
      GISAttributeConstants.LABEL,
      GISAttributeConstants.CURVILIGNE_DECALAGE
    };

    _zone.setAttributes(attrs, _cmd);
    _zone.setFixedAttributeValue(GISAttributeConstants.NATURE, GISAttributeConstants.ATT_NATURE_AH);
  }
}
