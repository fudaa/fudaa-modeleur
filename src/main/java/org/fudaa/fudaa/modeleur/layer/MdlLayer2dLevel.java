/*
 * @creation     20 janv. 08
 * @modification $Date: 2008/05/13 12:10:40 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.layer;

import java.awt.Color;

import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ebli.calque.BCalquePersistenceInterface;
import org.fudaa.ebli.calque.ZCalqueGeometry;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.ebli.calque.find.CalqueFindExpressionAtomic;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.modeleur.persistence.MdlLayer2dLevelPersistence;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.fudaa.fudaa.sig.layer.FSigEditor;
import org.nfunk.jep.Variable;

/**
 * Un calque pour le stockage et la manipulation des lignes de niveau.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MdlLayer2dLevel extends MdlLayer2dLine {
  public final static String EXT_NAME="level";
  
  /**
   * Sp�cifique, la valeur Z atomique pour les courbes de niveau.
   * @author Bertrand Marchand
   * @version $Id$
   */
  class CalqueFindExpressionAtomicLevel extends CalqueFindExpressionAtomic {
    Variable varZ_;

    @Override
    public void initialiseExprWhenAtomic(CtuluExpr _expr) {
      super.initialiseExprWhenAtomic(_expr);
      
      varZ_=_expr.addVar(GISAttributeConstants.BATHY.getName(), GISAttributeConstants.BATHY.getLongName());
    }

    @Override
    public void majVariableWhenAtomic(int _idx, Variable[] varToUpdate) {
      super.majVariableWhenAtomic(_idx, varToUpdate);
      
      int ivar=CtuluLibArray.getIndex(varZ_, varToUpdate);
      if (ivar!=-1) {
        varToUpdate[ivar].setValue(getOrdinate(_idx, 2));
      }
    }

    public CalqueFindExpressionAtomicLevel(ZCalqueGeometry _layer) {
      super(_layer);
    }
    
  }
  
  /**
   * Constructeur. Utilise un mod�le de donn�es et un editeur.
   * @param _model Modele
   * @param _editor Editeur.
   */
  private MdlLayer2dLevel(ZModeleLigneBriseeEditable _model,  final FSigEditor _editor) {
    super(_model,_editor);
//    _model.addListener(this); A implementer.
    setName(getExtName());
    setLineModel(0, new TraceLigneModel(TraceLigne.TIRETE, 2f, Color.GRAY));
    setLineModelOuvert(getLineModel(0));

    setTitle(MdlResource.getS("Lignes de niveau"));
    setAttributForLabels(GISAttributeConstants.LABEL);
  }
  
  public MdlLayer2dLevel(FSigEditor _editor) {
    this(new MdlModel2dLevel(_editor,_editor==null ? null:_editor.getMng()),_editor);
  }
  
//  public static MdlLayer2dLevel createNew(FSigEditor _editor) {
//    MdlLayer2dLevel cq=new MdlLayer2dLevel(new MdlModel2dLevel(_editor,_editor.getMng()), _editor);
//    return cq;
//  }

  public BCalquePersistenceInterface getPersistenceMng() {
    return new MdlLayer2dLevelPersistence();
  }
  
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return new CalqueFindExpressionAtomicLevel(this);
  }

  /**
   * Retourne le nom par defaut du calque.
   * @return Le nom.
   */
  public String getExtName() {
    return EXT_NAME;
  }
}
