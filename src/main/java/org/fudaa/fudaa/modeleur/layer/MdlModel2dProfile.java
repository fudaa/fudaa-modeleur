/*
 * @creation     20 janv. 08
 * @modification $Date: 2008-05-13 12:10:42 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.layer;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.gis.GISAttribute;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ebli.calque.ZModelGeometryListener;

/**
 * Un mod�le de calque pour le calque 2D des profils. Les profils sont des polylignes XYZ.
 * 
 * @author Bertrand Marchand
 * @version $Id: MdlModel2dProfile.java,v 1.1.2.6 2008-05-13 12:10:42 bmarchan Exp $
 */
public class MdlModel2dProfile extends MdlModel2dLine {

  public MdlModel2dProfile(final ZModelGeometryListener _listener, final CtuluCommandContainer _cmd) {
    super(_listener);
    normalizeZone(getGeomData(), null, false);
  }
  
  /**
   * Construction d'un modele de profil avec pile de commandes.
   * @param _cmd La pile de commandes pour le undo/redo.
   */
  public MdlModel2dProfile(final GISZoneCollectionLigneBrisee _zone, final ZModelGeometryListener _listener, final CtuluCommandContainer _cmd) {
    super(_listener,_zone);
    // Pas de container de commande pour cette op�ration, sinon conserv� en undo/redo.
    normalizeZone(getGeomData(), null, false);
  }

  /**
   * Normalise une zone pour etre en accord avec ce mod�le.
   * @param _zone La zone
   * @param _cmd La pile de commandes pour le undo/redo.
   * @param _forBief True : Modele pour un bief.
   */
  public static void normalizeZone(GISZoneCollectionLigneBrisee _zone, CtuluCommandContainer _cmd, boolean _forBief) {
    GISAttribute[] attrs2d=new GISAttribute[]{
      GISAttributeConstants.BATHY,
      GISAttributeConstants.ETAT_GEOM,
      GISAttributeConstants.TITRE,
      GISAttributeConstants.HISTORY,
      GISAttributeConstants.NATURE,
      GISAttributeConstants.COMMENTAIRE_HYDRO,
      GISAttributeConstants.VISIBILITE,
      GISAttributeConstants.LABEL
    };

    GISAttribute[] attrs1d=new GISAttribute[]{
      GISAttributeConstants.BATHY,
      GISAttributeConstants.ETAT_GEOM,
      GISAttributeConstants.TITRE,
      GISAttributeConstants.HISTORY,
      GISAttributeConstants.NATURE,
      GISAttributeConstants.COMMENTAIRE_HYDRO,
      GISAttributeConstants.VISIBILITE,
      GISAttributeConstants.LABEL,
      GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_DROITE,
      GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_GAUCHE,
      GISAttributeConstants.INTERSECTION_RIVE_DROITE,
      GISAttributeConstants.INTERSECTION_RIVE_GAUCHE,
      GISAttributeConstants.INTERSECTIONS_LIGNES_DIRECTRICES
    };

    if (_forBief)
      _zone.setAttributes(attrs1d, _cmd);
    else
      _zone.setAttributes(attrs2d, _cmd);

    _zone.setAttributeIsZ(GISAttributeConstants.BATHY);
    _zone.setFixedAttributeValue(GISAttributeConstants.NATURE, GISAttributeConstants.ATT_NATURE_PF);
  }
}
