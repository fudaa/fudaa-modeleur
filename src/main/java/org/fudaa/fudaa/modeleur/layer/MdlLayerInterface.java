/*
 * @creation     19 juin 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.layer;

/**
 * Une interface implementée par tous les calques MDL.
 * @author Bertrand Marchand
 * @version $Id:$
 */
public interface MdlLayerInterface {

  /**
   * Retourne le nom par defaut du calque.
   * @return Le nom.
   */
  public String getExtName();
}
