/*
 * @creation     20 janv. 08
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.layer;

import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 * Une fabrique de calques.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MdlLayerFactory {
  public final static int LAYER2D_CLOUD     =1;
  public final static int LAYER2D_PROFILE   =2;
  public final static int LAYER2D_LEVEL     =3;
  public final static int LAYER2D_CONST_LINE=4;
  public final static int LAYER2D_DIR_LINE  =5;
  public final static int LAYER2D_ZONE      =6;
  public final static int LAYER2D_CONTOUR   =7;
  public final static int LAYER1D_TRACE     =8;
  public final static int LAYER1D_AXE       =9;
  public final static int LAYER2D_CASIER    =10;
  
  private static MdlLayerFactory factory_=new MdlLayerFactory();
  
  /**
   * Can't instanciate.
   */
  private MdlLayerFactory() {}
  
  /**
   * Retourne le singleton.
   */
  public static MdlLayerFactory getInstance() {
    return factory_;
  }
  
  /**
   * Cr�ation d'un calque d'un type donn�e.
   * @param _type Le type de calque.
   * @param _editor L'�diteur associ� au calque.
   * @return Le calque nouvellement cr��.
   */
  public ZCalqueAffichageDonnees createLayer(int _type, FSigEditor _editor ) {
    switch (_type) {
    case LAYER2D_CLOUD:
      return new MdlLayer2dCloud(_editor);
    case LAYER2D_PROFILE:
      return new MdlLayer2dProfile(_editor);
    case LAYER2D_LEVEL:
      return new MdlLayer2dLevel(_editor);
    case LAYER2D_CONST_LINE:
      return new MdlLayer2dConstraintLine(_editor);
    case LAYER2D_DIR_LINE:
      return new MdlLayer2dDirectionLine(_editor);
    case LAYER2D_ZONE:
      return new MdlLayer2dZone(_editor);
    case LAYER2D_CONTOUR:
      return new MdlLayer2dContour(_editor);
    case LAYER2D_CASIER:
      return new MdlLayer2dCasier(_editor);
    case LAYER1D_TRACE:
      return new MdlLayer1dTrace(_editor);
    case LAYER1D_AXE:
      return new MdlLayer1dAxe(_editor);
    default:
      throw new IllegalArgumentException("Bad type");
    }
  }
}
