/*
 * @creation     20 janv. 08
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.layer;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.gis.GISAttribute;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ebli.calque.ZModelGeometryListener;

/**
 * Un mod�le de calque pour le calque 2D de zones g�om�triques. Les zones permettent de sp�cifier un strickler par la suite.
 * Une zone est en XY, ferm�e.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MdlModel2dZone extends MdlModel2dLine {

  /**
   * Construction d'un modele de contours avec container de commandes.
   * @param _listener L'auditeur des modifications du mod�le.
   * @param _cmd Le container de commandes pour le undo/redo.
   */
  public MdlModel2dZone(final ZModelGeometryListener _listener, final CtuluCommandContainer _cmd) {
    super(_listener);
    GISAttribute[] attrs=new GISAttribute[]{
        GISAttributeConstants.TITRE,
        GISAttributeConstants.ETAT_GEOM,
        GISAttributeConstants.HISTORY,
        GISAttributeConstants.NATURE,
        GISAttributeConstants.VISIBILITE,
        GISAttributeConstants.LABEL
    };
    // Pas de container de commande pour cette op�ration, sinon conserv� en undo/redo.
    getGeomData().setAttributes(attrs, null);
    getGeomData().setAttributeIsZ(null);
    getGeomData().setFixedAttributeValue(GISAttributeConstants.NATURE, GISAttributeConstants.ATT_NATURE_ZN);
  }
}
