/*
 * @creation     20 janv. 08
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.layer;

import java.awt.Color;
import java.util.ArrayList;
import org.fudaa.ctulu.gis.GISAttributeModelDoubleInterface;
import org.fudaa.ctulu.gis.GISAttributeModelObjectList;
import org.fudaa.ebli.calque.BCalqueCacheManager;
import org.fudaa.ebli.calque.BCalqueCacheManagerSelection;

import org.fudaa.ebli.calque.BCalquePersistenceInterface;
import org.fudaa.ebli.calque.ZModeleGeometry;
import org.fudaa.ebli.calque.ZModeleLigneBrisee;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.sig.layer.FSigEditor;
import org.fudaa.fudaa.sig.layer.FSigLayerLineEditable;

/**
 * Une classe m�re a implementer pour tous les calques stockant et manipulant des lignes.
 *
 * @author Bertrand Marchand
 * @version $Id$
 */
public abstract class MdlLayer2dLine extends FSigLayerLineEditable implements MdlLayerInterface {

  /**
   * l'indice des jeux de donn�es contenant la bathym�trie.
   */
  int bathyDonneesIndice = -1;

  /**
   * Constructeur. Utilise un mod�le de donn�es et un editeur.
   *
   * @param _model Modele
   * @param _editor Editeur.
   */
  public MdlLayer2dLine(ZModeleLigneBriseeEditable _model, final FSigEditor _editor) {
    super(_model, _editor);
    setDestructible(true);
    installActions();
    updatePositionDataBathy();
    if (_editor != null) {
      setLegende(_editor.getPanel().getCqLegend());
    }
    //a voir si a garder:
    BCalqueCacheManager.installDefaultCacheManager(this);
    BCalqueCacheManagerSelection.installDefaultCacheManagerSelection(this);
  }

  @Override
  public void modele(ZModeleLigneBrisee _modele) {
    super.modele(_modele);
    updatePositionDataBathy();
  }

  @Override
  public void modele(ZModeleGeometry _modele) {
    super.modele(_modele);
    updatePositionDataBathy();
  }

  @Override
  public boolean isPaletteModifiable() {
    return bathyDonneesIndice >= 0;
  }

  @Override
  public boolean isDonneesBoiteAvailable() {
    return bathyDonneesIndice >= 0;
  }

  /**
   * Le manager de persistence. Celui par defaut ne permet par une restauration des lignes de la g�ometrie, qui sont restitu�es uniquement si le
   * groupe est de type ZCalqueEditionGroup.
   */
  public abstract BCalquePersistenceInterface getPersistenceMng();

  /**
   * Installation des actions pour le layer. Les actions sont communes au layer et objets selectionn�s.
   */
  private void installActions() {
    FSigEditor editor = (FSigEditor) getEditor();
    if (editor == null) {
      return;
    }
    ArrayList<EbliActionInterface> vacts = new ArrayList<EbliActionInterface>();
    vacts.add(editor.getImportAction());
    vacts.add(editor.getExportAction());
    EbliActionInterface[] acts = vacts.toArray(new EbliActionInterface[0]);

    super.setActions(acts);
  }

  @Override
  public boolean canSetSelectable() {
    return true;
  }

  private void updatePositionDataBathy() {
    bathyDonneesIndice = modeleDonnees().getGeomData().getIndiceOf(modeleDonnees().getGeomData().getAttributeIsZ());
  }

  @Override
  protected void initTraceForAtomics(TraceIconModel iconeModel, int idxPoly, int idxVertexInPoly) {
    super.initTraceForAtomics(iconeModel, idxPoly, idxVertexInPoly);
    if (isPaletteCouleurUsed_ && bathyDonneesIndice >= 0 && isBathyAtomicValue()) {
      GISAttributeModelObjectList model = (GISAttributeModelObjectList) modeleDonnees().getGeomData().getModel(bathyDonneesIndice);
      GISAttributeModelDoubleInterface doubleModel = (GISAttributeModelDoubleInterface) model.getValueAt(idxPoly);
      Color c = ((BPalettePlage) super.paletteCouleur_).getColorFor(doubleModel.getValue(idxVertexInPoly));
      iconeModel.setCouleur(getColorWithAttenuAlpha(c));
    }
  }

  @Override
  protected void initTrace(TraceIconModel _icon, int _idxPoly) {
    super.initTrace(_icon, _idxPoly);
    if (isPaletteCouleurUsed_ && bathyDonneesIndice >= 0 && !isBathyAtomicValue()) {
      GISAttributeModelDoubleInterface model = (GISAttributeModelDoubleInterface) modeleDonnees().getGeomData().getModel(bathyDonneesIndice);
      Color c = ((BPalettePlage) super.paletteCouleur_).getColorFor(model.getValue(_idxPoly));
      _icon.setCouleur(getColorWithAttenuAlpha(c));
    }

  }

  @Override
  protected void initTrace(TraceLigneModel _ligne, int _idxPoly) {
    super.initTrace(_ligne, _idxPoly);
    //on utilise la couleur de la palette seulement si la valeur en Z n'est pas atomique
    if (isPaletteCouleurUsed_ && bathyDonneesIndice >= 0 && !isBathyAtomicValue()) {
      GISAttributeModelDoubleInterface model = (GISAttributeModelDoubleInterface) modeleDonnees().getGeomData().getModel(bathyDonneesIndice);
      Color c = ((BPalettePlage) super.paletteCouleur_).getColorFor(model.getValue(_idxPoly));
      _ligne.setCouleur(getColorWithAttenuAlpha(c));
    }
  }

  private boolean isBathyAtomicValue() {
    return modeleDonnees().getGeomData().getAttribute(bathyDonneesIndice).isAtomicValue();
  }
}
