/*
 * @creation     28 janv. 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.layer;

import java.awt.Color;

import org.fudaa.ebli.calque.BCalquePersistenceInterface;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.modeleur.persistence.MdlLayer1dLimiteStockageLinePersistence;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 * Un calque pour le stockage et la manipulation des limites de stockage.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 * */
public class MdlLayer1dLimiteStockage extends MdlLayer2dLine {

  /**
   * Constructeur. Utilise un mod�le de donn�es et un editeur.
   * 
   * @param _model
   *          Modele
   * @param _editor
   *          Editeur.
   */
  private MdlLayer1dLimiteStockage(ZModeleLigneBriseeEditable _model, final FSigEditor _editor) {
    super(_model, _editor);
    setLineModel(0, new TraceLigneModel(TraceLigne.LISSE, 1.5f, Color.green));
    setLineModelOuvert(getLineModel(0));
    setName(getExtName());
    setTitle(MdlResource.getS("Limites de stockages"));
  }

  public MdlLayer1dLimiteStockage(FSigEditor _editor) {
    this(new MdlModel1dLimiteStockage(_editor, _editor.getMng()), _editor);
  }

  public BCalquePersistenceInterface getPersistenceMng() {
    return new MdlLayer1dLimiteStockageLinePersistence();
  }

  /**
   * Retourne le nom par defaut du calque.
   * 
   * @return Le nom.
   */
  public String getExtName() {
    return "limiteStockage";
  }

  public boolean canAddForme(int _typeForme) {
    return _typeForme==DeForme.LIGNE_BRISEE;
  }
}
