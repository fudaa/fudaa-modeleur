/*
 * @creation     20 janv. 08
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.layer;

import java.awt.Color;

import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ebli.calque.BCalquePersistenceInterface;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.modeleur.persistence.MdlLayer1dAxePersistence;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 * Un calque pour le stockage et la manipulation des axes hydraliques.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MdlLayer1dAxe extends MdlLayer2dLine {
  public final static String EXT_NAME="axe";
  
  /**
   * Constructeur. Utilise un mod�le de donn�es et un editeur.
   * @param _model Modele
   * @param _editor Editeur.
   */
  private MdlLayer1dAxe(ZModeleLigneBriseeEditable _model,  final FSigEditor _editor) {
    super(_model,_editor);
    setLineModel(0, new TraceLigneModel(TraceLigne.POINTILLE, 2f, new Color(0,125,0)));
    setLineModelOuvert(getLineModel(0));
    setName(getExtName());
    setTitle(MdlResource.getS("Axes hydrauliques"));
    setAttributForLabels(GISAttributeConstants.LABEL);
  }
  
  public MdlLayer1dAxe(FSigEditor _editor) {
    this(new MdlModel1dAxe(_editor),_editor);
  }
  
  public BCalquePersistenceInterface getPersistenceMng() {
    return new MdlLayer1dAxePersistence();
  }

  /**
   * Retourne le nom par defaut du calque.
   * @return Le nom.
   */
  public String getExtName() {
    return EXT_NAME;
  }
  
  public boolean canAddForme(int _typeForme) {
    return _typeForme==DeForme.LIGNE_BRISEE;
  }
}
