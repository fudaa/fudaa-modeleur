/*
 * @creation     20 nov. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.layer;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.JComponent;

import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.commun.BPalettePanelInterface;
import org.fudaa.ebli.commun.EbliFormatterInterface;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuLabel;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Palette affichant un abscisse curviligne.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class BPaletteAbscisseCurviligne extends CtuluDialogPanel implements ActionListener, BPalettePanelInterface, PropertyChangeListener {

  /** Label affichant l'abscisse curviligne. */
  protected BuLabel lblCoordonneeCurviligne_=new BuLabel();
  /** Le calque qui contient les informations curvilignes. */
  protected ZCalqueAbscisseCurviligneInteraction calqueAbscisseCurviligne_;
  /** Le bouton 'reprise'. */
  protected BuButton btReprise_=new BuButton(MdlResource.getS("Reprise"));
  protected EbliFormatterInterface formatter_;
  private ZEbliCalquesPanel calquesPanel_;
  
  public BPaletteAbscisseCurviligne(ZCalqueAbscisseCurviligneInteraction _calqueAbscisseCurviligne, EbliFormatterInterface _formatter, ZEbliCalquesPanel _calquesPanel){
    calquesPanel_=_calquesPanel;
    calqueAbscisseCurviligne_=_calqueAbscisseCurviligne;
    calqueAbscisseCurviligne_.addPropertyChangeListener("gele", this);
    formatter_=_formatter;
    // Construction du panel \\
    setLayout(new BorderLayout());
    // Titre
    BuLabel lblTitre=new BuLabel("<html><b>"+MdlResource.getS("Abscisse curviligne")+"</b></html>");
    lblTitre.setHorizontalAlignment(BuLabel.CENTER);
    add(lblTitre, BorderLayout.NORTH);
    // Contenu
    Container body=new Container();
    body.setLayout(new GridLayout(1, 2, 2, 2));
    body.add(new BuLabel(MdlResource.getS("Valeur curviligne")+" : "));
    body.add(lblCoordonneeCurviligne_);
    add(body, BorderLayout.CENTER);
    // Bouton reprise
    add(btReprise_, BorderLayout.SOUTH);
    btReprise_.addActionListener(this);
    btReprise_.setEnabled(false);
    // Affichage
    setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
    setVisible(true);
    // Valeurs \\
    setAbcisseCurviligne(0);
  }

  @Override
  public boolean isDataValid(){
    return true;
  }
  
  public JComponent getComponent() {
    return this;
  }

  public boolean setPalettePanelTarget(Object _target) {
    return false;
  }
  
  public void paletteDeactivated() {}
  
  public void doAfterDisplay() {}

  public void propertyChange(PropertyChangeEvent evt) {
    if(evt.getPropertyName().equals("gele"))
      btReprise_.setEnabled((Boolean) evt.getNewValue());
  }

  /* (non-Javadoc)
   * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
   */
  public void actionPerformed(ActionEvent e) {
    calquesPanel_.setCalqueInteractionActif(calqueAbscisseCurviligne_);
  }
  
  public void setAbcisseCurviligne(double _value){
    lblCoordonneeCurviligne_.setText(formatter_.getXYFormatter().format(_value));
  }
  
  public void setMessage(String _message){
    lblCoordonneeCurviligne_.setText(_message);
  }
  
}
