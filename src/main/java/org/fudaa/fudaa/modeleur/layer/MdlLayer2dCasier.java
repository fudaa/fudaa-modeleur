/*
 * @creation     20 janv. 08
 * @modification $Date: 2008/05/13 12:10:43 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.layer;

import java.awt.Color;

import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ebli.calque.BCalquePersistenceInterface;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.ebli.trace.TraceSurface;
import org.fudaa.ebli.trace.TraceSurfaceModel;
import org.fudaa.fudaa.modeleur.persistence.MdlLayer2dCasierPersistence;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 * Un calque pour le stockage et la manipulation des casiers.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MdlLayer2dCasier extends MdlLayer2dLine {
  public final static String EXT_NAME="storage";
  
  /**
   * Constructeur. Utilise un mod�le de donn�es et un editeur.
   * @param _model Modele
   * @param _editor Editeur.
   */
  private MdlLayer2dCasier(MdlModel2dCasier _model,  final FSigEditor _editor) {
    super(_model,_editor);
    setIconModel(0, new TraceIconModel(TraceIcon.CARRE_PLEIN, 2, new Color(115,88,34)));
    setLineModel(0, new TraceLigneModel(TraceLigne.LISSE, 2f, new Color(181,139,54)));
    setSurfaceModel(0,new TraceSurfaceModel(TraceSurface.UNIFORME,new Color(255,230,140),Color.WHITE));
    
    setName(getExtName());
    setTitle(MdlResource.getS("Contours casiers"));
    setAttributForLabels(GISAttributeConstants.LABEL);
    setDestructible(true);
  }
  
  public MdlLayer2dCasier(FSigEditor _editor) {
    this(new MdlModel2dCasier(_editor,_editor==null ? null:_editor.getMng()),_editor);
  }

  public BCalquePersistenceInterface getPersistenceMng() {
    return new MdlLayer2dCasierPersistence();
  }

  /**
   * Retourne le nom par defaut du calque.
   * @return Le nom.
   */
  public String getExtName() {
    return EXT_NAME;
  }

  public int getNbSet() {
    return 1;
  }
  
  public boolean canSetSelectable() {
    return true;
  }
  
  public boolean canAddForme(int _typeForme) {
    return _typeForme==DeForme.POLYGONE || _typeForme==DeForme.ELLIPSE || _typeForme==DeForme.RECTANGLE;
  }
}
