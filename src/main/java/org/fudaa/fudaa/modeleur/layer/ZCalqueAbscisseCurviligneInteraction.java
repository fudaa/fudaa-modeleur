/*
 * @creation     20 nov. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.layer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import org.fudaa.ebli.calque.BCalqueInteraction;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.fudaa.modeleur.action.SceneAbscisseCurviligneAction;
import org.locationtech.jts.geom.Coordinate;

/**
 * Un calque d'interaction permettant de capter des �v�nements souris pour calculer un abscisse curviligne.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class ZCalqueAbscisseCurviligneInteraction extends BCalqueInteraction implements MouseListener {

  /** L'action g�rant le calcule de l'abcisse curviligne. */
  private SceneAbscisseCurviligneAction controller_;
  /** La position de la sonde en coordonn� r�el. */
  private Coordinate positionReelSonde_;
  /** Derni�re position de la sonde en coordonn� �cran. */
  private Coordinate oldPositionSonde_;
  /** La sonde � afficher � l'endroit du click. */
  private TraceIcon sonde_;
  /** Indique si la sonde est affich�e. */
  private boolean sondePainted_;
  /** Indique si le curseur est en dehors de la fen�tre. */
  private boolean cursOut_;
  
  public ZCalqueAbscisseCurviligneInteraction(SceneAbscisseCurviligneAction _controller){
    setName("cqInteractifAbscisseCurviligne");
    cursOut_=false;
    controller_=_controller;
    sondePainted_=false;
    sonde_=new TraceIcon(TraceIcon.RIEN, 6) {
      @Override
      public void paintIconCentre(Component _c, Graphics _g, int _x, int _y) {
        Color old=null;
        if (getCouleur()!=null) {
          old=_g.getColor();
          _g.setColor(getCouleur());
        }
        final int taille=super.getTaille();
        final int demiTaille=taille/2;
        _g.drawLine(_x-demiTaille, _y, _x-taille, _y+1);
        _g.drawLine(_x-demiTaille, _y, _x-taille, _y-1);
        _g.drawLine(_x+demiTaille, _y, _x+taille, _y+1);
        _g.drawLine(_x+demiTaille, _y, _x+taille, _y-1);
        _g.drawLine(_x, _y-demiTaille, _x+1, _y-taille);
        _g.drawLine(_x, _y-demiTaille, _x-1, _y-taille);
        _g.drawLine(_x, _y+demiTaille, _x+1, _y+taille);
        _g.drawLine(_x, _y+demiTaille, _x-1, _y+taille);
        _g.drawLine(_x, _y-taille, _x, _y+taille);
        _g.drawLine(_x-taille, _y, _x+taille, _y);
        if (super.getCouleur()!=null) {
          _g.setColor(old);
        }
      }
    };
    sonde_.setCouleur(Color.BLACK);
  }
  
  private void drawSonde(Graphics g, Coordinate c){
    sonde_.paintIconCentre(this, g, (int)c.x, (int)c.y);
  }
  
  protected void afficheSonde(){
    if(!sondePainted_){
      Graphics g=getGraphics();
      if(g!=null){
        g.setXORMode(getBackground());
        if (positionReelSonde_!=null) {
          drawSonde(g, reelToEcran(positionReelSonde_));
          oldPositionSonde_=reelToEcran(positionReelSonde_);
        }
      }
      sondePainted_=true;
    }
  }
  
  protected void effaceSonde(){
    if(sondePainted_){
      Graphics g=getGraphics();
      if(g!=null){
        g.setXORMode(getBackground());
        if (oldPositionSonde_!=null) {
          drawSonde(g, oldPositionSonde_);
          oldPositionSonde_=null;
        }
      }
      sondePainted_=false;
    }
  }
  
  /** Converti une coordonn�e �cran en coordonn�e r�el. */ 
  protected Coordinate ecranToReel(Coordinate coord){
    GrPoint tmp=new GrPoint(coord.x, coord.y, 0);
    tmp.autoApplique(getVersReel());
    return new Coordinate(tmp.x_, tmp.y_);
  }

  /** converti une coordonn�e �cran en coordonn�e r�el. */
  protected Coordinate reelToEcran(Coordinate coord){
    GrPoint tmp=new GrPoint(coord.x, coord.y, 0);
    tmp.autoApplique(getVersEcran());
    return new Coordinate(tmp.x_, tmp.y_);
  }
  
  @Override
  public Cursor getSpecificCursor() {
    return EbliResource.EBLI.getCursor("curseur_distance", -1, new Point(6, 6));
  }

  @Override
  public boolean alwaysPaint() {
    return true;
  }
  
 // M�thodes g�rant les �v�nements. \\

  public void mouseClicked(final MouseEvent _evt) {}

  public void mouseEntered(final MouseEvent _evt) {
    cursOut_=false;
    if(!isGele())
      afficheSonde();
  }

  public void mousePressed(final MouseEvent _evt) {}
  
  public void mouseExited(final MouseEvent _evt) {
    cursOut_=true;
    if(!isGele())
      effaceSonde();
  }

  public void mouseReleased(final MouseEvent _evt) {
    if (!isGele()&&_evt.getButton()==MouseEvent.BUTTON1){
      effaceSonde();
      // Calcule de la tol�rance au click
      double tolerance;
      GrPoint p1=new GrPoint(0, 0, 0);
      GrPoint p2=new GrPoint(1, 1, 0);
      tolerance=p1.distance(p2)*5;
      p1.autoApplique(getVersEcran());
      p2.autoApplique(getVersEcran());
      tolerance/=p1.distance(p2);
      // Mise a jour de l'action
      positionReelSonde_=controller_.coordinateClicked(ecranToReel(new Coordinate(_evt.getX(), _evt.getY())), tolerance);
      if(positionReelSonde_!=null)
        afficheSonde();
    }
  }
  
  /** Indique au calque qu'il faut s'arr�ter. */
  public void stopEdition(){
    positionReelSonde_=null;
  }
  
  @Override
  public void setGele(boolean _gele){
    if(_gele)
      effaceSonde();
    else if(!cursOut_)
      afficheSonde();
    super.setGele(_gele);
  }
}
