/*
 * @creation     20 janv. 08
 * @modification $Date: 2008/05/13 12:10:43 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur.layer;

import java.awt.Color;

import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ebli.calque.BCalquePersistenceInterface;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.modeleur.persistence.MdlLayer2dDirectionLinePersistence;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.fudaa.fudaa.sig.layer.FSigEditor;

/**
 * Un calque pour le stockage et la manipulation des lignes directrices.
 * @author Bertrand Marchand
 * @version $Id: MdlLayer2dProfile.java,v 1.1.2.5 2008/05/13 12:10:43 bmarchan Exp $
 */
public class MdlLayer2dDirectionLine extends MdlLayer2dLine {
  public final static String EXT_NAME="direct";
  
  /**
   * Constructeur. Utilise un mod�le de donn�es et un editeur.
   * @param _model Modele
   * @param _editor Editeur.
   */
  private MdlLayer2dDirectionLine(ZModeleLigneBriseeEditable _model,  final FSigEditor _editor) {
    super(_model,_editor);
    setLineModel(0, new TraceLigneModel(TraceLigne.LISSE, 1.5f, new Color(102,102,255)));
    setLineModelOuvert(getLineModel(0));
//    _model.addListener(this); A implementer.
    setName(getExtName());
    setTitle(MdlResource.getS("Lignes directrices"));
    setAttributForLabels(GISAttributeConstants.LABEL);
  }
  
  public MdlLayer2dDirectionLine(FSigEditor _editor) {
    this(new MdlModel2dDirectionLine(_editor,_editor==null ? null:_editor.getMng()),_editor);
  }
  
//  public static MdlLayer2dDirectionLine createNew(FSigEditor _editor) {
//    MdlLayer2dDirectionLine cq=new MdlLayer2dDirectionLine(new MdlModel2dDirectionLine(_editor,_editor.getMng()), _editor);
//    return cq;
//  }

  public BCalquePersistenceInterface getPersistenceMng() {
    return new MdlLayer2dDirectionLinePersistence();
  }

  /**
   * Retourne le nom par defaut du calque.
   * @return Le nom.
   */
  public String getExtName() {
    return EXT_NAME;
  }
  
  public boolean canAddForme(int _typeForme) {
    return _typeForme==DeForme.LIGNE_BRISEE;
  }
}
