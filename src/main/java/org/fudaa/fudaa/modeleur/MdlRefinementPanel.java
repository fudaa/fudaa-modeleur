/*
 * @creation     25 avr. 08
 * @modification $Date: 2008-05-13 12:10:11 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;

import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuTextField;

/**
 * Un panneau pour les parametres de rafinement entre 2 sommets ou sur la totalit� de la polyligne.
 * @author Bertrand Marchand
 * @version $Id: MdlInterpolationPanel.java,v 1.1.2.1 2008-05-13 12:10:11 bmarchan Exp $
 */
public class MdlRefinementPanel extends CtuluDialogPanel {

  private class RefinementActionListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
      tfDist_.setEnabled(e.getSource()==rbDist_);
      tfNbPts_.setEnabled(e.getSource()==rbNbPts_);
    }
  }
  
  private BuTextField tfNbPts_;
  private BuTextField tfDist_;
  private BuRadioButton rbNbPts_;
  private BuRadioButton rbDist_;
  private int nbPts_=0;
  private double dist_=0;
  
  public MdlRefinementPanel() {
    customize();
  }
  
  public void customize() {
    BuGridLayout lyThis=new BuGridLayout(2,5,5);
    this.setLayout(lyThis);
    RefinementActionListener l=new RefinementActionListener();
    ButtonGroup bg=new ButtonGroup();

    rbNbPts_=new BuRadioButton(MdlResource.getS("Nombre de points ajout�s entre 2 points"));
    rbNbPts_.addActionListener(l);
    bg.add(rbNbPts_);

    rbDist_=new BuRadioButton(MdlResource.getS("Distance maximale entre 2 points (m)"));
    rbDist_.addActionListener(l);
    bg.add(rbDist_);
    
    this.add(rbNbPts_);
    tfNbPts_=addIntegerText("1");
    this.add(rbDist_);
    tfDist_=addDoubleText("1.0");
    
    rbNbPts_.doClick();
  }

  @Override
  public boolean isDataValid() {
    if (rbNbPts_.isSelected() && (tfNbPts_.getText().trim().length()==0 || (nbPts_ = (Integer)tfNbPts_.getValue())<1)) {
      setErrorText(MdlResource.getS("Le nombre de points ajout�s doit �tre >=1") + '!');
      return false;
    }
    else if (rbDist_.isSelected() && (tfDist_.getText().trim().length()==0 || (dist_ = (Double)tfDist_.getValue())<=0)) {
      setErrorText(MdlResource.getS("La distance maximale entre 2 points doit �tre > 0 !"));
      return false;
    }
    return true;
  }
  
  public int getNbPts() {
    return nbPts_;
  }
  
  public double getDistance() {
    return dist_;
  }
  
  public boolean isNbPtsGiven() {
    return rbNbPts_.isSelected();
  }
}
