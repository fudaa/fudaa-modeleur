/*
 * @creation 7 juin 07
 * @modification $Date$
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.modeleur;

import java.io.File;
import java.text.DateFormat;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

import com.memoire.bu.BuInformationsDocument;

import org.fudaa.dodico.fichiers.FileFormatSoftware;

import org.fudaa.fudaa.commun.FudaaProjectStateFrameListener;
import org.fudaa.fudaa.commun.FudaaProjetStateDefault;
import org.fudaa.fudaa.commun.FudaaProjetStateInterface;
import org.fudaa.fudaa.commun.save.FudaaSavable;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.fudaa.fudaa.sig.FSigProjet;

/**
 * Une implementation de l'interface projet pour le modeleur.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MdlProjet implements FSigProjet, Observer {
  BuInformationsDocument doc_ = new BuInformationsDocument();

  File file_;
  /** Boolean pour indiquer l'�tat de fermeture du projet */
  boolean bisClosing_=false;

  FileFormatSoftware soft_ = new FileFormatSoftware();
  FudaaProjetStateDefault state_ = new FudaaProjetStateDefault();

  public void close() {
    // Etat is closing. Apr�s l'appel � close, le projet ne doit plus �tre utilis�.
    bisClosing_=true;
    state_.clearStates();
    // inutile mais par s�curit�
    state_.removeAllListeners();

  }

  public String getFrameTitle() {
    String log=MdlImplementation.informationsSoftware().name;
    String version=MdlImplementation.informationsSoftware().version;
    return log+" "+version+ (bisClosing_ ? "": " - "+(file_ == null ? MdlResource.getS("Nouveau projet") : file_.getAbsolutePath()));
  }

  public BuInformationsDocument getInformationsDocument() {
    return doc_;
  }

  /**
   * La date de derni�re sauvegarde du projet, obtenue � partir du fichier projet.
   */
  public String getLastSaveDate() {
    return file_ == null || file_.lastModified() <= 0 ? MdlResource.getS("Non sauvegard�") : DateFormat
        .getDateTimeInstance().format(new Date(file_.lastModified()));
  }

  public File getParamsFile() {
    return file_;
  }

  boolean opening_;

  public void setOpening(boolean _b) {
    opening_ = _b;
  }

  public FudaaSavable[] getSavableComponent() {
    return null;
  }

  public FudaaProjetStateInterface getProjectState() {
    return state_;
  }

  public FileFormatSoftware getSystemVersion() {
    return soft_;
  }

  public String getTitle() {
    return getFrameTitle();
  }

  public void setSaved() {
    state_.clearStates();
  }

  boolean isInstalled_;

  public void install(MdlImplementation _impl) {
    _impl.get2dFrame().getArbreCalqueModel().getObservable().addObserver(this);
    _impl.get1dFrame().getController().getVueBief().getArbreCalqueModel().getObservable().addObserver(this);
    state_.addListener(new FudaaProjectStateFrameListener(_impl.getFrame(), this));
    state_.addListener(_impl);
    state_.clearStates();
    isInstalled_ = true;
  }

  public void setFile(File _f) {
    file_ = _f;
  }

  public void setParamFile(File _f) {
    file_ = _f;
  }

  public void update(Observable _o, Object _arg) {
    if (opening_ || !isInstalled_) return;
    if ((_arg instanceof String) && ((String)_arg).equals("UI"))
      state_.setUiModified(true);
    else
      state_.setParamModified(true);
  }

}
