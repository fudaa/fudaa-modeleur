/*
 * @creation     21 janv. 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISCoordinateSequenceFactory;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISDataModelMultiAdapter;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ebli.calque.ZModeleGeometry;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.edition.ZCalqueEditable;
import org.fudaa.ebli.calque.edition.ZModeleEditable;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionMap;
import org.fudaa.fudaa.modeleur.layer.MdlModel2dLine;
import org.fudaa.fudaa.modeleur.layer.MdlModel2dMultiPoint;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;

/**
 * Un gestionnaire des op�rations de copier/couper/coller de la fen�tre 2D. La copie de plusieurs g�om�tries ou sommets est
 * autoris�e.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class MdlCutCopyPasteManager {
  /** La fen�tre 2d � g�rer. */
  protected MdlFille2d mdlFille2d_;
  /** La sc�ne permettant l'acc�s sur les g�om�tries et les selections. */
  protected ZScene zScene_;
  /** L'editeur de sc�ne contenant les m�thodes pour le d�placement des g�om�tries. */
  protected MdlSceneEditor zSceneEditor_;
  /** Copy action interface */
  protected EbliActionInterface copyAction_;
  /** Cut action interface */
  protected EbliActionInterface cutAction_;
  /** Paste action interface */
  protected EbliActionInterface pasteAction_;
  /** Les mod�les contenant les g�om�tries en cours de copy. */
  protected List<ZModeleGeometry> models_=new ArrayList<ZModeleGeometry>();
  /** Les points selectionn�s pour la copy en mode sommet. */
  protected List<Coordinate> coordinates_=new ArrayList<Coordinate>();
  // Les gestionnaires d'�v�nements \\
  protected EventFrameManager eventManager1_;
  protected EventSelectedGeometriesManager eventManager2_;
  protected EventSelectedCalquesManager eventManager3_;

  /**
   * Mise � jour correcte de bouton copier/coller/couper lors des diff�rente
   * manipulation de la fen�tre.
   */
  protected class EventFrameManager extends InternalFrameAdapter {
    public boolean disablePaste_=false;
    public boolean disableCopy_=false;
    public boolean disableCut_=false;

    @Override
    public void internalFrameActivated(InternalFrameEvent e) {
      disablePaste_=false;
      disableCopy_=false;
      disableCut_=false;
      updateButtons();
    }

    @Override
    public void internalFrameDeactivated(InternalFrameEvent e) {
      disablePaste_=true;
      disableCopy_=true;
      disableCut_=true;
      updateButtons();
    }
  }

  /**
   * Mise � jour des boutons en fonction de la selection qui est faite dans les
   * calques.
   */
  protected class EventSelectedGeometriesManager implements ZSelectionListener {
    public boolean disablePaste_=false;
    public boolean disableCopy_=false;
    public boolean disableCut_=false;
    
    public void selectionChanged(ZSelectionEvent _evt) {
      disableCopy_=zScene_.isSelectionEmpty();
      disableCut_=zScene_.isSelectionEmpty()||zScene_.isAtomicMode();
      updateButtons();
    }
  }

  /**
   * Mise � jour de du bouton 'coller' en fonction du calque selectionn� dans
   * l'arbre des calques.
   */
  protected class EventSelectedCalquesManager implements TreeSelectionListener {
    public boolean disablePaste_=false;
    public boolean disableCopy_=false;
    public boolean disableCut_=false;
    
    public void valueChanged(TreeSelectionEvent e) {
      disableCut_=!(mdlFille2d_.getArbreCalqueModel().getSelectedCalque() instanceof ZCalqueEditable);
      disablePaste_=!(mdlFille2d_.getArbreCalqueModel().getSelectedCalque() instanceof ZCalqueEditable);
      updateButtons();
    }
  }

  /**
   * Constructeur.
   * @param _mdlFille2d La fenetre fille
   * @param _zSceneEditor L'�diteur de scene.
   */
  public MdlCutCopyPasteManager(MdlFille2d _mdlFille2d, MdlSceneEditor _zSceneEditor) {
    mdlFille2d_=_mdlFille2d;
    zScene_=_zSceneEditor.getScene();
    zSceneEditor_=_zSceneEditor;
    copyAction_=EbliActionMap.getInstance().getAction("COPIER");
    cutAction_=EbliActionMap.getInstance().getAction("COUPER");
    pasteAction_=EbliActionMap.getInstance().getAction("COLLER");
    // Verification de la pr�sence des actions
    if (copyAction_==null||cutAction_==null||pasteAction_==null)
      throw new IllegalArgumentException("Les actions n�c�ssaire aux fonctionnalit�s de couper/copier/coller n'ont pas �t� trouv�s.");
    // Mise en place des diff�rentes �coutes
    eventManager1_=new EventFrameManager();
    eventManager2_=new EventSelectedGeometriesManager();
    eventManager3_=new EventSelectedCalquesManager();
    mdlFille2d_.addInternalFrameListener(eventManager1_);
    zScene_.addSelectionListener(eventManager2_);
    mdlFille2d_.getArbreCalqueModel().addTreeSelectionListener(eventManager3_);
    // Initialisation des valeurs de blockage des boutons.
    eventManager2_.selectionChanged(null);
    eventManager3_.valueChanged(null);
  }

  /**
   * Active les boutons qui doivent l'�tre.
   */
  protected void updateButtons() {
    copyAction_.setEnabled(!eventManager1_.disableCopy_&&!eventManager2_.disableCopy_&&!eventManager3_.disableCopy_);
    pasteAction_.setEnabled((models_.size()>0||coordinates_.size()>0)&&!eventManager1_.disablePaste_&&!eventManager2_.disablePaste_&&!eventManager3_.disablePaste_);
    cutAction_.setEnabled(!eventManager1_.disableCut_&&!eventManager2_.disableCut_&&!eventManager3_.disableCut_);
  }
  
  /**
   * Action de copie. Les objets s�lectionn�s sont copi�s dans un buffer temporaire.
   */
  public void copy() {
    models_.clear();
    coordinates_.clear();
    if (zScene_.isAtomicMode()) {
      // Copy en mode atomic \\
      int[] idxScene=zScene_.getLayerSelectionMulti().getIdxSelected();
      if (idxScene!=null) {
        for(int i=0;i<idxScene.length;i++) {
          int idx=zScene_.sceneId2LayerId(idxScene[i]);
          // Extraction des informations sur la zone
          GISZoneCollection zone=((ZModeleEditable) zScene_.getLayerForId(idxScene[i]).modeleDonnees()).getGeomData();
          GISAttributeInterface attrZ=zone.getAttributeIsZ();
          int idxAttrZ=-1;
          if(attrZ!=null)
            idxAttrZ=zone.getIndiceOf(zone.getAttributeIsZ());
          // La g�om�trie � traiter
          CoordinateSequence seqGeom=zone.getCoordinateSequence(idx);
          int[] selectedIdx=zScene_.getLayerSelectionMulti().getSelection(idxScene[i]).getSelectedIndex();
          // Extraction des coordonn�es
          for(int j=0;j<selectedIdx.length;j++) {
            Coordinate coord=seqGeom.getCoordinate(selectedIdx[j]);
            if(attrZ!=null) {
              if(attrZ.isAtomicValue())
                coord.z=(Double) ((GISAttributeModel) zone.getValue(idxAttrZ, idx)).getObjectValueAt(selectedIdx[j]);
              else
                coord.z=(Double) zone.getValue(idxAttrZ,idx);
            }
            coordinates_.add(coord);
          }
        }
      }
    }
    else {
      // Copy en mode global \\
      int[] idxScene=zScene_.getLayerSelection().getSelectedIndex();
      if (idxScene!=null) {
        // Tri des g�om�tries en fonction de leur calque d'origine
        Map<ZCalqueEditable, List<Integer>> selectedGeom=new HashMap<ZCalqueEditable, List<Integer>>();
        for (int i=0; i<idxScene.length; i++) {
          ZCalqueEditable calque=(ZCalqueEditable)zScene_.getLayerForId(idxScene[i]);
          if (!selectedGeom.containsKey(calque))
            selectedGeom.put(calque, new ArrayList<Integer>());
          selectedGeom.get(calque).add(zScene_.sceneId2LayerId(idxScene[i]));
        }
        // Cr�ation des mod�les de cache
        for (Map.Entry<ZCalqueEditable, List<Integer>> entry : selectedGeom.entrySet()) {
          // Cr�ation du mod�le
          ZModeleEditable modelSource=entry.getKey().getModelEditable();
          ZModeleEditable modelCache=null;
          if (modelSource instanceof MdlModel2dMultiPoint)
            modelCache=new MdlModel2dMultiPoint(null);
          else if (modelSource instanceof MdlModel2dLine)
            modelCache=new MdlModel2dLine(null);
          modelCache.getGeomData().setAttributes(modelSource.getGeomData().getAttributes(), null);
          // Remplissage du mod�le
          for (int i=0; i<entry.getValue().size(); i++) {
            int idxGeom=entry.getValue().get(i);
            Geometry geom=modelSource.getGeomData().getGeometry(idxGeom);
            GISAttributeModel[] models=modelSource.getGeomData().getModels();
            Object[] data=new Object[models.length];
            for (int j=0; j<data.length; j++)
              data[j]=models[j].getObjectValueAt(idxGeom);
            modelCache.getGeomData().addGeometry(geom, data, null);
          }
          // Ajout du mod�le de cache
          models_.add(modelCache);
        }
      }
    }
    updateButtons();
  }

  /**
   * Action de couper. Les objets s�lectionn�s sont tranf�r�s dans un buffer temporaire. Seuls les g�om�tries peuvent
   * �tre coup�es.
   */
  public void cut() {
    copy();
    zSceneEditor_.removeSelectedObjects(mdlFille2d_.getCmdMng());
  }

  /**
   * Action de coller. Les objets du buffer temporaire sont cr��s dans le calque s�lectionn�.
   */
  public void paste() {
    try {
      CtuluCommandComposite cmp;
      
      // Collage de g�om�trie
      if (models_.size()>0) {
        cmp=new CtuluCommandComposite(MdlResource.getS("Coller des g�om�tries"));
        GISDataModel[] mdls=new GISDataModel[models_.size()];
        for (int i=0; i<models_.size(); i++)
          mdls[i]=models_.get(i).getGeomData();
        
        // Si des g�om�tries ne sont pas conformes, aucune n'est copi�e.        
        if (!zSceneEditor_.canCopyGeometries(new GISDataModelMultiAdapter(mdls), (ZCalqueEditable)zScene_.getCalqueActif()))
          return;
        
        // La copie se fait sur chaque mod�le et non sur un mod�le global, pour tenir compte au maximum des attributs
        // li�s � chaque g�om�trie source.
        for (int i=0; i<models_.size(); i++) {
          zSceneEditor_.copyGeometries(mdls[i], (ZCalqueEditable)zScene_.getCalqueActif(), cmp);
        }
      }
      
      // Collage de points
      else {
        cmp=new CtuluCommandComposite(MdlResource.getS("Coller des sommets"));
        ZCalqueEditable calque=((ZCalqueEditable)zScene_.getCalqueActif());
        ZModeleEditable modele=(ZModeleEditable)calque.modeleDonnees();
        GISZoneCollection zone=modele.getGeomData();
        GISAttributeInterface attrZ=zone.getAttributeIsZ();
        int idxAttrZ=-1;
        if (attrZ!=null)
          idxAttrZ=zone.getIndiceOf(attrZ);
        // Cr�ation de la g�om�trie
        CoordinateSequence coordSeq=new GISCoordinateSequenceFactory().create(coordinates_.toArray(new Coordinate[0]));
        Geometry geom=null;
        if (modele instanceof MdlModel2dMultiPoint)
          geom=GISGeometryFactory.INSTANCE.createMultiPoint(coordSeq);
        else if (modele instanceof MdlModel2dLine) {
          boolean isFerme=coordSeq.getCoordinate(0).equals(coordSeq.getCoordinate(coordSeq.size()-1));
          if ((calque.canAddForme(DeForme.LIGNE_BRISEE)&&!isFerme)||!calque.canAddForme(DeForme.POLYGONE)) {
            if (coordSeq.size()<2)
              throw new IllegalArgumentException(MdlResource.getS("Il faut au moins deux points pour coller en tant que polyligne."));
            geom=GISLib.toPolyligne(coordSeq);
          }
          else {
            if (coordSeq.size()<2)
              throw new IllegalArgumentException(MdlResource.getS("Il faut au moins trois points pour coller en tant que polygone."));
            geom=GISLib.toPolygone(coordSeq);
          }
        }
        // Cr�ation de l'attribut z
        Object[] data=new Object[zone.getNbAttributes()];
        if (attrZ!=null)
          if (attrZ.isAtomicValue()) {
            data[idxAttrZ]=new Double[coordSeq.size()];
            for (int i=0; i<coordSeq.size(); i++)
              ((Double[])data[idxAttrZ])[i]=new Double(coordSeq.getOrdinate(i, 2));
          }
          else {
            boolean allSame=true;
            int i=0;
            while (allSame&&++i<coordSeq.size())
              allSame=coordSeq.getOrdinate(i-1, 2)==coordSeq.getOrdinate(i, 2);
            if (allSame)
              data[idxAttrZ]=coordSeq.getOrdinate(0, 2);
            else {
              MdlZDialog dialog =new MdlZDialog(mdlFille2d_.getVisuPanel().getEditor().getFrame(), MdlResource.getS("Choisissez un Z"));
              dialog.setVisible(true);
              data[idxAttrZ]=dialog.getValue();
            }
          }
        // Ajout de la g�om�trie
        zone.addGeometry(geom, data, cmp);
      }
      if (mdlFille2d_.getCmdMng()!=null)
        mdlFille2d_.getCmdMng().addCmd(cmp.getSimplify());
    }
    catch (IllegalArgumentException _exp) {
      mdlFille2d_.getVisuPanel().getController().getUI().error(_exp.getMessage());
    }
  }
}
