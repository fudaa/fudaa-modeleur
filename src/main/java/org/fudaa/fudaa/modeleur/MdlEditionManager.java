/*
 * @creation     28 janv. 08
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.AbstractButton;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluRunnable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.action.SceneJoinAction;
import org.fudaa.ebli.calque.action.SceneSplitAction;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.edition.BPaletteEdition;
import org.fudaa.ebli.calque.edition.ZCalqueEditable;
import org.fudaa.ebli.commun.EbliActionAbstract;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.fudaa.modeleur.action.SceneAbscisseCurviligneAction;
import org.fudaa.fudaa.modeleur.action.SceneDecimationAction;
import org.fudaa.fudaa.modeleur.action.SceneInterpolationAction;
import org.fudaa.fudaa.modeleur.action.SceneInvertAction;
import org.fudaa.fudaa.modeleur.action.SceneLinearisePolyligneAction;
import org.fudaa.fudaa.modeleur.action.SceneMoveInLayerAction;
import org.fudaa.fudaa.modeleur.action.SceneOrganizePointAction;
import org.fudaa.fudaa.modeleur.action.SceneProfilInterpolationAction;
import org.fudaa.fudaa.modeleur.action.SceneProjectionAction;
import org.fudaa.fudaa.modeleur.action.SceneRefinementAction;
import org.fudaa.fudaa.sig.layer.FSigEditor;
import org.fudaa.fudaa.sig.layer.FSigLayerExporterI;
import org.fudaa.fudaa.sig.layer.FSigLayerFilter;
import org.fudaa.fudaa.sig.layer.FSigVisuPanel;

import com.memoire.bu.BuWizardDialog;
import org.fudaa.ebli.calque.ZCalqueGeometry.SelectionMode;
import org.fudaa.fudaa.modeleur.action.SceneCompareProfileAction;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Un manager pour l'�dition des calques SIG.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MdlEditionManager extends FSigEditor {
  
  /** Le dialogue permettant l'export des g�om�tries */
  MdlLayerExportPanel pnExport_;
  
  /**
   * Constructeur minium.
   * @param _panel Le panneau de visu 2D.
   */
  public MdlEditionManager(MdlVisuPanel _panel) {
    super(_panel, new MdlSceneEditor(_panel,_panel.getScene()));
  }
  
  /**
   * Installation des actions propres au modeleur.
   */
  protected void installSceneActions() {
    super.installSceneActions();

    ArrayList<EbliActionInterface> acts=new ArrayList<EbliActionInterface>();
    EbliActionInterface[] actSuper=sceneEditor_.getActions();
    if (actSuper!=null) acts.addAll(Arrays.asList(actSuper));
    
    acts.add(new SceneMoveInLayerAction((MdlSceneEditor)sceneEditor_, getPanel().getArbreCalqueModel()));
    acts.add(null);
    acts.addAll(Arrays.asList(getEditAction()));
    acts.add(new SceneCompareProfileAction(getPanel()));
    acts.add(null);
    acts.add(new SceneJoinAction(sceneEditor_));
    acts.add(new SceneSplitAction(sceneEditor_));
    acts.add(null);
    acts.add(new SceneDecimationAction((MdlSceneEditor)sceneEditor_));
    acts.add(new SceneRefinementAction((MdlSceneEditor)sceneEditor_));
    acts.add(null);
    acts.add(new SceneInterpolationAction((MdlSceneEditor)sceneEditor_));
    acts.add(new SceneInvertAction((MdlSceneEditor)sceneEditor_));
    acts.add(new SceneProjectionAction((MdlSceneEditor)sceneEditor_));
    acts.add(new SceneProfilInterpolationAction((MdlSceneEditor)sceneEditor_));
    acts.add(new SceneLinearisePolyligneAction((MdlSceneEditor) sceneEditor_));
    acts.add(new SceneOrganizePointAction((MdlSceneEditor) sceneEditor_, "x"));
    acts.add(new SceneOrganizePointAction((MdlSceneEditor) sceneEditor_, "y"));
    acts.add(null);
    acts.add(new SceneAbscisseCurviligneAction(this));
    acts.add(null);
    getSceneEditor().setActions(acts.toArray(new EbliActionAbstract[0]));
  }
  
  /**
   * Surcharge de la m�thode d'import.
   */
  public void importSelectedLayer() {
    final ZCalqueEditable ed = super.getTarget();
    
    BGroupeCalque dest = null;
    BCalque parent = null;
    if (ed == null) {
      parent = super.getPanel().getArbreCalqueModel().getSelectedCalque();
    } else {
      parent = (BCalque) ((BCalque) ed).getParent();
    }
    if (parent instanceof BGroupeCalque) {
      dest = (BGroupeCalque) parent;
    } else {
      return;
    }
    
    final MdlWizardImport importWizard=new MdlWizardImport(getPanel(), getEnglobPolygone(), ed,
        ((FSigVisuPanel)getPanel()).getCtuluUI(), ((FSigVisuPanel)getPanel()).getCmdMng());
    final BuWizardDialog dialog=new BuWizardDialog(CtuluLibSwing.getFrameAncestorHelper(super.getPanel()), importWizard);
    importWizard.setDialog(dialog);      
    dialog.pack();
    dialog.setModal(true);
    dialog.setLocationRelativeTo(getPanel());
    dialog.setVisible(true);
  }

  /**
   * Exporte les donn�es de la fenetre.
   */
  public void exportLayers() {
    ZScene scn=getSupport();
    boolean bpreselect=
      !scn.isAtomicMode() && 
      !scn.isSelectionEmpty();

    if (pnExport_==null)
      pnExport_=new MdlLayerExportPanel(getUi());
    pnExport_.update(bpreselect);
    
    if (CtuluDialogPanel.isCancelResponse(pnExport_.afficheModale(getFrame(), MdlResource.getS("Exporter les donn�es de la fen�tre active")))) return;
    
    final FSigLayerFilter filter=new FSigLayerFilter();
    if (pnExport_.isAllLayers())
      getPanel().getDonneesCalque().apply(filter);
    else if (pnExport_.isSelectedLayers()) {
      final BCalque[] parent=getPanel().getArbreCalqueModel().getSelection();
      if (parent!=null)
        for (int i=0; i<parent.length; i++)
          parent[i].apply(filter);
    }
    else if (pnExport_.isVisibleLayers()) {
      filter.setTreatmentOnlyOnVisible(true);
      getPanel().getDonneesCalque().apply(filter);
    }
    else { // Only selected geometries
      filter.setTreatmentOnlySelectedGeometries(true);
      getPanel().getDonneesCalque().apply(filter);
    }

    if (filter.isEmpty())
      return;

    final File f=pnExport_.getFile();
    final FSigLayerExporterI exporter=pnExport_.getSelectedExporter();
    new CtuluRunnable(MdlResource.getS("Export de donn�es"), getUi()) {

      public boolean run(ProgressionInterface _proj) {
        final CtuluIOOperationSynthese op = filter.exportTo(exporter, getUi(), f, _proj);
        if (op != null) {
          getUi().manageErrorOperationAndIsFatal(op);
        }
        return true;
      }

    }.run();

  }
  
  @Override
  public void updatePalette() {
    if (palette_ == null) {
      return;
    }

    // B.M. C'est le meilleur moment pour customiser la palette, on ne maitrise pas
    // le processus de cr�ation de la palette auparavant.
    palette_.setButtonVisible(new String[]{BPaletteEdition.SEGMENT_ACTION}, false);

    if (target_ == null) {
      palette_.setAllEnable(false);
      palette_.setEnable(BPaletteEdition.ATOM_ACTION, true);
      SelectionMode mode = palette_.getSelectionMode();
      getSupport().setSelectionMode(mode);
      palette_.setAtomeEnable(getSupport().canUseAtomicMode(target_), getSupport().getSelectionMode() == SelectionMode.ATOMIC);
      palette_.setSegmentEnable(getSupport().canUseSegmentMode(target_), getSupport().getSelectionMode() == SelectionMode.SEGMENT);
      palette_.checkEnableAndCheckBt();
      changeState(null);
    } else {
      palette_.setAllEnable(true);
      SelectionMode mode = palette_.getSelectionMode();
      getSupport().setSelectionMode(mode);
      palette_.setAtomeEnable(getSupport().canUseAtomicMode(target_), getSupport().getSelectionMode() == SelectionMode.ATOMIC);
      palette_.setSegmentEnable(getSupport().canUseSegmentMode(target_), getSupport().getSelectionMode() == SelectionMode.SEGMENT);
      boolean isModifiable = true;
      if(target_.getModelEditable()!=null)
        if (target_.getModelEditable().getGeomData() != null)
          isModifiable = target_.getModelEditable().getGeomData().isGeomModifiable();
      palette_.setEnable(BPaletteEdition.ADD_POINT_ACTION, target_.canAddForme(DeForme.POINT) && isModifiable);
      palette_.setEnable(BPaletteEdition.ADD_POLYLINE_ACTION, target_.canAddForme(DeForme.LIGNE_BRISEE) && isModifiable);
      palette_.setEnable(BPaletteEdition.ADD_RECTANGLE_ACTION, target_.canAddForme(DeForme.RECTANGLE) && isModifiable);
      palette_.setEnable(BPaletteEdition.ADD_ELLIPSE_ACTION, target_.canAddForme(DeForme.ELLIPSE) && isModifiable);
      palette_.setEnable(BPaletteEdition.ADD_POLYGON_ACTION, target_.canAddForme(DeForme.POLYGONE) && isModifiable);
      palette_.setEnable(BPaletteEdition.ADD_MULTIPOINT_ACTION, target_.canAddForme(DeForme.MULTI_POINT) && isModifiable);
    }
    updatePaletteWhenSelectionChanged();
    palette_.checkEnableAndCheckBt();
    final AbstractButton bt = palette_.getSelectedButton();
    if (bt == null) {
      changeState(null);
    } else {
      changeState(bt.getActionCommand());
    }
  }

  public MdlVisuPanel getPanel() {
    return (MdlVisuPanel)super.getPanel();
  }
}
