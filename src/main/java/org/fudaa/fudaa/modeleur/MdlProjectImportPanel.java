/*
 * @creation 8 sept. 06
 * @modification $Date: 2008/05/13 12:10:21 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.modeleur;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;

import org.fudaa.ctulu.gui.CtuluCellRenderer;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.fudaa.commun.save.FudaaSaveProject;
import org.fudaa.fudaa.commun.save.FudaaSaveZipLoader;
import org.fudaa.fudaa.modeleur.MdlProjectImportTreeModel.LayerNode;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuVerticalLayout;
import com.memoire.fu.FuLog;

/**
 * Un panneau pour fusionner un fichier projet dans le projet courant par importation.
 * @author Bertrand Marchand
 * @version $Id: MdlProjectImportPanel.java,v 1.1.2.1 2008/05/13 12:10:21 bmarchan Exp $
 */
public class MdlProjectImportPanel extends CtuluDialogPanel {
  
  /**
   * Une classe permettant le rendu de l'arbre des calques dans le panneau d'import de projet.
   * @author Bertrand Marchand
   * @version $Id:$
   */
  private static class CellRenderer extends JLabel implements TreeCellRenderer {
    final Color selectedBackground_ = UIManager.getColor("Tree.selectionBackground");
    final Color selectedForground_ = UIManager.getColor("Tree.selectionForeground");
    final Border focusBorderColor_ = BorderFactory.createLineBorder(UIManager.getColor("Tree.selectionBorderColor"), 1);

    public CellRenderer() {
      this.setOpaque(true);
//      setPreferredSize(new Dimension(120, 25));
    }

    protected boolean isSelectable(final Object _value, final boolean _leaf) {
      return true;
    }

    public Component getTreeCellRendererComponent(final JTree _tree, final Object _value, final boolean _selected,
        final boolean _expanded, final boolean _leaf, final int _row, final boolean _hasFocus) {
      this.setFont(_tree.getFont());
      setIcon(BuResource.BU.getIcon("calque"));
      setText(_value.toString());
      final boolean selectable = isSelectable(_value, _leaf);
      if (_selected && selectable) {
        setBackground(selectedBackground_);
        setForeground(selectedForground_);
      } else {
        setBackground(_tree.getBackground());
        setForeground(_tree.getForeground());
      }
      if (selectable) {
        setToolTipText(getText());
      } else {
        setToolTipText(getText() + ": " + MdlResource.getS("Non s�lectionnable"));
      }
      setBorder((selectable && _hasFocus) ? focusBorderColor_ : CtuluCellRenderer.BORDER_NO_FOCUS);
      setEnabled(_tree.isEnabled());
      return this;
    }
  }


  CtuluFileChooserPanel pn_;
  String filename_="";
  BuRadioButton rbAllLayers_;
  BuRadioButton rbSelectedLayers_;
  FileFilter flt_;
  JTree trLayers_;
  FudaaSaveZipLoader loader_=null;
  String[] ignoredLayers=new String[0];

  public MdlProjectImportPanel() {
    ActionListener rbListener=new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        if (rbSelectedLayers_.isSelected()) {
          if (isFileOK()) loadFile();
        }
        trLayers_.setEnabled(rbSelectedLayers_.isSelected());
        trLayers_.setModel(new MdlProjectImportTreeModel(loader_));
      }
    };
    
    setLayout(new BuVerticalLayout(5,true,true));
    final String title = MdlResource.getS("Le fichier projet");
    flt_=MdlImplementation.FILTER;
    pn_ = new CtuluFileChooserPanel(title);
    pn_.getTf().addCaretListener(new CaretListener() {
      public void caretUpdate(CaretEvent e) {
        if (!filename_.equals(pn_.getTf().getText())) {
          filename_=pn_.getTf().getText();
          closeLoader();
          rbAllLayers_.doClick();
          setErrorText(null);
        }
      }
    });
    pn_.setAllFileFilter(false);
    pn_.setFileSelectMode(JFileChooser.FILES_ONLY);
    pn_.setWriteMode(false);
    pn_.setFilter(new FileFilter[] {flt_});
    
    BuLabel lbFile=new BuLabel(title);
    JPanel pnFile=new JPanel();
    pnFile.setLayout(new BorderLayout(3,3));
    pnFile.add(lbFile,BorderLayout.WEST);
    pnFile.add(pn_, BorderLayout.CENTER);
    add(pnFile);
    
    rbAllLayers_=new BuRadioButton(MdlResource.getS("Fusionner tous les calques"));
    rbAllLayers_.addActionListener(rbListener);
    add(rbAllLayers_);
    
    rbSelectedLayers_=new BuRadioButton(MdlResource.getS("S�lectionner les calques � fusionner"));
    rbSelectedLayers_.addActionListener(rbListener);
    add(rbSelectedLayers_);

    ButtonGroup bgLayers=new ButtonGroup();
    bgLayers.add(rbAllLayers_);
    bgLayers.add(rbSelectedLayers_);
    
    trLayers_=new JTree();
    trLayers_.setShowsRootHandles(true);
    trLayers_.setRootVisible(false);
    trLayers_.setCellRenderer(new CellRenderer());
    JScrollPane sp=new JScrollPane(trLayers_);
    sp.setPreferredSize(new Dimension(300,200));
    add(sp);
    
    rbAllLayers_.doClick();

  }

  @Override
  public boolean isDataValid() {
    if (!isFileOK() || loadFile()==null) return false;
    
    // Mise a jour des �tats des calques. Par defaut, les calques sopnt r�cup�r�s.
    if (rbSelectedLayers_.isSelected()) {
      List<LayerNode> nds=getChildrenLeafs((LayerNode)trLayers_.getModel().getRoot());
      
      TreePath[] paths=trLayers_.getSelectionPaths();
      if (paths!=null) {
        for (TreePath path : paths) {
          nds.remove((LayerNode)path.getLastPathComponent());
        }
      }
      
      ignoredLayers=new String[nds.size()];
      for (int i=0; i<ignoredLayers.length; i++) {
        ignoredLayers[i]=nds.get(i).name_;
      }
    }
    closeLoader();
    return true;
  }
  
  @Override
  public boolean cancel() {
    closeLoader();
    return super.cancel();
  }
  
  private void closeLoader() {
    if (loader_!=null) {
      try { loader_.close(); } catch (IOException _exc) {}
    }
    loader_=null;
  }
  
  private List<LayerNode> getChildrenLeafs(LayerNode _nd) {
    List<LayerNode> r=new ArrayList<LayerNode>();
    int nb=_nd.getChildCount();
    for (int i=0; i<nb; i++) {
      LayerNode nd=(LayerNode)_nd.getChildAt(i);
      if (nd.isLeaf()) {
        r.add(nd);
      }
      else {
        List<LayerNode> ls=getChildrenLeafs(nd);
        r.addAll(ls);
      }
    }
    return r;
  }
  
  private boolean isFileOK() {
    closeLoader();
    final File f = pn_.getFile();
    if (f == null || !f.exists()) {
      setErrorText(MdlResource.getS("Le fichier n'existe pas"));
      return false;
    }
    if (!flt_.accept(f)) {
      setErrorText(MdlResource.getS("Le fichier choisi n'est pas un fichier projet"));
      return false;
    }
    setErrorText(null);
    return true;
  }
  
  private FudaaSaveZipLoader loadFile() {
    if (loader_!=null) return loader_;
    
    try {
//      if (isFileOK()) {
        loader_=new FudaaSaveZipLoader(pn_.getFile());
        BuInformationsSoftware is=FudaaSaveProject.getSoftwareInfos(loader_);
        FuLog.warning("File version : "+(is.version==null ? "Undefined":is.version));
//      }
    }
    catch (IOException _exc) {
      setErrorText(MdlResource.getS("Le fichier n'est pas accessible"));
    }
    return loader_;
  }

  public File getFile() {
    return pn_.getFile();
  }
  
  public String[] getIgnoredLayers() {
    return ignoredLayers;
  }
}
