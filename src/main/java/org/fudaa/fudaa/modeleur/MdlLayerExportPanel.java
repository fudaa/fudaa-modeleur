/*
 * @creation 8 sept. 06
 * @modification $Date: 2008/05/13 12:10:21 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.modeleur;

import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.dodico.telemac.io.SinusxFileFormat;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.fudaa.fudaa.sig.layer.FSigLayerExporterI;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.dodico.ef.io.dunes.DunesGEOFileFormat;

/**
 * Un panneau pour exporter les donn�es 2D.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MdlLayerExportPanel extends CtuluDialogPanel implements DocumentListener {
  
  CtuluFileChooserPanel fileChooser_;
  String filename_="";
  BuRadioButton rbAllLayers_;
  BuRadioButton rbSelectedLayers_;
  BuRadioButton rbVisibleLayers_;
  protected BuRadioButton rbSelectedGeometries_; 
  BuFileFilter[] filters_;
  CtuluUI ui_;
  BuFileFilter selectedFilter_=null;
  Map<BuFileFilter, FSigLayerExporterI> filter2Exporter_;
  /** Le filtre de fichier pour Rubar */
  BuFileFilter fltRubar_;
  BuComboBox coFormat_; 
  FSigLayerExporterI[] exporters_;

  /**
   * @param _ui L'ui.
   */
  public MdlLayerExportPanel(CtuluUI _ui) {
    ui_=_ui;
    
    filters_=buildFilter2Export();

    setLayout(new BuVerticalLayout(5,true,true));

    JPanel pnFormat=new JPanel();
    pnFormat.setLayout(new BorderLayout(5,5));
    coFormat_=new BuComboBox();
    for (BuFileFilter filter: filters_)
      coFormat_.addItem(filter.getShortDescription());
    coFormat_.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange()==ItemEvent.DESELECTED) return;
        coFormatItemStateChanged();
      }
    });
    
    pnFormat.add(new BuLabel(MdlResource.getS("Format d'export")),BorderLayout.WEST);
    pnFormat.add(coFormat_);
    add(pnFormat);
    final String title = MdlResource.getS("Fichier(s) d'exportation");
    fileChooser_ = new CtuluFileChooserPanel(title) {
      @Override
      protected void fileChooserAccepted(CtuluFileChooser _fc) {
        // Cas de Rubar : On supprime l'extension au fichier
        if (filter2Exporter_.get(_fc.getFileFilter())==exporters_[0]) {
          tf_.setText(CtuluLibFile.getSansExtension(tf_.getText()));
        }
      }
    };
    fileChooser_.setAllFileFilter(false);
    fileChooser_.setFileSelectMode(JFileChooser.FILES_ONLY);
    fileChooser_.setWriteMode(true);
    
    BuLabel lbFile=new BuLabel(title);
    JPanel pnFile=new JPanel();
    pnFile.setLayout(new BorderLayout(3,3));
    pnFile.add(lbFile,BorderLayout.WEST);
    pnFile.add(fileChooser_, BorderLayout.CENTER);
    add(pnFile);
    
    rbAllLayers_=new BuRadioButton(MdlResource.getS("Exporter tous les calques"));
    rbSelectedLayers_=new BuRadioButton(MdlResource.getS("Exporter les calques s�lectionn�s (et leurs sous-calques)"));
    rbVisibleLayers_=new BuRadioButton(MdlResource.getS("Exporter les seuls calques visibles"));
    rbSelectedGeometries_=new BuRadioButton(MdlResource.getS("Exporter les g�om�tries selectionn�es"));
    add(rbAllLayers_);
    add(rbSelectedLayers_);
    add(rbVisibleLayers_);
    add(rbSelectedGeometries_);
    ButtonGroup bgLayers=new ButtonGroup();
    bgLayers.add(rbAllLayers_);
    bgLayers.add(rbSelectedLayers_);
    bgLayers.add(rbVisibleLayers_);
    bgLayers.add(rbSelectedGeometries_);

    BuLabel lbAide=new BuLabel(MdlResource.getS("Remarque : Les g�om�tries non visibles ne seront pas export�es"));
    lbAide.setFont(MdlResource.HELP_FONT);
    lbAide.setForeground(MdlResource.HELP_FORGROUND_COLOR);
    lbAide.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
    add(lbAide);

    coFormatItemStateChanged();
    rbAllLayers_.setSelected(true);
  }
  
  /**
   * Mise a jour de la fenetre.
   * @param _bpreselect true : Des g�om�tries pr�selectionn�es existent. false sinon.
   */
  public void update(boolean _bpreselect) {
    rbSelectedGeometries_.setEnabled(_bpreselect);
    if (_bpreselect)
      rbSelectedGeometries_.setSelected(true);
    else {
      if (rbSelectedGeometries_.isSelected())
        rbAllLayers_.setSelected(true);
    }
  }
  
  private BuFileFilter[] buildFilter2Export() {
    final BuFileFilter sx = SinusxFileFormat.getInstance().createFileFilter();
    fltRubar_=new BuFileFilter(new String[]{"cn","st","sem"},"Cemagref");
    final BuFileFilter dunes= DunesGEOFileFormat.getInstance().createFileFilter();
    filter2Exporter_ = new HashMap<BuFileFilter, FSigLayerExporterI>(5);// pour l'instant .....

    exporters_=new FSigLayerExporterI[]{
      new FSigLayerExporterI.ToRubar(),
      new FSigLayerExporterI.ToSinusX(),
      new FSigLayerExporterI.ToDunes()
      
    };
    filters_ = new BuFileFilter[]{fltRubar_,sx,dunes};
    
    for (int i=0; i<filters_.length; i++) 
      filter2Exporter_.put(filters_[i], exporters_[i]);

    return filters_;
  }

  /**
   * Changement de format d'export.
   */
  private void coFormatItemStateChanged() {
    selectedFilter_=filters_[coFormat_.getSelectedIndex()];
    fileChooser_.setFilter(new BuFileFilter[]{selectedFilter_});
    
    // Changement de l'extension du fichier.
    File f=fileChooser_.getFile();
    if (f!=null) {
      f=CtuluLibFile.getSansExtension(f);
      if (selectedFilter_!=fltRubar_) {
        f=CtuluLibFile.appendExtensionIfNeeded(f, selectedFilter_.getFirstExt());
      }
      fileChooser_.setFile(f);
    }
  }

  @Override
  public boolean isDataValid() {
    if (!isFileOK()) return false;
    return true;
  }
  
  private boolean isFileOK() {
    final File f = fileChooser_.getFile();
    if (f == null) {
      setErrorText(MdlResource.getS("Donnez un nom au(x) fichier(s) d'exportation"));
      return false;
    }

    String s="";
    int nbFiles=0;
    
    // Dans le cas d'un export vers Rubar, il faut aussi tester avec les extensions .sem et .cn et aussi <nom>_autres.st
    if (selectedFilter_==fltRubar_) {
      String name=CtuluLibFile.getSansExtension(f.getName());
      if ((new File(f.getParent()+File.separatorChar+name+".sem")).exists()) {
        s+=(s.length()!=0 ? ", ":"")+name+".sem";
        nbFiles++;
      }
      if ((new File(f.getParent()+File.separatorChar+name+".st")).exists()) {
        s+=(s.length()!=0 ? ", ":"")+name+".st";
        nbFiles++;
      }
      if ((new File(f.getParent()+File.separatorChar+name+".cn")).exists()) {
        s+=(s.length()!=0 ? ", ":"")+name+".cn";
        nbFiles++;
      }
      if ((new File(f.getParent()+File.separatorChar+name+"_autres.st")).exists()) {
        s+=(s.length()!=0 ? ", ":"")+name+"_autres.st";
        nbFiles++;
      }
    }
    
    // Cas des autres formats.
    else {
      if (CtuluLibFile.getExtension(f.getName())==null) {
        setErrorText(MdlResource.getS("Le fichier choisi doit avoir une extension."));
        return false;
      }
      if (CtuluLibFile.getExtension(CtuluLibFile.getSansExtension(f.getName()))!=null) {
        setErrorText(MdlResource.getS("Ne mettez pas plusieurs '.' dans le nom."));
        return false;
      }
      
      if (f.exists()){
        s+=f.getName();
        nbFiles++;
      }
    }
    
    if (nbFiles>0)
      if(nbFiles==1&&!ui_.question(MdlResource.getS("Export {0}",selectedFilter_.getShortDescription()),
          MdlResource.getS("Le fichier {0} existe d�j�.\nVoulez-vous l'�craser?", s)))
        return false;
      else if(nbFiles>1&&!ui_.question(MdlResource.getS("Export {0}",selectedFilter_.getShortDescription()),
          MdlResource.getS("Les fichiers {0} existent d�j�.\nVoulez-vous les �craser?", s)))
        return false;
    
    setErrorText(null);
    return true;
  }
  
  public File getFile() {
    return fileChooser_.getFile();
  }
  
  public FSigLayerExporterI getSelectedExporter() {
    return (FSigLayerExporterI)filter2Exporter_.get(selectedFilter_);
  }
  
  public boolean isAllLayers() {
    return rbAllLayers_.isSelected();
  }
  
  public boolean isSelectedLayers() {
    return rbSelectedLayers_.isSelected();
  }
  
  public boolean isVisibleLayers() {
    return rbVisibleLayers_.isSelected();
  }
  
  public boolean isSelectedGeometries() {
    return rbSelectedGeometries_.isSelected();
  }

  public void changedUpdate(final DocumentEvent _e) {
  }

  public void insertUpdate(final DocumentEvent _e) {
  }

  public void removeUpdate(final DocumentEvent _e) {
  }
}
