/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.modeleur.common;

import com.memoire.bu.BuPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.CtuluResult;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ebli.courbe.EGCourbeSimple;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.modeleur.MdlCompareProfilePanel;
import org.fudaa.fudaa.modeleur.MdlPaletteCourbe;
import org.fudaa.fudaa.modeleur.modeleur1d.view.VueCourbe;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Panel de comparaison de courbes.
 *
 * @author Adrien Hadoux
 */
public class ComparisonCurvesPanel extends BuPanel implements CtuluImageProducer {

    final JSplitPane parent;
    final JScrollPane scrollPanel;
    /**
     * Selection de profils a comparer. Représente la selection des indices des selectionCurves_ dans le modele du
     * graphe, pour nettoyer les courbes en cas de nouvelle selection.
     */
    int[] selectionCurves_;

    public ComparisonCurvesPanel(JSplitPane pane) {
        parent = pane;
        this.setBackground(Color.white);
        scrollPanel = new JScrollPane(this);
        scrollPanel.setBackground(Color.white);

        scrollPanel.getViewport().addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                ComparisonCurvesPanel.this.repaint();
            }
        });
    }

    public void setVisible(boolean isVisible) {
        super.setVisible(isVisible);
        parent.remove(scrollPanel);
        if (isVisible) {
            parent.add(scrollPanel, JSplitPane.BOTTOM);
        }
    }
JPanel panelConfig;
    private void buildConfig(final int[] selection, final VueCourbe vue, final int idxProf) {
        JButton button = new JButton(EbliResource.EBLI.getIcon("palettecouleur_16"));
        button.setBackground(Color.white);
        button.setPreferredSize(new Dimension(16, 16));
         panelConfig = new JPanel();
        panelConfig.setBackground(Color.white);
        panelConfig.add(new JLabel(MdlResource.getS("Configurer")));
        panelConfig.add(button);
        this.add(panelConfig);
        final ChangeListener changeEvent = new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                updateComparisonCurves(vue, selection,instParent,idxProf);
            }
        };
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MdlPaletteCourbe.getInstance().displayUiManager(null, changeEvent);

            }
        });
    }

    public TraceLigneModel getTraceLigneModel(int indice) {
        return MdlPaletteCourbe.getInstance().getTraceLigneModelForGroupAndCurve(0, indice);
    }

   private  List<BuPanel> entriesList = new ArrayList<BuPanel>();
  private MdlCompareProfilePanel instParent;
  
    public boolean updateComparisonCurves(final VueCourbe vue, final int[] selection,final MdlCompareProfilePanel compPanel,final int idxProf) {
        boolean structureChanged = false;
     if(compPanel != null) {
         instParent = compPanel;
     }
        if (selectionCurves_ != null && selectionCurves_.length > 0) {
            vue.getGrapheModel().removeCurves(selectionCurves_, null);
            structureChanged = true;
        }

        if (selection != null && selection.length > 1) {
            this.removeAll();
            entriesList = new ArrayList<BuPanel>();
            listeComponentToHide= new ArrayList<JComponent>();
            //this.setLayout(new GridLayout(Math.max(10, selection.length + 1), 1));
          //  this.setLayout(new GridLayout(selection.length + 1, 1));
          this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
          this.setBounds(10, 0, 150, 40 * (selection.length + 1) );
          buildConfig(selection, vue,idxProf);
            this.setBorder(BorderFactory.createTitledBorder(MdlResource.getS("Légende")));
            //-- set entry legende to the curve to compare with --//
            BuPanel entry = buildEntryLegende(vue.getCourbe().getTitle(), vue.getCourbe().getLigneModel().getCouleur(), 
                    vue.getCourbe(), true,vue, idxProf, -1);
            this.add(entry);
            entriesList.add(entry);
            selectionCurves_ = new int[selection.length - 1];
            int cptSelection = 0;
            for (int i = 0; i < selection.length; i++) {
                int idxProfil = selection[i];
                if (vue.getSelectedProfile() != idxProfil) {
                    TraceLigneModel modelLigne = getTraceLigneModel(cptSelection);
                    selectionCurves_[cptSelection] = vue.buildCurve(modelLigne, idxProfil);
                    EGCourbeSimple courbe = vue.getGrapheModel().getCourbe(selectionCurves_[cptSelection]);
                    this.add(buildEntryLegende(courbe.getTitle(), modelLigne.getCouleur(), courbe,vue,idxProfil,selectionCurves_[cptSelection] ));
                    structureChanged = true;
                    cptSelection++;
                }
            }
        } else {
            selectionCurves_ = null;
        }

        this.setVisible(selection.length > 1);
        return structureChanged;
    }

    private BuPanel buildEntryLegende(final String curveName, final Color color, final EGCourbeSimple courbe,
            final VueCourbe vue, final int idxProfil, final int cptCourbeModel) {
        return buildEntryLegende(curveName, color, courbe, false, vue,idxProfil, cptCourbeModel);
    }

    private BuPanel buildEntryLegende(final String curveName, final Color color, final EGCourbeSimple courbe, 
            final boolean important, final VueCourbe vue, final int idxProfil, final int cptCourbeModel) {
        final Dimension dimensionLegendeEntry = new Dimension(130, 40);
        final BuPanel panelLabelCurve = new BuPanel() {
            @Override
            public void paint(Graphics _g) {
                super.paint(_g);
                String name = curveName;
                _g.setColor(courbe.getLigneModel().getCouleur());
                int widthColor = _g.getClipBounds().width / 7;
                int heightColor = _g.getClipBounds().height;
                System.out.println(".paint() height color = " + heightColor);
                _g.fillRect(widthColor / 4, heightColor / 3, widthColor, dimensionLegendeEntry.height / 2);
                _g.setColor(Color.black);
                if (important) {
                    _g.setFont(new Font(_g.getFont().getName(), Font.BOLD, _g.getFont().getSize()));
                    name = name + " (*)";
                }
                //if(heightColor>dimensionLegendeEntry.height/2)
                _g.drawString(name, widthColor + widthColor / 4 + 10,heightColor / 3 +_g.getFont().getSize() );
            }

        };
        panelLabelCurve.setBackground(Color.WHITE);
        panelLabelCurve.setMinimumSize(dimensionLegendeEntry);
        panelLabelCurve.setMaximumSize(dimensionLegendeEntry);
        panelLabelCurve.setPreferredSize(dimensionLegendeEntry);

        BuPanel panelLeg = new BuPanel(new BorderLayout());
        panelLeg.add(panelLabelCurve);
        //final JCheckBox checkDisplay = new JCheckBox();
        final JButton buttonVisible = new JButton(CtuluResource.CTULU.getIcon("crystal_visible"));
        buttonVisible.addActionListener(new ActionListener() {
            boolean toggle=true;
            @Override
            public void actionPerformed(ActionEvent e) {
                courbe.setTitle(curveName);
                courbe.setDisplayTitleOnCurve(toggle);
                toggle = !toggle;
                           
            }
        });
        
        final JButton button = new JButton(CtuluResource.CTULU.getIcon("crystal_maj"));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                instParent.changeSelectedProfile(idxProfil);
                if(cptCourbeModel == -1)
                    vue.getGrapheModel().setSelectedComponent(courbe);
                else
                  vue.getGrapheModel().select(cptCourbeModel);    
            }
        });
        JPanel conteneurButton = new JPanel();
        JPanel conteneurCheck = new JPanel();
        
        buttonVisible.setBackground(Color.white);
        button.setBackground(Color.white);
        conteneurButton.setBackground(Color.white);
        panelLeg.setBackground(Color.white);
        conteneurCheck.setBackground(Color.white);
        button.setPreferredSize(new Dimension(16,12));
        button.setSize(new Dimension(16,12));
        button.setMaximumSize(new Dimension(16,12));
        buttonVisible.setPreferredSize(new Dimension(16,12));
        buttonVisible.setSize(new Dimension(16,12));
        buttonVisible.setMaximumSize(new Dimension(16,12));
        
        GridLayout layoutButton = new GridLayout(1,2);
        
        JPanel grid = new JPanel(layoutButton);
        grid.setPreferredSize(new Dimension(40,16));
        grid.setMaximumSize(new Dimension(40,16));
        grid.setSize(new Dimension(40,16));
        conteneurCheck.add(buttonVisible);
        grid.add(conteneurCheck);
        conteneurButton.add(button);
        grid.add(conteneurButton);
        listeComponentToHide.add(grid);
        panelLeg.add(grid, BorderLayout.EAST);
        return panelLeg;
    }
    
    private List<JComponent> listeComponentToHide= new ArrayList<JComponent>();
  //  private void updateSelectedCourbe()
    

    @Override
    public BufferedImage produceImage(Map _params) {
        return produceImage(getWidth(), getHeight(), _params);
    }

    
    private void setVisibleLegendeComp(boolean visible) {
        for( JComponent comp:listeComponentToHide)
            comp.setVisible(visible);
         panelConfig.setVisible(visible);
    }
    
    @Override
    public BufferedImage produceImage(int _w, int _h, Map _params) {
        setVisibleLegendeComp(false);
          
        BufferedImage bi = new BufferedImage(_w, _h, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = bi.createGraphics();
        
        this.paint(g);
        setVisibleLegendeComp(true);
        // this.setLayout(new GridLayout(Math.max(10, entriesList.size() + 1), 1));
         
        return bi;

    }

    @Override
    public Dimension getDefaultImageDimension() {
        return getSize();
    }

}
