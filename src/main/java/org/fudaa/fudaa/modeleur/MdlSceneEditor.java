/*
 * @creation     25 avr. 08
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeModelDoubleArray;
import org.fudaa.ctulu.gis.GISCoordinateSequenceContainerInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISDataModelFilterAdapter;
import org.fudaa.ctulu.gis.GISDataModelMultiAdapter;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISMultiPoint;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionGeometry;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.interpolation.InterpolationParameters;
import org.fudaa.ctulu.interpolation.InterpolationResultsHolderI;
import org.fudaa.ctulu.interpolation.InterpolationSupportGISAdapter;
import org.fudaa.ctulu.interpolation.InterpolationTarget;
import org.fudaa.ctulu.interpolation.InterpolationTargetGISAdapter;
import org.fudaa.ctulu.interpolation.bilinear.InterpolatorBilinear;
import org.fudaa.ctulu.interpolation.profile.GISDataModelToPointCloudAdapter;
import org.fudaa.ctulu.interpolation.profile.ProfileCalculator;
import org.fudaa.ctulu.interpolation.profile.ProfileCalculatorWindow;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZModeleDonnees;
import org.fudaa.ebli.calque.ZModeleGeometry;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.edition.ZCalqueEditable;
import org.fudaa.ebli.calque.edition.ZModeleEditable;
import org.fudaa.ebli.calque.edition.ZSceneEditor;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.fudaa.modeleur.layer.MdlModel2dLine;
import org.fudaa.fudaa.modeleur.layer.MdlModel2dMultiPoint;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;

import com.memoire.fu.FuLog;

import gnu.trove.TIntArrayList;

/**
 * L'�diteur effectuant les traitements sur la scene sp�cifiques au modeleur.
 *
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MdlSceneEditor extends ZSceneEditor {
  // Pour l'affichage des boites de dialogue.

  MdlVisuPanel pn_;
  // Pour conserver les valeurs IHM d'un appel a un autre.
  MdlDecimationPanel pnDecimation_ = null;
  // Pour conserver les valeurs IHM d'un appel a un autre.
  MdlInterpolationPanel pnInterpolation_ = null;
  // Pour conserver les valeurs IHM d'un appel a un autre.
  MdlRefinementPanel pnRaffinement_ = null;

  public MdlSceneEditor(MdlVisuPanel _pn, ZScene _scene) {
    super(_scene, _pn);
    pn_ = _pn;
  }

  /**
   * Interpole entre 2 sommets. Les 2 sommets peuvent appartenir au m�me bloc.
   */
  public void interpolateSelectedGeometries() {
    if (pnInterpolation_ == null) {
      pnInterpolation_ = new MdlInterpolationPanel();
    }

    if (pnInterpolation_.afficheModaleOk(pn_.getFrame(), MdlResource.getS("Interpolation entre 2 sommets"))) {
      int[] idxGeom = getScene().getLayerSelectionMulti().getIdxSelected();

      Coordinate[] coords = new Coordinate[2];
      int nbSom = 0;
      for (int i = 0; i < idxGeom.length; i++) {
        int[] idxSom = getScene().getLayerSelectionMulti().getSelection(idxGeom[i]).getSelectedIndex();
        for (int j = 0; j < idxSom.length; j++) {
          Geometry geom = (Geometry) getScene().getObject(idxGeom[i]);
          if (geom instanceof LineString) {
            LineString g = (LineString) geom;
            coords[nbSom++] = g.getCoordinateSequence().getCoordinate(idxSom[j]);
          } else if (geom instanceof GISMultiPoint) {
            GISMultiPoint g = (GISMultiPoint) geom;
            coords[nbSom++] = g.getCoordinateSequence().getCoordinate(idxSom[j]);
          }
        }
      }

      double dist;
      if (pnInterpolation_.isNbPtsGiven()) { // Nombre de points donn�.
        int nbpts = pnInterpolation_.getNbPts();
        dist = Math.sqrt((coords[1].x - coords[0].x) * (coords[1].x - coords[0].x)
                + (coords[1].y - coords[0].y) * (coords[1].y - coords[0].y))
                / (nbpts + 1);
      } else {  // Distance donn�e.
        dist = pnInterpolation_.getDistance();
      }

      ZModeleDonnees mdld = ((ZCalqueAffichageDonneesInterface) getScene().getCalqueActif()).modeleDonnees();
      if (mdld instanceof MdlModel2dMultiPoint) {
        MdlModel2dMultiPoint mdl = (MdlModel2dMultiPoint) mdld;
        if (!mdl.interpolate(coords, dist, pnInterpolation_.useBorderPoints(), getMng())) {
          ui_.warn(MdlResource.getS("Interpolation"), MdlResource.getS("L'interpolation n'a pas pu avoir lieu par manque de points."), false);
        }
      } else if (mdld instanceof MdlModel2dLine) {
        MdlModel2dLine mdl = (MdlModel2dLine) mdld;
        if (!mdl.interpolate(coords, dist, pnInterpolation_.useBorderPoints(), getMng())) {
          ui_.warn(MdlResource.getS("Interpolation"), MdlResource.getS("L'interpolation n'a pas pu avoir lieu par manque de points."), false);
        }
      }
    }
  }

  /**
   * Controle que les g�om�tries � copier vers le calque destination peuvent l'etre.<p> <b>Remarque</b> : Il n'est pas tenu compte des attributs li�s
   * au mod�le, uniquement des g�om�tries.
   *
   * @param _mdlSource Le modele contenant les g�om�tries � copier.
   * @param _cqDest Le calque de destination.
   * @return false si au moins une des g�om�tries n'a pas le nombre de sommets requis. true sinon.
   */
  public boolean canCopyGeometries(GISDataModel _mdlSource, ZCalqueEditable _cqDest) {
    int nbMin = 1;
    if (_cqDest.canAddForme(DeForme.POLYGONE) && !_cqDest.canAddForme(DeForme.LIGNE_BRISEE)) {
      nbMin = 3;
    } else if (_cqDest.canAddForme(DeForme.LIGNE_BRISEE)) {
      nbMin = 2;
    }

    // Controle sur le nombre de point minimum
    if (nbMin > 1) {
      int nbProbGeom = 0;
      for (int i = 0; i < _mdlSource.getNumGeometries(); i++) {
        Geometry g = _mdlSource.getGeometry(i);
        if (g.getNumPoints() < nbMin) {
          nbProbGeom++;
        }
      }
      if (nbProbGeom > 0) {
        ui_.error(MdlResource.getS("Vous ne pouvez pas coller ou d�placer ces g�om�tries.\n{0} g�om�trie(s) a/ont moins de {1} points.",
                nbProbGeom, nbMin));
        return false;
      }
    }
    return true;
  }

  /**
   * Copie des g�om�tries d'un mod�le source vers un calque. Le controle que les g�om�tries sont conformes est fait en amont de la copie.<p> Le mod�le
   * source contient les g�om�tries � copier d'un m�me calque, afin de r�cup�rer le maximum d'informations des attributs source. La m�thode doit donc
   * �tre appel�e en boucle pour chaque calque contenant des g�om�tries a copier.
   *
   * @param _mdlSource Le modele contenant les g�om�tries � copier.
   * @param _cqDest Le calque de destination.
   */
  public void copyGeometries(GISDataModel _mdlSource, ZCalqueEditable _cqDest, CtuluCommandContainer _cmd) {
    if (_mdlSource == null || _cqDest == null) {
      throw new IllegalArgumentException(MdlResource.getS("Aucun des param�tres ne doit �tre null."));
    }

    GISZoneCollection zoneDestination = _cqDest.getModelEditable().getGeomData();

    final CtuluCommandComposite cmp = new CtuluCommandComposite(MdlResource.getS("D�placement de g�om�tries"));

    // Nouveaux objets
    Geometry[] newGeom = new Geometry[_mdlSource.getNumGeometries()];
    Object[][] newData = new Object[_mdlSource.getNumGeometries()][];

    // Ajout des nouveaux objets.
    for (int i = 0; i < newGeom.length; i++) {
      Geometry geom = _mdlSource.getGeometry(i);

      // Les attributs
      Object[] datadest = new Object[zoneDestination.getNbAttributes()];
      for (int iatt = 0; iatt < datadest.length; iatt++) {
        int idxAtt = _mdlSource.getIndiceOf(zoneDestination.getAttribute(iatt));
        if (idxAtt != -1) {
          datadest[iatt] = _mdlSource.getValue(idxAtt, i);
          // Les nouvelles g�om�tries sont modifi�s, pas d'origine
          if (_mdlSource.getAttribute(idxAtt) == GISAttributeConstants.ETAT_GEOM) {
            datadest[iatt] = GISAttributeConstants.ATT_VAL_ETAT_MODI;
          }
        } // Cas particuliers de mapping entre les attributs Z et z \\
        else {
          // On va essayer de renseigner le Z global de destination
          if (zoneDestination.getAttribute(iatt).getID().equals("Z")) {
            // Cas o� un z atomique existe dans la source et qu'il est constant
            boolean identique = true;
            int indexBathy = _mdlSource.getIndiceOf(GISAttributeConstants.BATHY);
            if (indexBathy != -1) {
              GISAttributeModelDoubleArray data = (GISAttributeModelDoubleArray) _mdlSource.getValue(indexBathy, i);
              identique = data.getMin() == data.getMax();
            }
            if (identique && indexBathy != -1) {
              datadest[iatt] = ((GISAttributeModelDoubleArray) _mdlSource.getValue(indexBathy, i)).getObjectValueAt(0);
            } // Dans le cas contraire, on demande � l'utilisateur
            else {
              // Pour le titre de la popup, on met le titre de la geom si possible
              String titre = MdlResource.getS("Nom") + " : ";
              if (_mdlSource.getIndiceOf(GISAttributeConstants.TITRE) != -1) {
                titre += (String) _mdlSource.getValue(_mdlSource.getIndiceOf(GISAttributeConstants.TITRE), i);
              } else {
                titre += MdlResource.getS("Sans nom");
              }
              // Instanciation de la popup
              MdlZDialog dialog = new MdlZDialog(calquePanel_.getEditor().getFrame(), titre);
              dialog.setVisible(true);
              datadest[iatt] = dialog.getValue();
            }
          } else if (zoneDestination.getAttribute(iatt) == GISAttributeConstants.BATHY) {
            // Recherche d'un attribut ayant pour ID 'Z' pour pouvoir utiliser sa valeur
            boolean found = false;
            int j = -1;
            while (!found && ++j < _mdlSource.getNbAttributes()) {
              found = _mdlSource.getAttribute(j).getID().equals("Z");
            }
            if (found) {
              // Duplication de la valeur
              Object[] values = new Object[geom.getNumPoints()];
              for (int k = 0; k < values.length; k++) {
                values[k] = _mdlSource.getValue(j, i);
              }
              datadest[iatt] = values;
            }
          }
        }
      }

      // Enregistrement temporaire des data
      newData[i] = datadest;

      // La g�om�trie
      if (_cqDest.modeleDonnees() instanceof MdlModel2dMultiPoint) {
        newGeom[i] = GISGeometryFactory.INSTANCE.createMultiPoint(geom.getCoordinates());
      } else if (_cqDest.modeleDonnees() instanceof MdlModel2dLine) {
        CoordinateSequence coordSeq = ((GISCoordinateSequenceContainerInterface) geom).getCoordinateSequence();
        boolean isFerme = coordSeq.getCoordinate(0).equals(coordSeq.getCoordinate(coordSeq.size() - 1));
        if ((_cqDest.canAddForme(DeForme.LIGNE_BRISEE) && !isFerme) || !_cqDest.canAddForme(DeForme.POLYGONE)) {
          newGeom[i] = GISLib.toPolyligne(coordSeq);
        } else {
          newGeom[i] = GISLib.toPolygone(coordSeq);
        }
      }
    }

    // Ajout des nouvelles g�om�tries
    for (int i = 0; i < newGeom.length; i++) {
      zoneDestination.addGeometry(newGeom[i], newData[i], cmp);
    }

    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
  }

  /**
   * D�place les g�om�tries s�lectionn�es dans le calque cible.
   */
  public void moveInLayerSelectedGeometries() {
    // Rassemblement des informations
    Map<ZCalqueEditable, TIntArrayList> selectedGeom = new HashMap<ZCalqueEditable, TIntArrayList>();
    int[] idxScene = getScene().getLayerSelection().getSelectedIndex();
    if (idxScene != null) {
      // Tri des g�om�tries en fonction de leur calque
      for (int i = 0; i < idxScene.length; i++) {
        ZCalqueEditable calque = (ZCalqueEditable) getScene().getLayerForId(idxScene[i]);
        if (!selectedGeom.containsKey(calque)) {
          selectedGeom.put(calque, new TIntArrayList());
        }
        selectedGeom.get(calque).add(getScene().sceneId2LayerId(idxScene[i]));
      }
      ZCalqueEditable calqueDestination = (ZCalqueEditable) getScene().getCalqueActif();
      final CtuluCommandComposite cmp = new CtuluCommandComposite(MdlResource.getS("D�placer dans calque cible"));

      // Controle que les g�om�tries peuvent �tre d�plac�es.
      GISDataModel[] mdls = new GISDataModel[selectedGeom.size()];
      int icpt = 0;
      for (ZCalqueEditable cq : selectedGeom.keySet()) {
        GISDataModel mdl = cq.getModelEditable().getGeomData();
        int[] idxs = selectedGeom.get(cq).toNativeArray();
        mdls[icpt++] = new GISDataModelFilterAdapter(mdl, null, idxs);
      }

      // Si des g�om�tries ne sont pas conformes, aucune n'est copi�e.        
      if (!canCopyGeometries(new GISDataModelMultiAdapter(mdls), calqueDestination)) {
        return;
      }

      // Le d�placement se fait sur chaque mod�le et non sur un mod�le global, pour tenir compte au maximum des attributs
      // li�s � chaque g�om�trie source.
      for (int i = 0; i < mdls.length; i++) {
        copyGeometries(mdls[i], calqueDestination, cmp);
      }

      // Suppression des anciens.
      removeSelectedObjects(cmp);
      if (mng_ != null) {
        mng_.addCmd(cmp.getSimplify());
      }
    }
  }

  /**
   * D�cime la g�om�trie ou entre 2 sommmets de la g�om�trie s�lectionn�e.
   */
  public void decimateSelectedGeometries() {
    if (pnDecimation_ == null) {
      pnDecimation_ = new MdlDecimationPanel();
    }

    CtuluCommandComposite cmp = new CtuluCommandComposite(MdlResource.getS("D�cimer"));

    String title;
    if (getScene().isAtomicMode()) {
      title = MdlResource.getS("D�cimation entre 2 sommets");
    } else {
      title = MdlResource.getS("D�cimation sur une ou plusieurs polyligne(s)");
    }

    if (pnDecimation_.afficheModaleOk(pn_.getFrame(), title)) {
      // Les g�om�tries. On n'a plusieurs g�om�tries que dans le cas o� on n'est
      // pas en mode sommet.
      int nbRemoved=0;
      int[] geoms = getScene().getSelectionHelper().getSelectedIndexes();
      for (int i = 0; i < geoms.length; i++) {
        int idxGeom = geoms[i];
        Geometry geom = (Geometry) getScene().getObject(idxGeom);
        if (geom == null) {
          continue;
        }
        int idxdeb;
        int idxfin;
        if (getScene().isAtomicMode()) {
          int[] idxa = getScene().getLayerSelectionMulti().getSelection(idxGeom).getSelectedIndex();
          idxdeb = idxa[0];
          idxfin = idxa[1];
        } else {
          idxdeb = 0;
          idxfin = geom.getNumPoints() - 1;
        }

        ZModeleDonnees mdld = getScene().getLayerForId(idxGeom).modeleDonnees();
        if (mdld instanceof MdlModel2dLine) {
          MdlModel2dLine mdl = (MdlModel2dLine) mdld;
         nbRemoved+= mdl.decimate(getScene().sceneId2LayerId(idxGeom), idxdeb, idxfin, pnDecimation_.isNbPtsGiven() ? 0 : 1, pnDecimation_
                  .getNbPts(), pnDecimation_.getDistance(), cmp);
        }
      }
      final String msg = EbliLib.getS("D�cimation: {0} point(s) enlev�(s)", Integer.toString(nbRemoved));
      FuLog.warning("MOD:" + msg);
      getUI().message(null, msg, true);
    }
    getScene().clearSelection();

    if (getMng() != null) {
      getMng().addCmd(cmp.getSimplify());
    }
  }

  /**
   * Raffine la g�om�trie ou entre 2 sommmets de la g�om�trie s�lectionn�e.
   */
  public void refineSelectedGeometries() {
    if (pnRaffinement_ == null) {
      pnRaffinement_ = new MdlRefinementPanel();
    }

    CtuluCommandComposite cmd = new CtuluCommandComposite(MdlResource.getS("Raffiner"));

    String title;
    if (getScene().isAtomicMode()) {
      title = MdlResource.getS("Raffinement entre 2 sommets");
    } else {
      title = MdlResource.getS("Raffinement sur une polyligne");
    }

    if (pnRaffinement_.afficheModaleOk(pn_.getFrame(), title)) {
      // Les g�om�tries. On a plusieurs g�om�tries que dans le cas o� on n'est
      // pas en mode sommet.
      int[] geoms = getScene().getSelectionHelper().getSelectedIndexes();
      int nbATotaldded = 0;
      for (int i = 0; i < geoms.length; i++) {
        int idxGeom = geoms[i];
        Geometry geom = (Geometry) getScene().getObject(idxGeom);
        if (geom == null) {
          continue;
        }
        int idxdeb;
        int idxfin;
        if (getScene().isAtomicMode()) {
          int[] idxa = getScene().getLayerSelectionMulti().getSelection(idxGeom).getSelectedIndex();
          idxdeb = idxa[0];
          idxfin = idxa[1];
        } else {
          idxdeb = 0;
          idxfin = geom.getNumPoints() - 1;
        }

        ZModeleDonnees mdld = getScene().getLayerForId(idxGeom).modeleDonnees();
        if (mdld instanceof MdlModel2dLine) {
          MdlModel2dLine mdl = (MdlModel2dLine) mdld;
          int nb = mdl.refine(getScene().sceneId2LayerId(idxGeom), idxdeb, idxfin, pnRaffinement_.isNbPtsGiven() ? 0 : 1, pnRaffinement_
                  .getNbPts(), pnRaffinement_.getDistance(), cmd);
          
          nbATotaldded=nbATotaldded+nb;
        }
      }
      getScene().clearSelection();
      final String msg = EbliLib.getS("Raffinement: {0} point(s) ajout�(s)", Integer.toString(nbATotaldded));
      FuLog.warning("MOD:" + msg);
      getUI().message(null, msg, true);
    }
    if (getMng() != null) {
      getMng().addCmd(cmd.getSimplify());
    }
  }

  /**
   * Sort the points of selected polylignes. This sorting is done on the _icoord.
   *
   * @param _icoord 0: x, 1: y.
   */
  public void organizePoint(int _icoord) {
    CtuluCommandComposite cmp = new CtuluCommandComposite(MdlResource.getS("R�organisation de la g�om�trie"));
    int[] idxScenes = getScene().getSelectionHelper().getSelectedIndexes();
    for (int i = 0; i < idxScenes.length; i++) {
      // Data retrieval 
      int idxGeom = getScene().sceneId2LayerId(idxScenes[i]);
      ZModeleDonnees mdld = getScene().getLayerForId(idxScenes[i]).modeleDonnees();

      if (mdld instanceof MdlModel2dLine) {
        ((MdlModel2dLine) mdld).organizePoints(idxGeom, _icoord, cmp);
      }
    }

    if (getMng() != null) {
      getMng().addCmd(cmp.getSimplify());
    }
  }

  /**
   * Rend rectiligne une polyligne.
   */
  public void linearisePolyligne() {
    CtuluCommandComposite cmp = new CtuluCommandComposite(MdlResource.getS("Rendre rectiligne des polylignes"));
    int[] idxGeoms = getScene().getSelectionHelper().getSelectedIndexes();
    // Verification de la selection \\
    boolean selectionValid = true;
    int w = -1;
    while (selectionValid && ++w < idxGeoms.length) {
      selectionValid = getScene().getObject(idxGeoms[w]) instanceof GISPolyligne;
    }
    if (selectionValid) {
      // Pour chaque polyligne selectionn�es
      for (int idxGeom : idxGeoms) {
        int idx = getScene().sceneId2LayerId(idxGeom);
        GISPolyligne polyligne = ((GISPolyligne) getScene().getObject(idxGeom));
        ZCalqueAffichageDonneesInterface calque = getScene().getLayerForId(idxGeom);
        GISZoneCollectionLigneBrisee zone = (GISZoneCollectionLigneBrisee) ((ZModeleEditable) calque.modeleDonnees()).getGeomData();

        if (getScene().isAtomicMode() && getScene().getLayerSelectionMulti().getSelection(idxGeom).getNbSelectedIndex() > 1) {
          LibUtils.linearisePolyligne(zone, idx, getScene().getLayerSelectionMulti().getSelection(idxGeom).getSelectedIndex(), cmp);
        } else {
          LibUtils.linearisePolyligne(zone, idx, new int[]{0, polyligne.getNumPoints() - 1}, cmp);
        }
      }
    }
    // Mise � jour du undo/redo
    if (mng_ != null) {
      mng_.addCmd(cmp.getSimplify());
    }
    // Suppression de la selection
    getScene().clearSelection();
  }

  /**
   * Interpole un profil � partir d'un nuage de point.
   */
  public void interpolateProfile() {
    MdlInterpolateProfilePanel pnInterpolateProfile = new MdlInterpolateProfilePanel(pn_);
    if (pnInterpolateProfile.afficheModaleOk(pn_.getFrame(), MdlResource.getS("Cr�ation d'un profil")) && pnInterpolateProfile.getCalqueProfileDestination() != null) {
      CtuluCommandComposite cmp = new CtuluCommandComposite(MdlResource.getS("Cr�er un profil � partir d'une trace"));

      GISDataModel mdl = pnInterpolateProfile.getSupportCollection();
      ProfileCalculator calculator = new ProfileCalculator();

      // La fenetre
      double xmin = mdl.getEnvelopeInternal().getMinX();
      double xmax = mdl.getEnvelopeInternal().getMaxX();
      double ymin = mdl.getEnvelopeInternal().getMinY();
      double ymax = mdl.getEnvelopeInternal().getMaxY();
      calculator.setWindow(new ProfileCalculatorWindow(new GISPoint[]{
                new GISPoint(xmin, ymin, 0),
                new GISPoint(xmin, ymax, 0),
                new GISPoint(xmax, ymax, 0),
                new GISPoint(xmax, ymin, 0)
              }));

      // R�cup�ration des points de la trace du profil
      int idGeom = scene_.getSelectionHelper().getUniqueSelectedIdx();
      if (idGeom == -1 || !(scene_.getObject(idGeom) instanceof Geometry)) {
        ui_.error(MdlResource.getS("L'interpolation n'est pas possible � partir de cette s�lection."));
        return;
      }
      Coordinate[] coords = ((Geometry) scene_.getObject(idGeom)).getCoordinates();
      GISPoint[] points = new GISPoint[coords.length];
      for (int i = 0; i < coords.length; i++) {
        points[i] = new GISPoint(coords[i]);
      }
      calculator.setTrace(points);

      // Configuration du nuage de point
      GISAttributeInterface[] attrs = new GISAttributeInterface[mdl.getNbAttributes()];
      for (int i = 0; i < mdl.getNbAttributes(); i++) {
        attrs[i] = mdl.getAttribute(i);
      }
      int nbMultiPoint = 0;
      for (int i = 0; i < mdl.getNumGeometries(); i++) {
        if (mdl.getGeometry(i) instanceof GISMultiPoint) {
          nbMultiPoint++;
        }
      }
      int[] idxMultiPoint = new int[nbMultiPoint];
      int j = 0;
      for (int i = 0; i < mdl.getNumGeometries(); i++) {
        if (mdl.getGeometry(i) instanceof GISMultiPoint) {
          idxMultiPoint[j++] = i;
        }
      }
      calculator.setCloud(new GISDataModelToPointCloudAdapter(mdl, GISAttributeConstants.BATHY));

      // Interpolateur de profil
      CtuluAnalyze ana = new CtuluAnalyze();
      GISPoint[] profil = calculator.extractProfile(1, ana);
      if (ana.containsErrors()) {
        ui_.manageAnalyzeAndIsFatal(ana);
        return;
      }

      // Ajout du r�sultat dans le calque cible
      GISZoneCollection zone = ((ZModeleGeometry) pnInterpolateProfile.getCalqueProfileDestination().modeleDonnees()).getGeomData();
      coords = new Coordinate[profil.length];
      for (int i = 0; i < coords.length; i++) {
        coords[i] = new Coordinate(profil[i].getX(), profil[i].getY(), profil[i].getZ());
      }
      GISPolyligne poly = GISGeometryFactory.INSTANCE.createLineString(coords);
      Object[] data = new Object[zone.getNbAttributes()];
      if (zone.getAttributeIsZ() != null) {
        int idxZattr = zone.getIndiceOf(zone.getAttributeIsZ());
        data[idxZattr] = new Object[coords.length];
        for (int i = 0; i < coords.length; i++) {
          ((Object[]) data[idxZattr])[i] = profil[i].getZ();
        }
      }
      ((ZModeleGeometry) pnInterpolateProfile.getCalqueProfileDestination().modeleDonnees()).getGeomData().addGeometry(poly, data, cmp);

      if (mng_ != null) {
        mng_.addCmd(cmp.getSimplify());
      }

      ui_.manageAnalyzeAndIsFatal(ana);
    }
  }

  /**
   * Projete une g�om�trie sur un ensemble de points d'un ou plusieurs semis.
   */
  public void projectSelectedObject() {
    MdlProjectionPanel pnProjection = new MdlProjectionPanel(pn_);

    if (pnProjection.afficheModaleOk(pn_.getFrame(), MdlResource.getS("Projection sur des semis"))) {
      GISDataModel mdl = pnProjection.getSupportCollection();
      InterpolationSupportGISAdapter support = new InterpolationSupportGISAdapter(mdl);
      List<GISAttributeInterface> vars = new ArrayList<GISAttributeInterface>();
      vars.add(GISAttributeConstants.BATHY);

      // Cr�ation d'une liste contenant les g�om�tries s�lectionn�es.
      // FIXME BM: En cas de undo, les g�om�tries n'ont pas leur Z r�initialis� a partir du Z attribut.
      // Si nouvelle interpolation => Les valeurs ne sont pas les bonnes.
      GISZoneCollectionGeometry targetGeoms = new GISZoneCollectionGeometry();
      int[] idxGeom = getScene().getSelectionHelper().getSelectedIndexes();
      for (int i : idxGeom) {
        targetGeoms.addGeometry((Geometry) getScene().getObject(i), null, null);
      }

      targetGeoms.setAttributes(new GISAttributeInterface[]{GISAttributeConstants.BATHY}, null);
      targetGeoms.setAttributeIsZ(GISAttributeConstants.BATHY);

      // L'interpolation se fait sur la totalit� des sommets des g�om�tries, m�me si on est seulement en mode sommet.
      // Lors du transfert vers les attributs, seuls les valeurs des sommets s�lectionn�s (tous si mode global) seront modifi�es.
      InterpolationTarget target = new InterpolationTargetGISAdapter(targetGeoms);
      InterpolationParameters params = new InterpolationParameters(vars, target, support);
      InterpolatorBilinear interp = new InterpolatorBilinear(support);
      interp.interpolate(params);
      InterpolationResultsHolderI res = params.getResults();

      if (ui_.manageAnalyzeAndIsFatal(params.getAnalyze())) {
        return;
      }

      final CtuluCommandComposite cmp = new CtuluCommandComposite(MdlResource.getS("Projection sur des semis"));

      // Transfert du Z sur les sommets concern�s.
      int ipt = 0;
      for (int i = 0; i < targetGeoms.getNbGeometries(); i++) {
        CoordinateSequence seq = ((GISCoordinateSequenceContainerInterface) targetGeoms.getGeometry(i)).getCoordinateSequence();
        CtuluListSelectionInterface idxsom;
        if (getScene().isAtomicMode()) {
          idxsom = getScene().getLayerSelectionMulti().getSelection(idxGeom[i]);
        } else {
          CtuluListSelection tmp = new CtuluListSelection(seq.size());
          tmp.inverse(seq.size());
          idxsom = tmp;
        }
        for (int idx = 0; idx < seq.size(); idx++) {
          if (idxsom.isSelected(idx)) {
            seq.setOrdinate(idx, 2, res.getValuesForPt(ipt)[0]);
          }
          ipt++;
        }
      }
      targetGeoms.postImport(0);

      GISZoneCollection col = null;
      int zatt = 0;

      for (int i = 0; i < idxGeom.length; i++) {
        Object vals = targetGeoms.getValue(0, i);
        GISZoneCollection colTmp = ((ZModeleGeometry) getScene().getLayerForId(idxGeom[i]).modeleDonnees()).getGeomData();
        if (col != colTmp) {
          col = colTmp;
          zatt = col.getIndiceOf(col.getAttributeIsZ());
        }
        int idx = getScene().sceneId2LayerId(idxGeom[i]);
        if (zatt != -1) {
          col.setAttributValue(zatt, idx, vals, cmp);
        }
        // Changement de l'attribut ETAT_GEOM en modifi�
        int idxAttr = col.getIndiceOf(GISAttributeConstants.ETAT_GEOM);
        if (idxAttr >= 0) {
          col.getDataModel(idxAttr).setObject(idx, GISAttributeConstants.ATT_VAL_ETAT_MODI, null);
        }
      }

      if (mng_ != null) {
        mng_.addCmd(cmp.getSimplify());
      }
    }
  }

  /**
   * Inverse le sens de la polyligne.
   */
  public void invertOrientationSelectedGeometries() {
    CtuluCommandComposite cmp = new CtuluCommandComposite(MdlResource.getS("Inversion de l'orientation"));
    int[] geoms = getScene().getSelectionHelper().getSelectedIndexes();
    for (int i = 0; i < geoms.length; i++) {
      ZModeleDonnees mdld = getScene().getLayerForId(geoms[i]).modeleDonnees();
      if (mdld instanceof MdlModel2dLine) {
        ((MdlModel2dLine) mdld).invertGeometry(getScene().sceneId2LayerId(geoms[i]), cmp);
      }
    }
    if (getMng() != null) {
      getMng().addCmd(cmp.getSimplify());
    }
  }
}
