/*
 * @creation 8 sept. 06
 * @modification $Date: $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.modeleur;


import com.memoire.bu.BuPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluCommandManager;

import org.fudaa.ctulu.CtuluListSelectionEvent;
import org.fudaa.ctulu.CtuluListSelectionListener;
import org.fudaa.ctulu.gui.CtuluDialogPanel;

import org.fudaa.ebli.commun.EbliFormatterInterface;
import org.fudaa.fudaa.modeleur.action.MdlCompareProfileSet;
import org.fudaa.fudaa.modeleur.common.ComparisonCurvesPanel;
import org.fudaa.fudaa.modeleur.modeleur1d.controller.ProfileSelectionModel;
import org.fudaa.fudaa.modeleur.modeleur1d.controller.ProfileSetControllerI;
import org.fudaa.fudaa.modeleur.modeleur1d.model.ProfileSetI;
import org.fudaa.fudaa.modeleur.modeleur1d.view.PnGestionProfil;
import org.fudaa.fudaa.modeleur.modeleur1d.view.VueCourbe;
import org.fudaa.fudaa.modeleur.modeleur1d.view.VueProfilI;
import org.fudaa.fudaa.modeleur.modeleur1d.view.VueProfileSet;
import org.fudaa.fudaa.modeleur.modeleur1d.view.VueTableau;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Un panneau de pour comparer des profils.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MdlCompareProfilePanel extends CtuluDialogPanel implements VueProfilI {
  
  MdlVisuPanel pnCalques_;
  CtuluCommandManager mng_;
  CtuluCommandComposite cmp_;
  EbliFormatterInterface fmt_;
  private  ComparisonCurvesPanel  panelCompareCurves;
  protected VueTableau viewTable_;
  protected VueCourbe viewCurve_;
  protected VueProfileSet viewProfSet_;
  protected PnGestionProfil pnProfiles_;
  protected boolean listenEventSelection_=true;
  private ProfileSetController ctrl_;
   public  JSplitPane pnMain, pnLeft;
    
  class ProfileSetController implements ProfileSetControllerI {
    ProfileSetI profSet_;
    ProfileSelectionModel selmdl_;
    
    class ProfilSelectionModel extends DefaultListSelectionModel implements ProfileSelectionModel{
      
      public ProfilSelectionModel() {
        //super.setSelectionMode(SINGLE_SELECTION);
        super.setSelectionMode(MULTIPLE_INTERVAL_SELECTION);
      }

      public int getSelectedProfil() {
        return getMinSelectionIndex();
      }

      public void setSelectedProfil(int _idx) {
        if (_idx==-1)
          clearSelection();
        else
          setSelectionInterval(_idx, _idx);
      }
      
    }
    
    public ProfileSetController() {
      selmdl_=new ProfilSelectionModel();
    }
    
    public void setProfileSet(ProfileSetI _profSet) {
      profSet_=_profSet;
      selmdl_.setSelectedProfil(-1);
    }

    public void ajoutProfil() {
    }

    public void fusionnerProfil() {
    }

    public ProfileSelectionModel getProfilSelectionModel() {
      return selmdl_;
    }

    public void renameProfil(int _idxProfil, String _newName) {
    }

    public void supprimerSelectedProfil() {
      if (profSet_!=null)
        profSet_.removeProfil(selmdl_.getSelectedProfil(), null);
    }

    public ProfileSetI getProfilSet() {
      return profSet_;
    }
  }
  
  public MdlCompareProfilePanel(MdlVisuPanel _pnCalques) {
    pnCalques_=_pnCalques;
    mng_=pnCalques_.getCmdMng();
    fmt_=pnCalques_.getEbliFormatter();
    ctrl_=new ProfileSetController();
    
    // La commande est stock�e � la fin, ou elle est d�jou�e.
    initCommandComposite();

    // Le panneau des profils en travers
    viewCurve_=new VueCourbe(this, null);
    viewCurve_.setPrevNextVisible(true);
    viewCurve_.addSelectionListener(new CtuluListSelectionListener() {
      public void listeSelectionChanged(CtuluListSelectionEvent _e) {
        if (listenEventSelection_) {
          listenEventSelection_=false;
          viewTable_.setSelection(viewCurve_.getSelection());
          listenEventSelection_=true;
        }
      }
    });
    
    // Le panneau liste des profils
    pnProfiles_=new PnGestionProfil(ctrl_,false);
    
    // Le panneau des coordonn�es du profil courant.
    viewTable_=new VueTableau(this, true);
    viewTable_.addSelectionListener(new ListSelectionListener() {
      public void valueChanged(ListSelectionEvent e) {
        if (listenEventSelection_) {
          listenEventSelection_=false;
          viewCurve_.setSelection(viewTable_.getSelection());
          listenEventSelection_=true;
        }
      }
    });
    
    // Le panneau de la vue 2D
    viewProfSet_=new VueProfileSet(pnCalques_.getCtuluUI());
    viewProfSet_.addSelectionListener(new ListSelectionListener() {
      public void valueChanged(ListSelectionEvent e) {
        int indsel=viewProfSet_.getSelectedProfile();
        ctrl_.getProfilSelectionModel().setSelectedProfil(indsel);
      }
    });
    
    ctrl_.getProfilSelectionModel().addListSelectionListener(new ListSelectionListener() {
      public void valueChanged(ListSelectionEvent e) {
          valueCurveChanged(e);
      }
    });
    
    setLayout(new BorderLayout(2, 2));
    
     pnMain=new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
     pnLeft=new JSplitPane(JSplitPane.VERTICAL_SPLIT);
    final JSplitPane pnRight=new JSplitPane(JSplitPane.VERTICAL_SPLIT);
    pnLeft.add(pnProfiles_,JSplitPane.TOP);
    
     JSplitPane splitPaneBottomLeft=new JSplitPane(JSplitPane.VERTICAL_SPLIT);
     splitPaneBottomLeft.add(viewTable_,JSplitPane.TOP);    
     panelCompareCurves = new ComparisonCurvesPanel(splitPaneBottomLeft);
       
       
    pnLeft.add(splitPaneBottomLeft,JSplitPane.BOTTOM);
     pnLeft.setDividerLocation(0.5);
    pnRight.add(viewProfSet_, JSplitPane.TOP);
    pnRight.add(viewCurve_,JSplitPane.BOTTOM);
    pnRight.setDividerLocation(0.5);
    //pnRight.setResizeWeight(0.5);
    
    pnMain.add(pnRight,JSplitPane.RIGHT);
    pnMain.add(pnLeft,JSplitPane.LEFT);
    add(pnMain,BorderLayout.CENTER);
     pnMain.setDividerLocation(0.5);
   
    setPreferredSize(new Dimension(800,600));
    
    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        viewProfSet_.restaurer();
      }
    });
  }
  
  /**
   * called when an event is perfornmed on table to change curve.
   * @param e 
   */
   public void valueCurveChanged(ListSelectionEvent e) {
        int idxProf=ctrl_.getProfilSelectionModel().getSelectedProfil();
       valueCurveChanged(idxProf);
      }
   
   
   public void valueCurveChanged(int idxProf) {

        int[] selectedProfils = pnProfiles_.getTableProfils().getSelectedRows();
       
        viewCurve_.setSelectedProfile(idxProf);
        viewCurve_.updateComparisonCurves(selectedProfils, panelCompareCurves, this, idxProf);
        
        viewTable_.setProfil(ctrl_.getProfilSet().getProfil(idxProf));
        viewProfSet_.setSelectedProfile(idxProf);
      }
           
   public void changeSelectedProfile(int idxProf) {
       viewTable_.setProfil(ctrl_.getProfilSet().getProfil(idxProf));
        
   }
  
  private void initCommandComposite() {
    cmp_=new CtuluCommandComposite(MdlResource.getS("Comparer les profils"));
  }

    public VueCourbe getViewCurve() {
        return viewCurve_;
    }
  
  
  
  
  /**
   * Changement de profil set. Le controller est sens� avoir ce m�me
   * profile set.
   * @param _profSet Le profile set.
   */
  public void setProfileSet(MdlCompareProfileSet _profSet) {
    ctrl_.setProfileSet(_profSet);
    viewCurve_.setProfilSet(_profSet);
    viewProfSet_.setProfilSet(_profSet);
    pnProfiles_.setProfileSet(_profSet);
    viewTable_.setProfil(null);
  }
  
  /**
   * Definit le profil selectionn�.
   * @param _profId L'identifiant du profil.
   */
  public void setSelectedProfile(int _profId) {
    ctrl_.getProfilSelectionModel().setSelectedProfil(_profId);
  }
  
  @Override
  public boolean isDataValid() {
    return true;
  }

  @Override
  public boolean apply() {
    viewProfSet_.restoreZoneListeners();
    // La commande container est stock�e pour �tre �ventuellement d�jou�.
    mng_.addCmd(cmp_.getSimplify());
    initCommandComposite();
    // B.M. : C'est pas terrible, mais ca redessine la vue 2D.
    pnCalques_.clearSelection();;
    
    return true;
  }

  @Override
  public boolean cancel() {
    viewProfSet_.restoreZoneListeners();
    // On d�joue tout ce qui a �t� fait.
    cmp_.undo();
    initCommandComposite();
    return true;
  }

  public void clearError() {
    setErrorText("");
  }

  public void showError(String _message) {
    setErrorText(_message);
  }

  public CtuluCommandContainer getCommandContainer() {
    return cmp_;
  }

  public EbliFormatterInterface getFormater() {
    return fmt_;
  }

  public boolean  isCompareCurves() {
           return pnProfiles_.getTableProfils()!= null && pnProfiles_.getTableProfils().getSelectedRows() != null
            &&    pnProfiles_.getTableProfils().getSelectedRows().length >1;
      }
    public ComparisonCurvesPanel getPanelCompareCurves() {
        return panelCompareCurves;
    }
  
  
}