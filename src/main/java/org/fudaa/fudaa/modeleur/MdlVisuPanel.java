/*
 * @creation 7 juin 07
 * @modification $Date$
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.modeleur;

import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.Action;
import org.fudaa.ebli.calque.BCalque;

import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZEbliCalquePanelController;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.action.SceneShowOrientationAction;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionMap;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.fudaa.modeleur.action.CalqueDeleteCalqueAction;
import org.fudaa.fudaa.modeleur.action.CalqueNewCalqueAction;
import org.fudaa.fudaa.modeleur.action.MdlCasierExportAction;
import org.fudaa.fudaa.modeleur.action.MenuFactory;
import org.fudaa.fudaa.modeleur.action.SceneShowLabelAction;
import org.fudaa.fudaa.modeleur.layer.MdlLayer1dAxe;
import org.fudaa.fudaa.modeleur.layer.MdlLayer1dBank;
import org.fudaa.fudaa.modeleur.layer.MdlLayer1dLimiteStockage;
import org.fudaa.fudaa.modeleur.layer.MdlLayer1dTrace;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dCloud;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dConstraintLine;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dContour;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dDirectionLine;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dLevel;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dProfile;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dCasier;
import org.fudaa.fudaa.modeleur.layer.MdlLayer2dZone;
import org.fudaa.fudaa.modeleur.layer.MdlLayerFactory;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sig.layer.FSigEditor;
import org.fudaa.fudaa.sig.layer.FSigImageImportAction;
import org.fudaa.fudaa.sig.layer.FSigLayerGroup;
import org.fudaa.fudaa.sig.layer.FSigVisuPanel;

import com.memoire.bu.BuMenu;
import com.memoire.bu.BuPopupMenu;
import java.util.ArrayList;
import org.fudaa.ebli.calque.BCalqueVisitor;
import org.fudaa.ebli.commun.EbliActionAbstract;
import org.fudaa.fudaa.modeleur.layer.MdlLayerInterface;
import org.fudaa.fudaa.modeleur.modeleur1d.model.Bief;

/**
 * Le panneau de visu 2D des donn�es du modeleur au travers des calques. Contient le composant vue des calques + le composant d'affichage des
 * coordonn�es + le composant d'affichage du mode de selection
 *
 * @author fred deniger
 * @version $Id$
 */
public class MdlVisuPanel extends FSigVisuPanel {

  BGroupeCalque cqImg_;
  BGroupeCalque cqAlti_;
  BGroupeCalque cqSing_;
  BGroupeCalque cqMdl1d_;
  BGroupeCalque cqBiefs_;
  BGroupeCalque cqCasiers_;
  MdlImplementation impl_;

   
  /**
   * Construction des calques. Une partie est d�j� construite par d�faut dans les classes m�res.
   *
   * @param _impl L'implementation de l'application.
   */
  public MdlVisuPanel(MdlImplementation _impl/*, boolean _createDefault*/) {
    super(_impl);
    impl_ = _impl;
    setName("mdlMainFille");
    setDoubleBuffered(false);
    initCalques(true);
    installLayerActions();
    getScene().setRestrictedToCalqueActif(false);
  }

  @Override
  protected FSigEditor createGisEditor() {
    FSigEditor editor = new MdlEditionManager(this);
    mng_ = editor.getMng();
    return editor;
  }

  @Override
  public MdlEditionManager getEditor() {
    return (MdlEditionManager) super.getEditor();
  }

  @Override
  protected void initButtonGroupSpecific(final List<EbliActionInterface> _l, final ZEbliCalquePanelController _res) {
    super.initButtonGroupSpecific(_l, _res);
    _l.add(new MdlCasierExportAction((MdlEditionManager) gisEditor_));
  }

  @Override
  protected BuMenu[] createSpecificMenus(final String _title) {
    BuMenu[] ret = new BuMenu[3];
    final BuMenu mnGeom = new BuMenu(MdlResource.getS("G�om�trie"), "mnGEOM");
   // EbliComponentFactory.INSTANCE.addActionsToMenu(gisEditor_.getSceneEditor().getActions(), mnGeom);
    MenuFactory.addSynchronizedActionToMenu(gisEditor_.getSceneEditor().getActions(), mnGeom);
    ret[1] = mnGeom;

    BuMenu[] mnsuper = super.createSpecificMenus(_title);
    ret[0] = mnsuper[0];
    ret[2] = mnsuper[1];
    return ret;
  }

  @Override
  protected EbliActionInterface[] getActionsInterface() {
    EbliActionInterface[] actions = super.getActionsInterface();
    EbliActionInterface[] allActions = new EbliActionInterface[actions.length + 3];
    allActions[0] = null;
    allActions[1] = new SceneShowOrientationAction(gisEditor_.getPanel().getArbreCalqueModel());
    allActions[2] = new SceneShowLabelAction(gisEditor_.getPanel().getArbreCalqueModel());
    for (int i = 0; i < actions.length; i++) {
      allActions[i + 3] = actions[i];
    }
    return allActions;
  }

  @Override
  public FSigLayerGroup getGroupGIS() {
    return /*parent_*/ null;
  }

  /**
   * Retourne le groupe des calques de donn�es altim�triques
   */
  public BGroupeCalque getGroupAlti() {
    return cqAlti_;
  }

  public BGroupeCalque getGroupSing() {
    return cqSing_;
  }

  public List<BCalque> getCalquesContours() {
    BCalque[] calques = cqSing_.getCalques();
    return findCalques(calques, MdlLayer2dContour.EXT_NAME);
  }

  public List<BCalque> getCalquesContraintes() {
    BCalque[] calques = cqSing_.getCalques();
    return findCalques(calques, MdlLayer2dConstraintLine.EXT_NAME);
  }

  public List<BCalque> findCalques(BCalque[] in, String extName) {
    List<BCalque> res = new ArrayList<BCalque>();
    if (in != null) {
      for (int i = 0; i < in.length; i++) {
        MdlLayerInterface bCalque = (MdlLayerInterface) in[i];
        if (extName.equals(bCalque.getExtName())) {
          res.add((BCalque) bCalque);
        }
      }
    }
    return res;

  }

  public void importImage() {
//    actImport_.actionPerformed(null);
  }

  @Override
  public void setActive(boolean _active) {
  }

  /**
   * Initialisation ou reinitialisation des calques contenus dans le panneau de visualisation.
   *
   * @param _build Si true, les calques g�om�triques sont construits par d�faut, vides. Sinon, ils seront reconstruits par la proc�dure de restitution
   * du projet.
   */
  public void initCalques(boolean _build) {
    removeAllCalqueDonnees();

    buildDefaultTree(getDonneesCalque(), getEditor(), _build, true);
    cqAlti_ = (BGroupeCalque) getDonneesCalque().getCalqueParNom("gcAlti");
    cqSing_ = (BGroupeCalque) getDonneesCalque().getCalqueParNom("gcSing");
    cqMdl1d_ = (BGroupeCalque) getDonneesCalque().getCalqueParNom("gcMdl1d");
    cqCasiers_ = (BGroupeCalque) getDonneesCalque().getCalqueParNom("gcCasier");
    cqImg_ = (BGroupeCalque) getDonneesCalque().getCalqueParNom("gcImg");
    cqBiefs_ = (BGroupeCalque) getDonneesCalque().getCalqueParNom("gcBiefs");

    installLayerActions();
  }

  /**
   * Construit un arbre par defaut avec le nombre de calques par type.
   *
   * @param _root Le calque racine
   * @param _build Si true, les calques g�om�triques sont construits par d�faut, vides. Sinon, ils seront reconstruits par la proc�dure de restitution
   * du projet.
   * @param _editor L'�diteur. Attention : Peut �tre null.
   * @param _usePreferences True : Utilise les pr�f�rences pour la cr�ation (nombre de calques d'un certain type par exemple).
   */
  public static void buildDefaultTree(BGroupeCalque _root, MdlEditionManager _editor, boolean _build, boolean _usePreferences) {
    _root.add(buildAltiLayerGroup(_editor, _build, _usePreferences));
    _root.add(buildSingLayerGroup(_editor, _build, _usePreferences));
    _root.add(buildModel1dLayerGroup(_editor, _build, _usePreferences));
    _root.add(buildCasiersLayerGroup(_editor, _build, _usePreferences));
    _root.add(buildImageGroup());
    _root.add(buildBiefsLayerGroup());
  }

  protected void installLayerActions() {
    EbliActionInterface actNewCloud = new CalqueNewCalqueAction(MdlResource.getS("Nouveau calque semis"), FudaaResource.FUDAA.getIcon("ajouter"), MdlLayerFactory.LAYER2D_CLOUD, cqAlti_, getEditor());
    EbliActionInterface actNewLevel = new CalqueNewCalqueAction(MdlResource.getS("Nouveau calque courbes de niveaux"), FudaaResource.FUDAA.getIcon("ajouter"), MdlLayerFactory.LAYER2D_LEVEL, cqAlti_, getEditor());
    EbliActionInterface actNewProfile = new CalqueNewCalqueAction(MdlResource.getS("Nouveau calque profils"), FudaaResource.FUDAA.getIcon("ajouter"), MdlLayerFactory.LAYER2D_PROFILE, cqAlti_, getEditor());
    cqAlti_.setActions(new EbliActionInterface[]{actNewCloud, actNewLevel, actNewProfile});

    EbliActionInterface actNewZone = new CalqueNewCalqueAction(MdlResource.getS("Nouveau calque zones"), FudaaResource.FUDAA.getIcon("ajouter"), MdlLayerFactory.LAYER2D_ZONE, cqSing_, getEditor());
    EbliActionInterface actNewCnt = new CalqueNewCalqueAction(MdlResource.getS("Nouveau calque contours"), FudaaResource.FUDAA.getIcon("ajouter"), MdlLayerFactory.LAYER2D_CONTOUR, cqSing_, getEditor());
    EbliActionInterface actNewConst = new CalqueNewCalqueAction(MdlResource.getS("Nouveau calque lignes de contraintes"), FudaaResource.FUDAA.getIcon("ajouter"), MdlLayerFactory.LAYER2D_CONST_LINE, cqSing_, getEditor());
    EbliActionInterface actNewDir = new CalqueNewCalqueAction(MdlResource.getS("Nouveau calque lignes directrices"), FudaaResource.FUDAA.getIcon("ajouter"), MdlLayerFactory.LAYER2D_DIR_LINE, cqSing_, getEditor());
    cqSing_.setActions(new EbliActionInterface[]{actNewZone, actNewCnt, actNewConst, actNewDir});

    EbliActionInterface actNewTrace = new CalqueNewCalqueAction(MdlResource.getS("Nouveau calque trace de profils"), FudaaResource.FUDAA.getIcon("ajouter"), MdlLayerFactory.LAYER1D_TRACE, cqMdl1d_, getEditor());
    EbliActionInterface actNewAxe = new CalqueNewCalqueAction(MdlResource.getS("Nouveau calque axes hydrauliques"), FudaaResource.FUDAA.getIcon("ajouter"), MdlLayerFactory.LAYER1D_AXE, cqMdl1d_, getEditor());
    cqMdl1d_.setActions(new EbliActionInterface[]{actNewTrace, actNewAxe});

    EbliActionInterface actNewCasier = new CalqueNewCalqueAction(MdlResource.getS("Nouveau calque casiers"), FudaaResource.FUDAA.getIcon("ajouter"), MdlLayerFactory.LAYER2D_CASIER, cqCasiers_, getEditor());
    cqCasiers_.setActions(new EbliActionInterface[]{actNewCasier});

    EbliActionInterface actImport = new FSigImageImportAction(this, cqImg_);
    cqImg_.setActions(new EbliActionInterface[]{actImport});

    EbliActionInterface actDeleteLayer = new CalqueDeleteCalqueAction(getEditor(), this.getArbreCalqueModel());
    this.getArbreCalqueModel().setActionDelete(actDeleteLayer);

    EbliActionInterface actNewBief = new EbliActionSimple(MdlResource.getS("Nouveau bief"), FudaaResource.FUDAA.getIcon("ajouter"), null) {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        buildBiefLayers(null, true);
      }
    };
    EbliActionAbstract actImportBief = EbliActionMap.getInstance().getAction("IMPORTER_BIEF");
    cqBiefs_.setActions(new EbliActionInterface[]{actNewBief, actImportBief});
  }

  /**
   * Mise en place du groupe des calques images.
   */
  protected static BGroupeCalque buildImageGroup() {
    BGroupeCalque cqImg = new BGroupeCalque();
    cqImg.setTitle(MdlResource.getS("Fond de carte"));
    cqImg.setName("gcImg");
    cqImg.setDestructible(false);
    return cqImg;
  }

  private static int getInitLayerNumber(String _type, boolean _usePreferences) {
    if (!_usePreferences) {
      return 1;
    }
    return MdlPreferences.MDL.getIntegerProperty(MdlLayerTreePreferencesComponent.TREE_INITIAL_NUMBER_LAYER_OF_TYPE + _type, 1);
  }

  /**
   * Mise en place du groupe des donn�es altim�triques.
   */
  protected static BGroupeCalque buildAltiLayerGroup(MdlEditionManager _editor, boolean _build, boolean _usePreferences) {
    BGroupeCalque cqAlti = new BGroupeCalque();
    cqAlti.setTitle(MdlResource.getS("Donn�es altim�triques"));
    cqAlti.setName("gcAlti");
    cqAlti.putClientProperty(Action.SHORT_DESCRIPTION, MdlResource.getS("Permet le stockage des donn�es altim�triques"));
    cqAlti.setDestructible(false);

    if (!_build) {
      return cqAlti;
    }

    int nb;
    for (int i = 0; i < (nb = getInitLayerNumber(MdlLayer2dLevel.EXT_NAME, _usePreferences)); i++) {
      MdlLayer2dLevel cqLevel = new MdlLayer2dLevel(_editor);
      cqLevel.setName(BGroupeCalque.findUniqueChildName(cqAlti, cqLevel.getExtName()));
      if (nb > 1) {
        cqLevel.setTitle(cqAlti.findUniqueChildTitle(cqLevel.getTitle()));
      }
      cqAlti.add(cqLevel);
    }

    for (int i = 0; i < (nb = getInitLayerNumber(MdlLayer2dCloud.EXT_NAME, _usePreferences)); i++) {
      MdlLayer2dCloud cqCloud = new MdlLayer2dCloud(_editor);
      cqCloud.setName(BGroupeCalque.findUniqueChildName(cqAlti, cqCloud.getExtName()));
      if (nb > 1) {
        cqCloud.setTitle(cqAlti.findUniqueChildTitle(cqCloud.getTitle()));
      }
      cqAlti.add(cqCloud);
    }

    for (int i = 0; i < (nb = getInitLayerNumber(MdlLayer2dProfile.EXT_NAME, _usePreferences)); i++) {
      MdlLayer2dProfile cqProfile = new MdlLayer2dProfile(_editor);
      cqProfile.setName(BGroupeCalque.findUniqueChildName(cqAlti, cqProfile.getExtName()));
      if (nb > 1) {
        cqProfile.setTitle(cqAlti.findUniqueChildTitle(cqProfile.getTitle()));
      }
      cqAlti.add(cqProfile);
    }

    return cqAlti;
  }

  /**
   * Mise en place du groupe des singularit�s 2D.
   */
  protected static BGroupeCalque buildSingLayerGroup(MdlEditionManager _editor, boolean _build, boolean _usePreferences) {
    BGroupeCalque cqSing = new BGroupeCalque();
    cqSing.setTitle(MdlResource.getS("Singularit�s"));
    cqSing.setName("gcSing");
    cqSing.putClientProperty(Action.SHORT_DESCRIPTION, MdlResource.getS("Permet le stockage singularit�s 2D"));
    cqSing.setDestructible(false);

    if (!_build) {
      return cqSing;
    }

    int nb;
    for (int i = 0; i < (nb = getInitLayerNumber(MdlLayer2dContour.EXT_NAME, _usePreferences)); i++) {
      MdlLayer2dContour cqCnt = new MdlLayer2dContour(_editor);
      cqCnt.setName(BGroupeCalque.findUniqueChildName(cqSing, cqCnt.getExtName()));
      if (nb > 1) {
        cqCnt.setTitle(cqSing.findUniqueChildTitle(cqCnt.getTitle()));
      }
      cqSing.add(cqCnt);
    }

    for (int i = 0; i < (nb = getInitLayerNumber(MdlLayer2dZone.EXT_NAME, _usePreferences)); i++) {
      MdlLayer2dZone cqZone = new MdlLayer2dZone(_editor);
      cqZone.setName(BGroupeCalque.findUniqueChildName(cqSing, cqZone.getExtName()));
      if (nb > 1) {
        cqZone.setTitle(cqSing.findUniqueChildTitle(cqZone.getTitle()));
      }
      cqSing.add(cqZone);
    }

    for (int i = 0; i < (nb = getInitLayerNumber(MdlLayer2dConstraintLine.EXT_NAME, _usePreferences)); i++) {
      MdlLayer2dConstraintLine cqConst = new MdlLayer2dConstraintLine(_editor);
      cqConst.setName(BGroupeCalque.findUniqueChildName(cqSing, cqConst.getExtName()));
      if (nb > 1) {
        cqConst.setTitle(cqSing.findUniqueChildTitle(cqConst.getTitle()));
      }
      cqSing.add(cqConst);
    }

    for (int i = 0; i < (nb = getInitLayerNumber(MdlLayer2dDirectionLine.EXT_NAME, _usePreferences)); i++) {
      MdlLayer2dDirectionLine cqDirect = new MdlLayer2dDirectionLine(_editor);
      cqDirect.setName(BGroupeCalque.findUniqueChildName(cqSing, cqDirect.getExtName()));
      if (nb > 1) {
        cqDirect.setTitle(cqSing.findUniqueChildTitle(cqDirect.getTitle()));
      }
      cqSing.add(cqDirect);
    }

    return cqSing;
  }

  /**
   * Mise en place du groupe du mod�le 1d.
   */
  protected static BGroupeCalque buildModel1dLayerGroup(MdlEditionManager _editor, boolean _build, boolean _usePreferences) {
    BGroupeCalque cqMdl1d = new BGroupeCalque();
    cqMdl1d.setTitle(MdlResource.getS("Entit�s 1D"));
    cqMdl1d.setName("gcMdl1d");
    cqMdl1d.putClientProperty(Action.SHORT_DESCRIPTION, MdlResource.getS("Stocke le mod�le"));
    cqMdl1d.setDestructible(false);

    if (!_build) {
      return cqMdl1d;
    }

    int nb;
    for (int i = 0; i < (nb = getInitLayerNumber(MdlLayer1dTrace.EXT_NAME, _usePreferences)); i++) {
      MdlLayer1dTrace cqTrace = new MdlLayer1dTrace(_editor);
      cqTrace.setName(BGroupeCalque.findUniqueChildName(cqMdl1d, cqTrace.getExtName()));
      if (nb > 1) {
        cqTrace.setTitle(cqMdl1d.findUniqueChildTitle(cqTrace.getTitle()));
      }
      cqMdl1d.add(cqTrace);
    }

    for (int i = 0; i < (nb = getInitLayerNumber(MdlLayer1dAxe.EXT_NAME, _usePreferences)); i++) {
      MdlLayer1dAxe cqAxe = new MdlLayer1dAxe(_editor);
      cqAxe.setName(BGroupeCalque.findUniqueChildName(cqMdl1d, cqAxe.getExtName()));
      if (nb > 1) {
        cqAxe.setTitle(cqMdl1d.findUniqueChildTitle(cqAxe.getTitle()));
      }
      cqMdl1d.add(cqAxe);
    }

    return cqMdl1d;
  }

  /**
   * Mise en place du groupe du mod�le 1d.
   */
  protected static BGroupeCalque buildCasiersLayerGroup(MdlEditionManager _editor, boolean _build, boolean _usePreferences) {
    BGroupeCalque cqCasiers = new BGroupeCalque();
    cqCasiers.setTitle(MdlResource.getS("Casiers"));
    cqCasiers.setName("gcCasier");
    cqCasiers.putClientProperty(Action.SHORT_DESCRIPTION, MdlResource.getS("Les casiers"));
    cqCasiers.setDestructible(false);

    if (!_build) {
      return cqCasiers;
    }

    int nb;
    for (int i = 0; i < (nb = getInitLayerNumber(MdlLayer2dCasier.EXT_NAME, _usePreferences)); i++) {
      MdlLayer2dCasier cqStore = new MdlLayer2dCasier(_editor);
      cqStore.setName(BGroupeCalque.findUniqueChildName(cqCasiers, cqStore.getExtName()));
      if (nb > 1) {
        cqStore.setTitle(cqCasiers.findUniqueChildTitle(cqStore.getTitle()));
      }
      cqCasiers.add(cqStore);
    }

    return cqCasiers;
  }

  @Override
  protected void fillCmdContextuelles(final BuPopupMenu _menu) {
    if (getScene() != null) {
      fillMenuWithSceneActions(_menu);
    }
    _menu.add(getMenuSelectionPath());
    if (contextTools_ == null) {
      buildTools();
    }
    _menu.add(contextTools_);
  }

  /**
   * Mise en place du groupe des biefs.
   */
  protected static BGroupeCalque buildBiefsLayerGroup() {
    BGroupeCalque cqBiefs = new BGroupeCalque();
    cqBiefs.setTitle(MdlResource.getS("Biefs"));
    cqBiefs.setName("gcBiefs");
    cqBiefs.putClientProperty(Action.SHORT_DESCRIPTION, MdlResource.getS("Les biefs"));
    cqBiefs.setDestructible(false);
//    addCalque(cqBiefs_);
    return cqBiefs;
  }

  /**
   * Mise en place des calques d'un bief depuis un bief.
   *
   * @param _bief Le bief � cr�er. Peut �tre null, dans ce cas le bief est vide.
   * @param _build True : Les calques composant le bief doivent �tre construits
   */
  public void buildBiefLayers(Bief _bief, boolean _build) {
    // Un visitor pour les num�ros � attribuer aux calques.
    class NameCalqueVisitor implements BCalqueVisitor {

      public int max = 1;

      public boolean visit(BCalque _cq) {
        if (null != _cq.getTitle()) {
          String tt = _cq.getTitle();
          int ind = tt.indexOf(" B");
          if (ind != -1) {
            try {
              max = Math.max(max, Integer.parseInt(tt.substring(ind + 2).trim()) + 1);
            } catch (NumberFormatException _exc) {
            }
          }
        }
        return true;
      }
    }
    NameCalqueVisitor visitor = new NameCalqueVisitor();
    cqBiefs_.apply(visitor);
    String biefNum = "" + visitor.max;

    BGroupeCalque cqBief = new BGroupeCalque();
    cqBief.setTitleModifiable(true);
    String name = BGroupeCalque.findUniqueChildName(cqBiefs_);
    String biefTitle = MdlResource.getS("Bief") + " " + biefNum;
    cqBief.setTitle(biefTitle);
    cqBief.setName(name);
    cqBief.putClientProperty(Action.SHORT_DESCRIPTION, MdlResource.getS("Un bief"));
    cqBief.setDestructible(true);
    cqBiefs_.add(cqBief);

    if (!_build) {
      return;
    }

    MdlLayer2dProfile cqProfile = new MdlLayer2dProfile(getEditor());
    cqProfile.setName(BGroupeCalque.findUniqueChildName(cqBief, cqProfile.getExtName()));
    cqProfile.setTitle(cqProfile.getTitle() + " B" + biefNum);
    cqBief.add(cqProfile);

    MdlLayer1dAxe cqAxe = new MdlLayer1dAxe(getEditor());
    cqAxe.setName(BGroupeCalque.findUniqueChildName(cqBief, cqAxe.getExtName()));
    cqAxe.setTitle(cqAxe.getTitle() + " B" + biefNum);
    cqBief.add(cqAxe);

    MdlLayer2dDirectionLine cqDirect = new MdlLayer2dDirectionLine(getEditor());
    cqDirect.setName(BGroupeCalque.findUniqueChildName(cqBief, cqDirect.getExtName()));
    cqDirect.setTitle(cqDirect.getTitle() + " B" + biefNum);
    cqBief.add(cqDirect);

    MdlLayer1dLimiteStockage cqLimite = new MdlLayer1dLimiteStockage(getEditor());
    cqLimite.setName(BGroupeCalque.findUniqueChildName(cqBief, cqLimite.getExtName()));
    cqLimite.setTitle(cqLimite.getTitle() + " B" + biefNum);
    cqBief.add(cqLimite);

    MdlLayer2dConstraintLine cqConst = new MdlLayer2dConstraintLine(getEditor());
    cqConst.setName(BGroupeCalque.findUniqueChildName(cqBief, cqConst.getExtName()));
    cqConst.setTitle(cqConst.getTitle() + " B" + biefNum);
    cqBief.add(cqConst);

    MdlLayer1dBank cqBank = new MdlLayer1dBank(getEditor());
    cqBank.setName(BGroupeCalque.findUniqueChildName(cqBief, cqBank.getExtName()));
    cqBank.setTitle(cqBank.getTitle() + " B" + biefNum);
    cqBief.add(cqBank);

    if (_bief != null) {
      // On r�cup�re les mod�les depuis le bief
      cqProfile.modele(_bief.profils_);
      cqAxe.modele(_bief.axeHydraulique_);
      cqDirect.modele(_bief.lignesDirectrices_);
      cqLimite.modele(_bief.limitesStockages_);
      cqConst.modele(_bief.lignesContraints_);
      cqBank.modele(_bief.rives_);
      if (_bief.getName() != null) {
        cqBief.setTitle(_bief.getName());
      }
    }
  }

  /**
   * Supprime le bief d�sign�.
   *
   * @param _bief Le bief.
   */
  public void removeBief(Bief _bief) {
    BCalque cq = cqBiefs_.getCalqueParTitre(_bief.getName());
    if (cq != null) {
      cq.detruire();
    }
  }

  /**
   * Renomme le bief d�sign�.
   *
   * @param _bief Le bief.
   * @param _newName Son nouveau nom
   */
  public void renameBief(Bief _bief, String _oldName, String _newName) {
    BCalque cq = cqBiefs_.getCalqueParTitre(_oldName);
    if (cq != null) {
      cq.setTitle(_newName);
    }
  }

  /**
   * Mise a jour des actions.
   *
   * @see org.fudaa.ebli.calque.ZSelectionListener#selectionChanged(org.fudaa.ebli.calque.ZSelectionEvent)
   */
  @Override
  public void selectionChanged(ZSelectionEvent _evt) {
    super.selectionChanged(_evt);
    EbliActionInterface act = EbliActionMap.getInstance().getAction("DUPLIQUER");
    act.setEnabled(!getScene().isSelectionEmpty());
  }
  
   public MdlImplementation getImpl() {
        return impl_;
    }

}