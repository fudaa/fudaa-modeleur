/*
 * @creation 8 sept. 06
 * @modification $Date: 2008/05/13 12:10:18 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.modeleur;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import org.fudaa.ctulu.CtuluArkSaver;
import org.fudaa.ctulu.CtuluXmlReaderHelper;
import org.fudaa.fudaa.commun.save.FudaaSaveZipLoader;

/**
 * Un modele d'arbre bas� sur la lecture d'un fichier projet, pour la repr�sentation des calques existants dans ce fichier.
 * @author Bertrand Marchand
 * @version $Id: MdlProjectImportTreeModel.java,v 1.1.2.1 2008/05/13 12:10:18 bmarchan Exp $
 */
public class MdlProjectImportTreeModel extends DefaultTreeModel {
  
  private FudaaSaveZipLoader loader_;
  private static String NAME_ROOT="data";

  /**
   * Un noeud de ce mod�le d'arbre, contenant un titre et un nom.
   * @author Bertrand Marchand
   * @version $Id:$
   */
  static class LayerNode extends DefaultMutableTreeNode {
    public String name_;
    public String title_;
    
    public LayerNode(String _title, String _name) {
      super(_title,true);
      name_=_name;
      title_=_title;
    }
  }

  /**
   * @param _loader Le loader
   */
  public MdlProjectImportTreeModel(FudaaSaveZipLoader _loader) {
    super(new LayerNode(NAME_ROOT,"cqRoot"));
    loader_=_loader;
    if (loader_!=null) buildTree();
  }
  
  private void buildTree() {
    ((LayerNode)getRoot()).removeAllChildren();
    addLayer(NAME_ROOT+"/", (LayerNode)getRoot());
  }
  
  private void addLayer(String _parentDir, LayerNode _node) {
    for (String ent : loader_.getEntries(_parentDir)) {
      if (ent.endsWith(CtuluArkSaver.DESC_EXT)) {
        try {
          CtuluXmlReaderHelper helper=new CtuluXmlReaderHelper(loader_.getReader(_parentDir, ent));
          String cqTitle=helper.getTextFor("title");
          String cqName = loader_.getLayerProperty(_parentDir,ent,"name");
          if (cqName==null) cqName="_bad_";
          String entrep=ent.substring(0, ent.lastIndexOf(CtuluArkSaver.DESC_EXT));
          LayerNode nd=new LayerNode(cqTitle,cqName);
          _node.add(nd);
          addLayer(_parentDir+entrep+"/", nd);
        }
        catch (Exception _exc) {}
      }
    }
  }
}
