/*
 * @creation     25 avr. 08
 * @modification $Date: 2008-05-13 12:10:11 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.fudaa.ctulu.gui.CtuluDialogPanel;

import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuTextField;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Un panneau pour les parametres de densification de points entre 2 sommets.
 * @author Bertrand Marchand
 * @version $Id: MdlInterpolationPanel.java,v 1.1.2.1 2008-05-13 12:10:11 bmarchan Exp $
 */
public class MdlInterpolationPanel extends CtuluDialogPanel implements ChangeListener {

  private class InterpolationActionListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
      tfDist_.setEnabled(e.getSource()==rbDist_);
      tfNbPts_.setEnabled(e.getSource()==rbNbPts_);
    }
  }
  
  private BuTextField tfNbPts_;
  private BuTextField tfDist_;
  private BuRadioButton rbNbPts_;
  private BuRadioButton rbDist_;
  private boolean useBorderPoints_;
  private int nbPts_=0;
  private double dist_=0;
  
  public MdlInterpolationPanel() {
    customize();
  }
  
  public void customize() {
    BuGridLayout lyThis=new BuGridLayout(2,5,5);
    this.setLayout(lyThis);
    InterpolationActionListener l=new InterpolationActionListener();
    ButtonGroup bg=new ButtonGroup();

    rbNbPts_=new BuRadioButton(MdlResource.getS("Nombre de points"));
    rbNbPts_.addActionListener(l);
    bg.add(rbNbPts_);

    rbDist_=new BuRadioButton(MdlResource.getS("Distance entre 2 points (m)"));
    rbDist_.addActionListener(l);
    bg.add(rbDist_);
    
    this.add(rbNbPts_);
    tfNbPts_=addIntegerText("5");
    this.add(rbDist_);
    tfDist_=addDoubleText("1.0");
    
    rbNbPts_.doClick();
    
    // La checkBox indiquant l'utilisation ou non des points d'extr�mit�s
    useBorderPoints_=false;
    BuCheckBox cb=new BuCheckBox(MdlResource.getS("Duplication des points aux extr�mit�s."));
    cb.addChangeListener(this);
    add(cb);
  }

  @Override
  public boolean isDataValid() {
    if (rbNbPts_.isSelected() && (tfNbPts_.getText().trim().length()==0 || (nbPts_ = (Integer)tfNbPts_.getValue())<2)) {
      setErrorText(MdlResource.getS("Le nombre de points est invalide") + '!');
      return false;
    }
    else if (rbDist_.isSelected() && (tfDist_.getText().trim().length()==0 || (dist_ = (Double)tfDist_.getValue())<=0)) {
      setErrorText(MdlResource.getS("La distance entre 2 points cons�cutifs doit �tre > 0 !"));
      return false;
    }
    return true;
  }
  
  public int getNbPts() {
    return nbPts_;
  }
  
  public double getDistance() {
    return dist_;
  }
  
  public boolean isNbPtsGiven() {
    return rbNbPts_.isSelected();
  }

  public boolean useBorderPoints(){
    return useBorderPoints_;
  }
  
  /* (non-Javadoc)
   * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
   */
  public void stateChanged(ChangeEvent e) {
    useBorderPoints_=!useBorderPoints_;
  }
}
