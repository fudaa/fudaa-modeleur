/*
 * @creation     3 juin 08
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur;

import org.fudaa.fudaa.sig.exetools.FSigImportDestinationPanel;
import java.awt.Component;
import java.awt.Dimension;
import java.util.Arrays;

import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeNameComparator;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.edition.ZCalqueEditable;
import org.fudaa.fudaa.sig.FSigExportImportAttributesMapper;
import org.fudaa.fudaa.sig.FSigGeomSrcData;
import org.fudaa.fudaa.sig.wizard.FSigFileLoaderPanel;

import com.memoire.bu.BuButtonPanel;
import com.memoire.bu.BuGlassPaneStop;
import com.memoire.bu.BuWizardTask;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * Un Wizard d'import de fichiers contenant des donn�es g�om�triques sous des formats tr�s divers. Ce Wizard demande
 * en premiere �tape les fichiers � importer, puis propose les calques vers lequels transf�rer ces donn�es.
 * Lors du transfert, il est demand� � l'utilisateur de pr�ciser le mappage entre les variables lues et les variables
 * cibles de chaque calque.
 * 
 * @author Bertrand Marchand, Emmanuel Martin
 * @version $Id$
 */
public class MdlWizardImport extends BuWizardTask {
  private CtuluCommandManager mng_;

  private BGroupeCalque calqueRacine_;
  private FSigImportDestinationPanel pnDest_;
  
  protected final GISPolygone[] previewZone_;
  protected final ZCalqueEditable selectedLayer_;
  protected FSigGeomSrcData data_;
  protected JDialog dialog_;
  protected FSigFileLoaderPanel fileLoader_;
  protected final CtuluUI impl_;
  protected GISAttributeInterface[] src_;
  protected final DefaultListModel srcAtt_ = new DefaultListModel();
  protected final ZEbliCalquesPanel pnCalques_;
  
  /**
   * @param _previewZone la zone a utiliser pour la pr�visualisation
   * @param _selectedLayer le calque en cours de selection: peut-etre null
   * @param _impl l'impl parent
   */
  public MdlWizardImport(ZEbliCalquesPanel _pn, GISPolygone[] _previewZone, ZCalqueEditable _selectedLayer, CtuluUI _impl, CtuluCommandManager _mng) {
    mng_=_mng;
    calqueRacine_=_pn.getDonneesCalque();
    pnCalques_=_pn;
    impl_ = _impl;
    previewZone_ = _previewZone;
    selectedLayer_ = _selectedLayer;
    
    
    // Exemple de cr�ation d'un sous calque pour un BGroupeCalque
    /*
    int cqType_ = MdlLayerFactory.LAYER2D_CLOUD;
    ZCalqueAffichageDonnees cq=MdlLayerFactory.getInstance().createLayer(cqType_, editor_);
    cq.setName(BGroupeCalque.findUniqueChildName(parent_, ((MdlLayerInterface)cq).getExtName()));
    parent_.enDernier(cq);
   */
  }
  
  public JComponent getStepComponent() {
    if (current_==0) {
      if (fileLoader_==null) {
        srcAtt_.addListDataListener(new ListDataListener() {
          public void contentsChanged(final ListDataEvent _e) {
            rebuildAtt();
          }
          public void intervalAdded(final ListDataEvent _e) {
            rebuildAtt();
          }
          public void intervalRemoved(final ListDataEvent _e) {
            rebuildAtt();
          }
        });
        fileLoader_=new FSigFileLoaderPanel(srcAtt_, impl_, previewZone_);
        fileLoader_.setPreferredSize(new Dimension(450, 400));
        fileLoader_.setAttributeRequired(false);
        fileLoader_.getFileMng().getModel().addTableModelListener(new TableModelListener() {
          public void tableChanged(final TableModelEvent _e) {
            data_=null;
          }
        });
      }
      return fileLoader_;
    }
    else if (current_==1) {
      pnDest_=new FSigImportDestinationPanel(calqueRacine_, data_, selectedLayer_);
      return pnDest_;
    }
    return null;
  }
  
  @Override
  public void doTask() {
    if(pnDest_==null)
      return;
    final CtuluTaskDelegate task=impl_.createTask(MdlResource.getS("Importation"));
    task.start(new Runnable() {
      public void run() {
        pnDest_.doAtEnd(task.getStateReceiver(), mng_);
        pnCalques_.restaurer();
      }
    });
    super.doTask();
  }
 
  protected void rebuildAtt() {
    GISAttributeInterface[] newatt = new GISAttributeInterface[srcAtt_.size()];
    for (int i = newatt.length - 1; i >= 0; i--) {
      newatt[i] = (GISAttributeInterface) srcAtt_.get(i);
    }
    // pour l'instant on filtre le tout
    newatt = FSigExportImportAttributesMapper
        .filtreAttributes(newatt, new Class[] { String.class, Double.class }, null);
    if (newatt != null) {
      Arrays.sort(newatt, new GISAttributeNameComparator());
      // il n'y a pas de changement
      if (src_ != null && Arrays.equals(newatt, src_)) {
        return;
      }
    }
    src_ = newatt;

  }

  public int getStepCount() {
    return 2;
  }

  @Override
  public int getStepDisabledButtons() {
    return super.getStepDisabledButtons() | BuButtonPanel.DETRUIRE;
  }

  public String getStepText() {
    final StringBuffer r=new StringBuffer();
    if (super.current_==0) {
      r.append(MdlResource.getS("Les formats des fichiers sont choisis en fonction des extensions."));
      r.append(CtuluLibString.LINE_SEP+MdlResource.getS("Il est possible de les modifier dans la colonne 'Format'."));
      r.append(CtuluLibString.LINE_SEP+MdlResource.getS("La derni�re colonne permet de sp�cifier la tra�abilit� des fichiers."));
    }
    else if (super.current_==1) {
      r.append(MdlResource.getS("Pr�ciser les calques et les attributs � modifier"));
      r.append(CtuluLibString.LINE_SEP+MdlResource.getS("Le cadre 'Attributs' permet de mapper les attributs lus avec les attributs permis par le calque choisi dans le tableau."));
    }
    return r.toString();
  }

  public String getStepTitle() {
    return super.current_ == 0 ? MdlResource.getS("S�lection des fichiers") : MdlResource.getS("D�finir les calques cibles");
  }

  public String getTaskTitle() {
    return MdlResource.getS("Importation");
  }

  @Override
  public void nextStep() {
    if (current_ == 0 && data_ == null) {
      if (dialog_ != null) {
        dialog_.setGlassPane(new BuGlassPaneStop());
      }
      fileLoader_.buildPreview(false);
      data_ = fileLoader_.loadAll(null, new CtuluAnalyze());
      if (dialog_ != null) {
        final Component c = dialog_.getGlassPane();
        c.setVisible(false);
        dialog_.remove(c);
      }
      if (data_ == null) {
        return;
      }
    }
    super.nextStep();
  }
  
  public final void setDialog(final JDialog _dialog) {
    dialog_ = _dialog;
  }
  
  public final JDialog getDialog() {
    return dialog_;
  }
}
