/*
 * @creation     11 mars 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISDataModelFilterAdapter;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISMultiPoint;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionMultiPoint;
import org.fudaa.dodico.mascaret.io.MascaretCasierFileFormat;
import org.fudaa.dodico.rubar.io.RubarCasierFileFormat;
import org.fudaa.dodico.rubar.io.RubarCasierWriter;
import org.fudaa.ebli.calque.ZCalqueGeometry;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.fudaa.modeleur.resource.MdlResource;
import org.locationtech.jts.algorithm.locate.PointOnGeometryLocator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;

import gnu.trove.TIntArrayList;

/**
 * Un exporter de casiers de la fenetre 2D. A impl�menter suivant chaque format d'exportation.
 * @author Bertrand Marchand
 * @version $Id$
 */
public abstract class MdlCasierExporter {

  /**
   * Export des casiers.
   * @param _f Le fichier casiers.
   * @param _scn La scene. Utile pour la selection des g�om�tries a int�grer.
   * @param _mdCasiers Le modele des seuls contours de casier a exporter, selon l'option choisie (tous, selectionn�s, etc.). 
   * @param _selGeom Les g�om�tries a int�grer aux contours de casiers lors de l'export.
   * @return Le resultat du traitement d'export.
   */
  public abstract CtuluIOOperationSynthese export(File _f, ZScene _scn, GISDataModel _mdCasiers, CtuluListSelection _selGeom);
  
  /**
   * Retourne le file format associ� � l'exporter.
   * @return Le file format.
   */
  public abstract FileFormatVersionInterface getFileFormat();

  /**
   * Retourne le masque de saisie des g�om�tries a int�grer dans les casiers (voir {@link GISLib})
   * @return Le mask.
   */
  public abstract int getMaskForSubSelection();
  
  /**
   * Controle que les donn�es sont valides avant de passer � la suite.
   */
  public abstract boolean isDataOk(CtuluListSelection _sel, CtuluAnalyze _ana);
  
  /**
   * Un exporter de casiers au format Mascaret.
   * @author Bertrand Marchand
   * @version $Id$
   */
  public static class Mascaret extends MdlCasierExporter {
    
    public CtuluIOOperationSynthese export(File _f, ZScene _scn, GISDataModel _mdCasiers, CtuluListSelection _selSemis) {
      GISDataModel mdCasiers=_mdCasiers;

      CtuluAnalyze ana=new CtuluAnalyze();
      if (mdCasiers!=null&&mdCasiers.getNumGeometries()!=0) {
        int nbCasiers=mdCasiers.getNumGeometries();
        TIntArrayList idxConserves=new TIntArrayList();
        GISZoneCollectionMultiPoint mdSemis=buildSemis(_scn, mdCasiers, idxConserves, _selSemis, ana);
        if (mdSemis.getNumGeometries()!=0) {
          // On ne conserve du mod�le que les indexs conserv�s.
          if (idxConserves.size()!=nbCasiers) {
            mdCasiers=new GISDataModelFilterAdapter(mdCasiers, null, idxConserves.toNativeArray());
          }

          FileWriteOperationAbstract writer=getFileFormat().createWriter();
          CtuluAnalyze ana2=writer.write(new GISDataModel[]{mdCasiers, mdSemis}, _f, null).getAnalyze();
          ana.merge(ana2);
        }

        if (!ana.containsErrors()&&!ana.containsFatalError()) {
          if (nbCasiers>mdSemis.getNumGeometries()) {
            ana.addWarn(MdlResource.getS("{0} casiers sur {1} ont �t� export�s", mdSemis.getNumGeometries(), nbCasiers), -1);
          }
          else {
            ana.addInfo(MdlResource.getS("{0} casiers sur {1} ont �t� export�s", mdSemis.getNumGeometries(), nbCasiers), -1);
          }
        }
      }
      else {
        ana.addWarn(MdlResource.getS("Aucun casiers � exporter"), -1);
      }

      CtuluIOOperationSynthese synt=new CtuluIOOperationSynthese();
      synt.setAnalyze(ana);
      return synt;
    }
    
    public FileFormatVersionInterface getFileFormat() {
      return MascaretCasierFileFormat.getInstance();
    }

    public int getMaskForSubSelection() {
      return GISLib.MASK_MULTIPOINT;
    }

    public boolean isDataOk(CtuluListSelection _sel, CtuluAnalyze _ana) {
      if (_sel.isEmpty()) {
        _ana.addError(MdlResource.getS("Vous devez s�lectionner au moins 1 semis"),-1);
        return false;
      }
      
      return true;
    }
    
    /**
     * Construit les semis internes aux casiers. En fait, on recr�e des semis pour chaque contour de casier. 
     * Les casiers sans semis interne ou non conformes sont supprim�s du mod�le.
     * 
     * @param _scn La scene.
     * @param _mdCasiers Le modele des casiers.
     * @param _idxConserves Les index casiers conserv�s. (Parametre de retour)
     * @param _selSemis La selection des semis.
     * @return Les semis.
     */
    private GISZoneCollectionMultiPoint buildSemis(ZScene _scn, 
        GISDataModel _mdCasiers, TIntArrayList _idxConserves, CtuluListSelection _selSemis, CtuluAnalyze _ana) {
      
      GISZoneCollectionMultiPoint zsemis=new GISZoneCollectionMultiPoint();
      zsemis.setAttributes(new GISAttributeInterface[]{GISAttributeConstants.BATHY},null);
      zsemis.setAttributeIsZ(GISAttributeConstants.BATHY);
      
      List<Coordinate> coords=new ArrayList<Coordinate>();
      
      for (int idxgeom=0; idxgeom<_mdCasiers.getNumGeometries(); idxgeom++) {
        int iattr=_mdCasiers.getIndiceOf(GISAttributeConstants.TITRE);
        String name="";
        if (iattr!=-1)
          name=(String)_mdCasiers.getValue(iattr,idxgeom);
        
        coords.clear();
        GISPolygone geom=(GISPolygone)_mdCasiers.getGeometry(idxgeom);
        
        // On controle que le casier est conforme.
        if (geom.getCoordinateSequence().size()<5) {
          _ana.addWarn(MdlResource.getS("Casier '{0}' : Le contour n'est pas conforme",name), 0);
          continue;
        }
        
        // On teste que le contour contient bien au moins 1 point.
        PointOnGeometryLocator pir=GISLib.createPolygoneTester(geom);
        
        for (int j=_selSemis.getMinIndex(); j<=_selSemis.getMaxIndex(); j++) {
          if (!_selSemis.isSelected(j)) continue;
          
          GISMultiPoint semis=(GISMultiPoint)_scn.getObject(j);
          CoordinateSequence seq=semis.getCoordinateSequence();
          for (int k=0; k<seq.size(); k++) {
            Coordinate c=seq.getCoordinate(k);
            if (GISLib.isIn(c, geom, pir, 0))
              coords.add(c);
          }
        }
        
        // Le casier ne poss�de aucun point interne => On le retire des casiers a exporter.
        if (coords.size()==0) {
          _ana.addWarn(MdlResource.getS("Casier '{0}' : Aucun point des semis s�lectionn�s ne se trouve � l'int�rieur",name), -1);
          continue;
        }
        _idxConserves.add(idxgeom);
        
        // Recup�ration des points, et cr�ation d'un semis.
        GISMultiPoint semis=GISGeometryFactory.INSTANCE.createMultiPoint(coords.toArray(new Coordinate[0]));
        zsemis.addGeometry(semis,null,null);
      }
      
      return zsemis;
    }
  }

  /**
   * Un exporter de casiers au format Rubar (MAGE).
   * @author Bertrand Marchand
   * @version $Id$
   */
  public static class Rubar extends MdlCasierExporter {
    
    public CtuluIOOperationSynthese export(File _f, ZScene _scn, GISDataModel _mdCasiers, CtuluListSelection _selNiveaux) {
      GISDataModel mdCasiers=_mdCasiers;

      CtuluAnalyze ana=new CtuluAnalyze();
      CtuluIOOperationSynthese synt=new CtuluIOOperationSynthese();
      synt.setAnalyze(ana);
      
      if (mdCasiers==null || mdCasiers.getNumGeometries()==0) {
        ana.addWarn(MdlResource.getS("Aucun casiers � exporter"), -1);
        return synt;
      }

      if (!hasNoIntersectionWithContours(_scn, _mdCasiers, _selNiveaux) ||
          !hasNoIntersectionWithNiveaux(_scn, _mdCasiers, _selNiveaux)) {
        ana.addError(MdlResource.getS("Les lignes de niveaux s�lectionn�es coupent des\ncontours de casiers ou se coupent entre elles.\nAucun casiers export�"), -1);
        return synt;
      }
      
      int nbCasiers=mdCasiers.getNumGeometries();
      TIntArrayList idxConserves=new TIntArrayList();
      GISZoneCollectionLigneBrisee mdNiveaux=buildNiveaux(_scn, mdCasiers, idxConserves, _selNiveaux, ana);

      // On ne conserve du mod�le que les indexs conserv�s.
      if (idxConserves.size()!=nbCasiers) {
        mdCasiers=new GISDataModelFilterAdapter(mdCasiers, null, idxConserves.toNativeArray());
      }

      FileWriteOperationAbstract writer=getFileFormat().createWriter();
      CtuluAnalyze ana2=writer.write(new GISDataModel[]{mdCasiers, mdNiveaux}, _f, null).getAnalyze();
      ana.merge(ana2);

      if (ana.containsWarnings()) {
        ana.addWarn(MdlResource.getS("{0} casiers sur {1} ont �t� export�s", idxConserves.size(), nbCasiers), -1);
      }
      else {
        ana.addInfo(MdlResource.getS("{0} casiers sur {1} ont �t� export�s", nbCasiers, nbCasiers), -1);
      }

      return synt;
    }
    
    public FileFormatVersionInterface getFileFormat() {
      return RubarCasierFileFormat.getInstance();
    }

    public int getMaskForSubSelection() {
      return GISLib.MASK_POLYGONE;
    }
    
    public boolean isDataOk(CtuluListSelection _sel, CtuluAnalyze _ana) {
      return true;
    }
    
    /**
     * Test que les courbes de niveaux n'ont pas d'intersections entre elles.
     * @return true si pas d'intersections avec les autres courbes de niveaux.
     */
    private boolean hasNoIntersectionWithNiveaux(ZScene _scn, 
        GISDataModel _mdCasiers, CtuluListSelection _selNiveaux) {
      
      for (int iniv1=_selNiveaux.getMinIndex(); iniv1<=_selNiveaux.getMaxIndex(); iniv1++) {
        if (!_selNiveaux.isSelected(iniv1)) continue;
        GISPolygone niv1=(GISPolygone)_scn.getObject(iniv1);
        
        // Test avec les courbes de niveaux
        for (int iniv2=iniv1+1; iniv2<=_selNiveaux.getMaxIndex(); iniv2++) {
          if (!_selNiveaux.isSelected(iniv2)) continue;
          GISPolygone niv2=(GISPolygone)_scn.getObject(iniv2);
          if (niv2.intersects(niv1)) return false;
        }
      }
      return true;
    }
    
    /**
     * Test que les courbes de niveaux n'ont pas d'intersections avec les contours de casiers.
     * @return true si pas d'intersections avec les contours.
     */
    private boolean hasNoIntersectionWithContours(ZScene _scn, 
        GISDataModel _mdCasiers, CtuluListSelection _selNiveaux) {
      
      for (int iniv1=_selNiveaux.getMinIndex(); iniv1<=_selNiveaux.getMaxIndex(); iniv1++) {
        if (!_selNiveaux.isSelected(iniv1)) continue;
        GISPolygone niv1=(GISPolygone)_scn.getObject(iniv1);
        
        // Test avec les casiers
        for (int j=0; j<_mdCasiers.getNumGeometries(); j++) {
          GISPolygone casier=(GISPolygone)_mdCasiers.getGeometry(j);
          if (casier.intersects(niv1)) return false;
        }
      }
      return true;
    }
    
    /**
     * Construit les lignes de niveau internes aux casiers. 
     * Les casiers non conformes sont supprim�s du mod�le.
     * 
     * @param _scn La scene.
     * @param _mdCasiers Le modele des casiers.
     * @param _idxConserves Les index casiers conserv�s. (Parametre de retour)
     * @param _selNiveaux La selection des lignes de niveau.
     * @return Les semis.
     */
    private GISZoneCollectionLigneBrisee buildNiveaux(ZScene _scn, 
        GISDataModel _mdCasiers, TIntArrayList _idxConserves, CtuluListSelection _selNiveaux, CtuluAnalyze _ana) {

      // La selection est localement chang�e. On en fait une copie.
      CtuluListSelection selNiveaux=(CtuluListSelection)_selNiveaux.clone();
      
      GISZoneCollectionLigneBrisee zniveaux=new GISZoneCollectionLigneBrisee();
      zniveaux.setAttributes(new GISAttributeInterface[]{GISAttributeConstants.BATHY,RubarCasierWriter.INDEX_CASIER},null);
      zniveaux.setAttributeIsZ(GISAttributeConstants.BATHY);
      
      List<Coordinate> coords=new ArrayList<Coordinate>();
      
      for (int idxgeom=0; idxgeom<_mdCasiers.getNumGeometries(); idxgeom++) {
        int iattr=_mdCasiers.getIndiceOf(GISAttributeConstants.TITRE);
        String name="";
        if (iattr!=-1)
          name=(String)_mdCasiers.getValue(iattr,idxgeom);
        
        coords.clear();
        GISPolygone geom=(GISPolygone)_mdCasiers.getGeometry(idxgeom);
        
        // On controle que le casier est conforme (tous les points du contour on m�me Z)
        if (!GISLib.isNiveau(geom.getCoordinateSequence())) {
          _ana.addWarn(MdlResource.getS("Casier '{0}' : Le contour n'est pas conforme (Z non constant)",name), 0);
          continue;
        }
        
        // On recherche a quel contour appartient chaque ligne de niveau. A ce stade, les courbes de niveaux ne se coupent pas entre
        // elles, et ne coupent pas de contours.
        PointOnGeometryLocator pir=GISLib.createPolygoneTester(geom);
        
        for (int j=selNiveaux.getMinIndex(); j<=selNiveaux.getMaxIndex(); j++) {
          if (!selNiveaux.isSelected(j)) continue;
          
          GISPolygone niv=(GISPolygone)_scn.getObject(j);
          CoordinateSequence seq=niv.getCoordinateSequence();
          // Une coordonn�e dans le contour => La ligne de niveau appartient au contour.
          Coordinate c=seq.getCoordinate(0);
          if (GISLib.isIn(c, geom, pir, 0)) {
            zniveaux.addGeometry(niv,new Object[]{null,new Integer(idxgeom)},null);
            selNiveaux.remove(j);
          }
        }
        
        _idxConserves.add(idxgeom);
        
        // Recup�ration des points, et cr�ation d'un semis.
//        GISMultiPoint semis=GISGeometryFactory.INSTANCE.createMultiPoint(coords.toArray(new Coordinate[0]));
//        zniveaux.addGeometry(semis,null,null);
      }
      
      // Message pour les lignes de niveau non export�es.
      for (int j=selNiveaux.getMinIndex(); j<=selNiveaux.getMaxIndex(); j++) {
        if (!selNiveaux.isSelected(j)) continue;
        
        GISZoneCollection zniv=((ZCalqueGeometry)_scn.getLayerForId(j)).modeleDonnees().getGeomData();
        int iattr=zniv.getIndiceOf(GISAttributeConstants.TITRE);
        if (iattr!=-1) {
          String name=(String)zniv.getValue(iattr,_scn.sceneId2LayerId(j));
          _ana.addWarn(MdlResource.getS("La ligne de niveau '{0}' est en dehors des casiers � exporter",name), 0);
        }
      }
      
      // Pour intiailiser la BATHY.
      zniveaux.postImport(0);
      return zniveaux;
    }
  }
}
