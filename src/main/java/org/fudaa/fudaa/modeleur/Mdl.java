/*
 *
 * @creation 7 juin 07
 * @modification $Date: 2008/01/15 14:00:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.modeleur;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import org.fudaa.fudaa.commun.impl.Fudaa;
import org.fudaa.fudaa.ressource.FudaaResource;

import com.memoire.bu.BuLib;
import com.memoire.bu.BuResource;
import com.memoire.fu.FuLib;

/**
 * Le programme pricipal de lancement de l'application Fudaa-Modeleur
 * @author fred deniger
 * @version $Id$
 */
public final class Mdl {
  private Mdl() {}

  /**
   * Lancement de l'application Fudaa Modeleur
   * @param _args Les paramètres de lancement.
   */
  public static void main(String[] _args) {
    BuResource.BU.setIconFamily("crystal");

    JOptionPane.setRootFrame(BuLib.HELPER);
    BuLib.HELPER.setIconImage(FudaaResource.FUDAA.getImage("fudaa-logo"));
    // pour ne pas noircir une case sur deux
    UIManager.put("Table.darkerOddLines", Boolean.FALSE);
    System.setProperty("swing.boldMetal", "false");
    FuLib.setSystemProperty("lnf.menu.needed", "false");
    MdlPreferences.setRoot("mdl");
    Fudaa f = new Fudaa();
    f.launch(_args, MdlImplementation.informationsSoftware(), true);
    f.startApp(new MdlImplementation());
  }

}
