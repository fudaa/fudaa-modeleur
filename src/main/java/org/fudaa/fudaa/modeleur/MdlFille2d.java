/*
 * @creation     20 janv. 08
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.modeleur;

import java.awt.Dimension;

import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluExportDataInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.CtuluUndoRedoInterface;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.image.CtuluImageImporter;
import org.fudaa.ebli.calque.ZEbliFilleCalques;
import org.fudaa.fudaa.commun.save.FudaaFilleVisuPersistence;
import org.fudaa.fudaa.commun.save.FudaaSavable;

import com.db4o.ObjectContainer;
import com.memoire.bu.BuCutCopyPasteInterface;
import com.memoire.bu.BuUndoRedoInterface;
import java.beans.PropertyVetoException;
import org.fudaa.ctulu.CtuluArkSaver;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ebli.calque.BCalqueLegende;
import org.fudaa.fudaa.modeleur.resource.MdlResource;

/**
 * La fenetre interne vue 2D des donn�es du modeleur. Elle construit le composant arbre de
 * calques {@link org.fudaa.ebli.calque.BArbreCalque}. La plupart des traitements est
 * d�l�gu�e au composant {@link org.fudaa.ebli.calque.ZEbliCalquesPanel} encapsul�. 
 * 
 * @author fred deniger
 * @author bertrand marchand
 * @version $Id$
 */
public class MdlFille2d extends ZEbliFilleCalques implements BuUndoRedoInterface, CtuluExportDataInterface,
    CtuluImageImporter, CtuluUndoRedoInterface, FudaaSavable, BuCutCopyPasteInterface {

  /** Le gestionnaire de couper/coller/copier de la fen�tre. */
  protected MdlCutCopyPasteManager cutCopyPasteManager_;
  
  public MdlFille2d(MdlImplementation _ui) {
    super(new MdlVisuPanel(_ui), _ui, null);
    setDoubleBuffered(false);
    setTitle(MdlResource.getS("Vue 2D"));
    setPreferredSize(new Dimension(500, 400));
    cutCopyPasteManager_=new MdlCutCopyPasteManager(this, (MdlSceneEditor) getMdlVisuPanel().getEditor().getSceneEditor());
  }

  public void saveIn(final CtuluArkSaver _writer, final ProgressionInterface _prog) {
    new FudaaFilleVisuPersistence(this).saveIn(_writer, _prog);
  }

  public void saveIn(final ObjectContainer _db, final ProgressionInterface _prog) {
    new FudaaFilleVisuPersistence(this).saveIn(_db, _prog);
  }

  public MdlVisuPanel getMdlVisuPanel() {
    return (MdlVisuPanel) super.getVisuPanel();
  }

  public final String[] getEnabledActions() {
    return new String[] { "RECHERCHER", "IMPRIMER", "MISEENPAGE", "PREVISUALISER", CtuluExportDataInterface.EXPORT_CMD,
        CtuluLibImage.SNAPSHOT_COMMAND, "TOUTSELECTIONNER", "INVERSESELECTION", "CLEARSELECTION", "IMPORT_IMAGE" };
  }

  public void importImage() {
    getMdlVisuPanel().importImage();

  }

  @Override
  public void setClosed(boolean _closed) throws PropertyVetoException {
    super.setClosed(_closed);
    if(_closed){
      BCalqueLegende cqLegend = getVisuPanel().getCqLegend();
      cqLegend.enleveTousCalques();
    }
  }
  
  

  public void redo() {
    getMdlVisuPanel().redo();
  }

  public void startExport(CtuluUI _impl) {
    getMdlVisuPanel().getEditor().exportLayers();
  }

  public void undo() {
    getMdlVisuPanel().undo();
  }

  public void clearCmd(final CtuluCommandManager _source) {
    getMdlVisuPanel().clearCmd(_source);
  }

  public CtuluCommandManager getCmdMng() {
    return getMdlVisuPanel().getCmdMng();
  }

  public void setActive(boolean _active) {
    getMdlVisuPanel().setActive(_active);

  }

  public void copy() {
    cutCopyPasteManager_.copy();
  }

  public void cut() {
    cutCopyPasteManager_.cut();
  }

  public void duplicate() {
    getMdlVisuPanel().duplicate();
  }

  public void paste() {
    cutCopyPasteManager_.paste();
  }
  
}
